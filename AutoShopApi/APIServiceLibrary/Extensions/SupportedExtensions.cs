﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Newtonsoft.Json;
using System.Net.Http;


namespace APIServiceLibrary
{
	/// <summary>The date time extensions.</summary>
	public static class DateTimeExtensions
	{
		#region Public Methods and Operators

		/// <summary>This will return the UNIX time for a datetime object.</summary>
		/// <param name="date">The date.</param>
		/// <returns>The epoch.</returns>
		public static long ToUnixTime(this DateTime date)
		{
			var utc = date.ToUniversalTime();

			var ts = utc.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
			double epochtime = (((((ts.Days * 24) + ts.Hours) * 60) + ts.Minutes) * 60) + ts.Seconds;
			return Convert.ToInt64(epochtime);
		}

		#endregion
	}

	/// <summary>The enumeration extensions.</summary>
	public static class EnumerationExtensions
	{
		#region Public Methods and Operators

		/// <summary>This will get the description attribute's value, if the enumeration has one, or the string representation of the value.</summary>
		/// <param name="value">The enumeration value.</param>
		/// <returns>The description or the string representation of the value.</returns>
		public static string GetDescription(this Enum value)
		{
			var fi = value.GetType().GetField(value.ToString());

			var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

			if (attributes != null && attributes.Length > 0)
			{
				return attributes[0].Description;
			}

			return value.ToString();
		}

		#endregion
	}

	public static class HttpResponseExtensions
	{
		public static T ContentAsType<T>(this HttpResponseMessage response)
		{
			var data = response.Content.ReadAsStringAsync().Result;
			return string.IsNullOrEmpty(data) ?
							default(T) :
							JsonConvert.DeserializeObject<T>(data);
		}

		public static string ContentAsJson(this HttpResponseMessage response)
		{
			var data = response.Content.ReadAsStringAsync().Result;
			return JsonConvert.SerializeObject(data);
		}

		public static string ContentAsString(this HttpResponseMessage response)
		{
			return response.Content.ReadAsStringAsync().Result;
		}
	}


}
