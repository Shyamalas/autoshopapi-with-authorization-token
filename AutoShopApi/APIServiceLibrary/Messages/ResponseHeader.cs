﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIServiceLibrary
{
	public class ResponseHeader
	{
		#region Public Properties

		/// <summary>Gets or sets the date.</summary>
		public string Date
		{
			get;
			set;
		}

		/// <summary>Gets or sets the messages.</summary>
		public List<HeaderMessage> Messages
		{
			get;
			set;
		}

		/// <summary>Gets or sets the paging info.</summary>
		public PagingInformation PagingInfo
		{
			get;
			set;
		}

		/// <summary>Gets or sets the status.</summary>
		public string Status
		{
			get;
			set;
		}

		/// <summary>Gets or sets the status code.</summary>
		public int StatusCode
		{
			get;
			set;
		}

		/// <summary>Gets or sets the token.</summary>
		public string Token
		{
			get;
			set;
		}

		/// <summary>Gets or sets the version.</summary>
		public string Version
		{
			get;
			set;
		}

		#endregion
	}
}
