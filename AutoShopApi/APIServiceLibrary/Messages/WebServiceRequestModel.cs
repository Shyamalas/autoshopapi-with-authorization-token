﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;


namespace APIServiceLibrary
{
	/// <summary>The web services request model.</summary>
	public class WebServiceRequestModel
	{
		#region Public Properties

		/// <summary>Gets or sets the selected accept format.</summary>
		[DisplayName("Accept Format")]
		public string AcceptFormat
		{
			get;
			set;
		}

		/// <summary>Gets or sets the selected authentication scheme.</summary>
		[DisplayName("Type")]
		public string AuthenticationSchemeType
		{
			get;
			set;
		}

		/// <summary>Gets or sets the private key or token placement.</summary>
		[DisplayName("Placement")]
		public string AuthPlacement
		{
			get;
			set;
		}

		/// <summary>Gets or sets the selected content type format.</summary>
		[DisplayName("Content Type")]
		public string ContentTypeFormat
		{
			get;
			set;
		}

		/// <summary>Gets or sets the correlation id.</summary>
		[DisplayName("Correlation ID")]
		public string CorrelationID
		{
			get;
			set;
		}

		/// <summary>Gets or sets the CORS origin.</summary>
		[DisplayName("Origin")]
		public string CorsOrigin
		{
			get;
			set;
		}

		/// <summary>Gets or sets the CORS request headers.</summary>
		[DisplayName("Headers")]
		public string CorsRequestHeaders
		{
			get;
			set;
		}

		/// <summary>Gets or sets the CORS request method.</summary>
		[DisplayName("Verb")]
		public string CorsRequestMethod
		{
			get;
			set;
		}

		/// <summary>Gets or sets a value indicating whether enable CORS.</summary>
		[DisplayName("Enabled")]
		public bool EnableCors
		{
			get;
			set;
		}

		/// <summary>Gets or sets the selected endpoint.</summary>
		public string Endpoint
		{
			get;
			set;
		}

		/// <summary>Gets or sets the selected environment.</summary>
		public string Environment
		{
			get;
			set;
		}

		/// <summary>Gets or sets a value indicating whether hide web server headers.</summary>
		[DisplayName("Hide Web Server Headers")]
		public bool HideWebServerHeaders
		{
			get;
			set;
		}

		/// <summary>Gets or sets the selected HTTP verb.</summary>
		[DisplayName("Verb")]
		public string HttpVerb
		{
			get;
			set;
		}

		/// <summary>Gets or sets a value indicating whether include date header.</summary>
		[DisplayName("Include Date")]
		public bool IncludeDate
		{
			get;
			set;
		}

		/// <summary>Gets or sets the private key or token.</summary>
		[DisplayName("Private Key / Token")]
		public string PrivateKeyOrToken
		{
			get;
			set;
		}

		/// <summary>Gets or sets the selected protocol.</summary>
		public string Protocol
		{
			get;
			set;
		}

		/// <summary>Gets or sets the public key.</summary>
		[DisplayName("Public Key")]
		public string PublicKey
		{
			get;
			set;
		}

        /// <summary>Gets or sets the changing Date.</summary>
        [DisplayName("NumberOfDays")]
        public int NumberOfDays
        {
            get;
            set;
        }

		/// <summary>Gets or sets the query string parameters.</summary>
		[DisplayName("Query String")]
		public string QueryStringParameters
		{
			get;
			set;
		}

		/// <summary>Gets or sets the request data.</summary>
		[DisplayName("Request")]
		public string RequestData
		{
			get;
			set;
		}

		/// <summary>Gets or sets the request date override.</summary>
		[DisplayName("Date Override")]
		public DateTime? RequestDateOverride
		{
			get;
			set;
		}

		/// <summary>Gets or sets the entered route values.</summary>
		[DisplayName("Route Values")]
		public string RouteValues
		{
			get;
			set;
		}

		/// <summary>Gets or sets a value indicating whether show headers.</summary>
		[DisplayName("Show Headers")]
		public bool ShowHeaders
		{
			get;
			set;
		}

		/// <summary>Gets or sets a value indicating whether enable CORS.</summary>
		[DisplayName("CacheEnabled")]
		public bool EnableCache
		{
			get;
			set;
		}

		/// <summary>Gets or sets a value indicating whether enable CORS.</summary>
		[DisplayName("CheckCache")]
		public bool CheckCache
		{
			get;
			set;
		}

		[DisplayName("Request Time")]
		public DateTime RequestTime
		{
			get;
			set;
		}

		#endregion

		#region Public Methods and Operators

		/// <summary>This will validate the model (based specifically on the UI in this sample).</summary>
		/// <exception cref="ValidationException">One of the validation rules was violated.</exception>
		public void ValidateModel()
		{
			// Validate there is a route URL present
			if (string.IsNullOrWhiteSpace(Endpoint))
			{
				throw new ValidationException("Resource URL is required.");
			}

			// Make sure it starts with a forward slash, or the URL won't construct properly
			if (!Endpoint.StartsWith("/"))
			{
				throw new ValidationException("Resource URL must start with '/'.");
			}

			// Make sure there is a public key present
			if (string.IsNullOrWhiteSpace(PublicKey))
			{
				throw new ValidationException("Public Key is required.");
			}

			// Make sure there is a private key present
			if (string.IsNullOrWhiteSpace(PrivateKeyOrToken))
			{
				throw new ValidationException("Private Key is required.");
			}

			// Build the URL to make something is output
			var url = MessageRequest.BuildUrl(this);
			if (string.IsNullOrWhiteSpace(url))
			{
				throw new ValidationException("Could not build the URL properly.");
			}

			// Check to see if all of the route parameters replace the variables properly
			var regEx = new Regex(@"{[0-9a-zA-Z]+}");
			var matches = regEx.Matches(url);
			if (matches.Count > 0)
			{
				throw new ValidationException("All of the resource URL variables are not being replaced. Please include the values for all route variables in the route parameters text box.");
			}

			// Make sure none of the route parameters are set to an empty string
			if (url.Replace("https://", string.Empty).Contains("//"))
			{
				throw new ValidationException("Double slash detected (//). This is most likely an error where the resource URL is incorrect or a route parameter's value is an empty string (eg. {year}=");
			}

		}

		#endregion
	}
}
