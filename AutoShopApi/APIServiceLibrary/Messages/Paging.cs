﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIServiceLibrary
{
	public class PagingInformation
	{
		#region Public Properties

		/// <summary>Gets or sets the end index.</summary>
		public int EndIndex
		{
			get;
			set;
		}

		/// <summary>Gets or sets the items per result.</summary>
		public int ItemsPerPage
		{
			get;
			set;
		}

		/// <summary>Gets or sets the start index.</summary>
		public int StartIndex
		{
			get;
			set;
		}

		/// <summary>Gets or sets the total items.</summary>
		public int TotalItemCount
		{
			get;
			set;
		}

		#endregion
	}
}
