﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Web;

namespace APIServiceLibrary
{
    public class HttpRequestBuilder
    {
        private HttpMethod method = null;
        private string requestUri = "";
        private HttpContent content = null;
        private string bearerToken = "";
        private string acceptHeader = "application/json";
        private TimeSpan timeout = new TimeSpan(0, 0, 15);
        private bool allowAutoRedirect = false;
        private WebServiceRequestModel model = new WebServiceRequestModel();
        private DateTime requestTime = new DateTime();
        public DateTime RequestTime { get; set; }

        public WebServiceRequestModel Model
        {
            get;
            set;
        }

        public HttpRequestBuilder()
        {
            requestTime = DateTime.Now;
            RequestTime = requestTime;
        }

        public HttpRequestBuilder(WebServiceRequestModel Reqmodel)
        {
            requestTime = DateTime.Now;
            RequestTime = requestTime;
            model = Reqmodel;
            Model = model;
        }

        public HttpRequestBuilder AddMethod(HttpMethod method)
        {
            this.method = method;
            return this;
        }

        #region RequestURI

        public HttpRequestBuilder AddRequestUri(string requestUri)
        {
            this.requestUri = requestUri;
            return this;
        }
       
        public HttpRequestBuilder AddRequestUri(WebServiceRequestModel omodel)
        {
            var url = string.Format("{0}{1}{2}", omodel.Protocol, omodel.Environment, omodel.Endpoint);

            if (!string.IsNullOrWhiteSpace(omodel.QueryStringParameters))
            {
                if (omodel.QueryStringParameters.StartsWith("?"))
                {
                    url += omodel.QueryStringParameters;
                }
                else
                {
                    url = string.Format("{0}?{1}", url, omodel.QueryStringParameters);
                }
            }

            if (!string.IsNullOrWhiteSpace(omodel.RouteValues))
            {
                var routeValues = omodel.RouteValues.Split(new[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var routeValue in routeValues)
                {
                    var keyAndValue = routeValue.Split('=');
                    if (keyAndValue.Length == 1)
                    {
                        url = url.Replace(keyAndValue[0], string.Empty);
                    }
                    else if (keyAndValue.Length == 2)
                    {
                        url = url.Replace(keyAndValue[0], keyAndValue[1] ?? string.Empty);
                    }
                }
            }            

            this.requestUri = url;
            return this;
        }

        public HttpRequestBuilder AddRequestUri()
        {
            var url = string.Format("{0}{1}{2}", model.Protocol, model.Environment, model.Endpoint);

            if (!string.IsNullOrWhiteSpace(model.QueryStringParameters))
            {
                if (model.QueryStringParameters.StartsWith("?"))
                {
                    url += model.QueryStringParameters;
                }
                else
                {
                    url = string.Format("{0}?{1}", url, model.QueryStringParameters);
                }
            }

            if (!string.IsNullOrWhiteSpace(model.RouteValues))
            {
                var routeValues = model.RouteValues.Split(new[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var routeValue in routeValues)
                {
                    var keyAndValue = routeValue.Split('=');
                    if (keyAndValue.Length == 1)
                    {
                        url = url.Replace(keyAndValue[0], string.Empty);
                    }
                    else if (keyAndValue.Length == 2)
                    {
                        url = url.Replace(keyAndValue[0], keyAndValue[1] ?? string.Empty);
                    }
                }
            }            

            this.requestUri = url;
            return this;
        }


        #endregion

        #region AddContent
        public HttpRequestBuilder AddContent(HttpContent content)
        {
            this.content = content;
            return this;
        }

        #endregion

        #region AddBrearerTokens
        public HttpRequestBuilder AddBearerToken()
        {
            var dateTimeToUse = RequestTime.ToUniversalTime();
            var apiKey = model.PublicKey;

            var request = new HttpRequestMessage
            {
                Method = this.method,
                RequestUri = new Uri(this.requestUri)
            };

            var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), model.AuthenticationSchemeType);
            switch (authSchemeType)
            {
                case AuthenticationSchemeTypes.None:
                    return this;

                case AuthenticationSchemeTypes.Shared:

                    var hmac2 = Hashing.GenerateHMACSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, dateTimeToUse.ToUnixTime(), request.RequestUri, request.Method.Method.ToUpper());
                    bearerToken = (model.AuthPlacement == "Header") ? Convert.ToBase64String(hmac2) : HttpUtility.UrlEncode(Convert.ToBase64String(hmac2), Encoding.UTF8);
                    break;

                case AuthenticationSchemeTypes.MWS:
                    var sha256 = Hashing.GenerateSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, dateTimeToUse.ToUnixTime());
                    bearerToken = string.Concat(sha256.Select(b => string.Format("{0:x2}", b)));
                    break;
            }
            
            return this;
        }

        public HttpRequestBuilder AddBearerToken(string bearerToken)
        {
            this.bearerToken = bearerToken;
            return this;
        }

        public HttpRequestBuilder AddBearerToken(WebServiceRequestModel omodel)
        {
            var dateTimeToUse = RequestTime.ToUniversalTime();
            var apiKey = omodel.PublicKey;

            var request = new HttpRequestMessage
            {
                Method = this.method,
                RequestUri = new Uri(this.requestUri)
            };

            var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), omodel.AuthenticationSchemeType);
            switch (authSchemeType)
            {
                case AuthenticationSchemeTypes.None:
                    return this;

                case AuthenticationSchemeTypes.Shared:

                    var hmac2 = Hashing.GenerateHMACSHA256Auth(model.PublicKey, omodel.PrivateKeyOrToken, dateTimeToUse.ToUnixTime(), request.RequestUri, request.Method.Method.ToUpper());
                    bearerToken = (model.AuthPlacement == "Header") ? Convert.ToBase64String(hmac2) : HttpUtility.UrlEncode(Convert.ToBase64String(hmac2), Encoding.UTF8);
                    break;

                case AuthenticationSchemeTypes.MWS:
                    var sha256 = Hashing.GenerateSHA256Auth(model.PublicKey, omodel.PrivateKeyOrToken, dateTimeToUse.ToUnixTime());
                    bearerToken = string.Concat(sha256.Select(b => string.Format("{0:x2}", b)));
                    break;
            }            

            return this;
        }
        #endregion

        #region AddAccecptHeaders
        public HttpRequestBuilder AddAcceptHeader(string acceptHeader)
        {
            this.acceptHeader = acceptHeader;
            return this;
        }

        public HttpRequestBuilder AddAcceptHeader()
        {
            this.acceptHeader = model.AcceptFormat;
            return this;
        }

        public HttpRequestBuilder AddAcceptHeader(WebServiceRequestModel omodel)
        {
            this.acceptHeader = omodel.AcceptFormat;
            return this;
        }
        #endregion

        #region AddTimeouts
        public HttpRequestBuilder AddTimeout(TimeSpan timeout)
        {
            this.timeout = timeout;
            return this;
        }
        #endregion

        #region Allowredirects
        public HttpRequestBuilder AddAllowAutoRedirect(bool allowAutoRedirect)
        {
            this.allowAutoRedirect = allowAutoRedirect;
            return this;
        }
        #endregion

        #region Mainfunction
        /// <summary>
        /// SendAsync
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> SendAsync()
        {
            // Check required arguments
            EnsureArguments();

            var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), model.AuthenticationSchemeType,true);

            var currentDateTime = DateTime.UtcNow;            

            // Set up request
            var request = new HttpRequestMessage
            {
                Method = this.method,
                RequestUri = new Uri(this.requestUri)             
            };           

            // All dates should be in UTC so we are creating the overridden date as UTC
            if (model.RequestDateOverride.HasValue)
            {
                currentDateTime = new DateTime(model.RequestDateOverride.Value.Year, model.RequestDateOverride.Value.Month, model.RequestDateOverride.Value.Day, model.RequestDateOverride.Value.Hour, model.RequestDateOverride.Value.Minute, model.RequestDateOverride.Value.Second, DateTimeKind.Utc);
            }

            // A date is used in some of the authentication types
            if (model.IncludeDate)
            {
                request.Headers.Date = currentDateTime;
            }

            if (this.content != null)
                request.Content = this.content;             

            request.Headers.Accept.Clear();
            if (!string.IsNullOrEmpty(this.acceptHeader))
                //request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(this.acceptHeader));
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(model.AcceptFormat));

            request.Headers.ExpectContinue = null;

            #region Cors
            if (model.EnableCors)
            {
                request.Headers.Add("Origin", model.CorsOrigin);

                if (!string.IsNullOrWhiteSpace(model.CorsRequestHeaders))
                {
                    request.Headers.Add("Access-Control-Request-Headers", model.CorsRequestHeaders);
                }

                if (!string.IsNullOrWhiteSpace(model.CorsRequestMethod))
                {
                    request.Headers.Add("Access-Control-Request-Method", model.CorsRequestMethod);
                }
            }

            if (!string.IsNullOrWhiteSpace(model.CorrelationID))
            {
                request.Headers.Add("X-CorrelationID", model.CorrelationID);
            }
            #endregion

            #region PlaceAuthorization
            if (!string.IsNullOrEmpty(this.bearerToken))
            {   

                if (model.AuthPlacement.ToUpper() == AuthenticationPlacements.QueryString.ToString().ToUpper())
                {
                    request.RequestUri = new Uri(string.Format(request.RequestUri.ToString().Contains("?") ? "{0}&ApiKey={1}&Sig={2}&Scheme={3}" : "{0}?ApiKey={1}&Sig={2}&Scheme={3}", request.RequestUri.ToString(), model.PublicKey, this.bearerToken, authSchemeType));

                    if (model.IncludeDate)
                    {
                        request.RequestUri = new Uri(string.Format("{0}&xdate={1}", request.RequestUri, currentDateTime.ToUnixTime()));
                    }
                }

                if (model.AuthPlacement.ToUpper() == AuthenticationPlacements.Header.ToString())
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue(authSchemeType.ToString(), string.Format("{0}:{1}", model.PublicKey, this.bearerToken));

                    if (model.IncludeDate)
                    {
                        request.Headers.Add("X-Date", currentDateTime.ToString("r"));
                    }
                }

                if (model.AuthPlacement.ToUpper() == AuthenticationPlacements.None.ToString())
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", this.bearerToken);
                }

            }
            #endregion

            // Setup client
            var handler = new HttpClientHandler();
            handler.AllowAutoRedirect = this.allowAutoRedirect;

            var client = new System.Net.Http.HttpClient(handler);
            client.Timeout = this.timeout;
            
            return await client.SendAsync(request);
        }

        /// <summary>
        /// SendAsync
        /// </summary>
        /// <param name="omodel"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> SendAsync(WebServiceRequestModel omodel)
        {
            // Check required arguments
            EnsureArguments();

            var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), omodel.AuthenticationSchemeType, true);

            var currentDateTime = RequestTime.ToUniversalTime();

            // Set up request
            var request = new HttpRequestMessage
            {
                Method = this.method,
                RequestUri = new Uri(this.requestUri)
            };

            // All dates should be in UTC so we are creating the overridden date as UTC
            if (omodel.RequestDateOverride.HasValue)
            {
                currentDateTime = new DateTime(omodel.RequestDateOverride.Value.Year, omodel.RequestDateOverride.Value.Month, omodel.RequestDateOverride.Value.Day, omodel.RequestDateOverride.Value.Hour, omodel.RequestDateOverride.Value.Minute, omodel.RequestDateOverride.Value.Second, DateTimeKind.Utc);
            }

            // A date is used in some of the authentication types
            if (omodel.IncludeDate)
            {
                request.Headers.Date = currentDateTime;
            }

            if (this.content != null)
                request.Content = this.content;

            request.Headers.Accept.Clear();
            if (!string.IsNullOrEmpty(this.acceptHeader))
                //request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(this.acceptHeader));
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(omodel.AcceptFormat));

            request.Headers.ExpectContinue = null;

            #region Cors
            if (omodel.EnableCors)
            {
                request.Headers.Add("Origin", omodel.CorsOrigin);

                if (!string.IsNullOrWhiteSpace(omodel.CorsRequestHeaders))
                {
                    request.Headers.Add("Access-Control-Request-Headers", omodel.CorsRequestHeaders);
                }

                if (!string.IsNullOrWhiteSpace(omodel.CorsRequestMethod))
                {
                    request.Headers.Add("Access-Control-Request-Method", omodel.CorsRequestMethod);
                }
            }

            if (!string.IsNullOrWhiteSpace(omodel.CorrelationID))
            {
                request.Headers.Add("X-CorrelationID", omodel.CorrelationID);
            }
            #endregion

            #region PlaceAuthorization
            if (!string.IsNullOrEmpty(this.bearerToken))
            {

                if (omodel.AuthPlacement.ToUpper() == AuthenticationPlacements.QueryString.ToString().ToUpper())
                {
                    request.RequestUri = new Uri(string.Format(request.RequestUri.ToString().Contains("?") ? "{0}&ApiKey={1}&Sig={2}&Scheme={3}" : "{0}?ApiKey={1}&Sig={2}&Scheme={3}", request.RequestUri.ToString(), omodel.PublicKey, this.bearerToken, authSchemeType));

                    if (omodel.IncludeDate)
                    {
                        request.RequestUri = new Uri(string.Format("{0}&xdate={1}", request.RequestUri, currentDateTime.ToUnixTime()));
                    }
                }

                if (omodel.AuthPlacement.ToUpper() == AuthenticationPlacements.Header.ToString().ToUpper())
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue(authSchemeType.ToString(), string.Format("{0}:{1}", omodel.PublicKey, this.bearerToken));

                    if (omodel.IncludeDate)
                    {
                        request.Headers.Add("X-Date", currentDateTime.ToString("r"));
                    }
                }

                if (omodel.AuthPlacement.ToUpper() == AuthenticationPlacements.None.ToString())
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", this.bearerToken);
                }

            }
            #endregion

			WebProxy WebP = new WebProxy(@"http://pac.wellsfargo.net/");
			NetworkCredential Nc = new NetworkCredential("U396058", "Ashwin4y", "AD-ENT");
			WebP.Credentials = Nc;			

            // Setup client
            var handler = new HttpClientHandler();
            handler.AllowAutoRedirect = this.allowAutoRedirect;	   

			handler.Credentials = Nc;
			handler.Proxy = WebP;
			ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            var client = new System.Net.Http.HttpClient(handler);
            client.Timeout = this.timeout;

            return await client.SendAsync(request);
        }

		public HttpResponseMessage Sendsync(WebServiceRequestModel omodel)
		{

			try
{
			// Check required arguments
			EnsureArguments();

			var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), omodel.AuthenticationSchemeType, true);

			var currentDateTime = RequestTime.ToUniversalTime();

			// Set up request
			var request = new HttpRequestMessage
			{
				Method = this.method,
				RequestUri = new Uri(this.requestUri)
			};

			// All dates should be in UTC so we are creating the overridden date as UTC
			if (omodel.RequestDateOverride.HasValue)
			{
				currentDateTime = new DateTime(omodel.RequestDateOverride.Value.Year, omodel.RequestDateOverride.Value.Month, omodel.RequestDateOverride.Value.Day, omodel.RequestDateOverride.Value.Hour, omodel.RequestDateOverride.Value.Minute, omodel.RequestDateOverride.Value.Second, DateTimeKind.Utc);
			}

			// A date is used in some of the authentication types
			if (omodel.IncludeDate)
			{
				request.Headers.Date = currentDateTime;
			}

			if (this.content != null)
				request.Content = this.content;

			request.Headers.Accept.Clear();
			if (!string.IsNullOrEmpty(this.acceptHeader))
				//request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(this.acceptHeader));
				request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(omodel.AcceptFormat));

			request.Headers.ExpectContinue = null;

			#region Cors
			if (omodel.EnableCors)
			{
				request.Headers.Add("Origin", omodel.CorsOrigin);

				if (!string.IsNullOrWhiteSpace(omodel.CorsRequestHeaders))
				{
					request.Headers.Add("Access-Control-Request-Headers", omodel.CorsRequestHeaders);
				}

				if (!string.IsNullOrWhiteSpace(omodel.CorsRequestMethod))
				{
					request.Headers.Add("Access-Control-Request-Method", omodel.CorsRequestMethod);
				}
			}

			if (!string.IsNullOrWhiteSpace(omodel.CorrelationID))
			{
				request.Headers.Add("X-CorrelationID", omodel.CorrelationID);
			}
			#endregion

			#region PlaceAuthorization
			if (!string.IsNullOrEmpty(this.bearerToken))
			{

				if (omodel.AuthPlacement.ToUpper() == AuthenticationPlacements.QueryString.ToString().ToUpper())
				{
					request.RequestUri = new Uri(string.Format(request.RequestUri.ToString().Contains("?") ? "{0}&ApiKey={1}&Sig={2}&Scheme={3}" : "{0}?ApiKey={1}&Sig={2}&Scheme={3}", request.RequestUri.ToString(), omodel.PublicKey, this.bearerToken, authSchemeType));

					if (omodel.IncludeDate)
					{
						request.RequestUri = new Uri(string.Format("{0}&xdate={1}", request.RequestUri, currentDateTime.ToUnixTime()));
					}
				}

				if (omodel.AuthPlacement.ToUpper() == AuthenticationPlacements.Header.ToString().ToUpper())
				{
					request.Headers.Authorization = new AuthenticationHeaderValue(authSchemeType.ToString(), string.Format("{0}:{1}", omodel.PublicKey, this.bearerToken));

					if (omodel.IncludeDate)
					{
						request.Headers.Add("X-Date", currentDateTime.ToString("r"));
					}
				}

				if (omodel.AuthPlacement.ToUpper() == AuthenticationPlacements.None.ToString())
				{
					request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", this.bearerToken);
				}

			}
			#endregion
			

			WebProxy WebP = new WebProxy(@"http://pac.wellsfargo.net/");
			NetworkCredential Nc = new NetworkCredential("U396058", "Ashwin4y", "AD-ENT");
			WebP.Credentials = Nc;

			// Setup client
			var handler = new HttpClientHandler();
			handler.AllowAutoRedirect = this.allowAutoRedirect;

			handler.Credentials = Nc;
			handler.Proxy = WebP;
			ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

			var client = new System.Net.Http.HttpClient(handler);
			client.Timeout = this.timeout;

			var datare =   client.SendAsync(request).Result;

			return datare;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        #endregion

        #region " Private "

        private void EnsureArguments()
        {

            if (this.method == null)
                throw new ArgumentNullException("Method");
            
            if (string.IsNullOrEmpty(this.requestUri))
                throw new ArgumentNullException("Request Uri");
            
            if (string.IsNullOrWhiteSpace(model.Endpoint))
                throw new ArgumentNullException("Endpoint");

        }

        #endregion
    }
}
