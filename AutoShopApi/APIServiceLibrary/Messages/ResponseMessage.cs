﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Xml.Linq;
using System.IO;
using System.Net;


namespace APIServiceLibrary
{
	/// <summary>This class will help to pull out the information returned from the web services.</summary>
	public static class ResponseMessage
	{
		#region Public Methods and Operators

		/// <summary>This will validate the web services response and return a string representation of the response.</summary>
		/// <param name="response">The response from the web services.</param>
		/// <param name="model">The web services request.</param>
		/// <returns>The web services response, as a string, with or without the header information based on the request.</returns>
		public static string ValidateResult(HttpResponseMessage response, WebServiceRequestModel model)
		{
			var responseMessage = response.Content.ReadAsStringAsync().Result;

			if (response.Content != null && response.Content.Headers.ContentType != null)
			{
				var returnedAcceptType = response.Content.Headers.ContentType.MediaType;

				if (returnedAcceptType == "application/json")
				{
					var jt = JToken.Parse(responseMessage);
					responseMessage = jt.ToString(Formatting.Indented);
				}
				else if (returnedAcceptType == "text/xml")
				{
					responseMessage = XElement.Parse(responseMessage).ToString();
				}
			}

			var headerString = string.Empty;
			if (model.ShowHeaders)
			{
				headerString += "HTTP/" + response.Version + " " + Convert.ToInt32(response.StatusCode) + " " + response.ReasonPhrase + Environment.NewLine + Environment.NewLine;

				var headers = new Dictionary<string, string>();

				foreach (var header in response.Headers)
				{
					headers.Add(header.Key.Trim(), string.Join(" ", header.Value).Trim());
				}

				if (response.Content != null)
				{
					foreach (var header in response.Content.Headers)
					{
						if (!headers.ContainsKey(header.Key))
						{
							headers.Add(header.Key.Trim(), string.Join(" ", header.Value).Trim());
						}
					}
				}

				foreach (var header in headers)
				{
					if (model.HideWebServerHeaders)
					{
						if (header.Key == "X-SourceFiles" || header.Key == "X-Powered-By" || header.Key == "Server")
						{
							continue;
						}
					}

					headerString += header.Key.Trim() + ": " + header.Value.Trim() + Environment.NewLine;
				}

				headerString += Environment.NewLine;
			}

			return headerString + responseMessage;
		}

		public static string ValidateResult(HttpWebResponse response, WebServiceRequestModel model)
		{
			try
			{
				var responseMessage = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();

				if (responseMessage != null && response.ContentType != null)
				{
					var returnedAcceptType = response.ContentType;

					if (returnedAcceptType == "application/json")
					{
						var jt = JToken.Parse(responseMessage);
						responseMessage = jt.ToString(Formatting.Indented);
					}
					else if (returnedAcceptType == "text/xml")
					{
						responseMessage = XElement.Parse(responseMessage).ToString();
					}
				}

				var headerString = string.Empty;
				if (model.ShowHeaders)
				{
					headerString += "HTTP/" + response.Server + " " + Convert.ToInt32(response.StatusCode) + " " + response.StatusDescription + Environment.NewLine + Environment.NewLine;

					var headers = new Dictionary<string, string>();
					int x = 0;
					foreach (var header in response.Headers)
					{

						headers.Add(response.Headers.AllKeys[x], string.Join(" ", response.Headers[x].ToString()));
						x++;
					}

					foreach (var header in headers)
					{
						if (model.HideWebServerHeaders)
						{
							if (header.Key == "X-SourceFiles" || header.Key == "X-Powered-By" || header.Key == "Server")
							{
								continue;
							}
						}

						headerString += header.Key.Trim() + ": " + header.Value.Trim() + Environment.NewLine;
					}

					headerString += Environment.NewLine;
				}

				return headerString + responseMessage;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static string ValidateResult(HttpWebRequest httprequest, WebServiceRequestModel model)
		{
			try
			{
				var response = (HttpWebResponse)httprequest.GetResponse();
				var responseMessage = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();

				if (responseMessage != null && response.ContentType != null)
				{
					var returnedAcceptType = response.ContentType;

                    if (returnedAcceptType == "application/json")
                    {
                        var jt = JToken.Parse(responseMessage);
                        responseMessage = jt.ToString(Formatting.Indented);
                    }
                    else if (returnedAcceptType == "text/xml")
                    {
                        responseMessage = XElement.Parse(responseMessage).ToString();
                    }                   
				}

				var headerString = string.Empty;
				if (model.ShowHeaders)
				{
					headerString += "HTTP/" + response.Server + " " + Convert.ToInt32(response.StatusCode) + " " + response.StatusDescription + Environment.NewLine + Environment.NewLine;

					var headers = new Dictionary<string, string>();
					int x = 0;
					foreach (var header in response.Headers)
					{

						headers.Add(response.Headers.AllKeys[x], string.Join(" ", response.Headers[x].ToString()));
						x++;
					}

					foreach (var header in headers)
					{
						if (model.HideWebServerHeaders)
						{
							if (header.Key == "X-SourceFiles" || header.Key == "X-Powered-By" || header.Key == "Server")
							{
								continue;
							}
						}

						headerString += header.Key.Trim() + ": " + header.Value.Trim() + Environment.NewLine;
					}

					headerString += Environment.NewLine;
				}

				return headerString + responseMessage;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        /// <summary>A standard MOTOR web service response is made up of two pieces; the body and header. 
        /// The header will contain information about the request. In the event of an error with the request, 
        /// the header will contain the information to help resolve the error. The body is the actual data the 
        /// request was trying to retrieve.
        /// 
        /// This method isn't used in this sample, but is here to show how a base class could be used in conjunction 
        /// with an object (like an Estimated Work Time) to get the data that was requested. This is just an example 
        /// that will work for JSON.</summary>
        /// <param name="result">The web service response.</param>
        /// <typeparam name="T">The type of data that is expected to be returned.</typeparam>
        /// <returns>The data the request asked to retrieve.</returns>
        public static T ValidateResult<T>(HttpResponseMessage result) where T : class
		{
			var resultString = result.Content.ReadAsStringAsync().Result;
			var response = JsonConvert.DeserializeObject<ResponseBase<T>>(resultString);
			if (response != null)
			{
				return response.Body;
			}

			return null;
		}

		public static T ValidateResult<T>(string result) where T : class
		{
			var resultString = result;
			var response = JsonConvert.DeserializeObject<T>(resultString);
			if (response != null)
			{
				return response;
			}

			return null;
		}

		#endregion
	}

	public class FileContent : MultipartFormDataContent
	{
		public FileContent(string filePath, string apiParamName)
		{
			var filestream = File.Open(filePath, FileMode.Open);
			var filename = Path.GetFileName(filePath);

			Add(new StreamContent(filestream), apiParamName, filename);
		}
	}

	public class JsonContent : StringContent
	{
		public JsonContent(object value)
			: base(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json")
		{
		}

		public JsonContent(object value, string mediaType)
			: base(JsonConvert.SerializeObject(value), Encoding.UTF8, mediaType)
		{
		}
	}

	public class PatchContent : StringContent
	{
		public PatchContent(object value)
			: base(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json-patch+json")
		{
		}
	}


}
