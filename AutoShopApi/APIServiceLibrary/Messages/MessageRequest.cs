﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Data;
using System.Threading;
using System.Drawing;

namespace APIServiceLibrary
{
	/// <summary>This helps to build up the request message to send to the web services.</summary>
	public static class MessageRequest
	{

		//static HttpClient client = new HttpClient();
		//static async Task<string> GetAsync(string path)
		//{
		//	string result;
		//	HttpResponseMessage response = await client.GetAsync(path);
		//	if (response.IsSuccessStatusCode)
		//	{
		//		result = await response.Content.ReadAsStringAsync();
		//	}
		//	return result;
		//}

		//static async Task RunAsync()
		//{
		//	// Update port # in the following line.
		//	client.BaseAddress = new Uri("http://localhost:64195/");
		//	client.DefaultRequestHeaders.Accept.Clear();
		//	client.DefaultRequestHeaders.Accept.Add(
		//		new MediaTypeWithQualityHeaderValue("application/json"));

		//	try
		//	{
                
		//		// Get the product
		//		product = await GetProductAsync(url.PathAndQuery);
		//		ShowProduct(product);


		//	}
		//	catch (Exception e)
		//	{
		//		Console.WriteLine(e.Message);
		//	}

            
		//}

		#region Public Methods and Operators

		/// <summary>This will build URL to call the web services based on the information in the request. This will substitute the route parameters and append the query string information.</summary>
		/// <param name="model">The web services request.</param>
		/// <returns>A string representing the URL of the web services to be queried.</returns>
		public static string BuildUrl(WebServiceRequestModel model)
		{
			var url = string.Format("{0}{1}{2}", model.Protocol, model.Environment, model.Endpoint);

			if (!string.IsNullOrWhiteSpace(model.QueryStringParameters))
			{
				if (model.QueryStringParameters.StartsWith("?"))
				{
					url += model.QueryStringParameters;
				}
				else
				{
					url = string.Format("{0}?{1}", url, model.QueryStringParameters);
				}
			}

			if (!string.IsNullOrWhiteSpace(model.RouteValues))
			{
				var routeValues = model.RouteValues.Split(new[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
				foreach (var routeValue in routeValues)
				{
					var keyAndValue = routeValue.Split('=');
					if (keyAndValue.Length == 1)
					{
						url = url.Replace(keyAndValue[0], string.Empty);
					}
					else if (keyAndValue.Length == 2)
					{
						url = url.Replace(keyAndValue[0], keyAndValue[1] ?? string.Empty);
					}
				}
			}

			return url;
		}

		/// <summary>This will create the web services request.</summary>
		/// <param name="model">The web services request.</param>
		/// <returns>The HTTP request that can be executed to retrieve information from the web services.</returns>
		public static HttpRequestMessage CreateRequest(WebServiceRequestModel model)
		{
			if (string.IsNullOrWhiteSpace(model.Endpoint))
			{
				return null;
			}

			var currentDateTime = DateTime.UtcNow;

			// All dates should be in UTC so we are creating the overridden date as UTC
			if (model.RequestDateOverride.HasValue)
			{
				currentDateTime = new DateTime(model.RequestDateOverride.Value.Year, model.RequestDateOverride.Value.Month, model.RequestDateOverride.Value.Day, model.RequestDateOverride.Value.Hour, model.RequestDateOverride.Value.Minute, model.RequestDateOverride.Value.Second, DateTimeKind.Utc);
			}

			var url = BuildUrl(model);
			var method = new HttpMethod(model.HttpVerb);
			var message = new HttpRequestMessage(method, url);

			// A date is used in some of the authentication types
			if (model.IncludeDate)
			{
				message.Headers.Date = currentDateTime;
			}

			message.Headers.ExpectContinue = null;
			message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(model.AcceptFormat));

			if (((method == HttpMethod.Post) || (method == HttpMethod.Put)) && !string.IsNullOrWhiteSpace(model.RequestData))
			{
				message.Content = new StringContent(model.RequestData);
				message.Content.Headers.ContentType.MediaType = model.ContentTypeFormat;
			}

			if (model.EnableCors)
			{
				message.Headers.Add("Origin", model.CorsOrigin);

				if (!string.IsNullOrWhiteSpace(model.CorsRequestHeaders))
				{
					message.Headers.Add("Access-Control-Request-Headers", model.CorsRequestHeaders);
				}

				if (!string.IsNullOrWhiteSpace(model.CorsRequestMethod))
				{
					message.Headers.Add("Access-Control-Request-Method", model.CorsRequestMethod);
				}
			}

			if (!string.IsNullOrWhiteSpace(model.CorrelationID))
			{
				message.Headers.Add("X-CorrelationID", model.CorrelationID);
			}



			ApplyAuthenticataion(message, model, currentDateTime);

			return message;
		}

        public static HttpWebRequest GetWebResponseValidate(WebServiceRequestModel model)
        {
            var url = BuildUrl(model);
            var method = new HttpMethod(model.HttpVerb);
            var currentDateTime = DateTime.UtcNow;
            var apiKey = model.PublicKey;
            var httpVerb = method.Method.ToUpper();
            var varUri = new Uri(url);

            var signature = string.Empty;

            var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), model.AuthenticationSchemeType);

            switch (authSchemeType)
            {
                case AuthenticationSchemeTypes.None:
                    break;

                case AuthenticationSchemeTypes.Shared:
                    var hmac2 = Hashing.GenerateHMACSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, currentDateTime.ToUnixTime(), new Uri(url), httpVerb);
                    signature = (model.AuthPlacement == "Header") ? Convert.ToBase64String(hmac2) : HttpUtility.UrlEncode(Convert.ToBase64String(hmac2), Encoding.UTF8);
                    break;

                case AuthenticationSchemeTypes.MWS:
                    var sha256 = Hashing.GenerateSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, currentDateTime.ToUnixTime());
                    signature = string.Concat(sha256.Select(b => string.Format("{0:x2}", b)));
                    break;
            }

            if (model.AuthPlacement == AuthenticationPlacements.QueryString.ToString())
            {
                varUri = new Uri(string.Format(url.Contains("?") ? "{0}&ApiKey={1}&Sig={2}&Scheme={3}" : "{0}?ApiKey={1}&Sig={2}&Scheme={3}", url, apiKey, signature, authSchemeType));

                if (model.IncludeDate)
                {
                    varUri = new Uri(string.Format("{0}&xdate={1}", varUri, currentDateTime.ToUnixTime()));
                }
            }

            HttpWebRequest httprequest = WebRequest.Create(varUri) as HttpWebRequest;
            httprequest.Headers.Clear();


            if (string.IsNullOrWhiteSpace(model.Endpoint))
            {
                return null;
            }

            // All dates should be in UTC so we are creating the overridden date as UTC
            if (model.RequestDateOverride.HasValue)
            {
                currentDateTime = new DateTime(model.RequestDateOverride.Value.Year, model.RequestDateOverride.Value.Month, model.RequestDateOverride.Value.Day, model.RequestDateOverride.Value.Hour, model.RequestDateOverride.Value.Minute, model.RequestDateOverride.Value.Second, DateTimeKind.Utc);
            }

            httprequest.UserAgent = ".NET Framework Test Client";
            httprequest.Expect = null;
            httprequest.ContentType = model.AcceptFormat; //"text/xml; encoding='utf-8'";
            httprequest.MediaType = model.AcceptFormat;
            httprequest.Method = model.HttpVerb;
            httprequest.Date = currentDateTime;

            httprequest.Accept = "application/x-ms-application,image/jpeg,application/xaml+xml,image/gif,image/pjpeg,application/x-ms-xbap,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/msword,*/*";
            //httprequest.Headers["Accept-Language"] = "en-US";
            httprequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
            //httprequest.AllowReadStreamBuffering = true;
            //httprequest.AllowWriteStreamBuffering = true;
            //httprequest.UseDefaultCredentials = true;
            httprequest.KeepAlive = false;
            httprequest.AllowAutoRedirect = true;
            httprequest.Timeout = 600000;



            if (model.EnableCors)
            {
                httprequest.Headers.Add("Origin", model.CorsOrigin);

                if (!string.IsNullOrWhiteSpace(model.CorsRequestHeaders))
                {
                    httprequest.Headers.Add("Access-Control-Request-Headers", model.CorsRequestHeaders);
                }

                if (!string.IsNullOrWhiteSpace(model.CorsRequestMethod))
                {
                    httprequest.Headers.Add("Access-Control-Request-Method", model.CorsRequestMethod);
                }
            }

            if (!string.IsNullOrWhiteSpace(model.CorrelationID))
            {
                httprequest.Headers.Add("X-CorrelationID", model.CorrelationID);
            }

            if (model.AuthPlacement.ToUpper() == AuthenticationPlacements.Header.ToString().ToUpper())
            {

                httprequest.Headers.Add("Authorization", string.Format("{0} {1}", authSchemeType.ToString(), string.Format("{0}:{1}", apiKey, signature)));
                if (model.IncludeDate)
                {
                    httprequest.Headers.Add("X-Date", currentDateTime.ToString("r"));
                }
            }
            var data = Encoding.ASCII.GetBytes(string.IsNullOrEmpty(model.RequestData) ? "" : model.RequestData);
            httprequest.ContentLength = data.Length;

            if (((method == HttpMethod.Post) || (method == HttpMethod.Put)) && !string.IsNullOrWhiteSpace(model.RequestData))
            {
                //var data = new StringContent(model.RequestData);
                //var data = Encoding.ASCII.GetBytes(model.RequestData);

                using (var stream = httprequest.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

            }

            return httprequest;
        }

        public static Stream GetWebResponseImageStream(WebServiceRequestModel model)
        {
            //Image vehicleImg;
            HttpWebRequest httprequest = GetWebResponseValidate(model);
            var response = (HttpWebResponse)httprequest.GetResponse();

            System.IO.Stream stream = response.GetResponseStream();

            //vehicleImg = Image.FromStream(stream);
            return stream;
        }
		public static string GetWebResponse(WebServiceRequestModel model)
		{
			try
			{
                HttpWebRequest httprequest = GetWebResponseValidate(model);
                var responseString = ResponseMessage.ValidateResult(httprequest, model);//new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
				return responseString;

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		#endregion

		#region Methods

		/// <summary>This will apply the type of authentication based on the web services request to the HTTP request.</summary>
		private static void ApplyAuthenticataion(HttpRequestMessage message, WebServiceRequestModel model, DateTime dateTimeToUse)
		{
			var url = message.RequestUri.ToString();
			var apiKey = model.PublicKey;
			var httpVerb = message.Method.Method.ToUpper();

			var signature = string.Empty;

			var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), model.AuthenticationSchemeType);
			switch (authSchemeType)
			{
				case AuthenticationSchemeTypes.None:
					return;

				case AuthenticationSchemeTypes.Shared:
					var hmac2 = Hashing.GenerateHMACSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, dateTimeToUse.ToUnixTime(), message.RequestUri, httpVerb);
					signature = (model.AuthPlacement == "Header") ? Convert.ToBase64String(hmac2) : HttpUtility.UrlEncode(Convert.ToBase64String(hmac2), Encoding.UTF8);
					break;

				case AuthenticationSchemeTypes.MWS:
					var sha256 = Hashing.GenerateSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, dateTimeToUse.ToUnixTime());
					signature = string.Concat(sha256.Select(b => string.Format("{0:x2}", b)));
					break;
			}

			if (model.AuthPlacement == AuthenticationPlacements.QueryString.ToString())
			{
				message.RequestUri = new Uri(string.Format(url.Contains("?") ? "{0}&ApiKey={1}&Sig={2}&Scheme={3}" : "{0}?ApiKey={1}&Sig={2}&Scheme={3}", url, apiKey, signature, authSchemeType));

				if (model.IncludeDate)
				{
					message.RequestUri = new Uri(string.Format("{0}&xdate={1}", message.RequestUri, dateTimeToUse.ToUnixTime()));
				}
			}
			else
			{
				message.Headers.Authorization = new AuthenticationHeaderValue(authSchemeType.ToString(), string.Format("{0}:{1}", apiKey, signature));

				if (model.IncludeDate)
				{
					message.Headers.Add("X-Date", dateTimeToUse.ToString("r"));
				}
			}
		}

		

		#endregion
	}

	public class RequestState
	{
		const int BufferSize = 1024;
		public StringBuilder RequestData;
		public byte[] BufferRead;
		public WebRequest Request;

		//public HttpWebRequest request;
		//public HttpWebResponse response;

		public string Message = string.Empty;
		public string Status = string.Empty;

		public Stream ResponseStream;
		// Create Decoder for appropriate enconding type.  
		public Decoder StreamDecode = Encoding.UTF8.GetDecoder();
		public StringBuilder ResponseData;
		public WebServiceRequestModel Model{get; set;}
		public RequestState()
		{
			BufferRead = new byte[BufferSize];
			RequestData = new StringBuilder(String.Empty);
			ResponseData = new StringBuilder(String.Empty);
			Request = null;
			ResponseStream = null;
		}
		public RequestState(WebServiceRequestModel model)
		{
			BufferRead = new byte[BufferSize];
			RequestData = new StringBuilder(String.Empty);
			ResponseData = new StringBuilder(String.Empty);
			Request = null;
			ResponseStream = null;
			Model = model;
		}
	}

	static public class ClientGetAsync
	{
		public static ManualResetEvent allDone = new ManualResetEvent(false);
		const int BUFFER_SIZE = 1024;
		const int DefaultTimeout = 2 * 60 * 1000;

		private static void TimeoutCallback(object state, bool timedOut)
		{
			if (timedOut)
			{
				//HttpWebRequest request = state as HttpWebRequest;
				WebRequest request = state as WebRequest;
				if (request != null)
				{
					request.Abort();
				}
			}
		}

		private static HttpWebRequest GetWebResponse(WebServiceRequestModel model)
		{
			try
			{
				var url = MessageRequest.BuildUrl(model);
				var method = new HttpMethod(model.HttpVerb);
				var currentDateTime = DateTime.UtcNow;
				var apiKey = model.PublicKey;
				var httpVerb = method.Method.ToUpper();
				var varUri = new Uri(url);

				var signature = string.Empty;

				var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), model.AuthenticationSchemeType);

				switch (authSchemeType)
				{
					case AuthenticationSchemeTypes.None:
						break;

					case AuthenticationSchemeTypes.Shared:
						var hmac2 = Hashing.GenerateHMACSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, currentDateTime.ToUnixTime(), new Uri(url), httpVerb);
						signature = (model.AuthPlacement == "Header") ? Convert.ToBase64String(hmac2) : HttpUtility.UrlEncode(Convert.ToBase64String(hmac2), Encoding.UTF8);
						break;

					case AuthenticationSchemeTypes.MWS:
						var sha256 = Hashing.GenerateSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, currentDateTime.ToUnixTime());
						signature = string.Concat(sha256.Select(b => string.Format("{0:x2}", b)));
						break;
				}

				if (model.AuthPlacement == AuthenticationPlacements.QueryString.ToString())
				{
					varUri = new Uri(string.Format(url.Contains("?") ? "{0}&ApiKey={1}&Sig={2}&Scheme={3}" : "{0}?ApiKey={1}&Sig={2}&Scheme={3}", url, apiKey, signature, authSchemeType));

					if (model.IncludeDate)
					{
						varUri = new Uri(string.Format("{0}&xdate={1}", varUri, currentDateTime.ToUnixTime()));
					}
				}

				HttpWebRequest httprequest = WebRequest.Create(varUri) as HttpWebRequest;
				httprequest.Headers.Clear();


				if (string.IsNullOrWhiteSpace(model.Endpoint))
				{
					return null;
				}

				// All dates should be in UTC so we are creating the overridden date as UTC
				if (model.RequestDateOverride.HasValue)
				{
					currentDateTime = new DateTime(model.RequestDateOverride.Value.Year, model.RequestDateOverride.Value.Month, model.RequestDateOverride.Value.Day, model.RequestDateOverride.Value.Hour, model.RequestDateOverride.Value.Minute, model.RequestDateOverride.Value.Second, DateTimeKind.Utc);
				}

				httprequest.UserAgent = ".NET Framework Test Client";
				httprequest.Expect = null;
				httprequest.ContentType = model.AcceptFormat; //"text/xml; encoding='utf-8'";
				httprequest.MediaType = model.AcceptFormat;
				httprequest.Method = model.HttpVerb;
				httprequest.Date = currentDateTime;

				httprequest.Accept = "application/x-ms-application,image/jpeg,application/xaml+xml,image/gif,image/pjpeg,application/x-ms-xbap,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/msword,*/*";
				//httprequest.Headers["Accept-Language"] = "en-US";
				httprequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
				//httprequest.AllowReadStreamBuffering = true;
				//httprequest.AllowWriteStreamBuffering = true;
				//httprequest.UseDefaultCredentials = true;
				httprequest.KeepAlive = false;
				httprequest.AllowAutoRedirect = true;
				httprequest.Timeout = 600000;



				if (model.EnableCors)
				{
					httprequest.Headers.Add("Origin", model.CorsOrigin);

					if (!string.IsNullOrWhiteSpace(model.CorsRequestHeaders))
					{
						httprequest.Headers.Add("Access-Control-Request-Headers", model.CorsRequestHeaders);
					}

					if (!string.IsNullOrWhiteSpace(model.CorsRequestMethod))
					{
						httprequest.Headers.Add("Access-Control-Request-Method", model.CorsRequestMethod);
					}
				}

				if (!string.IsNullOrWhiteSpace(model.CorrelationID))
				{
					httprequest.Headers.Add("X-CorrelationID", model.CorrelationID);
				}

				if (model.AuthPlacement.ToUpper() == AuthenticationPlacements.Header.ToString().ToUpper())
				{

					httprequest.Headers.Add("Authorization", string.Format("{0} {1}", authSchemeType.ToString(), string.Format("{0}:{1}", apiKey, signature)));
					if (model.IncludeDate)
					{
						httprequest.Headers.Add("X-Date", currentDateTime.ToString("r"));
					}
				}


				//if (((method == HttpMethod.Post) || (method == HttpMethod.Put)) && !string.IsNullOrWhiteSpace(model.RequestData))
				//{
				//	//var data = new StringContent(model.RequestData);
				//	//var data = Encoding.ASCII.GetBytes(model.RequestData);

				//	using (var stream = httprequest.GetRequestStream())
				//	{
				//		stream.Write(data, 0, data.Length);
				//	}

				//}
				//var responseString = ResponseMessage.ValidateResult(httprequest, model);//new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
				return httprequest;

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private static WebRequest GetResponse(WebServiceRequestModel model)
		{
			try
			{
				var url = MessageRequest.BuildUrl(model);
				var method = new HttpMethod(model.HttpVerb);
				var currentDateTime = DateTime.UtcNow;
				var apiKey = model.PublicKey;
				var httpVerb = method.Method.ToUpper();
				var varUri = new Uri(url);

				var signature = string.Empty;

				var authSchemeType = (AuthenticationSchemeTypes)Enum.Parse(typeof(AuthenticationSchemeTypes), model.AuthenticationSchemeType);

				switch (authSchemeType)
				{
					case AuthenticationSchemeTypes.None:
						break;

					case AuthenticationSchemeTypes.Shared:
						var hmac2 = Hashing.GenerateHMACSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, currentDateTime.ToUnixTime(), new Uri(url), httpVerb);
						signature = (model.AuthPlacement == "Header") ? Convert.ToBase64String(hmac2) : HttpUtility.UrlEncode(Convert.ToBase64String(hmac2), Encoding.UTF8);
						break;

					case AuthenticationSchemeTypes.MWS:
						var sha256 = Hashing.GenerateSHA256Auth(model.PublicKey, model.PrivateKeyOrToken, currentDateTime.ToUnixTime());
						signature = string.Concat(sha256.Select(b => string.Format("{0:x2}", b)));
						break;
				}

				if (model.AuthPlacement == AuthenticationPlacements.QueryString.ToString())
				{
					varUri = new Uri(string.Format(url.Contains("?") ? "{0}&ApiKey={1}&Sig={2}&Scheme={3}" : "{0}?ApiKey={1}&Sig={2}&Scheme={3}", url, apiKey, signature, authSchemeType));

					if (model.IncludeDate)
					{
						varUri = new Uri(string.Format("{0}&xdate={1}", varUri, currentDateTime.ToUnixTime()));
					}
				}

				WebRequest httprequest = WebRequest.Create(varUri);
				httprequest.Headers.Clear();


				if (string.IsNullOrWhiteSpace(model.Endpoint))
				{
					return null;
				}

				// All dates should be in UTC so we are creating the overridden date as UTC
				if (model.RequestDateOverride.HasValue)
				{
					currentDateTime = new DateTime(model.RequestDateOverride.Value.Year, model.RequestDateOverride.Value.Month, model.RequestDateOverride.Value.Day, model.RequestDateOverride.Value.Hour, model.RequestDateOverride.Value.Minute, model.RequestDateOverride.Value.Second, DateTimeKind.Utc);
				}

				//httprequest.UserAgent = ".NET Framework Test Client";
				//httprequest.Expect = null;
				httprequest.ContentType = model.AcceptFormat; //"text/xml; encoding='utf-8'";
				//httprequest.MediaType = model.AcceptFormat;
				httprequest.Method = model.HttpVerb;
				//httprequest.Date = currentDateTime;

				//httprequest.Accept = "application/x-ms-application,image/jpeg,application/xaml+xml,image/gif,image/pjpeg,application/x-ms-xbap,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/msword,*/*";
				//httprequest.Headers["Accept-Language"] = "en-US";
				//httprequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
				//httprequest.AllowReadStreamBuffering = true;
				//httprequest.AllowWriteStreamBuffering = true;
				//httprequest.UseDefaultCredentials = true;
				//httprequest.KeepAlive = false;
				//httprequest.AllowAutoRedirect = true;
				httprequest.Timeout = 600000;



				if (model.EnableCors)
				{
					httprequest.Headers.Add("Origin", model.CorsOrigin);

					if (!string.IsNullOrWhiteSpace(model.CorsRequestHeaders))
					{
						httprequest.Headers.Add("Access-Control-Request-Headers", model.CorsRequestHeaders);
					}

					if (!string.IsNullOrWhiteSpace(model.CorsRequestMethod))
					{
						httprequest.Headers.Add("Access-Control-Request-Method", model.CorsRequestMethod);
					}
				}

				if (!string.IsNullOrWhiteSpace(model.CorrelationID))
				{
					httprequest.Headers.Add("X-CorrelationID", model.CorrelationID);
				}

				if (model.AuthPlacement.ToUpper() == AuthenticationPlacements.Header.ToString().ToUpper())
				{

					httprequest.Headers.Add("Authorization", string.Format("{0} {1}", authSchemeType.ToString(), string.Format("{0}:{1}", apiKey, signature)));
					if (model.IncludeDate)
					{
						httprequest.Headers.Add("X-Date", currentDateTime.ToString("r"));
					}
				}

				//if (((method == HttpMethod.Post) || (method == HttpMethod.Put)) && !string.IsNullOrWhiteSpace(model.RequestData))
				//{
				//	//var data = new StringContent(model.RequestData);
				//	//var data = Encoding.ASCII.GetBytes(model.RequestData);

				//	using (var stream = httprequest.GetRequestStream())
				//	{
				//		stream.Write(data, 0, data.Length);
				//	}

				//}
				//var responseString = ResponseMessage.ValidateResult(httprequest, model);//new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
				return httprequest;

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		private static void RespCallback(IAsyncResult ar)
		{
			// Get the RequestState object from the async result.  
			RequestState rs = (RequestState)ar.AsyncState;
			try
			{
			
			// Get the WebRequest from RequestState.  
			WebRequest req = rs.Request;

			// Call EndGetResponse, which produces the WebResponse object  
			//  that came from the request issued above.  
			WebResponse resp = req.EndGetResponse(ar);

			//  Start reading data from the response stream.  
			Stream ResponseStream = resp.GetResponseStream();

			// Store the response stream in RequestState to read   
			// the stream asynchronously.  
			rs.ResponseStream = ResponseStream;

			//  Pass rs.BufferRead to BeginRead. Read data into rs.BufferRead  
			IAsyncResult iarRead = ResponseStream.BeginRead(rs.BufferRead, 0,
			   BUFFER_SIZE, new AsyncCallback(ReadCallBack), rs);
			return;
			}
			catch (WebException e)
			{
				rs.Message =  e.Message;
				rs.Status =  e.Status.ToString();
			}
			allDone.Set();
		}

		public static string ProcessRequest(WebServiceRequestModel model)
		{
			RequestState rs =null;
			 try
			{

			
			// Create the state object.  
			rs = new RequestState(model);

			// Get the URI from the command line.  
			//Uri httpSite = new Uri(args[0]);

			// Create the request object.  
			WebRequest wreq = GetResponse(model);
			//NetworkCredential NC = new NetworkCredential("U396058","Ashwin4y","AD-ENT");

			//WebProxy WP = new WebProxy();
			//WP.UseDefaultCredentials = true; 
			//WP.Credentials = NC;  			
			//wreq.Proxy = WP;
			//// Put the request into the state object so it can be passed around.  
			rs.Request = wreq;

			// Issue the async request.  
			IAsyncResult r = (IAsyncResult)wreq.BeginGetResponse(
			   new AsyncCallback(RespCallback), rs);

			// this line implements the timeout, if there is a timeout, the callback fires and the request becomes aborted
			ThreadPool.RegisterWaitForSingleObject(r.AsyncWaitHandle, new WaitOrTimerCallback(TimeoutCallback), wreq, DefaultTimeout, true);

			// Wait until the ManualResetEvent is set so that the application   
			// does not exit until after the callback is called.  
			allDone.WaitOne();

			//rs.response.Close();

			
			}
			catch(WebException webex)
			{
				rs.Message = webex.Message;
				rs.Status = webex.Status.ToString();
			}
			catch(Exception ex)
			{
				rs.Message = ex.Message;
				rs.Status = ex.Source.ToString();
			}
			 return (rs.RequestData.ToString());
		}  

		private static void ReadCallBack(IAsyncResult asyncResult)
		{
			// Get the RequestState object from AsyncResult.  
			RequestState rs = (RequestState)asyncResult.AsyncState;
			try
			{
			// Retrieve the ResponseStream that was set in RespCallback.   
			Stream responseStream = rs.ResponseStream;

			// Read rs.BufferRead to verify that it contains data.   
			int read = responseStream.EndRead(asyncResult);
			if (read > 0)
			{
				// Prepare a Char array buffer for converting to Unicode.  
				Char[] charBuffer = new Char[BUFFER_SIZE];

				// Convert byte stream to Char array and then to String.  
				// len contains the number of characters converted to Unicode.  
				int len =
				   rs.StreamDecode.GetChars(rs.BufferRead, 0, read, charBuffer, 0);

				String str = new String(charBuffer, 0, len);

				// Append the recently read data to the RequestData stringbuilder  
				// object contained in RequestState.  
				rs.RequestData.Append(
				   Encoding.ASCII.GetString(rs.BufferRead, 0, read));

				// Continue reading data until   
				// responseStream.EndRead returns –1.  
				IAsyncResult ar = responseStream.BeginRead(
				   rs.BufferRead, 0, BUFFER_SIZE,
				   new AsyncCallback(ReadCallBack), rs);
			}
			else
			{
				if (rs.RequestData.Length > 0)
				{
					//  Display data to the console.  
					string strContent;
					strContent = rs.RequestData.ToString();
				}
				// Close down the response stream.  
				responseStream.Close();
				// Set the ManualResetEvent so the main thread can exit.  
				
			}
			return;
			}
			catch (WebException e)
			{
				rs.Message = e.Message;
				rs.Status = e.Status.ToString();
			}
			allDone.Set();
		}
	}  
}
