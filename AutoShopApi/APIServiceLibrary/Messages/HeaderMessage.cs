﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIServiceLibrary
{
	public class HeaderMessage
	{
		#region Public Properties

		/// <summary>Gets or sets the code.</summary>
		public string Code
		{
			get;
			set;
		}

		/// <summary>Gets or sets the long description.</summary>
		public string LongDescription
		{
			get;
			set;
		}

		/// <summary>Gets or sets the short description.</summary>
		public string ShortDescription
		{
			get;
			set;
		}

		/// <summary>Gets or sets the type.</summary>
		public string Type
		{
			get;
			set;
		}

		#endregion
	}
}
