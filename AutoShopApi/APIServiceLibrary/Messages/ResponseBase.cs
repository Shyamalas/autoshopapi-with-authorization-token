﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIServiceLibrary//.Messages
{
    /// <summary>The base response.</summary>
    /// <typeparam name="T">The object/data that should be returned.</typeparam>
    public class ResponseBase<T>
    {
        #region Public Properties

        /// <summary>Gets or sets the body.</summary>
        public T Body
        {
            get;
            set;
        }

        /// <summary>Gets or sets the header.</summary>
        public ResponseHeader Header
        {
            get;
            set;
        }

        #endregion
    }
}
