﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace APIServiceLibrary
{
	public enum AcceptFormats
	{
		[Description("")]
		None = 0,

		[Description("application/json")]
		Json = 1,

		[Description("text/xml")]
		Xml = 2,

		[Description("application/bson")]
		Bson = 3
	}

	public enum AuthenticationPlacements
	{
		[Description("")]
		None = 0,

		[Description("Header")]
		Header = 1,

		[Description("Query String")]
		QueryString = 2
	}

	public enum AuthenticationSchemeTypes
	{
		/// <summary>The none.</summary>
		None = 0,

		/// <summary>The SHA256.</summary>
		MWS = 1,

		/// <summary>The HMAC-SHA256.</summary>
		Shared = 2,
	}
}
