﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using System.IO;
using log4net.Layout;
using log4net.Core;

namespace APIServiceLibrary
{
	public interface ILogger
	{
		void Debug(object message);
		void Debug(object message, Exception t);
		void Debug(string message, string messageExtended, object omessage);
		void Debug(string message, string messageExtended, object omessage, Exception t);

		void Error(object message);
		void Error(object message, Exception t);
		void Error(string message, string messageExtended, object omessage);
		void Error(string message, string messageExtended, object omessage, Exception t);

		void Fatal(object message);
		void Fatal(object message, Exception t);
		void Fatal(string message, string messageExtended, object omessage);
		void Fatal(string message, string messageExtended, object omessage, Exception t);

		void Info(object message);
		void Info(object message, Exception t);
		void Info(string message, string messageExtended, object omessage);
		void Info(string message, string messageExtended, object omessage, Exception t);

		void Log(string message);
		//void Log(string message, string level);
		void Log(string message, Exception ex);
		void Log(Exception ex);

		void Log(string message, Exception ex, out Guid logID);
		void Log(Exception ex, out Guid logID);
	}

	public class RawExceptionLayout : IRawLayout
	{
		public virtual object Format(LoggingEvent loggingEvent)
		{
			if (loggingEvent.ExceptionObject != null)
				return loggingEvent.ExceptionObject.ToString();
			else
				return null;
		}
	}

	public class LoggingAdapters : ILogger
	{
		private readonly log4net.ILog _log;

		/// <summary>
		/// 
		/// </summary>
		public LoggingAdapters()
		{
			XmlConfigurator.Configure();		
			_log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
			log4net.Util.SystemInfo.NullText = null;
			log4net.Util.SystemInfo.NotAvailableText = null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		public void Log(string message)
		{
			_log.Info(message);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="ex"></param>
		/// <param name="logID"></param>
		public void Log(string message, Exception ex, out Guid logID)
		{
			logID = Guid.NewGuid();
			log4net.LogicalThreadContext.Properties["logID"] = logID;
			_log.Error(message, ex);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ex"></param>
		/// <param name="logID"></param>
		public void Log(Exception ex, out Guid logID)
		{
			Log(ex.Message, ex, out logID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="ex"></param>
		public void Log(string message, Exception ex)
		{
			_log.Error(message, ex);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ex"></param>
		public void Log(Exception ex)
		{
			Log(ex.Message, ex);
		} 

		#region ILogger Members

		/// <summary>
		///   Debug
		/// </summary>
		/// <param name="message"></param>
		public void Debug(object message)
		{
			_log.Debug(message);
		}

		/// <summary>
		///  Debug
		/// </summary>
		/// <param name="message"></param>
		/// <param name="t"></param>
		public void Debug(object message, Exception t)
		{
			_log.Debug(message,t);
		}

		/// <summary>
		///   Debug
		/// </summary>
		/// <param name="message=Format"></param>
		/// <param name="messageExtended=objArg0"></param>
		/// <param name="omessage=objArg1"></param>		
		public void Debug(string message, string messageExtended, object omessage)
		{
			_log.DebugFormat(message, messageExtended, omessage); 
		}

		/// <summary>
		///   Debug
		/// </summary>
		/// <param name="message=Format"></param>
		/// <param name="messageExtended=objArg0"></param>
		/// <param name="omessage=objArg1"></param>
		/// <param name="t=objArg2"></param>
		public void Debug(string message, string messageExtended, object omessage, Exception t)
		{
			_log.DebugFormat(message, messageExtended, omessage, t);   			
		}

		/// <summary>
		///   Error
		/// </summary>
		/// <param name="message"></param>
		public void Error(object message)
		{
			_log.Error(message);
		}

		/// <summary>
		///    Error
		/// </summary>
		/// <param name="message"></param>
		/// <param name="t"></param>
		public void Error(object message, Exception t)
		{
			_log.Error(message,t);
		}

		/// <summary>
		///    Error
		/// </summary>
		/// <param name="message=Format"></param>
		/// <param name="messageExtended=objArg0"></param>
		/// <param name="omessage=objArg1"></param>			
		public void Error(string message, string messageExtended, object omessage)
		{
			_log.ErrorFormat(message, messageExtended, omessage); 
		}

		/// <summary>
		///   Error
		/// </summary>
		/// <param name="message=Format"></param>
		/// <param name="messageExtended=objArg0"></param>
		/// <param name="omessage=objArg1"></param>
		/// <param name="t=objArg2"></param>
		public void Error(string message, string messageExtended, object omessage, Exception t)
		{
			_log.ErrorFormat(message, messageExtended, omessage, t); 			
		}

		/// <summary>
		///   Fatal
		/// </summary>
		/// <param name="message"></param>
		public void Fatal(object message)
		{
			_log.Fatal(message);
		}

		/// <summary>
		///   Fatal
		/// </summary>
		/// <param name="message"></param>
		/// <param name="t"></param>
		public void Fatal(object message, Exception t)
		{
			_log.Fatal(message,t);
		}

		/// <summary>
		///   Fatal
		/// </summary>
		/// <param name="message=Format"></param>
		/// <param name="messageExtended=objArg0"></param>
		/// <param name="omessage=objArg1"></param>			
		public void Fatal(string message, string messageExtended, object omessage)
		{
			_log.FatalFormat(message, messageExtended, omessage); 
		}

		/// <summary>
		///    Fatal
		/// </summary>
		/// <param name="message=Format"></param>
		/// <param name="messageExtended=objArg0"></param>
		/// <param name="omessage=objArg1"></param>
		/// <param name="t=objArg2"></param>
		public void Fatal(string message, string messageExtended, object omessage, Exception t)
		{
			_log.FatalFormat(message, messageExtended, omessage, t); 			
		}

		/// <summary>
		///  Info
		/// </summary>
		/// <param name="message"></param>
		public void Info(object message)
		{
			_log.Info(message);
		}

		/// <summary>
		///   Info
		/// </summary>
		/// <param name="message"></param>
		/// <param name="t"></param>
		public void Info(object message, Exception t)
		{
			_log.Info(message,t);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="message=Format"></param>
		/// <param name="messageExtended=objArg0"></param>
		/// <param name="omessage=objArg1"></param>
		public void Info(string message, string messageExtended, object omessage)
		{
			_log.InfoFormat(message, messageExtended, omessage);
		}


		/// <summary>
		///   Info
		/// </summary>
		/// <param name="message=Format"></param>
		/// <param name="messageExtended=objArg0"></param>
		/// <param name="omessage=objArg1"></param>
		/// <param name="t=objArg2"></param>
		public void Info(string message, string messageExtended, object omessage, Exception t)
		{
			_log.InfoFormat(message,messageExtended,omessage,t);  			
		}

		#endregion
	}
}
