﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Web;

namespace APIServiceLibrary
{
	/// <summary>This is the hashing utility that is used in authentication.</summary>
	public static class Hashing
	{
		#region Public Methods and Operators

		/// <summary>Generates the HMAC SHA 256.</summary>
		/// <param name="publicKey">The public key.</param>
		/// <param name="privateKey">The private key.</param>
		/// <param name="epoch">The epoch.</param>
		/// <param name="uri">The URI.</param>
		/// <param name="httpVerb">The http Verb.</param>
		/// <returns>The <see cref="byte[]"/>.</returns>
		public static byte[] GenerateHMACSHA256Auth(string publicKey, string privateKey, long epoch, Uri uri, string httpVerb)
		{
			var uriToHash = GetRelativePath(uri.ToString());

			var encodedUri = HttpUtility.UrlPathEncode(uriToHash);
			var verb = (!string.IsNullOrWhiteSpace(httpVerb)) ? httpVerb.ToUpper() : string.Empty;

			var toSign = string.Format("{0}\n{1}\n{2}\n{3}", publicKey ?? string.Empty, verb ?? string.Empty, epoch, encodedUri);

			// get the byte[] representation of the private key
			var encoding = new ASCIIEncoding();
			var key = encoding.GetBytes(privateKey);
			var bytesToSign = encoding.GetBytes(toSign);

			byte[] data = null;
			using (var hmac = new HMACSHA256(key))
			{
				data = hmac.ComputeHash(bytesToSign);
			}

			return data;
		}

		/// <summary>The generate sh a 256 auth.</summary>
		/// <param name="publickKey">The public key.</param>
		/// <param name="privateKey">The private key.</param>
		/// <param name="epoch">The epoch.</param>
		/// <returns>The <see cref="string"/>.</returns>
		public static byte[] GenerateSHA256Auth(string publickKey, string privateKey, long epoch)
		{
			var toSign = string.Format("{0}{1}{2}", publickKey, privateKey, epoch);

			var encoding = new ASCIIEncoding();
			var bytesToSign = encoding.GetBytes(toSign);

			byte[] data = null;
			using (var provider = new SHA256CryptoServiceProvider())
			{
				return provider.ComputeHash(bytesToSign);
			}
		}

		#endregion

		#region Methods

		/// <summary>Gets the relative path.</summary>
		/// <param name="uri">The URI.</param>
		/// <returns>The <see cref="string"/>.</returns>
		private static string GetRelativePath(string uri)
		{
			// GET RID OF QUERY STRING
			var returnUri = uri.Contains("?") ? uri.Substring(0, uri.IndexOf("?")) : uri;

			// GET RID OF PROTOCOL/HOST/PORT
			var firstSlash = returnUri.Contains("://") ? returnUri.IndexOf('/', returnUri.IndexOf("://") + 3) : returnUri.IndexOf('/');

			returnUri = returnUri.Substring(firstSlash, returnUri.Length - firstSlash);
			return returnUri;
		}

		#endregion
	}
}
