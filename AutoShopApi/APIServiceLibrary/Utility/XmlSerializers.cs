﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.ObjectModel;
using System.Dynamic;  
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Data;

namespace APIServiceLibrary
{
	
	public class XmlSerializers
	{
		public static string SerializeToString(Type type, object objectValue)
		{
			if (objectValue != null)
			{
				XmlSerializer serializer;
				try
				{
					serializer = new XmlSerializer(type);
				}
				catch (Exception ex)
				{
					throw ex;
				}
				StringWriter sw = new StringWriter();
				serializer.Serialize(sw, objectValue);
				return sw.ToString();
			}
			else
				return null;
		}

		public static object DeserializeFromString(Type type, string serializedXml)
		{
			if (serializedXml != null)
			{
				XmlSerializer serializer = new XmlSerializer(type);
				return serializer.Deserialize(new StringReader(serializedXml));
			}
			else
				return null;
		}
	}

	public class XmlSerializerHelper<T>
	{
		public static string SerializeToString(object objectValue)
		{
			return XmlSerializers.SerializeToString(typeof(T), objectValue);
		}

		public static T DeserializeFromString(string serializedXml)
		{
			return (T)XmlSerializers.DeserializeFromString(typeof(T), serializedXml);
		}
	}

	public class JsonSerilizerHelper
	{
        public static string SerializeDataSetToJson(DataSet ds)
        {
            System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            Dictionary<string, object> dtname = new Dictionary<string, object>();
            Dictionary<string, object> dsname = new Dictionary<string, object>();
            foreach (DataTable table in ds.Tables)
            {
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                string tablename = table.TableName;
                foreach (DataRow row in table.Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in table.Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                dtname.Add(tablename, parentRow);
            }
            string datasetname = ds.DataSetName;
            dsname.Add(datasetname, dtname);
            return jsSerializer.Serialize(dsname);
        }

        public static string SerializeJsonXmlNode(string xmldocstring)
		{
			XmlDocument document = new XmlDocument();
			document.LoadXml(xmldocstring);
			return Newtonsoft.Json.JsonConvert.SerializeXmlNode(document);
		}

		public static string SerializeJsonToString(object objectValue)
		{
			return Newtonsoft.Json.JsonConvert.SerializeObject(objectValue);
		}

		public static string SerializeJsonToString(object objectValue,Type T, JsonSerializerSettings settings)
		{
			return Newtonsoft.Json.JsonConvert.SerializeObject(objectValue, (T), settings);
		}

		public static object DeserializeJsonFromString(string jsonObjectRepresentationsToDeserialize)
		{
			return Newtonsoft.Json.JsonConvert.DeserializeObject(jsonObjectRepresentationsToDeserialize);
		}

		public static object DeserializeJsonFromString(string jsonObjectRepresentationsToDeserialize, JsonSerializerSettings settings)
		{
			return Newtonsoft.Json.JsonConvert.DeserializeObject(jsonObjectRepresentationsToDeserialize, settings);
		}
	}

	public class JsonSerilizerHelper<T>
	{
		public static string SerializeJsonToString(object objectValue)
		{
			return Newtonsoft.Json.JsonConvert.SerializeObject(objectValue);
		}

		public static string SerializeJsonToString(object objectValue, JsonSerializerSettings settings)
		{
			return Newtonsoft.Json.JsonConvert.SerializeObject(objectValue, typeof(T),settings);
		}

		public static T DeserializeJsonFromString<T>(string jsonObjectRepresentationsToDeserialize)
		{
			return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonObjectRepresentationsToDeserialize);
		}

		public static T DeserializeJsonFromString<T>(string jsonObjectRepresentationsToDeserialize, JsonSerializerSettings settings)
		{
			return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonObjectRepresentationsToDeserialize, settings);
		}
	}

	public class JsonHelper
	{
		///// <summary>
		///// JSON Serialization
		///// </summary>
		//public static string JsonSerializer<T>(T t)
		//{
		//	DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
		//	MemoryStream ms = new MemoryStream();
		//	ser.WriteObject(ms, t);
		//	string jsonString = Encoding.UTF8.GetString(ms.ToArray());
		//	ms.Close();
		//	return jsonString;
		//}
		///// <summary>
		///// JSON Deserialization
		///// </summary>
		//public static T JsonDeserialize<T>(string jsonString)
		//{
		//	DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
		//	MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
		//	T obj = (T)ser.ReadObject(ms);
		//	return obj;
		//}

		/// <summary>
		/// JSON Serialization
		/// </summary>
		public static string JsonSerializer<T>(T t)
		{
			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
			MemoryStream ms = new MemoryStream();
			ser.WriteObject(ms, t);
			string jsonString = Encoding.UTF8.GetString(ms.ToArray());
			ms.Close();
			//Replace Json Date String                                         
			string p = @"\\/Date\((\d+)\+\d+\)\\/";
			MatchEvaluator matchEvaluator = new MatchEvaluator(ConvertJsonDateToDateString);
			Regex reg = new Regex(p);
			jsonString = reg.Replace(jsonString, matchEvaluator);
			return jsonString;
		}

		/// <summary>
		/// JSON Deserialization
		/// </summary>
		public static T JsonDeserialize<T>(string jsonString)
		{
			//Convert "yyyy-MM-dd HH:mm:ss" String as "\/Date(1319266795390+0800)\/"
			string p = @"\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}";
			MatchEvaluator matchEvaluator = new MatchEvaluator(
				ConvertDateStringToJsonDate);
			Regex reg = new Regex(p);
			jsonString = reg.Replace(jsonString, matchEvaluator);
			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
			MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
			T obj = (T)ser.ReadObject(ms);
			return obj;
		}

		/// <summary>
		/// Convert Serialization Time /Date(1319266795390+0800) as String
		/// </summary>
		private static string ConvertJsonDateToDateString(Match m)
		{
			string result = string.Empty;
			DateTime dt = new DateTime(1970, 1, 1);
			dt = dt.AddMilliseconds(long.Parse(m.Groups[1].Value));
			dt = dt.ToLocalTime();
			result = dt.ToString("yyyy-MM-dd HH:mm:ss");
			return result;
		}

		/// <summary>
		/// Convert Date String as Json Time
		/// </summary>
		private static string ConvertDateStringToJsonDate(Match m)
		{
			string result = string.Empty;
			DateTime dt = DateTime.Parse(m.Groups[0].Value);
			dt = dt.ToUniversalTime();
			TimeSpan ts = dt - DateTime.Parse("1970-01-01");
			result = string.Format("\\/Date({0}+0800)\\/", ts.TotalMilliseconds);
			return result;
		}

	}	 	

	public sealed class DynamicJsonConverter : JavaScriptConverter
	{
		public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
		{
			if (dictionary == null)
				throw new ArgumentNullException("dictionary");

			return type == typeof(object) ? new DynamicJsonObject(dictionary) : null;
		}

		public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override IEnumerable<Type> SupportedTypes
		{
			get { return new ReadOnlyCollection<Type>(new List<Type>(new[] { typeof(object) })); } 
		}  

		private sealed class DynamicJsonObject : DynamicObject
		{
			private readonly IDictionary<string, object> _dictionary;

			public DynamicJsonObject(IDictionary<string, object> dictionary)
			{
				if (dictionary == null)
					throw new ArgumentNullException("dictionary");
				_dictionary = dictionary;
			}

			public override string ToString()
			{
				var sb = new StringBuilder("{");
				ToString(sb);
				return sb.ToString();
			}

			private void ToString(StringBuilder sb)
			{
				var firstInDictionary = true;
				foreach (var pair in _dictionary)
				{
					if (!firstInDictionary)
						sb.Append(",");
					firstInDictionary = false;
					var value = pair.Value;
					var name = pair.Key;
					if (value is string)
					{
						sb.AppendFormat("{0}:\"{1}\"", name, value);
					}
					else if (value is IDictionary<string, object>)
					{
						new DynamicJsonObject((IDictionary<string, object>)value).ToString(sb);
					}
					else if (value is ArrayList)
					{
						sb.Append(name + ":[");
						var firstInArray = true;
						foreach (var arrayValue in (ArrayList)value)
						{
							if (!firstInArray)
								sb.Append(",");
							firstInArray = false;
							if (arrayValue is IDictionary<string, object>)
								new DynamicJsonObject((IDictionary<string, object>)arrayValue).ToString(sb);
							else if (arrayValue is string)
								sb.AppendFormat("\"{0}\"", arrayValue);
							else
								sb.AppendFormat("{0}", arrayValue);

						}
						sb.Append("]");
					}
					else
					{
						sb.AppendFormat("{0}:{1}", name, value);
					}
				}
				sb.Append("}");
			}

			public override bool TryGetMember(GetMemberBinder binder, out object result)
			{
				if (!_dictionary.TryGetValue(binder.Name, out result))
				{
					// return null to avoid exception.  caller can check for null this way...
					result = null;
					return true;
				}

				result = WrapResultObject(result);
				return true;
			}

			public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
			{
				if (indexes.Length == 1 && indexes[0] != null)
				{
					if (!_dictionary.TryGetValue(indexes[0].ToString(), out result))
					{
						// return null to avoid exception.  caller can check for null this way...
						result = null;
						return true;
					}

					result = WrapResultObject(result);
					return true;
				}

				return base.TryGetIndex(binder, indexes, out result);
			}

			private static object WrapResultObject(object result)
			{
				var dictionary = result as IDictionary<string, object>;
				if (dictionary != null)
					return new DynamicJsonObject(dictionary);

				var arrayList = result as ArrayList;
				if (arrayList != null && arrayList.Count > 0)
				{
					return arrayList[0] is IDictionary<string, object> 
						? new List<object>(arrayList.Cast<IDictionary<string, object>>().Select(x => new DynamicJsonObject(x))) 
						: new List<object>(arrayList.Cast<object>());
				}

				return result;
			}
		}	    

	}

	public static class DynamicJson
	{
		public static dynamic Parse(string json)
		{
			JavaScriptSerializer jss = new JavaScriptSerializer();
			jss.RegisterConverters(new JavaScriptConverter[] { new DynamicJsonConverter() });

			dynamic glossaryEntry = jss.Deserialize(json, typeof(object)) as dynamic;
			return glossaryEntry;
		}

		class DynamicJsonConverter : JavaScriptConverter
		{
			public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
			{
				if (dictionary == null)
					throw new ArgumentNullException("dictionary");

				var result = ToExpando(dictionary);

				return type == typeof(object) ? result : null;
			}

			private static ExpandoObject ToExpando(IDictionary<string, object> dictionary)
			{
				var result = new ExpandoObject();
				var dic = result as IDictionary<String, object>;

				foreach (var item in dictionary)
				{
					var valueAsDic = item.Value as IDictionary<string, object>;
					if (valueAsDic != null)
					{
						dic.Add(item.Key, ToExpando(valueAsDic));
						continue;
					}
					var arrayList = item.Value as ArrayList;
					if (arrayList != null && arrayList.Count > 0)
					{
						dic.Add(item.Key, ToExpando(arrayList));
						continue;
					}

					dic.Add(item.Key, item.Value);
				}
				return result;
			}

			private static ArrayList ToExpando(ArrayList obj)
			{
				ArrayList result = new ArrayList();

				foreach (var item in obj)
				{
					var valueAsDic = item as IDictionary<string, object>;
					if (valueAsDic != null)
					{
						result.Add(ToExpando(valueAsDic));
						continue;
					}

					var arrayList = item as ArrayList;
					if (arrayList != null && arrayList.Count > 0)
					{
						result.Add(ToExpando(arrayList));
						continue;
					}

					result.Add(item);
				}
				return result;
			}

			public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
			{
				throw new NotImplementedException();
			}

			public override IEnumerable<Type> SupportedTypes
			{
				get
				{
					return new ReadOnlyCollection<Type>(new List<Type>(new[] { typeof(object) }));
				}
			}
		}
	}  

}
