namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Login : IDisposable
    {
        private DataAccess _dataAccess;

        public Login()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_Logins_By_LoginTypeId(System.Nullable<int> LoginTypeId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Logins_By_LoginTypeId");
            this._dataAccess.AddParameter("LoginTypeId", LoginTypeId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Login_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Login_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Login_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Login_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Login_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string LoginName, System.Nullable<int> LoginTypeId, string ScreenName, string Password, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_Login_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("LoginName", LoginName, ParameterDirection.Input);
            this._dataAccess.AddParameter("LoginTypeId", LoginTypeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ScreenName", ScreenName, ParameterDirection.Input);
            this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Login_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Login_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Login_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string LoginName, System.Nullable<int> LoginTypeId, string ScreenName, string Password, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_Login_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("LoginName", LoginName, ParameterDirection.Input);
            this._dataAccess.AddParameter("LoginTypeId", LoginTypeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ScreenName", ScreenName, ParameterDirection.Input);
            this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }
        public virtual System.Data.DataSet Get_LoginDetail_ByContactId(System.Nullable<int> LoginTypeId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetUserInfoByID");
            this._dataAccess.AddParameter("LoginTypeId", LoginTypeId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }


        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}