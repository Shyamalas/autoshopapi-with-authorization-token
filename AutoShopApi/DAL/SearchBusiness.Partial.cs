namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class SearchBusiness : IDisposable
    {
        private DataAccess _dataAccess;

        public SearchBusiness()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet SearchBusinessByZipCodeAndMiles(int Zipcode, System.Nullable<int> Miles)
        {
            this._dataAccess.CreateProcedureCommand("spBusinessEntity_GetNearZipcode");
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet SearchBusinessByZipCodeMilesType(int Zipcode, System.Nullable<int> Miles, string AutoshopType, int ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetBusinessSearchResult");
            this._dataAccess.AddParameter("Zipcode", Zipcode.ToString(), ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoshopType", AutoshopType, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet UpdateSearchResult(int Zipcode, System.Nullable<int> Miles, string AutoshopType, string weekdayId, int contactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetBusinessSearchResult");
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoshopType", AutoshopType, ParameterDirection.Input);
            this._dataAccess.AddParameter("weekdayId", weekdayId, ParameterDirection.Input);
            this._dataAccess.AddParameter("contactId", contactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessRevisedSearch(int Zipcode, System.Nullable<int> Miles, string AutoType, string WeekdayID, int cntId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetBusinessRevisedSearchResult");
            this._dataAccess.AddParameter("Zipcode", Zipcode.ToString(), ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoshopType", AutoType, ParameterDirection.Input);
            this._dataAccess.AddParameter("weekdayId", WeekdayID, ParameterDirection.Input);
            this._dataAccess.AddParameter("contactId", cntId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet SearchBusinessByFavoriteItems(string items)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetBusinessEntitybyFavoriteItems");
            this._dataAccess.AddParameter("Favoriteitems", items, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet SearchBusinessByFilter(int Zipcode, System.Nullable<int> Miles, System.Nullable<int> NAICS_Code)
        {
            this._dataAccess.CreateProcedureCommand("sp_Holiday_Select_One");
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet SearchBusinessByAddress(string City, string State, System.Nullable<int> Miles, System.Nullable<int> NAICS_Code)
        {
            this._dataAccess.CreateProcedureCommand("sp_Holiday_Select_One");
            this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("NAICS_Code", NAICS_Code, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_FavoriteAutoshopList_By_ContactId(int ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_FavoriteAutoshopList_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet UpdateSearchResultFromCity(string CityName, string StateName, System.Nullable<int> Miles, string AutoshopType,string weekDayId,int cntId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetBusinessSearchResultFromCity");
            this._dataAccess.AddParameter("CityName", CityName, ParameterDirection.Input);
            this._dataAccess.AddParameter("StateName", StateName, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoshopType", AutoshopType, ParameterDirection.Input);
            this._dataAccess.AddParameter("weekdayId", weekDayId, ParameterDirection.Input);
            this._dataAccess.AddParameter("contactId", cntId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessSearchFromMypage(string Zipcode, string AutoshopType)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSearchMypage");
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoshopType", AutoshopType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessSearchFromMypage_Quote(string Zipcode, int Miles)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSearchMypage_Quote");
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }
        public virtual System.Data.DataSet BusinessSearchFromMypage_Quote_Filter(string Zipcode, int Miles,int QuoteId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSearchMypage_Quote_Filter");
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("QuotId", QuoteId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GetAutoshopDetails(int BusineesId, System.Nullable<int> ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetAutoshopDetails");
            this._dataAccess.AddParameter("BusinessId", BusineesId.ToString(), ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactId", ContactId.ToString(), ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}