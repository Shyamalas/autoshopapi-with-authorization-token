namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Quoteresponse : IDisposable
    {
        private DataAccess _dataAccess;

        public Quoteresponse()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_QuoteResponses_By_QuoteId(System.Nullable<int> QuoteId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_QuoteResponses_By_QuoteId");
            this._dataAccess.AddParameter("QuoteId", QuoteId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_QuoteResponses_By_QuoteId_srItems(System.Nullable<int> QuoteId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_QuoteResponses_By_QuoteId_srItems");
            this._dataAccess.AddParameter("QuoteId", QuoteId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet QuoteResponse_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_QuoteResponse_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet QuoteResponse_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_QuoteResponse_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void QuoteResponse_Insert(System.Nullable<int> BusinessId, System.Nullable<decimal> Price, string Comments, System.Nullable<int> QuoteId)
        {
            this._dataAccess.CreateProcedureCommand("sp_QuoteResponse_Insert");                        
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Price", Price, ParameterDirection.Input);
            this._dataAccess.AddParameter("Comments", Comments, ParameterDirection.Input);
            this._dataAccess.AddParameter("QuoteId", QuoteId, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }

        public virtual System.Nullable<int> QuoteResponse_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_QuoteResponse_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> QuoteResponse_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> QuoteId, System.Nullable<int> BusinessId, System.Nullable<decimal> Price, string Comments)
        {
            this._dataAccess.CreateProcedureCommand("sp_QuoteResponse_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("QuoteId", QuoteId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Price", Price, ParameterDirection.Input);
            this._dataAccess.AddParameter("Comments", Comments, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}