namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Businesssubscription : IDisposable
    {
        private DataAccess _dataAccess;

        public Businesssubscription()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_BusinessSubscriptions_By_BusinessId(System.Nullable<int> BusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessSubscriptions_By_BusinessId");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_BusinessSubscriptions_By_SubscriptionId(System.Nullable<int> SubscriptionId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessSubscriptions_By_SubscriptionId");
            this._dataAccess.AddParameter("SubscriptionId", SubscriptionId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessSubscription_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSubscription_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessSubscription_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSubscription_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> BusinessSubscription_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> SubscriptionId, System.Nullable<System.DateTime> StartDate, System.Nullable<System.DateTime> EndDate, System.Nullable<bool> IsActive, string Comments, System.Nullable<int> BusinessId, string Status)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSubscription_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("SubscriptionId", SubscriptionId, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartDate", StartDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("EndDate", EndDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Comments", Comments, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> BusinessSubscription_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSubscription_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> BusinessSubscription_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> SubscriptionId, System.Nullable<System.DateTime> StartDate, System.Nullable<System.DateTime> EndDate, System.Nullable<bool> IsActive, string Comments, System.Nullable<int> BusinessId, string Status)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSubscription_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("SubscriptionId", SubscriptionId, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartDate", StartDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("EndDate", EndDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Comments", Comments, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void InsertBusSubscription(string Createdby, string Modifiedby, int? Subscriptionid, System.DateTime? Startdate, System.DateTime? Enddate, int? BusnessId, string Status, string Paymode, string Name, string Billingstreetname, string Billingcity, string BillingState, int? Billingzipcode, string transactionId, string pendingreason, decimal? SubAmount, int? Discount, decimal? PaidAmount, bool? AutoRenewal)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSubscriptionDetail");
            this._dataAccess.AddParameter("CreatedBy", Createdby, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", Modifiedby, ParameterDirection.Input);
            this._dataAccess.AddParameter("SubscriptionId",Subscriptionid, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartDate", Startdate, ParameterDirection.Input);
            this._dataAccess.AddParameter("EndDate",Enddate , ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId",BusnessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status",Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("PayMode",Paymode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Name",Name, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingStreetName",Billingstreetname , ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingCity", Billingcity, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingState",BillingState, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingZipCode", Billingzipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("TransacttionId", transactionId, ParameterDirection.Input);
            this._dataAccess.AddParameter("PendingReason", pendingreason, ParameterDirection.Input);
            this._dataAccess.AddParameter("SubAmount", SubAmount, ParameterDirection.Input);
            this._dataAccess.AddParameter("Discount", Discount, ParameterDirection.Input);
            this._dataAccess.AddParameter("PaidAmount", PaidAmount, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoRenewal", AutoRenewal, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
            // return value;
        }

        public virtual System.Data.DataSet Get_BusinessSubscripton_By_ContactId(System.Nullable<int> contactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_BusinessSubscripton_By_ContactId");
            this._dataAccess.AddParameter("contactId", contactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }
         
        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }

        public virtual System.Nullable<int> BusinessSubscription_UpdateSubscriptionDetail(string Createdby, string Modifiedby, int? Subscriptionid, System.DateTime? Startdate, System.DateTime? Enddate, int? BusnessId, string Status, string Paymode, string Name, string Billingstreetname, string Billingcity, string BillingState, int? Billingzipcode, string transactionId, string pendingreason, int businessSubscriptionid, decimal? PaidAmount, bool? AutoRenewal)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessSubscription_Update");
            this._dataAccess.AddParameter("CreatedBy", Createdby, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", Modifiedby, ParameterDirection.Input);
            this._dataAccess.AddParameter("SubscriptionId", Subscriptionid, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartDate", Startdate, ParameterDirection.Input);
            this._dataAccess.AddParameter("EndDate", Enddate, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusnessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("PayMode", Paymode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Name", Name, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingStreetName", Billingstreetname, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingCity", Billingcity, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingState", BillingState, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingZipCode", Billingzipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("TransacttionId", transactionId, ParameterDirection.Input);
            this._dataAccess.AddParameter("PendingReason", pendingreason, ParameterDirection.Input);
            this._dataAccess.AddParameter("PaidAmount", PaidAmount, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessSubscriptionId", businessSubscriptionid, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoRenewal", AutoRenewal, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> updateIsActiveBusinessSubscription(int isActive, int busSubId)
        {
            this._dataAccess.CreateProcedureCommand("[sp_BusinessSubscription_IsActiveUpdate]");
            this._dataAccess.AddParameter("IsActive", isActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusSubId", busSubId, ParameterDirection.Input);
           
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual int CheckTrialPlan(int busID,out int returnCount)
        {
            this._dataAccess.CreateProcedureCommand("[sp_CheckTrialPlan]");
            this._dataAccess.AddParameter("busID", busID, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnCount", SqlDbType.Int, ParameterDirection.Output);
            returnCount = this._dataAccess.ExecuteNonQueryWithOutputParam("returnCount");
            return returnCount;
        }


    }
}