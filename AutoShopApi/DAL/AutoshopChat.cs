﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace AutoShop.DAL
{
    public partial class AutoshopChat
    {
       
        public virtual void Chat_UpdateStatus(string IsOnline,string username)
        {            
            //this._dataAccess.CreateProcedureCommand("sp_Chat_UpdateStatus");
            //this._dataAccess.AddParameter("IsOnline", IsOnline, ParameterDirection.Input);
            //this._dataAccess.AddParameter("username", username, ParameterDirection.Input);
            //int value = this._dataAccess.ExecuteNonQuery();
            //return value;
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ChatConnections"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_Chat_UpdateStatus");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@IsOnline", IsOnline);
                cmd.Parameters.AddWithValue("@username", username);
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
