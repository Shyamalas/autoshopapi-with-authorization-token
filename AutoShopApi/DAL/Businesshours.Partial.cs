namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Businesshours : IDisposable
    {
        private DataAccess _dataAccess;

        public Businesshours()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_BusinessHourss_By_BusinessId(System.Nullable<int> BusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessHourss_By_BusinessId");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_BusinessHourss_By_BusinessIdAndDay(System.Nullable<int> BusinessId, string Day)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessHourss_By_BusinessIdAndDayId");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Day", Day, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_BusinessHourss_By_BusinessIdAndDay_Today(System.Nullable<int> BusinessId, string Day)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessHourss_By_BusinessIdAndDayId_Today");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Day", Day, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_BusinessHourss_By_WeekDayId(System.Nullable<int> WeekDayId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessHourss_By_WeekDayId");
            this._dataAccess.AddParameter("WeekDayId", WeekDayId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessHours_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessHours_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessHours_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessHours_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> BusinessHours_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<System.DateTime> StartTime, System.Nullable<System.DateTime> EndTime, System.Nullable<bool> IsActive, System.Nullable<int> BusinessId, System.Nullable<int> WeekDayId, System.Nullable<bool> IsBusinessOpen)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessHours_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartTime", StartTime, ParameterDirection.Input);
            this._dataAccess.AddParameter("EndTime", EndTime, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("WeekDayId", WeekDayId, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsBusinessOpen", IsBusinessOpen, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> BusinessHours_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessHours_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> BusinessHours_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<System.DateTime> StartTime, System.Nullable<System.DateTime> EndTime, System.Nullable<bool> IsActive, System.Nullable<int> BusinessId, System.Nullable<int> WeekDayId, System.Nullable<bool> IsBusinessOpen)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessHours_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartTime", StartTime, ParameterDirection.Input);
            this._dataAccess.AddParameter("EndTime", EndTime, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("WeekDayId", WeekDayId, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsBusinessOpen", IsBusinessOpen, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet GetBusinessHours(int BusinessId,string Day)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_BusinessHourss_By_BusinessId");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Day", Day, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}