﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AutoShop.DAL
{
    public class Deal
    {
        private DataAccess _dataAccess;

        public Deal()
        {
            this._dataAccess = new DataAccess();
        }

        public Deal(string ConnectionString)
        {
            this._dataAccess = new DataAccess(ConnectionString);
        }

        public virtual System.Data.DataSet GetAutoshopDealList(string SearchText, string AutoshopType)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetAutoshopDealList");
            this._dataAccess.AddParameter("SearchText", SearchText, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoshopType", AutoshopType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }
    }
}
