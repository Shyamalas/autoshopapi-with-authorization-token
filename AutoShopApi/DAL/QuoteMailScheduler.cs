﻿namespace AutoShop.DAL
{
    using Microsoft.VisualBasic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using DotNetNuke.Services.Scheduling;
    using System.IO;
    using System.Configuration;
    using System.Net.Mail;
    using System.Net;
    public class QuoteMailScheduler : SchedulerClient
    {
        private DataAccess _dataAccess = new DataAccess();

        public QuoteMailScheduler(DotNetNuke.Services.Scheduling.ScheduleHistoryItem objScheduleHistoryItem)
            : base()
        {
            this.ScheduleHistoryItem = objScheduleHistoryItem;
        }


        public override void DoWork()
        {
            try
            {
                // start the processing
                this.Progressing();
                checkQuoteMailStatus();
                this.ScheduleHistoryItem.Succeeded = true;
            }
            catch (Exception ex)
            {
                this.ScheduleHistoryItem.Succeeded = false;
                //this.Errored(ex);
            }
        }

        private void checkQuoteMailStatus()
        {
            int i=0;
            
            System.Data.DataSet ds = Get_Pending_QuoteNotification();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["BusEmail"].ToString()))
                {
                    bool status = B2BQuoteNotificationMail(ds,i);

                  if (status)
                  {
                      Update_QuoteEmail_Status(Convert.ToInt32(ds.Tables[0].Rows[i]["QutReqId"].ToString()));
                  }
                }
                i++;
            }
        }

        public virtual System.Data.DataSet Get_Pending_QuoteNotification()
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_Pending_QuoteNotification");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Update_QuoteEmail_Status(int QutReqId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Update_QuoteEmail_Status");
            this._dataAccess.AddParameter("QutReqId", Convert.ToInt32(QutReqId), ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        private bool B2BQuoteNotificationMail(DataSet ds,int r)
        {
            string fromAddress = ConfigurationSettings.AppSettings["FromEmail"];
            string toAddress = ds.Tables[0].Rows[r]["BusEmail"].ToString();
            string fromPassword = ConfigurationSettings.AppSettings["Password"];
            string subject = DotNetNuke.Services.Localization.Localization.GetString("B2CQuoteRequest.Text", System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_GlobalResources\\EmailSubject.resx");
            string body = "";
            MailMessage Msg = new MailMessage();
            MailAddress fromMail = new MailAddress(fromAddress);
            // Sender e-mail address.
            Msg.From = fromMail;
            // Recipient e-mail address.
            Msg.To.Add(new MailAddress(toAddress));
            // Subject of e-mail
            Msg.Subject = subject;
            Msg.Body = B2BQuoteNotificationMailTemplate(ds,r);
            Msg.IsBodyHtml = true;

            var smtp = new System.Net.Mail.SmtpClient();
            {

                smtp.Host = ConfigurationSettings.AppSettings["SMTP"];
                if (ConfigurationSettings.AppSettings["SMTPPort"] != "")
                    smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["SMTPPort"]);

                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                if (ConfigurationSettings.AppSettings["Password"] != "")
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);

                smtp.Timeout = 20000;
            }
            //Passing values to smtp object
            try
            {
                smtp.Send(Msg);
                return true;
            }
            catch (System.Net.Mail.SmtpException smtpExc)
            {
                return false;
            }            
        }

        private string B2BQuoteNotificationMailTemplate(DataSet ds,int r)
        {
            string qtDetailTbl = string.Empty;
            string[] ServiceItem;

            if (string.IsNullOrEmpty(ds.Tables[0].Rows[r]["ServiceItem"].ToString()))
            {
                qtDetailTbl = string.Empty;
            }
            else
            {
                if (ds.Tables[0].Rows[r]["ServiceItem"].ToString() != "No records")
                {
                    ServiceItem = ds.Tables[0].Rows[r]["ServiceItem"].ToString().Split(',');
                    qtDetailTbl += "<tr><td style='border:1px black solid'>Service Action</td><td style='border:1px black solid'>Service Item</td></tr>";

                    for (int i = 0; i < ServiceItem.Length; i++)
                    {
                        int temp = ServiceItem[i].LastIndexOf(" (");
                        int lastIndex = ServiceItem[i].Length;

                        qtDetailTbl += "<tr><td style='border:1px black solid'>" + ServiceItem[i].Substring(0, temp) + "</td><td style='border:1px black solid'>" + ServiceItem[i].Substring(temp + 2, lastIndex - temp - 3) + "</td></tr>";
                    }
                }
            }

            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_Quote_Request_Notification.html"));
            string readFile = reader.ReadToEnd();

            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$Autoshop$$", ds.Tables[0].Rows[r]["BusinessName"].ToString());

            myString = myString.Replace("$$AutoTitle$$", ds.Tables[0].Rows[r]["AutoTitle"].ToString());
            myString = myString.Replace("$$ServiceName$$", ds.Tables[0].Rows[r]["ServiceName"].ToString());

            myString = myString.Replace("$$TableContent$$", qtDetailTbl);

            myString = myString.Replace("$$QuoteID$$", ds.Tables[0].Rows[r]["QuoteId"].ToString());

            reader.Dispose();
            return myString;

        }
    }
}
