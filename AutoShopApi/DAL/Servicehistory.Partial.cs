namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Servicehistory : IDisposable
    {
        private DataAccess _dataAccess;

        public Servicehistory()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_ServiceHistorys_By_AutomobildeId(System.Nullable<int> AutomobildeId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_ServiceHistorys_By_AutomobildeId");
            this._dataAccess.AddParameter("AutomobildeId", AutomobildeId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_ServiceHistorys_By_ServiceId(System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_ServiceHistorys_By_ServiceId");
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet ServiceHistory_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceHistory_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet ServiceHistory_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceHistory_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void ServiceHistory_Insert(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Status, System.Nullable<int> Miles, System.Nullable<int> AutomobildeId, System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceHistory_Insert");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutomobildeId", AutomobildeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }

        public virtual System.Nullable<int> ServiceHistory_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceHistory_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> ServiceHistory_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Status, System.Nullable<int> Miles, System.Nullable<int> AutomobildeId, System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceHistory_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutomobildeId", AutomobildeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_ServiceHistory_By_AutomobileId(System.Nullable<int> AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_ServiceHistory_By_AutoId");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GetServiceHistory_Details(int AutoId, int ServiceId, int RepairTypeId, int RecallId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetServiceHistory_Details");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            this._dataAccess.AddParameter("RepairTypeId", RepairTypeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("RecallId", RecallId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}