﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AutoShop.DAL
{
    public partial class AutomobileDetailDAL
    {
        private DataAccess _dataAccess;

        public AutomobileDetailDAL()
        {
            this._dataAccess = new DataAccess();
        }

        public int AutomobileDetail_Insert_Using_StyleId(int contactId,int Mileage, string VINNumber, int year, int makeId, string makeName, int modelId, string modelName,
                                                                                  int SubModelId, string subModelName, int BaseVehicleId, int enginId, string engineName,
                                                                                  int cylinders, int DriveTypeId, string DriveType, int BodyStyleId, int DoorCount, string type, int AxleTypeId,
                                                                                  int CabTypeId, int TransmissionId, string ControlType, string ElectronicControl, string ManufacturerCode, int speed,
                                                                                  int VehicleID, bool IsActive, string ImageName, out int ReturnAutoId)
        {
            try
            {
                IsActive = true;
                this._dataAccess.CreateProcedureCommand("sp_AutomobileDetail_Insert_Using_StyleId");

                this._dataAccess.AddParameter("ContactId", contactId, ParameterDirection.Input);
                this._dataAccess.AddParameter("Mileage", Mileage, ParameterDirection.Input);
                this._dataAccess.AddParameter("VINNumber", VINNumber, ParameterDirection.Input);
                this._dataAccess.AddParameter("Year", year, ParameterDirection.Input);
                this._dataAccess.AddParameter("MakeId", makeId, ParameterDirection.Input);
                this._dataAccess.AddParameter("MakeName", makeName, ParameterDirection.Input);
                this._dataAccess.AddParameter("ModelId", modelId, ParameterDirection.Input);
                this._dataAccess.AddParameter("ModelName", modelName, ParameterDirection.Input);
                this._dataAccess.AddParameter("SubModelId", SubModelId, ParameterDirection.Input);
                this._dataAccess.AddParameter("SubModelName", subModelName, ParameterDirection.Input);
                this._dataAccess.AddParameter("BaseVehicleId", BaseVehicleId, ParameterDirection.Input);
                this._dataAccess.AddParameter("EngineId", enginId, ParameterDirection.Input);
                this._dataAccess.AddParameter("EngineName", engineName, ParameterDirection.Input);
                this._dataAccess.AddParameter("Cylinders", cylinders, ParameterDirection.Input);
                this._dataAccess.AddParameter("DriveTypeId", DriveTypeId, ParameterDirection.Input);
                this._dataAccess.AddParameter("DriveType", DriveType, ParameterDirection.Input);
                this._dataAccess.AddParameter("BodyStyleId", BodyStyleId, ParameterDirection.Input);
                this._dataAccess.AddParameter("DoorCount", DoorCount, ParameterDirection.Input);
                this._dataAccess.AddParameter("Type", type, ParameterDirection.Input);
                this._dataAccess.AddParameter("AxleTypeId", AxleTypeId, ParameterDirection.Input);
                this._dataAccess.AddParameter("CabTypeId", CabTypeId, ParameterDirection.Input);
                this._dataAccess.AddParameter("TransmissionId", TransmissionId, ParameterDirection.Input);
                this._dataAccess.AddParameter("ControlType", ControlType, ParameterDirection.Input);
                this._dataAccess.AddParameter("ElectronicControl", ElectronicControl, ParameterDirection.Input);
                this._dataAccess.AddParameter("ManufacturerCode", ManufacturerCode, ParameterDirection.Input);
                this._dataAccess.AddParameter("Speed", speed, ParameterDirection.Input);
                this._dataAccess.AddParameter("VehicleID", VehicleID, ParameterDirection.Input);
                this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
                this._dataAccess.AddParameter("ReturnAutoId", SqlDbType.Int, ParameterDirection.Output);
                ReturnAutoId = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnAutoId");

            }
            catch (Exception ex)
            {
                string error = ex.StackTrace.ToString();
                ReturnAutoId = 0;
            }
            return ReturnAutoId;
        }
    }
}
