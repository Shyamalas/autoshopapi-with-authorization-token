namespace AutoShop.DAL
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    public partial class Contact : IDisposable
    {
        private DataAccess _dataAccess;

        public Contact()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Contact_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Contact_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Contact_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Contact_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Contact_Insert(
                    string FirstName,
                    string LastName,
                    string EmailAddress,
                    string Type,
                    string CellPhone,
                    string Password,
                    string StreetAddress,
                    string City,
                    string State,
                    System.Nullable<int> ZipCode,
                    string ActivationCode,
                    string Photo,
                    out int ReturnContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Contact_Insert");
            this._dataAccess.AddParameter("FirstName", FirstName, ParameterDirection.Input);
            this._dataAccess.AddParameter("LastName", LastName, ParameterDirection.Input);
            this._dataAccess.AddParameter("EmailAddress", EmailAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("Type", Type, ParameterDirection.Input);
            this._dataAccess.AddParameter("CellPhone", CellPhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress", StreetAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("ZipCode", ZipCode, ParameterDirection.Input);
            this._dataAccess.AddParameter("ActivationCode", ActivationCode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Photo", Photo, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnContactId", SqlDbType.Int, ParameterDirection.Output);
            ReturnContactId = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnContactId");
            return ReturnContactId;
        }

        public virtual System.Nullable<int> Contact_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Contact_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Contact_Update(
                    System.Nullable<int> RowId,
                    string CreatedBy,
                    System.Nullable<System.DateTime> CreatedDate,
                    string ModifiedBy,
                    System.Nullable<System.DateTime> ModifiedDate,
                    string Type,
                    string FirstName,
                    string LastName,
                    string EmailAddress,
                    string HomePhone,
                    string CellPhone,
                    string WorkPhone,
                    System.Nullable<bool> IsActive,
                    string Password,
                    System.Nullable<int> LoginId,
                    string StreetAddress,
                    string City,
                    string State,
                    System.Nullable<int> ZipCode)
        {
            this._dataAccess.CreateProcedureCommand("sp_Contact_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Type", Type, ParameterDirection.Input);
            this._dataAccess.AddParameter("FirstName", FirstName, ParameterDirection.Input);
            this._dataAccess.AddParameter("LastName", LastName, ParameterDirection.Input);
            this._dataAccess.AddParameter("EmailAddress", EmailAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("HomePhone", HomePhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("CellPhone", CellPhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("WorkPhone", WorkPhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
            this._dataAccess.AddParameter("LoginId", LoginId, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress", StreetAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("ZipCode", ZipCode.ToString(), ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> GetContactIdByEmail(string Email, out int ReturnContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetContactId_By_Email");
            this._dataAccess.AddParameter("Email", Email, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnContactId", SqlDbType.Int, ParameterDirection.Output);
            ReturnContactId = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnContactId");
            return ReturnContactId;
        }

        public virtual System.Data.DataSet Select_All_State()
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_All_State");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_City_By_State(string stateCode)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_City_By_State");
            this._dataAccess.AddParameter("stateCode", stateCode, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual bool CheckEmailExists(string Email)
        {
            this._dataAccess.CreateProcedureCommand("sp_CheckEmailExists");
            this._dataAccess.AddParameter("Email", Email, ParameterDirection.Input);
            SqlDataReader reader = this._dataAccess.ExecuteDataReader();
            if (reader.HasRows == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool CheckLoginNameExists(string fn)
        {
            this._dataAccess.CreateProcedureCommand("sp_CheckLoginNameExists");
            this._dataAccess.AddParameter("fn", fn, ParameterDirection.Input);
            SqlDataReader reader = this._dataAccess.ExecuteDataReader();
            if (reader.HasRows == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool CheckWorkingDay(string day, int busId)
        {
            this._dataAccess.CreateProcedureCommand("sp_CheckWorkingDay");
            this._dataAccess.AddParameter("Day", day, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", busId, ParameterDirection.Input);
            SqlDataReader reader = this._dataAccess.ExecuteDataReader();
            if (reader.HasRows == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual System.Nullable<int> GetReturnBusinessIdByEmail(string Email, out int ReturnBusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetBusinessId_By_Email");
            this._dataAccess.AddParameter("Email", Email, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnBusinessId", SqlDbType.Int, ParameterDirection.Output);
            ReturnBusinessId = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnBusinessId");
            return ReturnBusinessId;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }

        public virtual System.Data.DataSet ForgotPassword(string Email)
        {
            this._dataAccess.CreateProcedureCommand("sp_ForgotPassword");
            this._dataAccess.AddParameter("Email", Email, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> ContactSaveByAdmin(int cntid, int busid, string fname, string lname, string email, string password, string celPhone, out int returnEmail)
        {
            this._dataAccess.CreateProcedureCommand("sp_Contact_ModifyByAdmin");
            this._dataAccess.AddParameter("@BusinessId", busid, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ContactId", cntid, ParameterDirection.Input);
            this._dataAccess.AddParameter("@FirstName", fname, ParameterDirection.Input);
            this._dataAccess.AddParameter("@LastName", lname, ParameterDirection.Input);
            this._dataAccess.AddParameter("@EmailAddress", email, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Password", password, ParameterDirection.Input);
            this._dataAccess.AddParameter("@CellPhone", celPhone, ParameterDirection.Input);

            this._dataAccess.AddParameter("@returnEmail", SqlDbType.Int, ParameterDirection.Output);
            returnEmail = this._dataAccess.ExecuteNonQueryWithOutputParam("@returnEmail");
            return returnEmail;
        }

        public virtual System.Data.DataSet AdminInfo(string email)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetAdminInfo_By_Email");
            this._dataAccess.AddParameter("Email", email, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;

        }

        public virtual System.Nullable<int> ActiveRegisterUser(string ActiveCode)
        {
            this._dataAccess.CreateProcedureCommand("sp_Active_RegisterUser");
            this._dataAccess.AddParameter("ActiveCode", ActiveCode, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Check_UserActive(string Email, out int ReturnValue)
        {
            this._dataAccess.CreateProcedureCommand("sp_CheckUserActive_By_Email");
            this._dataAccess.AddParameter("Email", Email, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.Output);
            ReturnValue = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnValue");
            return ReturnValue;
        }

        public virtual System.Data.DataSet LoginActivation(string ActiveCode)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetLoginIfo_By_ActiveCode");
            this._dataAccess.AddParameter("ActiveCode", ActiveCode, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;

        }

        public virtual System.Nullable<int> removeActivationcode(string ActiveCode)
        {
            this._dataAccess.CreateProcedureCommand("sp_remove_Activationcode");
            this._dataAccess.AddParameter("ActiveCode", ActiveCode, ParameterDirection.Input);
            //this._dataAccess.AddParameter("Email", SqlDbType.NVarChar, ParameterDirection.Output);
            //returnEmail = this._dataAccess.ExecuteNonQueryWithOutputParam("Email").ToString();
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual string GetEmailByActivationCode(string activationCode)
        {
            string email = string.Empty;
            this._dataAccess.CreateProcedureCommand("sp_GetEmailByActivationCode");
            this._dataAccess.AddParameter("ActiveCode", activationCode, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            //return value;
            email = value.Tables[0].Rows[0]["EmailAddress"].ToString();
            return email;
        }

        public virtual void SetContactOnline(string emailAddress)
        {
            string email = string.Empty;
            this._dataAccess.CreateProcedureCommand("sp_Contact_Update_Setonline");
            this._dataAccess.AddParameter("EmailAddress", emailAddress, ParameterDirection.Input);
            this._dataAccess.ExecuteDataSet();

        }

        public virtual System.Data.DataSet GetB2CProfileByID(int RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetB2CProfileByID");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GetB2BdetailByID(int? RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetB2BProfileByID");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> B2CProfile_Update(
                    System.Nullable<int> RowId,
                    string FirstName,
                    string LastName,
                    string CellPhone,
                    string StreetAddress,
                    string City,
                    string State,
                    int ZipCode, string Photo)
        {
            this._dataAccess.CreateProcedureCommand("sp_B2CProfile_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("FirstName", FirstName, ParameterDirection.Input);
            this._dataAccess.AddParameter("LastName", LastName, ParameterDirection.Input);
            this._dataAccess.AddParameter("CellPhone", CellPhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress", StreetAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("ZipCode", ZipCode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Photo", Photo, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual int ChangeB2CPassword(string EmailAddress, string CurrentPass, string NewPass, out int ReturnValue)
        {
            this._dataAccess.CreateProcedureCommand("sp_ChangeB2CPassword");
            this._dataAccess.AddParameter("EmailAddress", EmailAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("CurrentPass", CurrentPass, ParameterDirection.Input);
            this._dataAccess.AddParameter("NewPass", NewPass, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.Output);
            ReturnValue = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnValue");
            return ReturnValue;
        }

        public virtual bool LoginAuth(string EmailAddress, string Password)
        {
            this._dataAccess.CreateProcedureCommand("sp_LogoinAuthorization");
            this._dataAccess.AddParameter("EmailAddress", EmailAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
            SqlDataReader reader = this._dataAccess.ExecuteDataReader();
            if (reader.HasRows == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual int LoginUpdate(string EmailAddress, string ResetToken, out int ReturnValue)
        {
            this._dataAccess.CreateProcedureCommand("sp_LogoinUpdate_ResetToken");
            this._dataAccess.AddParameter("EmailAddress", EmailAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("ResetToken", ResetToken, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.Output);
            ReturnValue = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnValue");
            return ReturnValue;
        }

        public virtual int UpdatePassword(string EmailAddress, string ResetToken, string NewPassword, out int ReturnValue)
        {
            this._dataAccess.CreateProcedureCommand("sp_UpdatePassword");
            this._dataAccess.AddParameter("EmailAddress", EmailAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("ResetToken", ResetToken, ParameterDirection.Input);
            this._dataAccess.AddParameter("NewPassword", NewPassword, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.Output);
            ReturnValue = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnValue");
            return ReturnValue;
        }
    }
}