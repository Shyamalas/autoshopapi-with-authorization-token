namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Subscription : IDisposable
    {
        private DataAccess _dataAccess;

        public Subscription()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Subscription_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Subscription_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Subscription_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Subscription_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Subscription_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Type, System.Nullable<long> Amount, System.Nullable<int> ValidationDays, System.Nullable<bool> IsActive, System.Nullable<int> Frequency)
        {
            this._dataAccess.CreateProcedureCommand("sp_Subscription_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Type", Type, ParameterDirection.Input);
            this._dataAccess.AddParameter("Amount", Amount, ParameterDirection.Input);
            this._dataAccess.AddParameter("ValidationDays", ValidationDays, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Frequency", Frequency, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Subscription_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Subscription_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Subscription_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Type, System.Nullable<long> Amount, System.Nullable<int> ValidationDays, System.Nullable<bool> IsActive, System.Nullable<int> Frequency)
        {
            this._dataAccess.CreateProcedureCommand("sp_Subscription_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Type", Type, ParameterDirection.Input);
            this._dataAccess.AddParameter("Amount", Amount, ParameterDirection.Input);
            this._dataAccess.AddParameter("ValidationDays", ValidationDays, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Frequency", Frequency, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_SubscriptionType()
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Subscription_Type");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_SubscriptionDuration(string subType)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_SubscriptionDuration");
            this._dataAccess.AddParameter("SubscriptionType", subType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_SubscriptionAmount(string subDuration)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_SubscriptionAmount");
            this._dataAccess.AddParameter("SubscriptionDuration", subDuration, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }

    }
}