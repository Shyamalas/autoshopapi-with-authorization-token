namespace AutoShop.DAL
{
    using System;
    using System.Data;
    using System.Xml;
    
    
    public partial class Recall : IDisposable {
        
        private DataAccess _dataAccess;
        
        public Recall() {
            this._dataAccess = new DataAccess();
        }
        
        public virtual System.Data.DataSet Recall_Select_All() {
            this._dataAccess.CreateProcedureCommand("sp_Recall_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }
        
        public virtual System.Data.DataSet Recall_Select_One(System.Nullable<int> RowId) {
            this._dataAccess.CreateProcedureCommand("sp_Recall_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }
        
        public virtual System.Nullable<int> Recall_Insert(System.Nullable<int> AutomobileId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string RecallNumber, string ManufacturerRecallNumber, System.Nullable<System.DateTime> NotificationDate, System.Nullable<System.DateTime> ManufacturedFromDate, System.Nullable<System.DateTime> ManufacturedToDate, string ComponentDescription, string DefectDescription, string Consequence, string CorrectiveAction) {
            this._dataAccess.CreateProcedureCommand("sp_Recall_Insert");
            this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("RecallNumber", RecallNumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("ManufacturerRecallNumber", ManufacturerRecallNumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("NotificationDate", NotificationDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ManufacturedFromDate", ManufacturedFromDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ManufacturedToDate", ManufacturedToDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ComponentDescription", ComponentDescription, ParameterDirection.Input);
            this._dataAccess.AddParameter("DefectDescription", DefectDescription, ParameterDirection.Input);
            this._dataAccess.AddParameter("Consequence", Consequence, ParameterDirection.Input);
            this._dataAccess.AddParameter("CorrectiveAction", CorrectiveAction, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }
        
        public virtual System.Nullable<int> Recall_Delete(System.Nullable<int> RowId) {
            this._dataAccess.CreateProcedureCommand("sp_Recall_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }
        
        public virtual System.Nullable<int> Recall_Update(System.Nullable<int> RowId, System.Nullable<int> AutomobileId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string RecallNumber, string ManufacturerRecallNumber, System.Nullable<System.DateTime> NotificationDate, System.Nullable<System.DateTime> ManufacturedFromDate, System.Nullable<System.DateTime> ManufacturedToDate, string ComponentDescription, string DefectDescription, string Consequence, string CorrectiveAction) {
            this._dataAccess.CreateProcedureCommand("sp_Recall_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("RecallNumber", RecallNumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("ManufacturerRecallNumber", ManufacturerRecallNumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("NotificationDate", NotificationDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ManufacturedFromDate", ManufacturedFromDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ManufacturedToDate", ManufacturedToDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ComponentDescription", ComponentDescription, ParameterDirection.Input);
            this._dataAccess.AddParameter("DefectDescription", DefectDescription, ParameterDirection.Input);
            this._dataAccess.AddParameter("Consequence", Consequence, ParameterDirection.Input);
            this._dataAccess.AddParameter("CorrectiveAction", CorrectiveAction, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }
        
        public virtual void Dispose() {
            if ((this._dataAccess != null)) {
                this._dataAccess.Dispose();
            }
        }
    }
}
