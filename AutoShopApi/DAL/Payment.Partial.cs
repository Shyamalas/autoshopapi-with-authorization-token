namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Payment : IDisposable
    {
        private DataAccess _dataAccess;

        public Payment()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_Payments_By_BusinessSubscriptionId(System.Nullable<int> BusinessSubscriptionId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Payments_By_BusinessSubscriptionId");
            this._dataAccess.AddParameter("BusinessSubscriptionId", BusinessSubscriptionId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Payments_By_PaymentInfoId(System.Nullable<int> PaymentInfoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Payments_By_PaymentInfoId");
            this._dataAccess.AddParameter("PaymentInfoId", PaymentInfoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Payment_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Payment_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Payment_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Payment_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Payment_Insert(
                    string CreatedBy,
                    System.Nullable<System.DateTime> CreatedDate,
                    string ModifiedBy,
                    System.Nullable<System.DateTime> ModifiedDate,
                    System.Nullable<int> ContactId,
                    System.Nullable<int> CardNumber,
                    string CardType,
                    System.Nullable<int> SecurityCode,
                    System.Nullable<System.DateTime> CardExpirationDate,
                    string Name,
                    string BillingStreetName,
                    string BillingCity,
                    System.Nullable<char> BillingState,
                    System.Nullable<int> BillingZipCode,
                    string TransactionId,
                    string TransactionXML,
                    string Status,
                    System.Nullable<int> BusinessSubscriptionId,
                    System.Nullable<int> PaymentInfoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Payment_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CardNumber", CardNumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("CardType", CardType, ParameterDirection.Input);
            this._dataAccess.AddParameter("SecurityCode", SecurityCode, ParameterDirection.Input);
            this._dataAccess.AddParameter("CardExpirationDate", CardExpirationDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Name", Name, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingStreetName", BillingStreetName, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingCity", BillingCity, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingState", BillingState, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingZipCode", BillingZipCode, ParameterDirection.Input);
            this._dataAccess.AddParameter("TransactionId", TransactionId, ParameterDirection.Input);
            this._dataAccess.AddParameter("TransactionXML", TransactionXML, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessSubscriptionId", BusinessSubscriptionId, ParameterDirection.Input);
            this._dataAccess.AddParameter("PaymentInfoId", PaymentInfoId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Payment_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Payment_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Payment_Update(
                    System.Nullable<int> RowId,
                    string CreatedBy,
                    System.Nullable<System.DateTime> CreatedDate,
                    string ModifiedBy,
                    System.Nullable<System.DateTime> ModifiedDate,
                    System.Nullable<int> ContactId,
                    System.Nullable<int> CardNumber,
                    string CardType,
                    System.Nullable<int> SecurityCode,
                    System.Nullable<System.DateTime> CardExpirationDate,
                    string Name,
                    string BillingStreetName,
                    string BillingCity,
                    System.Nullable<char> BillingState,
                    System.Nullable<int> BillingZipCode,
                    string TransactionId,
                    string TransactionXML,
                    string Status,
                    System.Nullable<int> BusinessSubscriptionId,
                    System.Nullable<int> PaymentInfoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Payment_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CardNumber", CardNumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("CardType", CardType, ParameterDirection.Input);
            this._dataAccess.AddParameter("SecurityCode", SecurityCode, ParameterDirection.Input);
            this._dataAccess.AddParameter("CardExpirationDate", CardExpirationDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Name", Name, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingStreetName", BillingStreetName, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingCity", BillingCity, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingState", BillingState, ParameterDirection.Input);
            this._dataAccess.AddParameter("BillingZipCode", BillingZipCode, ParameterDirection.Input);
            this._dataAccess.AddParameter("TransactionId", TransactionId, ParameterDirection.Input);
            this._dataAccess.AddParameter("TransactionXML", TransactionXML, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessSubscriptionId", BusinessSubscriptionId, ParameterDirection.Input);
            this._dataAccess.AddParameter("PaymentInfoId", PaymentInfoId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}