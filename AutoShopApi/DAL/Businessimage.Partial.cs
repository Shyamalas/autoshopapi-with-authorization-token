namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Businessimage : IDisposable
    {
        private DataAccess _dataAccess;

        public Businessimage()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_BusinessImages_By_BusinessId(System.Nullable<int> BusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessImages_By_BusinessId");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessImage_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessImage_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessImage_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessImage_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void BusinessImage_Insert(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<bool> IsActive, System.Nullable<int> BusinessId, string Name, string Image)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessImage_Insert");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Name", Name, ParameterDirection.Input);
            this._dataAccess.AddParameter("Image", Image, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }

        public virtual System.Nullable<int> BusinessImage_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessImage_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> BusinessImage_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<bool> IsActive, System.Nullable<int> BusinessId, string Name, string Image)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessImage_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Name", Name, ParameterDirection.Input);
            this._dataAccess.AddParameter("Image", Image, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}