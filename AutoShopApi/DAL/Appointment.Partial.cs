namespace AutoShop.DAL
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    public partial class Appointment : IDisposable
    {
        private DataAccess _dataAccess;

        public Appointment()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_Appointments_By_AutomobileId(System.Nullable<int> AutomobileId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Appointments_By_AutomobileId");
            this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Appointments_By_ContactId(System.Nullable<int> ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Appointments_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Appointments_By_BusinessId(System.Nullable<int> BusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Appointments_By_BusinessId");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Appointments_By_ServiceId(System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Appointments_By_ServiceId");
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Appointment_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Appointment_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Appointment_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Appointment_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int Appointment_Insert(System.Nullable<System.DateTime> AppoinmentDate, System.Nullable<System.DateTime> StartTime, System.Nullable<int> AutomobileId, System.Nullable<int> BusinessId, System.Nullable<int> ServiceId, System.Nullable<int> RecallId, System.Nullable<int> RepairTypeId, out int returnId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Appointment_Insert");            
            this._dataAccess.AddParameter("AppoinmentDate", AppoinmentDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartTime", StartTime, ParameterDirection.Input);            
            this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            this._dataAccess.AddParameter("RecallId", RecallId, ParameterDirection.Input);
            this._dataAccess.AddParameter("RepairTypeId", RepairTypeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnId", SqlDbType.Int, ParameterDirection.Output);
            returnId = this._dataAccess.ExecuteNonQueryWithOutputParam("returnId");
            return returnId;
        }

        public virtual System.Nullable<int> Appointment_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Appointment_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Appointment_Update(System.Nullable<int> RowId, System.Nullable<System.DateTime> AppoinmentDate, System.Nullable<System.DateTime> StartTime, System.Nullable<int> BusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Appointment_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("AppoinmentDate", AppoinmentDate, ParameterDirection.Input);
            //this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            //this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            //this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            //this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartTime", StartTime, ParameterDirection.Input);
            //this._dataAccess.AddParameter("EndTime", EndTime, ParameterDirection.Input);
            //this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            //this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            //this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            //this._dataAccess.AddParameter("AutoRecallId", AutoRecallId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_ServiceDueList_By_ContactId(System.Nullable<int> ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_ServiceDueList_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual bool CheckAppointmentExistByAutoId(int AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_CheckAppointmentExist_By_AutoId");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            SqlDataReader reader = this._dataAccess.ExecuteDataReader();
            if (reader.HasRows == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool CheckAppointmentExists(int AutoId, DateTime AppoinmentDate, int BusinessId, DateTime StartTime)
        {
            this._dataAccess.CreateProcedureCommand("sp_CheckAppointmentExist");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            this._dataAccess.AddParameter("AppoinmentDate", AppoinmentDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartTime", StartTime, ParameterDirection.Input);
            SqlDataReader reader = this._dataAccess.ExecuteDataReader();
            if (reader.HasRows == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual System.Data.DataSet Get_Appointments_By_BusId(System.Nullable<int> BusinessId, string TodayDate, string IsConfirmed)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_Appointments_By_BusId");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("TodayDate", TodayDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsConfirmed", IsConfirmed, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_Appointment_By_RowId(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("Get_Appointment_By_RowId");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Update_Appointment_By_RowId(System.Nullable<int> RowId, string IsConfirmed, string status)
        {
            this._dataAccess.CreateProcedureCommand("sp_Appointment_Status_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);            
            this._dataAccess.AddParameter("IsConfirmed", IsConfirmed, ParameterDirection.Input);
            this._dataAccess.AddParameter("status", status, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_Appointments_By_ContactId_Style(System.Nullable<int> ContactId)
        {
            //this._dataAccess.CreateProcedureCommand("sp_Select_Appointments_By_ContactId_Style");
            this._dataAccess.CreateProcedureCommand("sp_Get_Appointments_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_ServiceDueList_By_ContactId_Style(System.Nullable<int> ContactId)
        {
            //this._dataAccess.CreateProcedureCommand("sp_Select_ServiceDueList_By_ContactId_Style");
            this._dataAccess.CreateProcedureCommand("sp_Get_ServiceDue_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_Appointments_By_BusId_Style(System.Nullable<int> BusinessId, string TodayDate, string IsConfirmed)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_Appointments_By_BusId_Style");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("TodayDate", TodayDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsConfirmed", IsConfirmed, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_Appointment_By_RowId_Style(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("Get_Appointment_By_RowId_Style");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_AppointmentDetails_By_RowId(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_AppointmentDetails_By_RowId");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int Quote_Appointment_Insert(System.Nullable<System.DateTime> AppoinmentDate, System.Nullable<System.DateTime> StartTime, System.Nullable<int> AutomobileId, System.Nullable<int> BusinessId, System.Nullable<int> ServiceId, System.Nullable<int> RepairTypeId, System.Nullable<int> QuoteId, out int returnId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Quote_Appointment_Insert");
            this._dataAccess.AddParameter("AppoinmentDate", AppoinmentDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("StartTime", StartTime, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            this._dataAccess.AddParameter("RepairTypeId", RepairTypeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("QuoteId", QuoteId, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnId", SqlDbType.Int, ParameterDirection.Output);
            returnId = this._dataAccess.ExecuteNonQueryWithOutputParam("returnId");
            return returnId;
        }

        public virtual System.Data.DataSet GET_Appointment_By_Automobile(int ContactId, int StyleId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GET_Appointment_By_Automobile");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("StyleId", StyleId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_Appointments_By_AutoId(int AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_Appointments_By_AutoId");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }       

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}