namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Automobilemodel : IDisposable
    {
        private DataAccess _dataAccess;

        public Automobilemodel()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_AutomobileModels_By_ManufacturerId(System.Nullable<int> ManufacturerId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_AutomobileModels_By_ManufacturerId");
            this._dataAccess.AddParameter("ManufacturerId", ManufacturerId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet AutomobileModel_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileModel_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet AutomobileModel_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileModel_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> AutomobileModel_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Model, System.Nullable<int> ManufacturerId, string Description, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileModel_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Model", Model, ParameterDirection.Input);
            this._dataAccess.AddParameter("ManufacturerId", ManufacturerId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Description", Description, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> AutomobileModel_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileModel_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> AutomobileModel_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Model, System.Nullable<int> ManufacturerId, string Description, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileModel_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Model", Model, ParameterDirection.Input);
            this._dataAccess.AddParameter("ManufacturerId", ManufacturerId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Description", Description, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}