namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Address : IDisposable
    {
        private DataAccess _dataAccess;

        public Address()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Address_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Address_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Address_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Address_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Address_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string StreetAddress1, string StreetAddress2, string City, string State, System.Nullable<int> Zipcode, System.Nullable<int> ZipcodeSuffix, string Country)
        {
            this._dataAccess.CreateProcedureCommand("sp_Address_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress1", StreetAddress1, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress2", StreetAddress2, ParameterDirection.Input);
            this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("ZipcodeSuffix", ZipcodeSuffix, ParameterDirection.Input);
            this._dataAccess.AddParameter("Country", Country, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual int Address_Insert_RowId(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string StreetAddress1, string StreetAddress2, string City, string State, System.Nullable<int> Zipcode, System.Nullable<int> ZipcodeSuffix, string Country)
        {
            this._dataAccess.CreateProcedureCommand("sp_Address_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress1", StreetAddress1, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress2", StreetAddress2, ParameterDirection.Input);
            this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("ZipcodeSuffix", ZipcodeSuffix, ParameterDirection.Input);
            this._dataAccess.AddParameter("Country", Country, ParameterDirection.Input);
            int value = Convert.ToInt32(this._dataAccess.ExecuteScalar());
            return value;
        }


        public virtual System.Nullable<int> Address_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Address_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Address_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string StreetAddress1, string StreetAddress2, string City, string State, System.Nullable<int> Zipcode, System.Nullable<int> ZipcodeSuffix, string Country)
        {
            this._dataAccess.CreateProcedureCommand("sp_Address_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress1", StreetAddress1, ParameterDirection.Input);
            this._dataAccess.AddParameter("StreetAddress2", StreetAddress2, ParameterDirection.Input);
            this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("ZipcodeSuffix", ZipcodeSuffix, ParameterDirection.Input);
            this._dataAccess.AddParameter("Country", Country, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet AutoComplete_By_ZipCode(int zipcode)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutoComplete_By_ZipCode");
            this._dataAccess.AddParameter("zipcode", zipcode.ToString(), ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}