namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Automobileengine : IDisposable
    {
        private DataAccess _dataAccess;

        public Automobileengine()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_AutomobileEngines_By_ModelId(System.Nullable<int> ModelId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_AutomobileEngines_By_ModelId");
            this._dataAccess.AddParameter("ModelId", ModelId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet AutomobileEngine_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileEngine_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet AutomobileEngine_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileEngine_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> AutomobileEngine_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Engine, System.Nullable<int> ModelId, string Description, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileEngine_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Engine", Engine, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModelId", ModelId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Description", Description, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> AutomobileEngine_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileEngine_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> AutomobileEngine_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Engine, System.Nullable<int> ModelId, string Description, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileEngine_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Engine", Engine, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModelId", ModelId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Description", Description, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }

        public virtual System.Data.DataSet Select_AutomobileStyles_By_ModelId(System.Nullable<int> ModelId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_AutomobileStyles_By_ModelId");
            this._dataAccess.AddParameter("ModelId", ModelId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }
    }
}