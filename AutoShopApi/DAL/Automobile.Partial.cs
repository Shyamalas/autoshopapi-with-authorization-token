namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Automobile : IDisposable
    {
        private DataAccess _dataAccess;

        public Automobile()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_Automobiles_By_ContactIid(System.Nullable<int> ContactIid)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Automobiles_By_ContactIid");
            this._dataAccess.AddParameter("ContactIid", ContactIid, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Automobiles_List_By_ContactIid(System.Nullable<int> ContactIid)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Automobiles_List_By_ContactIid");
            this._dataAccess.AddParameter("ContactIid", ContactIid, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Automobile_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Automobile_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Automobile_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Automobile_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Automobile_Insert(System.Nullable<int> ContactIid, string EngineId, out int ReturnAutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Automobile_Insert");
            //this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            //this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            //this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            //this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactIid", ContactIid, ParameterDirection.Input);
            //this._dataAccess.AddParameter("Mileage", Mileage, ParameterDirection.Input);
            //this._dataAccess.AddParameter("VINNumber", VINNumber, ParameterDirection.Input);
            //this._dataAccess.AddParameter("Year", Year, ParameterDirection.Input);
            //this._dataAccess.AddParameter("Manufacturer", Manufacturer, ParameterDirection.Input);
            //this._dataAccess.AddParameter("Model", Model, ParameterDirection.Input);
            this._dataAccess.AddParameter("EngineId", EngineId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnAutoId", SqlDbType.Int, ParameterDirection.Output);
            ReturnAutoId = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnAutoId");
            return ReturnAutoId;
        }

        public virtual System.Nullable<int> Automobile_Delete(System.Nullable<int> RowId, out int returnID)
        {
            this._dataAccess.CreateProcedureCommand("sp_Automobile_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnID", SqlDbType.Int, ParameterDirection.Output);
            returnID = this._dataAccess.ExecuteNonQueryWithOutputParam("returnID");
            return returnID;
        }

        public virtual System.Nullable<int> Automobile_Update(System.Nullable<int> RowId, System.Nullable<int> Mileage)
        {
            this._dataAccess.CreateProcedureCommand("sp_Automobile_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Mileage", Mileage, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Automobile_Image_Update(System.Nullable<int> RowId, string ImageName)
        {
            this._dataAccess.CreateProcedureCommand("sp_Automobile_Image_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ImageName", ImageName, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }

        public virtual System.Nullable<int> Automobile_Insert_Using_StyleId(System.Nullable<int> ContactIid, string StyleId, out int ReturnAutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Automobile_Insert_Using_StyleId");
            this._dataAccess.AddParameter("ContactIid", ContactIid, ParameterDirection.Input);
            this._dataAccess.AddParameter("StyleId", StyleId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnAutoId", SqlDbType.Int, ParameterDirection.Output);
            ReturnAutoId = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnAutoId");
            return ReturnAutoId;
        }

        public virtual System.Data.DataSet Select_Automobiles_By_ContactIid_Style(System.Nullable<int> ContactIid)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Automobiles_By_ContactIid_Style");
            this._dataAccess.AddParameter("ContactIid", ContactIid, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Automobiles_List_By_ContactIid_Style(System.Nullable<int> ContactIid)
        {
            //this._dataAccess.CreateProcedureCommand("sp_Select_Automobiles_List_By_ContactIid_Style");
            this._dataAccess.CreateProcedureCommand("sp_Select_Automobiles_List_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactIid, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }


        
        public virtual int Add_NewAutoShop_To_Sel_Qt(int? quoteId, int? multiCount, string busMultiId,string multiMileage,int? zipcode, int expDay)
        {
            this._dataAccess.CreateProcedureCommand("sp_Add_NewAutoShop_To_Sel_Qt");
            this._dataAccess.AddParameter("QuoteId", quoteId, ParameterDirection.Input);
            this._dataAccess.AddParameter("MultiCount", multiCount, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusMultiId", busMultiId, ParameterDirection.Input);
            this._dataAccess.AddParameter("MultiMileage", multiMileage, ParameterDirection.Input);
            this._dataAccess.AddParameter("Zipcode", zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("ExpDay", expDay, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        //public virtual System.Data.DataSet Select_Filter_Automobiles_List_By_ContactIid_Style_QuoteId(System.Nullable<int> ContactIid, int quoteId)
        //{
        //    this._dataAccess.CreateProcedureCommand("sp_Select_Automobiles_List_By_ContactIid_Style");
        //    this._dataAccess.AddParameter("ContactIid", ContactIid, ParameterDirection.Input);
        //    DataSet value = this._dataAccess.ExecuteDataSet();
        //    return value;
        //}

        public virtual System.Data.DataSet GetAutoMobileImage(string year, string make, string model, int edmundsStyleId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Automobiles_Image");
            this._dataAccess.AddParameter("Year", year, ParameterDirection.Input);
            this._dataAccess.AddParameter("Make", make, ParameterDirection.Input);
            this._dataAccess.AddParameter("Model", model, ParameterDirection.Input);
            this._dataAccess.AddParameter("EdmundsStyleId", edmundsStyleId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int Automobile_Inactive(int RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Automobile_Inactive");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_Automobiles_List_By_ContactIid_Mobile(System.Nullable<int> ContactIid)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Automobiles_List_By_ContactIid_Mobile");
            this._dataAccess.AddParameter("ContactIid", ContactIid, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_Automobile_Datails_By_RowId(int RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_Automobile_Datails_By_RowId");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int Automobile_Insert_Image(int AutoId, string ImageName)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileImage_Insert");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ImageName", ImageName, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }
    }
}