namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Service : IDisposable
    {
        private DataAccess _dataAccess;

        public Service()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Service_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Service_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Service_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Service_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Service_Insert(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Name, System.Nullable<bool> IsActive, string Type)
        {
            this._dataAccess.CreateProcedureCommand("sp_Service_Insert");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Name", Name, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Type", Type, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }

        public virtual System.Nullable<int> Service_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Service_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Service_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Name, System.Nullable<bool> IsActive, string Type)
        {
            this._dataAccess.CreateProcedureCommand("sp_Service_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Name", Name, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Type", Type, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_Services_By_EngineId(string engineId, string ServiceType)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Services_By_EngineId");
            this._dataAccess.AddParameter("engineId", engineId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceType", ServiceType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Recommanded_Services_By_AutoId(int AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Recommanded_Services_By_AutoId");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Services_By_StyleId(string styleId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Services_By_StyleId");
            this._dataAccess.AddParameter("styleId", styleId, ParameterDirection.Input);
            //this._dataAccess.AddParameter("ServiceType", ServiceType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Recommanded_Services_By_AutoId_Style(int AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Recommanded_Services_By_AutoId_Style");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Recommanded_Services_By_StyleId(int styleID, int contactID)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Recommanded_Services_By_AutoId_Style");
            this._dataAccess.AddParameter("styleID", styleID, ParameterDirection.Input);
            this._dataAccess.AddParameter("contactID", contactID, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Repair_Types()
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_RepairTypes");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Services_By_RepairTypeId(int repairTypeId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Services_By_RepairTypeId");
            this._dataAccess.AddParameter("repairTypeId", repairTypeId, ParameterDirection.Input);
            //this._dataAccess.AddParameter("ServiceType", ServiceType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_ServiceLines_By_ServiceId(int serviceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_ServiceLines_By_ServiceId");
            this._dataAccess.AddParameter("serviceId", serviceId, ParameterDirection.Input);
            //this._dataAccess.AddParameter("ServiceType", ServiceType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }


        //public virtual System.Data.DataSet Select_ServiceLines_By_StyleId(int styleID, int mileage)
        //{
        //    this._dataAccess.CreateProcedureCommand("sp_Select_ServiceLines_By_ServiceId");
        //    this._dataAccess.AddParameter("styleID", styleID, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("mileage", mileage, ParameterDirection.Input);
        //    DataSet value = this._dataAccess.ExecuteDataSet();
        //    return value;
        //}

        public virtual System.Data.DataSet Select_Repair_Types_By_AutoshopType(string AutoshopType)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Repair_Types_By_AutoshopType");
            this._dataAccess.AddParameter("AutoshopType", AutoshopType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}