namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Automobilemanufacturer : IDisposable
    {
        private DataAccess _dataAccess;

        public Automobilemanufacturer()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_AutomobileManufacturers_By_YearId(System.Nullable<int> YearId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_AutomobileManufacturers_By_YearId");
            this._dataAccess.AddParameter("YearId", YearId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet AutomobileManufacturer_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileManufacturer_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet AutomobileManufacturer_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileManufacturer_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> AutomobileManufacturer_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Manufacturer, System.Nullable<int> YearId, string Description, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileManufacturer_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Manufacturer", Manufacturer, ParameterDirection.Input);
            this._dataAccess.AddParameter("YearId", YearId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Description", Description, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> AutomobileManufacturer_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileManufacturer_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> AutomobileManufacturer_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string Manufacturer, System.Nullable<int> YearId, string Description, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileManufacturer_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Manufacturer", Manufacturer, ParameterDirection.Input);
            this._dataAccess.AddParameter("YearId", YearId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Description", Description, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }


        public virtual System.Data.DataSet SearchAutoManufacturer()
        {
            this._dataAccess.CreateProcedureCommand("sp_AutomobileManufacturer_AutoSuggest");
            this._dataAccess.AddParameter("Manufacturer", "", ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }
    }
}