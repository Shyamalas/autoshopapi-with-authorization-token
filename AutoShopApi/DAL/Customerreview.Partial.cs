namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Customerreview : IDisposable
    {
        private DataAccess _dataAccess;

        public Customerreview()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_CustomerReviews_By_AppointmentId(System.Nullable<int> AppointmentId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_CustomerReviews_By_AppointmentId");
            this._dataAccess.AddParameter("AppointmentId", AppointmentId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet CustomerReview_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_CustomerReview_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet CustomerReview_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_CustomerReview_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> CustomerReview_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<char> Status, System.Nullable<int> QualityRating, System.Nullable<int> SatisfactionRating, System.Nullable<int> CustomerService, System.Nullable<int> AppointmentId)
        {
            this._dataAccess.CreateProcedureCommand("sp_CustomerReview_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("QualityRating", QualityRating, ParameterDirection.Input);
            this._dataAccess.AddParameter("SatisfactionRating", SatisfactionRating, ParameterDirection.Input);
            this._dataAccess.AddParameter("CustomerService", CustomerService, ParameterDirection.Input);
            this._dataAccess.AddParameter("AppointmentId", AppointmentId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> CustomerReview_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_CustomerReview_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> CustomerReview_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<char> Status, System.Nullable<int> QualityRating, System.Nullable<int> SatisfactionRating, System.Nullable<int> CustomerService, System.Nullable<int> AppointmentId)
        {
            this._dataAccess.CreateProcedureCommand("sp_CustomerReview_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("QualityRating", QualityRating, ParameterDirection.Input);
            this._dataAccess.AddParameter("SatisfactionRating", SatisfactionRating, ParameterDirection.Input);
            this._dataAccess.AddParameter("CustomerService", CustomerService, ParameterDirection.Input);
            this._dataAccess.AddParameter("AppointmentId", AppointmentId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}