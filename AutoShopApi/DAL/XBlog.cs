﻿
namespace AutoShop.DAL
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    
    public partial class XBlog : IDisposable
    {
        private DataAccess _dataAccess;

        public XBlog()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }

        public virtual System.Data.DataSet XBlog_Select_One()
        {            
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_Select_BlogArticles_By_CategoryId", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;                   

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }
            return ds;
        }

        public virtual System.Data.DataSet XBlog_Select_All_Home()
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("sp_Select_BlogNews_Home", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }
            return ds;
        }

    }
}