namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Weekdays : IDisposable
    {
        private DataAccess _dataAccess;

        public Weekdays()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet WeekDays_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_WeekDays_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet WeekDays_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_WeekDays_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> WeekDays_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string WeekDay, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_WeekDays_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("WeekDay", WeekDay, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> WeekDays_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_WeekDays_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> WeekDays_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, string WeekDay, System.Nullable<bool> IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_WeekDays_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("WeekDay", WeekDay, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}