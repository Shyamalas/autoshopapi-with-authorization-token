namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Businessrating : IDisposable
    {
        private DataAccess _dataAccess;

        public Businessrating()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_BusinessRatings_By_ContactId(System.Nullable<int> ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessRatings_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessRating_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessRating_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet BusinessRating_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessRating_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> BusinessRating_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> QualityRating, System.Nullable<int> CustomerServiceRating, string Notes, System.Nullable<bool> IsActive, System.Nullable<int> ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessRating_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("QualityRating", QualityRating, ParameterDirection.Input);
            this._dataAccess.AddParameter("CustomerServiceRating", CustomerServiceRating, ParameterDirection.Input);
            this._dataAccess.AddParameter("Notes", Notes, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> BusinessRating_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessRating_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> BusinessRating_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> QualityRating, System.Nullable<int> CustomerServiceRating, string Notes, System.Nullable<bool> IsActive, System.Nullable<int> ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessRating_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("QualityRating", QualityRating, ParameterDirection.Input);
            this._dataAccess.AddParameter("CustomerServiceRating", CustomerServiceRating, ParameterDirection.Input);
            this._dataAccess.AddParameter("Notes", Notes, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}