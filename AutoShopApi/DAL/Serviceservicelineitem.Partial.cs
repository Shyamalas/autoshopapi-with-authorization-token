namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Serviceservicelineitem : IDisposable
    {
        private DataAccess _dataAccess;

        public Serviceservicelineitem()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_ServiceServiceLineItems_By_ServiceId(System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_ServiceServiceLineItems_By_ServiceId");
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_ServiceServiceLineItems_By_ServiceLineItemId(System.Nullable<int> ServiceLineItemId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_ServiceServiceLineItems_By_ServiceLineItemId");
            this._dataAccess.AddParameter("ServiceLineItemId", ServiceLineItemId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet ServiceServiceLineItem_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceServiceLineItem_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet ServiceServiceLineItem_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceServiceLineItem_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> ServiceServiceLineItem_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> ServiceLineItemId, System.Nullable<bool> IsActive, System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceServiceLineItem_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceLineItemId", ServiceLineItemId, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> ServiceServiceLineItem_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceServiceLineItem_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> ServiceServiceLineItem_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> ServiceLineItemId, System.Nullable<bool> IsActive, System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_ServiceServiceLineItem_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceLineItemId", ServiceLineItemId, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
    }
}