namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Autorecall : IDisposable
    {
        private DataAccess _dataAccess;

        public Autorecall()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_AutoRecalls_By_AutomobileId(System.Nullable<int> AutomobileId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_AutoRecalls_By_AutomobileId");
            this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_AutoRecalls_By_RecallId(System.Nullable<int> RecallId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_AutoRecalls_By_RecallId");
            this._dataAccess.AddParameter("RecallId", RecallId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet AutoRecall_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_AutoRecall_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet AutoRecall_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutoRecall_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> AutoRecall_Insert(string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> RecallId, System.Nullable<bool> IsActive, System.Nullable<int> AutomobileId, string Status)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutoRecall_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("RecallId", RecallId, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> AutoRecall_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutoRecall_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> AutoRecall_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> RecallId, System.Nullable<bool> IsActive, System.Nullable<int> AutomobileId, string Status)
        {
            this._dataAccess.CreateProcedureCommand("sp_AutoRecall_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("RecallId", RecallId, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutomobileId", AutomobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_AutoRecalls_By_ContactId(System.Nullable<int> contactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_AutoRecalls_By_ContactId");
            this._dataAccess.AddParameter("contactId", contactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GET_Recall_Details(string year, string mak, string model, string style, string AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GET_Recall_Details");
            this._dataAccess.AddParameter("@Year", year, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Manufacturer", mak, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Model", model, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Style", style, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoId", AutoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GET_Recall_Details_Expand(int RecallId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GET_Recall_Details_Expand");
            this._dataAccess.AddParameter("@RecallId", RecallId, ParameterDirection.Input);            
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Bulletins_By_ContactId(System.Nullable<int> contactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Bulletins_By_ContactId");
            this._dataAccess.AddParameter("contactId", contactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GET_Bulletins_Details(string year, string mak, string model, string style)
        {
            this._dataAccess.CreateProcedureCommand("sp_GET_Bulletins_Details");
            this._dataAccess.AddParameter("@Year", year, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Manufacturer", mak, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Model", model, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Style", style, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GET_Bultn_Details_Expand(int BultId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GET_Bultn_Details_Expand");
            this._dataAccess.AddParameter("@BultId", BultId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_AutoRecalls_By_AutoId(System.Nullable<int> AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_AutoRecalls_By_AutoId");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Bulletins_By_AutoId(System.Nullable<int> AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Bulletins_By_AutoId");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }        
    }
}