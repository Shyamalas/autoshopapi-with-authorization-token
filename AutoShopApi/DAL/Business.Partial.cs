namespace AutoShop.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    public partial class Business : IDisposable
    {
        private DataAccess _dataAccess;

        public Business()
        {
            this._dataAccess = new DataAccess();
        }

        public Business(string ConnectionString)
        {
            this._dataAccess = new DataAccess(ConnectionString);
        }

        public virtual System.Data.DataSet Select_Businesss_By_RowId(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Businesss_By_RowId");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Business_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Business_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Business_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetAutoshopDetails");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Business_Insert(
                    string CreatedBy,
                    System.Nullable<System.DateTime> CreatedDate,
                    string ModifiedBy,
                    System.Nullable<System.DateTime> ModifiedDate,
                    string BusinessName,
                    string BusinessType,
                    System.Nullable<int> AddressId,
                    string BusinessPhone1,
                    string BusinessPhone2,
                    string CellPhone,
                    string FaxNumber,
                    System.Nullable<bool> MemberofAAA,
                    System.Nullable<bool> MemberofACE,
                    System.Nullable<bool> MemberofASE,
                    System.Nullable<bool> MemberofBBB,
                    string URL,
                    System.Nullable<bool> IsActive,
                    System.Nullable<float> Longitude,
                    System.Nullable<float> Latitude,
                    System.Nullable<int> NAICS_Code,
                    string NAICS_Description,
                    System.Nullable<int> SIC_Code,
                    string SIC_Description)
        {
            this._dataAccess.CreateProcedureCommand("sp_Business_Insert");
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessName", BusinessName, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessType", BusinessType, ParameterDirection.Input);
            this._dataAccess.AddParameter("AddressId", AddressId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessPhone1", BusinessPhone1, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessPhone2", BusinessPhone2, ParameterDirection.Input);
            this._dataAccess.AddParameter("CellPhone", CellPhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("FaxNumber", FaxNumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("MemberofAAA", MemberofAAA, ParameterDirection.Input);
            this._dataAccess.AddParameter("MemberofACE", MemberofACE, ParameterDirection.Input);
            this._dataAccess.AddParameter("MemberofASE", MemberofASE, ParameterDirection.Input);
            this._dataAccess.AddParameter("MemberofBBB", MemberofBBB, ParameterDirection.Input);
            this._dataAccess.AddParameter("URL", URL, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Longitude", Longitude, ParameterDirection.Input);
            this._dataAccess.AddParameter("Latitude", Latitude, ParameterDirection.Input);
            this._dataAccess.AddParameter("NAICS_Code", NAICS_Code, ParameterDirection.Input);
            this._dataAccess.AddParameter("NAICS_Description", NAICS_Description, ParameterDirection.Input);
            this._dataAccess.AddParameter("SIC_Code", SIC_Code, ParameterDirection.Input);
            this._dataAccess.AddParameter("SIC_Description", SIC_Description, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Business_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Business_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Business_Update(
                    System.Nullable<int> RowId,
                    string CreatedBy,
                    System.Nullable<System.DateTime> CreatedDate,
                    string ModifiedBy,
                    System.Nullable<System.DateTime> ModifiedDate,
                    string BusinessName,
                    string BusinessType,
                    System.Nullable<int> AddressId,
                    string BusinessPhone1,
                    string BusinessPhone2,
                    string CellPhone,
                    string FaxNumber,
                    System.Nullable<bool> MemberofAAA,
                    System.Nullable<bool> MemberofACE,
                    System.Nullable<bool> MemberofASE,
                    System.Nullable<bool> MemberofBBB,
                    string URL,
                    System.Nullable<bool> IsActive,
                    System.Nullable<float> Longitude,
                    System.Nullable<float> Latitude,
                    System.Nullable<int> NAICS_Code,
                    string NAICS_Description,
                    System.Nullable<int> SIC_Code,
                    string SIC_Description)
        {
            this._dataAccess.CreateProcedureCommand("sp_Business_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessName", BusinessName, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessType", BusinessType, ParameterDirection.Input);
            this._dataAccess.AddParameter("AddressId", AddressId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessPhone1", BusinessPhone1, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessPhone2", BusinessPhone2, ParameterDirection.Input);
            this._dataAccess.AddParameter("CellPhone", CellPhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("FaxNumber", FaxNumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("MemberofAAA", MemberofAAA, ParameterDirection.Input);
            this._dataAccess.AddParameter("MemberofACE", MemberofACE, ParameterDirection.Input);
            this._dataAccess.AddParameter("MemberofASE", MemberofASE, ParameterDirection.Input);
            this._dataAccess.AddParameter("MemberofBBB", MemberofBBB, ParameterDirection.Input);
            this._dataAccess.AddParameter("URL", URL, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("Longitude", Longitude, ParameterDirection.Input);
            this._dataAccess.AddParameter("Latitude", Latitude, ParameterDirection.Input);
            this._dataAccess.AddParameter("NAICS_Code", NAICS_Code, ParameterDirection.Input);
            this._dataAccess.AddParameter("NAICS_Description", NAICS_Description, ParameterDirection.Input);
            this._dataAccess.AddParameter("SIC_Code", SIC_Code, ParameterDirection.Input);
            this._dataAccess.AddParameter("SIC_Description", SIC_Description, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_Favorite_Bussiness_By_ContactId(System.Nullable<int> ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Favorite_Bussiness_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_Filter_FavBus_ByQuot(System.Nullable<int> ContactId, int quoteId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_Filter_FavBus_ByQuot");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("QuoteId", quoteId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }



        //public virtual System.Data.DataSet Get_FavoriteAutoshopList_By_ContactId(int ContactId)
        //{
        //    this._dataAccess.CreateProcedureCommand("sp_Get_FavoriteAutoshopList_By_ContactId");
        //    this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
        //    DataSet value = this._dataAccess.ExecuteDataSet();
        //    return value;
        //}

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }
        public void Business_UpdateWithAddress(string Streetaddress1, string City, string State, int? Zipcode, string Businessphone1, string Businessname, bool Repairshop, bool BodyShop, bool TireShop, bool CarWash, bool SmogCheck, string Faxnumber, string Url, string Firstname, string Lastname, string CellPhone, bool IsDealer, string AutoManufacturer, bool StarCertified, int ContactID)
        {
            this._dataAccess.CreateProcedureCommand("sp_UpdateB2BUserDetails");
            this._dataAccess.AddParameter("address", Streetaddress1, ParameterDirection.Input);
            this._dataAccess.AddParameter("city", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("state", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("zip", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("phoneNumber", Businessphone1, ParameterDirection.Input);
            this._dataAccess.AddParameter("autoShopName", Businessname, ParameterDirection.Input);
            this._dataAccess.AddParameter("repairShop", Repairshop, ParameterDirection.Input);
            this._dataAccess.AddParameter("bodyShop", BodyShop, ParameterDirection.Input);
            this._dataAccess.AddParameter("tireShop", TireShop, ParameterDirection.Input);
            this._dataAccess.AddParameter("carWash", CarWash, ParameterDirection.Input);
            this._dataAccess.AddParameter("smogCheck", SmogCheck, ParameterDirection.Input);
            //this._dataAccess.AddParameter("autoShopType", Businesstype, ParameterDirection.Input);
            this._dataAccess.AddParameter("faxNumber", Faxnumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("websiteAddress", Url, ParameterDirection.Input);
            //this._dataAccess.AddParameter("email", Emailaddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("firstName", Firstname, ParameterDirection.Input);
            this._dataAccess.AddParameter("lastName", Lastname, ParameterDirection.Input);
            this._dataAccess.AddParameter("contactNumber", CellPhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("isDealer", IsDealer, ParameterDirection.Input);
            this._dataAccess.AddParameter("manufacurer", AutoManufacturer, ParameterDirection.Input);
            this._dataAccess.AddParameter("contactId", ContactID, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }
        public virtual System.Nullable<int> Business_InsertwithAddress(string Streetaddress1, string City, string State, int? Zipcode, string Businessphone1, string Businessname, string Businesstype, string Faxnumber, string Url, string Emailaddress, string Password, string Firstname, string Lastname, string Businessphone2, string ContactPhoto, string Country, string Modifiedby, string Createdby, bool IsDealer, string AutoManufacturer, bool StarCertified, out int returnBusinessID)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessWithAddress_Insert");
            this._dataAccess.AddParameter("StreetAddress1", Streetaddress1, ParameterDirection.Input);
            this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
            this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessPhone1", Businessphone1, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessName", Businessname, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessType", Businesstype, ParameterDirection.Input);
            this._dataAccess.AddParameter("FaxNumber", Faxnumber, ParameterDirection.Input);
            this._dataAccess.AddParameter("URL", Url, ParameterDirection.Input);
            this._dataAccess.AddParameter("LoginEmail", Emailaddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactFirstName", Firstname, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactLastName", Lastname, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactPhone", Businessphone2, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactPhoto", ContactPhoto, ParameterDirection.Input);
            this._dataAccess.AddParameter("Country", Country, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", Modifiedby, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", Createdby, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsDealer", IsDealer, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoManufacturer", AutoManufacturer, ParameterDirection.Input);
            this._dataAccess.AddParameter("StarCertified", StarCertified, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnBusinessID", SqlDbType.Int, ParameterDirection.Output);
            returnBusinessID = this._dataAccess.ExecuteNonQueryWithOutputParam("returnBusinessID");
            return returnBusinessID;
        }

        public virtual System.Nullable<int> Insert_BusinessUser(int? BusinessID,string LoginEmail,string Password,string ContactFirstName,string ContactLastName,string ContactPhone,string ContactPhoto,string ModifiedBy,string CreatedBy, out int returnUserId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessUser_Insert");
            this._dataAccess.AddParameter("BusinessID", BusinessID, ParameterDirection.Input);
            this._dataAccess.AddParameter("LoginEmail", LoginEmail, ParameterDirection.Input);
            this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactFirstName", ContactFirstName, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactLastName", ContactLastName, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactPhone", ContactPhone, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactPhoto", ContactPhoto, ParameterDirection.Input);           
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnUserId", SqlDbType.Int, ParameterDirection.Output);
            returnUserId = this._dataAccess.ExecuteNonQueryWithOutputParam("returnUserId");
            return returnUserId;
        }

        public virtual void Insert_FavoriteDetail(int contactId, int businessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_FavouriteAutoshop_Insert");
            this._dataAccess.AddParameter("ContactId", contactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", businessId, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
            // return value;
        }

        //public virtual System.Data.DataSet Business_Select_Detail(int RowId, int currentUserID)
        //{
        //    this._dataAccess.CreateProcedureCommand("sp_GetAutoshopDetails");
        //    this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("UserId", currentUserID, ParameterDirection.Input);
        //    DataSet value = this._dataAccess.ExecuteDataSet();
        //    return value;
        //} 
        public virtual System.Data.DataSet Get_Businesss_FullDetails_By_BusId(System.Nullable<int> BusId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_Businesss_FullDetails_By_BusId");
            this._dataAccess.AddParameter("BusId", BusId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Delete_Favorite_Bussiness_By_ContactId(int BusId, int ContId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Fav_Business_Delete");
            this._dataAccess.AddParameter("BusId", BusId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContId", ContId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;


        }

        public virtual System.Data.DataSet Select_Dealer_List(string busManufacturer, string zipcode)
        {
            this._dataAccess.CreateProcedureCommand("Select_Dealer_List");
            this._dataAccess.AddParameter("busManufacturer", busManufacturer, ParameterDirection.Input);
            this._dataAccess.AddParameter("zipcode", zipcode, ParameterDirection.Input);

            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Business_Select_OnewithContactId(System.Nullable<int> RowId, int contactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetAutoshopDetails");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("UserId", contactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public List<string> AutosuggestShop(string AutoName)
        {
            List<string> result = new List<string>();
            this._dataAccess.CreateProcedureCommand("sp_GetAutosuggestBussiness");
            this._dataAccess.AddParameter("AutoName", AutoName, ParameterDirection.Input);
            SqlDataReader dr = this._dataAccess.ExecuteDataReader();
            while (dr.Read())
            {
                result.Add(string.Format("{0}/{1}", dr["BusinessName"], dr["RowId"]));
            }

            return result;
        }

        public virtual System.Data.DataSet Select_BusinessNote_By_BusId(System.Nullable<int> BusId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessNote_By_BusId");
            this._dataAccess.AddParameter("BusId", BusId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Insert_BusinessNote(string noteDate, string noteDescription, int BusId)
        {
            this._dataAccess.CreateProcedureCommand("sp_BusinessNote_Insert");
            this._dataAccess.AddParameter("noteDate", noteDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("noteDescription", noteDescription, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusId", BusId, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }

        public virtual System.Data.DataSet Search_BusinessNote(string frmDate, string toDate, int BusId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_BusinessNote_By_Date");
            this._dataAccess.AddParameter("frmDate", frmDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("toDate", toDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusId", BusId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet B2BListing(int PageIndex, int PageSize, string searchValue, string autoShopType,string searchType, out int TotalRecords)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetB2BListing");
            this._dataAccess.AddParameter("@PageIndex", PageIndex, ParameterDirection.Input);
            this._dataAccess.AddParameter("@PageSize", PageSize, ParameterDirection.Input);
            this._dataAccess.AddParameter("@SearchValue", searchValue, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoShopType", autoShopType, ParameterDirection.Input);
            this._dataAccess.AddParameter("@SearchType", searchType, ParameterDirection.Input);
            this._dataAccess.AddParameter("@TotalRecords", SqlDbType.Int, ParameterDirection.Output);
            DataSet value = this._dataAccess.ExecuteDataSet();
            //TotalRecords = this._dataAccess.ExecuteNonQueryWithOutputParam("@TotalRecords");
            if (value.Tables.Count > 0)
                TotalRecords = Convert.ToInt32(value.Tables[1].Rows[0]["TotalRecords"].ToString());
            else
                TotalRecords = 0;

            return value;
        }

        public virtual void UpdateIsActiveBusContact(bool chk, int cntId)
        {
            this._dataAccess.CreateProcedureCommand("sp_UpdateIsActiveByAdmin");
            this._dataAccess.AddParameter("@contactId", cntId, ParameterDirection.Input);
            this._dataAccess.AddParameter("@IsActive", chk, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }

        public virtual void UpdateIsPrimaryBusContact(int busId, int cntId)
        {
            this._dataAccess.CreateProcedureCommand("sp_UpdateIsPrimaryByAdmin");
            this._dataAccess.AddParameter("@BusinessId", busId, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ContactId", cntId, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }

        public virtual System.Data.DataSet B2BRegSearchList(string busName, int zipcode)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetB2BRegSearchList");
            this._dataAccess.AddParameter("busName", busName, ParameterDirection.Input);
            this._dataAccess.AddParameter("zipcode", zipcode.ToString(), ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void RegisterBusiness(string Emailaddress, string Password, string Firstname, string Lastname, int busID)
        {
            this._dataAccess.CreateProcedureCommand("sp_RegisterBusiness");
            this._dataAccess.AddParameter("LoginEmail", Emailaddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactFirstName", Firstname, ParameterDirection.Input);
            this._dataAccess.AddParameter("ContactLastName", Lastname, ParameterDirection.Input);
            this._dataAccess.AddParameter("busID", busID, ParameterDirection.Input);
            this._dataAccess.ExecuteNonQuery();
        }

        public virtual System.Data.DataSet GetB2BDetailsByBusinessId(int busId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetB2BDetailsByBusinessId");
            this._dataAccess.AddParameter("@BusinessId", busId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet B2CAppointment_Completion_Detail(System.Nullable<int> RowId)
        {

            this._dataAccess.CreateProcedureCommand("sp_B2CAppointment_Completion_Detail");
            this._dataAccess.AddParameter("@AppointmentID", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Business_ActiveInactive(
                    System.Nullable<int> RowId,
                    System.Nullable<bool> IsActive
                    )
        {
            this._dataAccess.CreateProcedureCommand("sp_Business_ActiveInactive");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("IsActive", IsActive, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet GetFavorite_Autoshop_By_Type(int ContactId, string AutoshopType)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetFavorite_Autoshop_By_Type");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutoshopType", AutoshopType, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int GetCurrentBusinessId(int contactId, out int ReturnBusId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetCurrentBusinessId");
            this._dataAccess.AddParameter("contactId", contactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ReturnBusId", SqlDbType.Int, ParameterDirection.Output);
            ReturnBusId = this._dataAccess.ExecuteNonQueryWithOutputParam("ReturnBusId");
            return ReturnBusId;
        }

        public virtual System.Data.DataSet Get_Business_Adress(int ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_Business_Adress");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        //public virtual System.Nullable<int> Insertwith_AddressBilling(string Streetaddress1, string City, string State, int? Zipcode, string Businessphone1, string Businessname, string Businesstype, string Faxnumber, string Url, string Emailaddress, string Password, string Firstname, string Lastname, string Businessphone2, string Country, string Modifiedby, string Createdby, bool IsDealer, string AutoManufacturer, bool StarCertified, string Billingaddress, string BillingCity, string BillingState, int? BillingZipcode, out int returnBusinessID)
        //{
        //    this._dataAccess.CreateProcedureCommand("sp_BusinessWithAddressBilling_Insert");
        //    this._dataAccess.AddParameter("StreetAddress1", Streetaddress1, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("City", City, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("State", State, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("BusinessPhone1", Businessphone1, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("BusinessName", Businessname, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("BusinessType", Businesstype, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("FaxNumber", Faxnumber, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("URL", Url, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("LoginEmail", Emailaddress, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("Password", Password, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("ContactFirstName", Firstname, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("ContactLastName", Lastname, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("ContactPhone", Businessphone2, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("Country", Country, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("ModifiedBy", Modifiedby, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("CreatedBy", Createdby, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("IsDealer", IsDealer, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("AutoManufacturer", AutoManufacturer, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("StarCertified", StarCertified, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("Billingaddress", Billingaddress, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("BillingCity", BillingCity, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("BillingState", BillingState, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("BillingZipcode", BillingZipcode, ParameterDirection.Input);
        //    this._dataAccess.AddParameter("returnBusinessID", SqlDbType.Int, ParameterDirection.Output);
        //    returnBusinessID = this._dataAccess.ExecuteNonQueryWithOutputParam("returnBusinessID");
        //    return returnBusinessID;
        //}
    }
}