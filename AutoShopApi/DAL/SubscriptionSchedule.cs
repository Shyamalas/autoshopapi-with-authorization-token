﻿namespace AutoShop.DAL
{
    using Microsoft.VisualBasic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using DotNetNuke.Services.Scheduling;
    using System.IO;
    using System.Configuration;
    using System.Net.Mail;
    using System.Net;
    public class SubscriptionSchedule : SchedulerClient
    {
        private DataAccess _dataAccess = new DataAccess();

        public SubscriptionSchedule(DotNetNuke.Services.Scheduling.ScheduleHistoryItem objScheduleHistoryItem)
            : base()
        {
            this.ScheduleHistoryItem = objScheduleHistoryItem;
        }


        public override void DoWork()
        {
            try
            {
                // start the processing
                this.Progressing();
                checkSubscriptionPlan();
                SendAppointmentAlert();                
                this.ScheduleHistoryItem.Succeeded = true;
            }
            catch (Exception ex)
            {
                this.ScheduleHistoryItem.Succeeded = false;
                //this.Errored(ex);
            }
        }

        private void checkSubscriptionPlan()
        {
            
            int i=0;
            var currentDate = DateTime.Now;
            var nwDate = currentDate.AddDays(5);

            var curDate = DateTime.Now.ToString("yyyy/MM/dd");
            var NewDate = nwDate.ToString("yyyy/MM/dd");
            
            DateTime CurrDate = Convert.ToDateTime(curDate);
            DateTime NewDate1 = Convert.ToDateTime(NewDate);

            System.Data.DataSet ds = Select_All_UserSubscription();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                DateTime expDate =Convert.ToDateTime(ds.Tables[0].Rows[i]["EndDate"]);
                string Email = ds.Tables[0].Rows[i]["Email"].ToString();
                string subPlan = ds.Tables[0].Rows[i]["SubPlan"].ToString();
                string usrName = ds.Tables[0].Rows[i]["FirstName"].ToString();
                string busName = ds.Tables[0].Rows[i]["BusinessName"].ToString();
                if (expDate.Date < NewDate1.Date && expDate.Date > CurrDate.Date)
                {
                    NotificationMailToUser(ds.Tables[0].Rows[i]["EndDate"].ToString(), Email, subPlan,usrName,busName);
                }
                else if (expDate.Date <= CurrDate.Date)
                {
                    Update_User_Subscription(ds.Tables[0].Rows[i]["RowId"].ToString());
                    PlanExpireMailToUser(ds.Tables[0].Rows[i]["EndDate"].ToString(), Email, usrName, busName);
                }
                i++;
            }
        }

        public virtual System.Data.DataSet Select_All_UserSubscription()
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_All_UserSubscription");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> Update_User_Subscription(string RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Update_User_Subscription");
            this._dataAccess.AddParameter("RowId", Convert.ToInt32(RowId), ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        private void NotificationMailToUser(string ExpDate, string Email, string subPlan, string firstName,string busName)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_Subscription_Expiration_Notice.html"));            
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$FirstName$$", firstName);
            myString = myString.Replace("$$AutoShopName$$", busName);
            myString = myString.Replace("$$expireddate$$", ExpDate);
            myString = myString.Replace("$$SubscriptionType$$", subPlan);
            string fromAddress = ConfigurationSettings.AppSettings["FromEmail"];
            //string toAddress = "piyushparmar195@gmail.com";
            string toAddress = Email;
            string fromPassword = ConfigurationSettings.AppSettings["Password"];
            string subject = DotNetNuke.Services.Localization.Localization.GetString("B2BSubscriptionExpiration.Text", System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_GlobalResources\\EmailSubject.resx");
            string body = myString.ToString();
            MailMessage Msg = new MailMessage();
            MailAddress fromMail = new MailAddress(fromAddress);
            // Sender e-mail address.
            Msg.From = fromMail;
            // Recipient e-mail address.
            Msg.To.Add(new MailAddress(toAddress));
            // Subject of e-mail
            Msg.Subject = subject;
            Msg.Body = myString.ToString();
            Msg.IsBodyHtml = true;
            reader.Dispose();
            var smtp = new System.Net.Mail.SmtpClient();
            {

                smtp.Host = ConfigurationSettings.AppSettings["SMTP"];
                if (ConfigurationSettings.AppSettings["SMTPPort"] != "")
                    smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["SMTPPort"]);

                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                if (ConfigurationSettings.AppSettings["Password"] != "")
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);

                smtp.Timeout = 20000;
            }
            //Passing values to smtp object
            try
            {
                smtp.Send(Msg);
            }
            catch (System.Net.Mail.SmtpException smtpExc)
            {
                if (smtpExc.StatusCode == SmtpStatusCode.GeneralFailure)
                {
                    //Response.Write("<script> alert('Sorry, e-mail sending fail...');window.location='Home.aspx'; </script>");
                }
                else
                {
                    throw smtpExc;
                }
            }
        }


        private void PlanExpireMailToUser(string ExpDate, string Email, string FirstName,string Autoshop)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/PlanExpireMailToUser.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$ExpDate$$", ExpDate);
            myString = myString.Replace("$$FirstName$$", FirstName);
            myString = myString.Replace("$$Autoshop$$", Autoshop);
            string fromAddress = ConfigurationSettings.AppSettings["FromEmail"];
            //string toAddress = "piyushparmar195@gmail.com";
            string toAddress = Email;
            string fromPassword = ConfigurationSettings.AppSettings["Password"];
            string subject = DotNetNuke.Services.Localization.Localization.GetString("PlanExpired.Text", System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_GlobalResources\\EmailSubject.resx");
            string body = myString.ToString();
            MailMessage Msg = new MailMessage();
            MailAddress fromMail = new MailAddress(fromAddress);
            // Sender e-mail address.
            Msg.From = fromMail;
            // Recipient e-mail address.
            Msg.To.Add(new MailAddress(toAddress));
            // Subject of e-mail
            Msg.Subject = subject;
            Msg.Body = myString.ToString();
            Msg.IsBodyHtml = true;
            reader.Dispose();
            var smtp = new System.Net.Mail.SmtpClient();
            {

                smtp.Host = ConfigurationSettings.AppSettings["SMTP"];
                if (ConfigurationSettings.AppSettings["SMTPPort"] != "")
                    smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["SMTPPort"]);

                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                if (ConfigurationSettings.AppSettings["Password"] != "")
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);

                smtp.Timeout = 20000;
            }
            //Passing values to smtp object
            try
            {
                smtp.Send(Msg);
            }
            catch (System.Net.Mail.SmtpException smtpExc)
            {
                if (smtpExc.StatusCode == SmtpStatusCode.GeneralFailure)
                {
                    //Response.Write("<script> alert('Sorry, e-mail sending fail...');window.location='Home.aspx'; </script>");
                }
                else
                {
                    throw smtpExc;
                }
            }
        }


        private void SendAppointmentAlert()
        {

            int i = 0;
            var currentDate = DateTime.Now;
            var nwDate = currentDate.AddDays(1);

            var curDate = DateTime.Now.ToString("yyyy/MM/dd");
            var NewDate = nwDate.ToString("yyyy/MM/dd");

            DateTime CurrDate = Convert.ToDateTime(curDate);
            DateTime NewDate1 = Convert.ToDateTime(NewDate);

            System.Data.DataSet ds = Select_Appointments_All();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                DateTime appDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["AppDate"]);

                if (appDate.Date == NewDate1.Date)
                {
                    B2C_Appointment_alert(ds.Tables[0].Rows[i]);
                }                
                i++;
            }
        }

        public virtual System.Data.DataSet Select_Appointments_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Appointments_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        private void B2C_Appointment_alert(DataRow dr)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Appointment_alert.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            string qtDetailTbl = string.Empty;
            string[] ServiceItem;

            if (string.IsNullOrEmpty(dr["ServiceItem"].ToString()))
            {
                qtDetailTbl = string.Empty;
            }
            else
            {
                if (dr["ServiceItem"].ToString() != "No records")
                {
                    ServiceItem = dr["ServiceItem"].ToString().Split(',');
                    qtDetailTbl += "<tr><td style='border:1px black solid'>Service Action</td><td style='border:1px black solid'>Service Item</td></tr>";

                    for (int i = 0; i < ServiceItem.Length; i++)
                    {
                        int temp = ServiceItem[i].LastIndexOf(" (");
                        int lastIndex = ServiceItem[i].Length;

                        qtDetailTbl += "<tr><td style='border:1px black solid'>" + ServiceItem[i].Substring(0, temp) + "</td><td style='border:1px black solid'>" + ServiceItem[i].Substring(temp + 2, lastIndex - temp - 3) + "</td></tr>";
                    }
                }
            }

            myString = readFile;
            myString = myString.Replace("$$FirstName$$", dr["FirstName"].ToString());
            myString = myString.Replace("$$AutoShopName$$", dr["BusinessName"].ToString());
            myString = myString.Replace("$$StreetName$$", dr["StreetAddress1"].ToString());
            myString = myString.Replace("$$City$$", dr["City"].ToString());
            myString = myString.Replace("$$State$$", dr["State"].ToString());
            myString = myString.Replace("$$Zipcode$$", dr["Zipcode"].ToString());
            myString = myString.Replace("$$PhoneNumber$$", dr["BusinessPhone1"].ToString());
            myString = myString.Replace("$$Automobile$$", dr["AutoTitle"].ToString());
            myString = myString.Replace("$$ServiceType$$", dr["Type"].ToString());
            myString = myString.Replace("$$Service$$", dr["Name"].ToString());
            myString = myString.Replace("$$TableContent$$", qtDetailTbl);
            myString = myString.Replace("$$AppointmentDate$$", dr["AppDate"].ToString());
            myString = myString.Replace("$$AppointmentTime$$", Convert.ToDateTime(dr["AppoinmentDate"].ToString()).TimeOfDay.ToString());
            myString = myString.Replace("$$AppointmentID$$", dr["AppId"].ToString());

            string fromAddress = ConfigurationSettings.AppSettings["FromEmail"];
            //string toAddress = "piyushparmar195@gmail.com";
            string toAddress = dr["EmailAddress"].ToString();
            string fromPassword = ConfigurationSettings.AppSettings["Password"];
            string subject = DotNetNuke.Services.Localization.Localization.GetString("B2CAppointmentAlert.Text", System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_GlobalResources\\EmailSubject.resx");
            string body = myString.ToString();
            MailMessage Msg = new MailMessage();
            MailAddress fromMail = new MailAddress(fromAddress);
            // Sender e-mail address.
            Msg.From = fromMail;
            // Recipient e-mail address.
            Msg.To.Add(new MailAddress(toAddress));
            // Subject of e-mail
            Msg.Subject = subject;
            Msg.Body = myString.ToString();
            Msg.IsBodyHtml = true;
            reader.Dispose();
            var smtp = new System.Net.Mail.SmtpClient();
            {

                smtp.Host = ConfigurationSettings.AppSettings["SMTP"];
                if (ConfigurationSettings.AppSettings["SMTPPort"] != "")
                    smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["SMTPPort"]);

                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                if (ConfigurationSettings.AppSettings["Password"] != "")
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);

                smtp.Timeout = 20000;
            }
            //Passing values to smtp object
            try
            {
                smtp.Send(Msg);
            }
            catch (System.Net.Mail.SmtpException smtpExc)
            {
                if (smtpExc.StatusCode == SmtpStatusCode.GeneralFailure)
                {
                    //Response.Write("<script> alert('Sorry, e-mail sending fail...');window.location='Home.aspx'; </script>");
                }
                else
                {
                    throw smtpExc;
                }
            }
        }
    }
}