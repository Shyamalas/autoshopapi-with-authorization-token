﻿namespace AutoShop.DAL
{
using System;
using System.Data;

    public partial class BusinessProfile : IDisposable
    {
        private DataAccess _dataAccess;

        public BusinessProfile()
        {
            this._dataAccess = new DataAccess();
        }

        public BusinessProfile(string ConnectionString)
        {
            this._dataAccess = new DataAccess(ConnectionString);
        }

        public virtual System.Data.DataSet Get_BusinessProfile_By_ContactId(System.Nullable<int> contactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_BusinessProfile_By_ContactId");
            this._dataAccess.AddParameter("@ContactId", contactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int Submit_Business_Profile(int? contactId, int? businessId, string autoShopName, string autoShopType, string contactFirstName, string contactLastName, string autoShopAddress, 
            string autoShopCity, string autoShopState, int? autoShopZipCode, string serviceCenterPh, string contactPhNo, string faxNo, string website, string serviceOffered, string specialities,
            string modifiedBy, bool? memAAA, bool? memACE, bool? memASE, bool? memBBB, string operationHours, bool Isdealer, string AutoManufacturer, bool StarCertified)//, string businessCoupons)
        {
            this._dataAccess.CreateProcedureCommand("sp_Submit_Business_Profile");
            this._dataAccess.AddParameter("@ModifiedBy", modifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ContactId", contactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("@BusinessId", businessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoShopName", autoShopName, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoShopType", autoShopType, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ContactFirstName", contactFirstName, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ContactLastName", contactLastName, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoShopAddress", autoShopAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoShopCity", autoShopCity, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoshopState", autoShopState, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoShopZipCode", autoShopZipCode, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ServiceCenterPh", serviceCenterPh, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ContactPhNo", contactPhNo, ParameterDirection.Input);
            this._dataAccess.AddParameter("@FaxNumber", faxNo, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Website", website, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ServiceOffered", serviceOffered, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Specialities", specialities, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@CouponName", couponName, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@CouponStartDate", couponStartDate, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@CouponEndDate", couponEndTime, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@CouponActiveFlag", couponActiveFlag, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@RepairShop", repairShop, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@BodyShop", bodyShop, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@TyreShop", tyreShop, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@CarWash", carWash, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@Transport", transport, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@Towing", towing, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@SmogCheck", smogCheck, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@GlassShop", glassShop, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@Dealer", dealer, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@Parts", parts, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@NewCarSales", newCarSales, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@UsedCarSales", usedCarSales, ParameterDirection.Input);
            this._dataAccess.AddParameter("@MemAAA", memAAA, ParameterDirection.Input);
            this._dataAccess.AddParameter("@MemACE", memACE, ParameterDirection.Input);
            this._dataAccess.AddParameter("@MemASE", memASE, ParameterDirection.Input);
            this._dataAccess.AddParameter("@MemBBB", memBBB, ParameterDirection.Input);
            this._dataAccess.AddParameter("@OperationHrs", operationHours, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@BusinessCoupons", businessCoupons, ParameterDirection.Input);
            this._dataAccess.AddParameter("@IsDealer", Isdealer , ParameterDirection.Input);
            this._dataAccess.AddParameter("@Manufacturer", AutoManufacturer, ParameterDirection.Input);
            this._dataAccess.AddParameter("@StarCertified", StarCertified, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnFlag", SqlDbType.Int, ParameterDirection.Output);
            int value = this._dataAccess.ExecuteNonQueryWithOutputParam("returnFlag");
            return value;
        }

        public virtual int Submit_Business_Coupon(string modifiedBy, DateTime? startDate, DateTime? endDate, bool? activeFlag, int businessId, string couponName, string couponImage, int? couponNo)
        {
            this._dataAccess.CreateProcedureCommand("sp_Submit_Business_Coupon");
            this._dataAccess.AddParameter("@ModifiedBy", modifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("@StartDate", startDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("@EndDate", endDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ActiveFlag", activeFlag, ParameterDirection.Input);
            this._dataAccess.AddParameter("@BusinessId", businessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("@CouponName", couponName, ParameterDirection.Input);
            this._dataAccess.AddParameter("@CouponImage", couponImage, ParameterDirection.Input);
            this._dataAccess.AddParameter("@CouponNo", couponNo, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnFlag", SqlDbType.Int, ParameterDirection.Output);
            int value = this._dataAccess.ExecuteNonQueryWithOutputParam("returnFlag");
            return value;
        }

        public virtual int Submit_Autoshop_Image(string modifiedBy, bool? activeFlag, int businessId, string imgName, string autoshopImage, int? imgNo)
        {
            this._dataAccess.CreateProcedureCommand("sp_Submit_Business_Image");
            this._dataAccess.AddParameter("@ModifiedBy", modifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ActiveFlag", activeFlag, ParameterDirection.Input);
            this._dataAccess.AddParameter("@BusinessId", businessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ImageName", imgName, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoshopImage", autoshopImage, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ImageNo", imgNo, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnFlag", SqlDbType.Int, ParameterDirection.Output);
            int value = this._dataAccess.ExecuteNonQueryWithOutputParam("returnFlag");
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }

        public virtual System.Data.DataSet Get_BusinessProfile_By_BusinessId(int busId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_B2BDetailsForAdmin");
            this._dataAccess.AddParameter("@BusinessId", busId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GetServiceCenterImagesForAdmin_By_BusinessId(int busId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_B2BImagesForAdmin");
            this._dataAccess.AddParameter("@BusinessId", busId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GetCouponForAdmin_By_BusinessId(int busId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_B2BCouponForAdmin");
            this._dataAccess.AddParameter("@BusinessId", busId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet GetHoursOFOPerationForAdmin_By_BusinessId(int busId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Get_B2BBusinessHourForAdmin");
            this._dataAccess.AddParameter("@BusinessId", busId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int UpdateServiceCenterByAdmin(int? busId, string modifiedBy, string busName, string busType, string address, string city, string state,
            int? zipcode, string servicePh, string faxNo, string webAddress, string serviceOffered,
            string specialities, bool? AAA, bool? ACE, bool? ASE, bool? BBB, string operationHours, bool? IsDealer, string Maufacturer,bool StarCertified,bool? IsActive)
        {
            this._dataAccess.CreateProcedureCommand("sp_UpdateServiceCenterByAdmin");
            this._dataAccess.AddParameter("@BusId", busId, ParameterDirection.Input);
           // this._dataAccess.AddParameter("@CntId", cntId, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ModifyBy", modifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoShopName", busName, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AutoShopType", busType, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Address", address, ParameterDirection.Input);
            this._dataAccess.AddParameter("@City", city, ParameterDirection.Input);
            this._dataAccess.AddParameter("@State", state, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Zipcode", zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Serviceph", servicePh, ParameterDirection.Input);
            //this._dataAccess.AddParameter("@Contactph", cntPh, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Faxnumber", faxNo, ParameterDirection.Input);
            this._dataAccess.AddParameter("@WebAddress", webAddress, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Serviceoffered", serviceOffered, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Specialities", specialities, ParameterDirection.Input);
            this._dataAccess.AddParameter("@AAA", AAA, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ACE", ACE, ParameterDirection.Input);
            this._dataAccess.AddParameter("@ASE", ASE, ParameterDirection.Input);
            this._dataAccess.AddParameter("@BBB", BBB, ParameterDirection.Input);
            this._dataAccess.AddParameter("@OperationHrs", operationHours, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Dealer", IsDealer, ParameterDirection.Input);
            this._dataAccess.AddParameter("@Manufacture", Maufacturer, ParameterDirection.Input);
            this._dataAccess.AddParameter("@StarCertified", StarCertified, ParameterDirection.Input);
            this._dataAccess.AddParameter("@IsActive", IsActive, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnFlag", SqlDbType.Int, ParameterDirection.Output);
            int value = this._dataAccess.ExecuteNonQueryWithOutputParam("returnFlag");
            return value;
        }

        public virtual System.Data.DataSet Get_PaymentHistory_By_BusinessId(int busId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetPaymentHistoryForAdminByBusId");
            this._dataAccess.AddParameter("@BusId", busId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }
    }
}
