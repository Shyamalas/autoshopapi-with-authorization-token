﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Configuration;
using System.Drawing;
using System.IO;

namespace AutoShop.DAL
{
    public class UploadFileOnAmazon
    {
        public void UploadFile(byte[] imageBytes, string CurrentPath)
        {
            Stream stream = new MemoryStream(imageBytes);
            var client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSAccessKey"].ToString(), ConfigurationManager.AppSettings["AWSSecretKey"].ToString(), Amazon.RegionEndpoint.USEast1);
            try
            {
                PutObjectRequest putRequest = new PutObjectRequest
                {
                    BucketName = ConfigurationManager.AppSettings["AWSBucketName"].ToString(),
                    CannedACL = S3CannedACL.PublicRead,
                    Key = CurrentPath,
                    InputStream = stream,
                };
                PutObjectResponse response = client.PutObject(putRequest);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Check the provided AWS Credentials.");
                }
                else
                {
                    throw new Exception("Error occurred: " + amazonS3Exception.Message);
                }
            }
        }

        public void ResizeImage(byte[] byteImage, string NewFilePath, int NewWidth, int MaxHeight, bool OnlyResizeIfWider)
        {
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromStream(new MemoryStream(byteImage)); //System.Drawing.Image.FromFile(OriginalFilePath);

            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (OnlyResizeIfWider)
            {
                if (FullsizeImage.Width <= NewWidth)
                {
                    NewWidth = FullsizeImage.Width;
                }
            }

            int NewHeight = (FullsizeImage.Height * NewWidth) / FullsizeImage.Width;
            if (NewHeight > MaxHeight)
            {
                NewWidth = (FullsizeImage.Width * MaxHeight) / FullsizeImage.Height;
                NewHeight = MaxHeight;
            }

            Bitmap ImgTemp;
            ImgTemp = new Bitmap(FullsizeImage, NewWidth, NewHeight);

            ImageConverter converter = new ImageConverter();

            UploadFile((byte[])converter.ConvertTo(ImgTemp, typeof(byte[])), NewFilePath);
        }
    }
}
