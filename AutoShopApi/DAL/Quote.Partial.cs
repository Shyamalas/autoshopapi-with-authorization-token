namespace AutoShop.DAL
{
    using System;
    using System.Data;

    public partial class Quote : IDisposable
    {
        private DataAccess _dataAccess;

        public Quote()
        {
            this._dataAccess = new DataAccess();
        }

        public virtual System.Data.DataSet Select_Quotes_By_AutomobildeId(System.Nullable<int> AutomobildeId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Quotes_By_AutomobildeId");
            this._dataAccess.AddParameter("AutomobildeId", AutomobildeId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Quotes_By_ServiceId(System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Quotes_By_ServiceId");
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Quote_Select_All()
        {
            this._dataAccess.CreateProcedureCommand("sp_Quote_Select_All");
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Quote_Select_One(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Quote_Select_One");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int Quote_Insert(System.Nullable<int> AutoMobileId, System.Nullable<int> BusinessId, System.Nullable<int> ServiceId, System.Nullable<int> RepairTypeId, System.Nullable<int> Zipcode, System.Nullable<double> Mileage, string Comments, int ExpireDay, out int returnId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Quote_Insert");
            this._dataAccess.AddParameter("AutoMobileId", AutoMobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            this._dataAccess.AddParameter("RepairTypeId", RepairTypeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Mileage", Mileage, ParameterDirection.Input);
            this._dataAccess.AddParameter("Comments", Comments, ParameterDirection.Input);
            this._dataAccess.AddParameter("ExpireDay", ExpireDay, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnId", SqlDbType.Int, ParameterDirection.Output);
            returnId = this._dataAccess.ExecuteNonQueryWithOutputParam("returnId");
            return returnId;
        }

        public virtual System.Nullable<int> Quote_Delete(System.Nullable<int> RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Quote_Delete");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Nullable<int> Quote_Update(System.Nullable<int> RowId, string CreatedBy, System.Nullable<System.DateTime> CreatedDate, string ModifiedBy, System.Nullable<System.DateTime> ModifiedDate, System.Nullable<int> AutomobildeId, string Status, System.Nullable<int> Miles, string Comments, System.Nullable<int> ServiceId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Quote_Update");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedBy", CreatedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("CreatedDate", CreatedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedBy", ModifiedBy, ParameterDirection.Input);
            this._dataAccess.AddParameter("ModifiedDate", ModifiedDate, ParameterDirection.Input);
            this._dataAccess.AddParameter("AutomobildeId", AutomobildeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Status", Status, ParameterDirection.Input);
            this._dataAccess.AddParameter("Miles", Miles, ParameterDirection.Input);
            this._dataAccess.AddParameter("Comments", Comments, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_Quotes_By_ContactId(System.Nullable<int> ContactId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Quotes_By_ContactId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Quotes_By_BusinessId(System.Nullable<int> BusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Quotes_By_BusinessId");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Quotes_By_QuoteId(System.Nullable<int> BusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Quotes_By_QuoteId");
            this._dataAccess.AddParameter("QuoteId", BusinessId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Quotes_QuoteHeader(System.Nullable<int> BusinessId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Quotes_QuoteHeader");
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet QuoteB2B_Select_By_RowID(System.Nullable<int> RowId, System.Nullable<int> BusId)
        {
            this._dataAccess.CreateProcedureCommand("sp_QuoteB2B_Select_By_RowID");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusId", BusId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual int Send_Existing_Quote(int QuoteId, int BusinessId, double Mileage, int Zipcode, int expDay)
        {
            this._dataAccess.CreateProcedureCommand("sp_QuoteRequest_Insert");
            this._dataAccess.AddParameter("QuoteId", QuoteId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusinessId", BusinessId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("Mileage", Mileage, ParameterDirection.Input);
            this._dataAccess.AddParameter("ExpireDay", expDay, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Select_Quotes_By_ContactId_fltBusId(System.Nullable<int> ContactId, System.Nullable<int> BusId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Quotes_By_ContactId_fltBusId");
            this._dataAccess.AddParameter("ContactId", ContactId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusId", BusId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Nullable<int> CheckQuoteRequest(int QuoteId, out int retVar)
        {
            this._dataAccess.CreateProcedureCommand("sp_CheckQuoteRequest");
            this._dataAccess.AddParameter("QuoteId", QuoteId, ParameterDirection.Input);
            this._dataAccess.AddParameter("retVar", SqlDbType.Int, ParameterDirection.Output);
            retVar = this._dataAccess.ExecuteNonQueryWithOutputParam("retVar");
            return retVar;
        }

        public virtual System.Nullable<int> QuoteRequest_Cancel(System.Nullable<int> QuoteId, System.Nullable<int> BusId)
        {
            this._dataAccess.CreateProcedureCommand("sp_QuoteRequest_Cancel");
            this._dataAccess.AddParameter("QuoteId", QuoteId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusId", BusId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual int Quote_InsertMultiple(System.Nullable<int> AutoMobileId, string BusMultiId, System.Nullable<int> ServiceId, System.Nullable<int> RepairTypeId, System.Nullable<int> Zipcode, string MultiMileage, string Comments, System.Nullable<int> MultiCount, int Expireday, out int returnId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Quote_InsertMultiple");
            this._dataAccess.AddParameter("AutoMobileId", AutoMobileId, ParameterDirection.Input);
            this._dataAccess.AddParameter("BusMultiId", BusMultiId, ParameterDirection.Input);
            this._dataAccess.AddParameter("ServiceId", ServiceId, ParameterDirection.Input);
            this._dataAccess.AddParameter("RepairTypeId", RepairTypeId, ParameterDirection.Input);
            this._dataAccess.AddParameter("Zipcode", Zipcode, ParameterDirection.Input);
            this._dataAccess.AddParameter("MultiMileage", MultiMileage, ParameterDirection.Input);
            this._dataAccess.AddParameter("Comments", Comments, ParameterDirection.Input);
            this._dataAccess.AddParameter("MultiCount", MultiCount, ParameterDirection.Input);
            this._dataAccess.AddParameter("Expireday", Expireday, ParameterDirection.Input);
            this._dataAccess.AddParameter("returnId", SqlDbType.Int, ParameterDirection.Output);
            returnId = this._dataAccess.ExecuteNonQueryWithOutputParam("returnId");
            return returnId;
        }

        public virtual System.Data.DataSet GetQuoteDetailsForMail(System.Nullable<int> RepID, System.Nullable<int> SerID)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetQuoteDetailsForMail");
            this._dataAccess.AddParameter("RepID", RepID, ParameterDirection.Input);
            this._dataAccess.AddParameter("SerID", SerID, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual void Dispose()
        {
            if ((this._dataAccess != null))
            {
                this._dataAccess.Dispose();
            }
        }

        public virtual System.Nullable<int> Delete_Quotes_By_QuoteId(int quotId)
        {
            this._dataAccess.CreateProcedureCommand("sp_DeleteQuoteByQuoteId");
            this._dataAccess.AddParameter("QuoteId", quotId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;

        }

        public virtual int Inactive_Quotes_By_QuoteId(int quotId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Inactive_Quotes_By_QuoteId");
            this._dataAccess.AddParameter("QuoteId", quotId, ParameterDirection.Input);
            int value = this._dataAccess.ExecuteNonQuery();
            return value;
        }

        public virtual System.Data.DataSet Get_Quote_By_RowId(int RowId)
        {
            this._dataAccess.CreateProcedureCommand("sp_GetQuoteByRowId");
            this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Select_Quotes_By_AutoId(System.Nullable<int> AutoId)
        {
            this._dataAccess.CreateProcedureCommand("sp_Select_Quotes_By_AutoId");
            this._dataAccess.AddParameter("AutoId", AutoId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        public virtual System.Data.DataSet Get_NewQuote_EmailAddress(int quotId)
        {
            this._dataAccess.CreateProcedureCommand("GetNewQuoteEmailAddress");
            this._dataAccess.AddParameter("QuoteId", quotId, ParameterDirection.Input);
            DataSet value = this._dataAccess.ExecuteDataSet();
            return value;
        }

        //public virtual System.Data.DataSet Get_UserLocation_By_RowId(int RowId)
        //{
        //    this._dataAccess.CreateProcedureCommand("sp_GetUserLocationByRowId");
        //    this._dataAccess.AddParameter("RowId", RowId, ParameterDirection.Input);
        //    DataSet value = this._dataAccess.ExecuteDataSet();
        //    return value;
        //}

    }
}