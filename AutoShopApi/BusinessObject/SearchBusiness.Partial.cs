using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Configuration;
namespace AutoShop.BusinessObjects
{
    [Serializable]
    public partial class SearchBusiness
    {
        private System.Nullable<int> _businessIdNumber;

        private string _businessName;

        private string _address;

        private string _city;

        private string _state;

        private string _statecode;

        private System.Nullable<int> _zip;


        private System.Nullable<int> _zip4;

        private string _phone;

        private System.Nullable<int> _sicCode;

        private string _sicDescription;

        private System.Nullable<int> _naicsCode;

        private string _naicsDescription;

        private string _locationType;

        private string _adSize;

        private string _associates;

        private string _businessAtHome;

        private System.Nullable<int> _ultimateParentId;

        private string _ultimateParentName;

        private System.Nullable<int> _subsidiaryParentId;

        private string _subsidiaryParentName;

        private System.Nullable<decimal> _latitude;

        private System.Nullable<decimal> _longitude;

        private System.Nullable<int> _matchFlag;

        private System.Nullable<int> _cbsaCode;

        private System.Nullable<int> _rating;

        private string _cbsaName;

        private System.Nullable<int> _fipsCode;

        private string _county;

        private System.Nullable<decimal> _Miles;

        private string _image;

        private string _businessHour;

        private bool _memberofAAA;

        private string _memberofAAAstr;

        private string _memberofAAAdisp;

        private bool _starCertified;

        private string _starCertifiedstr;

        private string _starCertifieddisp;

        private bool _memberofACE;

        private string _memberofACEstr;

        private string _memberofACEdisp;

        private bool _memberofASE;

        private string _memberofASEstr;

        private string _memberofASEdisp;

        private bool _memberofBBB;

        private string _memberofBBBstr;

        private string _memberofBBBdisp;

        private string _isFavorite;

        private string _isFavoriteClass;

        private string _email;

        private string _isOnline;

        private string _coupon;

        private string _appointment;

        private string _chat;

        private string _quotedisp;

        private string _isActiveSubscription;

        private System.Nullable<bool> _repairShop;

        private System.Nullable<bool> _carWash;

        public virtual string Chat
        {
            get
            {
                return _chat;
            }
            set
            {
                _chat = value;
            }
        }

        public virtual string Appointment
        {
            get
            {
                return _appointment;
            }
            set
            {
                _appointment = value;
            }
        }

        public virtual string Coupon
        {
            get
            {
                return _coupon;
            }
            set
            {
                _coupon = value;
            }
        }

        public virtual string Online
        {
            get
            {
                return _isOnline;
            }
            set
            {
                _isOnline = value;
            }
        }

        public virtual string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        public virtual System.Nullable<int> BusinessIdNumber
        {
            get
            {
                return _businessIdNumber;
            }
            set
            {
                _businessIdNumber = value;
            }
        }

        public virtual string BusinessName
        {
            get
            {
                return _businessName;
            }
            set
            {
                _businessName = value;
            }
        }

        public virtual string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
            }
        }

        public virtual string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        public virtual string State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        public virtual string Statecode
        {
            get
            {
                return _statecode;
            }
            set
            {
                _statecode = value;
            }
        }

        public virtual System.Nullable<int> Zip
        {
            get
            {
                return _zip;
            }
            set
            {
                _zip = value;
            }
        }

        public virtual System.Nullable<int> Zip4
        {
            get
            {
                return _zip4;
            }
            set
            {
                _zip4 = value;
            }
        }

        public virtual System.Nullable<int> Rating
        {
            get
            {
                return _rating;
            }
            set
            {
                _rating = value;
            }
        }

        public virtual System.Nullable<decimal> Miles
        {
            get
            {
                return _Miles;
            }
            set
            {
                _Miles = value;
            }
        }

        public virtual string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }

        public virtual System.Nullable<int> SicCode
        {
            get
            {
                return _sicCode;
            }
            set
            {
                _sicCode = value;
            }
        }

        public virtual string SicDescription
        {
            get
            {
                return _sicDescription;
            }
            set
            {
                _sicDescription = value;
            }
        }

        public virtual string Associates
        {
            get
            {
                return _associates;
            }
            set
            {
                _associates = value;
            }
        }

        public virtual System.Nullable<int> NaicsCode
        {
            get
            {
                return _naicsCode;
            }
            set
            {
                _naicsCode = value;
            }
        }

        public virtual string NaicsDescription
        {
            get
            {
                return _naicsDescription;
            }
            set
            {
                _naicsDescription = value;
            }
        }

        public virtual string LocationType
        {
            get
            {
                return _locationType;
            }
            set
            {
                _locationType = value;
            }
        }

        public virtual string AdSize
        {
            get
            {
                return _adSize;
            }
            set
            {
                _adSize = value;
            }
        }

        public virtual string BusinessAtHome
        {
            get
            {
                return _businessAtHome;
            }
            set
            {
                _businessAtHome = value;
            }
        }

        public virtual System.Nullable<int> UltimateParentId
        {
            get
            {
                return _ultimateParentId;
            }
            set
            {
                _ultimateParentId = value;
            }
        }

        public virtual string UltimateParentName
        {
            get
            {
                return _ultimateParentName;
            }
            set
            {
                _ultimateParentName = value;
            }
        }

        public virtual System.Nullable<int> SubsidiaryParentId
        {
            get
            {
                return _subsidiaryParentId;
            }
            set
            {
                _subsidiaryParentId = value;
            }
        }

        public virtual string SubsidiaryParentName
        {
            get
            {
                return _subsidiaryParentName;
            }
            set
            {
                _subsidiaryParentName = value;
            }
        }

        public virtual System.Nullable<decimal> Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
            }
        }

        public virtual System.Nullable<decimal> Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
            }
        }

        public virtual System.Nullable<int> MatchFlag
        {
            get
            {
                return _matchFlag;
            }
            set
            {
                _matchFlag = value;
            }
        }

        public virtual System.Nullable<int> CbsaCode
        {
            get
            {
                return _cbsaCode;
            }
            set
            {
                _cbsaCode = value;
            }
        }

        public virtual string CbsaName
        {
            get
            {
                return _cbsaName;
            }
            set
            {
                _cbsaName = value;
            }
        }

        public virtual System.Nullable<int> FipsCode
        {
            get
            {
                return _fipsCode;
            }
            set
            {
                _fipsCode = value;
            }
        }

        public virtual string County
        {
            get
            {
                return _county;
            }
            set
            {
                _county = value;
            }
        }

        public virtual string Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
            }
        }

        public virtual string BusinessHour
        {
            get
            {
                return _businessHour;
            }
            set
            {
                _businessHour = value;
            }
        }

        public virtual bool MemberofAAA
        {
            get
            {
                return _memberofAAA;
            }
            set
            {
                _memberofAAA = value;
            }
        }

        public virtual string MemberofAAAstr
        {
            get
            {
                return _memberofAAAstr;
            }
            set
            {
                _memberofAAAstr = value;
            }
        }

        public virtual string MemberofAAAdisp
        {
            get
            {
                return _memberofAAAdisp;
            }
            set
            {
                _memberofAAAdisp = value;
            }
        }


        public virtual bool StarCertified
        {
            get 
            {
                return _starCertified;
            }
            set 
            {
                _starCertified = value;
            }
        }

        public virtual string StarCertifiedstr
        {
            get
            {
                return _starCertifiedstr;
            }
            set
            {
                _starCertifiedstr = value;
            }
        }

        public virtual string StarCertifieddisp
        {
            get
            {
                return _starCertifieddisp;
            }
            set
            {
                _starCertifieddisp = value;
            }
        }

        public virtual bool MemberofACE
        {
            get
            {
                return _memberofACE;
            }
            set
            {
                _memberofACE = value;
            }
        }

        public virtual string MemberofACEstr
        {
            get
            {
                return _memberofACEstr;
            }
            set
            {
                _memberofACEstr = value;
            }
        }

        public virtual string MemberofACEdisp
        {
            get
            {
                return _memberofACEdisp;
            }
            set
            {
                _memberofACEdisp = value;
            }
        }

        public virtual bool MemberofASE
        {
            get
            {
                return _memberofASE;
            }
            set
            {
                _memberofASE = value;
            }
        }

        public virtual string MemberofASEstr
        {
            get
            {
                return _memberofASEstr;
            }
            set
            {
                _memberofASEstr = value;
            }
        }

        public virtual string MemberofASEdisp
        {
            get
            {
                return _memberofASEdisp;
            }
            set
            {
                _memberofASEdisp = value;
            }
        }

        public virtual bool MemberofBBB
        {
            get
            {
                return _memberofBBB;
            }
            set
            {
                _memberofBBB = value;
            }
        }

        public virtual string MemberofBBBstr
        {
            get
            {
                return _memberofBBBstr;
            }
            set
            {
                _memberofBBBstr = value;
            }
        }

        public virtual string MemberofBBBdisp
        {
            get
            {
                return _memberofBBBdisp;
            }
            set
            {
                _memberofBBBdisp = value;
            }
        }

        public virtual string IsFavorite
        {
            get
            {
                return _isFavorite;
            }
            set
            {
                _isFavorite = value;
            }
        }

        public virtual string IsFavoriteClass
        {
            get
            {
                return _isFavoriteClass;
            }
            set
            {
                _isFavoriteClass = value;
            }
        }

        public virtual string Quotedisp
        {
            get
            {
                return _quotedisp;
            }
            set
            {
                _quotedisp = value;
            }
        }

        public virtual string IsActiveSubscription
        {
            get
            {
                return _isActiveSubscription;
            }
            set
            {
                _isActiveSubscription = value;
            }
        }

        public virtual System.Nullable<bool> RepairShop
        {
            get
            {
                return _repairShop;
            }
            set
            {
                _repairShop = value;
            }
        }

        public virtual System.Nullable<bool> CarWash
        {
            get
            {
                return _carWash;
            }
            set
            {
                _carWash = value;
            }
        }

        private void Clean()
        {
            this.BusinessIdNumber = null;
            this.BusinessName = string.Empty;
            this.Address = string.Empty;
            this.City = string.Empty;
            this.State = string.Empty;
            this.Statecode = string.Empty;
            this.Zip = null;
            this.Zip4 = null;
            this.Rating = null;
            this.Associates = string.Empty;
            this.Phone = string.Empty;
            this.SicCode = null;
            this.SicDescription = string.Empty;
            this.NaicsCode = null;
            this.NaicsDescription = string.Empty;
            this.LocationType = string.Empty;
            this.AdSize = string.Empty;
            this.BusinessAtHome = string.Empty;
            this.UltimateParentId = null;
            this.UltimateParentName = string.Empty;
            this.SubsidiaryParentId = null;
            this.SubsidiaryParentName = string.Empty;
            this.Latitude = null;
            this.Longitude = null;
            this.MatchFlag = null;
            this.CbsaCode = null;
            this.CbsaName = string.Empty;
            this.FipsCode = null;
            this.County = string.Empty;
            this.Image = string.Empty;
            this.BusinessHour = string.Empty;
            this.MemberofAAA = false;
            this.MemberofAAAstr = string.Empty;
            this.MemberofAAAdisp = string.Empty;
            this.StarCertified = false;
            this.StarCertifiedstr = string.Empty;
            this.StarCertifieddisp = string.Empty;
            this.MemberofACE = false;
            this.MemberofACEstr = string.Empty;
            this.MemberofACEdisp = string.Empty;
            this.MemberofASE = false;
            this.MemberofASEstr = string.Empty;
            this.MemberofASEdisp = string.Empty;
            this.MemberofBBB = false;
            this.MemberofBBBstr = string.Empty;
            this.MemberofBBBdisp = string.Empty;
            this.IsFavorite = string.Empty;
            this.IsFavoriteClass = string.Empty;
            this.Online = string.Empty;
            this.Email = string.Empty;
            this.Coupon = string.Empty;
            this.Appointment = string.Empty;
            this.Chat = string.Empty;
            this.Quotedisp = string.Empty;
            this.RepairShop = false;
        }        

        private void Fill(System.Data.DataRow dr)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("MemberofAAA", "/images/Member/AAA.jpg");
            dictionary.Add("MemberofACE", "/images/Member/ACE.jpg");
            dictionary.Add("MemberofASE", "/images/Member/ASE.jpg");
            dictionary.Add("MemberofBBB", "/images/Member/BBB.jpg");
            dictionary.Add("StarCertified", "/images/Member/strcrt.jpg");
            

            this.Clean();
            if ((dr["Business_ID_Number"] != System.DBNull.Value))
            {
                this.BusinessIdNumber = ((System.Nullable<int>)(dr["Business_ID_Number"]));
            }
            if ((dr["Business_Name"] != System.DBNull.Value))
            {
                this.BusinessName = ((string)(dr["Business_Name"]));
            }
            if ((dr["Address"] != System.DBNull.Value))
            {
                this.Address = ((string)(dr["Address"]));
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                this.City = ((string)(dr["City"]));
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                this.State = ((string)(dr["State"]));
            }
            if ((dr["StateCode"] != System.DBNull.Value))
            {
                this.Statecode = ((string)(dr["StateCode"]));
            }
            if ((dr["ZIP"] != System.DBNull.Value))
            {
                this.Zip = ((System.Nullable<int>)(dr["ZIP"]));
            }
            if ((dr["ZIP_4"] != System.DBNull.Value))
            {
                this.Zip4 = ((System.Nullable<int>)(dr["ZIP_4"]));
            }
            if ((dr["Phone"] != System.DBNull.Value))
            {
                this.Phone = ((string)(dr["Phone"]));
            }
            if ((dr["Rating"] != System.DBNull.Value))
            {
                this.Rating = ((System.Nullable<int>)(dr["Rating"]));
            }
            else
            {
                this.Rating = 0;
            }
            //if ((dr["SIC_Code"] != System.DBNull.Value))
            //{
            //    this.SicCode = ((System.Nullable<int>)(dr["SIC_Code"]));
            //}
            //if ((dr["SIC_Description"] != System.DBNull.Value))
            //{
            //    this.SicDescription = ((string)(dr["SIC_Description"]));
            //}            
            if ((dr["NAICS_Code"] != System.DBNull.Value))
            {
                this.NaicsCode = ((System.Nullable<int>)(dr["NAICS_Code"]));
            }
            if ((dr["NAICS_Description"] != System.DBNull.Value))
            {
                this.NaicsDescription = ((string)(dr["NAICS_Description"]));
            }
            if ((dr["Miles"] != System.DBNull.Value))
            {
                this.Miles = ((System.Nullable<decimal>)(dr["Miles"]));
            }
            else
            {
                this.Miles = 0;
            }
            //if ((dr["Location_Type"] != System.DBNull.Value))
            //{
            //    this.LocationType = ((string)(dr["Location_Type"]));
            //}
            //if ((dr["Ad_Size"] != System.DBNull.Value))
            //{
            //    this.AdSize = ((string)(dr["Ad_Size"]));
            //}
            //if ((dr["Business_At_Home"] != System.DBNull.Value))
            //{
            //    this.BusinessAtHome = ((string)(dr["Business_At_Home"]));
            //}
            //if ((dr["Ultimate_Parent_Id"] != System.DBNull.Value))
            //{
            //    this.UltimateParentId = ((System.Nullable<int>)(dr["Ultimate_Parent_Id"]));
            //}
            //if ((dr["Ultimate_Parent_Name"] != System.DBNull.Value))
            //{
            //    this.UltimateParentName = ((string)(dr["Ultimate_Parent_Name"]));
            //}
            //if ((dr["Subsidiary_Parent_Id"] != System.DBNull.Value))
            //{
            //    this.SubsidiaryParentId = ((System.Nullable<int>)(dr["Subsidiary_Parent_Id"]));
            //}
            //if ((dr["Subsidiary_Parent_Name"] != System.DBNull.Value))
            //{
            //    this.SubsidiaryParentName = ((string)(dr["Subsidiary_Parent_Name"]));
            //}
            if ((dr["Latitude"] != System.DBNull.Value))
            {
                this.Latitude = Convert.ToDecimal(dr["Latitude"]);
            }
            if ((dr["Longitude"] != System.DBNull.Value))
            {
                this.Longitude = Convert.ToDecimal(dr["Longitude"]);
            }
            //if ((dr["Match_Flag"] != System.DBNull.Value))
            //{
            //    this.MatchFlag = ((System.Nullable<int>)(dr["Match_Flag"]));
            //}
            //if ((dr["CBSA_Code"] != System.DBNull.Value))
            //{
            //    this.CbsaCode = ((System.Nullable<int>)(dr["CBSA_Code"]));
            //}
            //if ((dr["CBSA_Name"] != System.DBNull.Value))
            //{
            //    this.CbsaName = ((string)(dr["CBSA_Name"]));
            //}
            //if ((dr["FIPS_Code"] != System.DBNull.Value))
            //{
            //    this.FipsCode = ((System.Nullable<int>)(dr["FIPS_Code"]));
            //}
            if ((dr["County"] != System.DBNull.Value))
            {
                this.County = ((string)(dr["County"]));
            }

            if ((dr["Image"] != System.DBNull.Value))
            {
                this.Image = ((string)(dr["Image"]));
            }
            else
            {
                //this.Image = "/images/Logo/noimage.jpg";
                this.Image = WebConfigurationManager.AppSettings["DefaultPath"]; ;
            }

            if ((dr["BusinessHour"] != System.DBNull.Value))
            {
                this.BusinessHour = ((string)(dr["BusinessHour"]));
            }
            else
            {
                //this.BusinessHour = "Not found";
                this.BusinessHour = "";
            }

            if ((dr["MemberofAAA"] != System.DBNull.Value))
            {
                this.MemberofAAA = ((bool)(dr["MemberofAAA"]));

                if (this.MemberofAAA == true)
                {
                    this.MemberofAAAstr = dictionary["MemberofAAA"];
                }
                else
                {
                    this.MemberofAAAdisp = "none";
                }
            }
            else
            {
                this.MemberofAAAdisp = "none";
            }
            //vikas 

            if ((dr["StarCertified"] != System.DBNull.Value))
            {
                this.StarCertified = ((bool)(dr["StarCertified"]));

                if (this.StarCertified == true)
                {
                    this.StarCertifiedstr = dictionary["StarCertified"];
                }
                else
                {
                    this.StarCertifieddisp = "none";
                }
            }
            else
            {
                this.StarCertifieddisp = "none";
            }

            //
            if ((dr["MemberofACE"] != System.DBNull.Value))
            {
                this.MemberofACE = ((bool)(dr["MemberofACE"]));

                if (this.MemberofACE == true)
                {
                    this.MemberofACEstr = dictionary["MemberofACE"];
                }
                else
                {
                    this.MemberofACEdisp = "none";
                }
            }
            else
            {
                this.MemberofACEdisp = "none";
            }

            if ((dr["MemberofASE"] != System.DBNull.Value))
            {
                this.MemberofASE = ((bool)(dr["MemberofASE"]));

                if (this.MemberofASE == true)
                {
                    this.MemberofASEstr = dictionary["MemberofASE"];
                }
                else
                {
                    this.MemberofASEdisp = "none";
                }
            }
            else
            {
                this.MemberofASEdisp = "none";
            }

            if ((dr["MemberofBBB"] != System.DBNull.Value))
            {
                this.MemberofBBB = ((bool)(dr["MemberofBBB"]));

                if (this.MemberofBBB == true)
                {
                    this.MemberofBBBstr = dictionary["MemberofBBB"];
                }
                else
                {
                    this.MemberofBBBdisp = "none";
                }
            }
            else
            {
                this.MemberofBBBdisp = "none";
            }

            if ((dr["ISFavorite"] != System.DBNull.Value))
            {
                this.IsFavorite = ((string)(dr["ISFavorite"]));
            }

            if ((dr["ISFavoriteClass"] != System.DBNull.Value))
            {
                this.IsFavoriteClass = ((string)(dr["ISFavoriteClass"]));
            }

            if ((dr["Online"] != System.DBNull.Value))
            {
                this.Online = ((string)(dr["Online"]));
            }

            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                this.Email = ((string)(dr["EmailAddress"]));
            }

            if ((dr["Coupon"] != System.DBNull.Value))
            {
                this.Coupon = ((string)(dr["Coupon"]));
            }

            if ((dr["Appointment"] != System.DBNull.Value))
            {
                this.Appointment = ((string)(dr["Appointment"]));

                if ((dr["CarWash"] != System.DBNull.Value))
                {
                    this.CarWash = ((System.Nullable<bool>)(dr["CarWash"]));

                    if (this.CarWash == true)
                    {
                        this.Appointment = "none";
                    }
                }
            }

            if ((dr["Chat"] != System.DBNull.Value))
            {
                this.Chat = ((string)(dr["Chat"]));
            }

            if ((dr["RepairShop"] != System.DBNull.Value))
            {
                this.RepairShop = ((System.Nullable<bool>)(dr["RepairShop"]));

                if (this.RepairShop == true)
                {
                    if ((dr["Quote"] != System.DBNull.Value))
                        {
                            if ((string)(dr["Quote"]) == "block")
                            {
                                this.Quotedisp = "block";
                            }
                            else
                            {
                                this.Quotedisp = "none";
                            }
                        }
                        else
                        {
                            this.Quotedisp = "none";
                        }
                }
                else
                {
                    this.Quotedisp = "none";
                }
            }
            else
            {
                this.Quotedisp = "none";
            }

            if ((dr["IsActiveSubscription"] != System.DBNull.Value))
            {
                this.IsActiveSubscription = dr["IsActiveSubscription"].ToString();
            }
            else
            {
                this.IsActiveSubscription = "none";
            }
        }


        /// <summary>
        ///
        /// </summary>
        /// <param name="zipcode"></param>
        /// <param name="miles"></param>
        /// <returns></returns>
        public static SearchBusinessCollection SearchBusinessByZipCodeAndMiles(int zipcode, int miles)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.SearchBusinessByZipCodeAndMiles(zipcode, miles);
                SearchBusinessCollection collection = new SearchBusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        SearchBusiness obj = new SearchBusiness();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="zipcode"></param>
        /// <param name="miles"></param>
        /// <param name="nacis_code"></param>
        /// <returns></returns>
        public static SearchBusinessCollection SearchBusinessByZipCodeMilesType(int zipcode, System.Nullable<int> miles, String AutoshopType, int ContactId)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.SearchBusinessByZipCodeMilesType(zipcode, miles, AutoshopType, ContactId);
                SearchBusinessCollection collection = new SearchBusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    foreach (DataRow h in ds.Tables[0].Rows)
                    {
                        SearchBusiness obj = new SearchBusiness();
                        obj.Fill(h);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static SearchBusinessCollection UpdateSearchResult(int zipcode, System.Nullable<int> miles, string AutoshopType, string weekdayId,int contactId)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.UpdateSearchResult(zipcode, miles, AutoshopType, weekdayId, contactId);
                SearchBusinessCollection collection = new SearchBusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        SearchBusiness obj = new SearchBusiness();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        /// <summary>
        /// SearchBusinessByFavoriteItems
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static SearchBusinessCollection SearchBusinessByFavoriteItems(string items)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.SearchBusinessByFavoriteItems(items);
                SearchBusinessCollection collection = new SearchBusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        SearchBusiness obj = new SearchBusiness();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="miles"></param>
        /// <param name="nacis_code"></param>
        /// <returns></returns>
        public static SearchBusinessCollection SearchBusinessByAddress(string city, string state, System.Nullable<int> miles, System.Nullable<int> nacis_code)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.SearchBusinessByAddress(city, state, miles, nacis_code);
                SearchBusinessCollection collection = new SearchBusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        SearchBusiness obj = new SearchBusiness();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="zipcode"></param>
        /// <param name="miles"></param>
        /// <param name="nacis_code"></param>
        /// <returns></returns>
        public static SearchBusinessCollection SearchBusinessByFilter(int zipcode, System.Nullable<int> miles, System.Nullable<int> nacis_code)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.SearchBusinessByFilter(zipcode, miles, nacis_code);
                SearchBusinessCollection collection = new SearchBusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        SearchBusiness obj = new SearchBusiness();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="ZipCode"></param>
        /// <param name="Miles"></param>
        /// <returns></returns>
        public static SearchBusiness Load(int ZipCode, System.Nullable<int> Miles)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.SearchBusinessByZipCodeAndMiles(ZipCode, Miles);
                SearchBusiness obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new SearchBusiness();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static SearchBusinessCollection Get_FavoriteAutoshopList_By_ContactId(int ContactId)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.Get_FavoriteAutoshopList_By_ContactId(ContactId);
                SearchBusinessCollection collection = new SearchBusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        SearchBusiness obj = new SearchBusiness();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static SearchBusinessCollection UpdateSearchResultFromCity(string city, string state, System.Nullable<int> miles, string AutoshopType,string weekDayId,int cntId)
        {
            AutoShop.DAL.SearchBusiness dbo = null;
            try
            {
                dbo = new AutoShop.DAL.SearchBusiness();
                System.Data.DataSet ds = dbo.UpdateSearchResultFromCity(city, state, miles, AutoshopType, weekDayId, cntId);
                SearchBusinessCollection collection = new SearchBusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        SearchBusiness obj = new SearchBusiness();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="ZipCode"></param>
        /// <param name="Miles"></param>
        //public virtual void Load(int ZipCode, System.Nullable<int> Miles)
        //{
        //    AutoShop.DAL.SearchBusiness dbo = null;
        //    try
        //    {
        //        dbo = new AutoShop.DAL.SearchBusiness();
        //        System.Data.DataSet ds = dbo.SearchBusinessByZipCodeAndMiles(ZipCode, Miles);
        //        if (GlobalTools.IsSafeDataSet(ds))
        //        {
        //            if ((ds.Tables[0].Rows.Count > 0))
        //            {
        //                this.Fill(ds.Tables[0].Rows[0]);
        //            }
        //        }
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if ((dbo != null))
        //        {
        //            dbo.Dispose();
        //        }
        //    }
        //}

        //public virtual void Insert()
        //{
        //    AutoShop.DAL.SearchBusiness dbo = null;
        //    try
        //    {
        //        dbo = new AutoShop.DAL.SearchBusiness();
        //        dbo.BusinessEntity_Insert(this.BusinessIdNumber, this.BusinessName, this.Address, this.City, this.State, this.Statecode, this.Zip, this.Zip4, this.Phone, this.SicCode, this.SicDescription, this.NaicsCode, this.NaicsDescription, this.LocationType, this.AdSize, this.BusinessAtHome, this.UltimateParentId, this.UltimateParentName, this.SubsidiaryParentId, this.SubsidiaryParentName, this.Latitude, this.Longitude, this.MatchFlag, this.CbsaCode, this.CbsaName, this.FipsCode, this.County);
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if ((dbo != null))
        //        {
        //            dbo.Dispose();
        //        }
        //    }
        //}

        //public virtual void Delete()
        //{
        //    AutoShop.DAL.Businessentity dbo = null;
        //    try
        //    {
        //        dbo = new AutoShop.DAL.Businessentity();
        //        dbo.BusinessEntity_Delete(this.BusinessIdNumber);
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if ((dbo != null))
        //        {
        //            dbo.Dispose();
        //        }
        //    }
        //}

        //public virtual void Update()
        //{
        //    AutoShop.DAL.SearchBusiness dbo = null;
        //    try
        //    {
        //        dbo = new AutoShop.DAL.SearchBusiness();
        //        dbo.SearchBusinessByFilter(this.BusinessIdNumber, this.BusinessName, this.Address, this.City, this.State, this.Statecode, this.Zip, this.Zip4, this.Phone, this.SicCode, this.SicDescription, this.NaicsCode, this.NaicsDescription, this.LocationType, this.AdSize, this.BusinessAtHome, this.UltimateParentId, this.UltimateParentName, this.SubsidiaryParentId, this.SubsidiaryParentName, this.Latitude, this.Longitude, this.MatchFlag, this.CbsaCode, this.CbsaName, this.FipsCode, this.County);
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if ((dbo != null))
        //        {
        //            dbo.Dispose();
        //        }
        //    }
        //}
    }
}