using System;
using System.Collections.Generic;
using System.Text;
namespace AutoShop.BusinessObjects
{
    public partial class Subscription
    {
        public virtual System.Data.DataSet Select_SubscriptionDuration(string subType)
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                System.Data.DataSet ds = dbo.Select_SubscriptionDuration(subType);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_SubscriptionType()
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                System.Data.DataSet ds = dbo.Select_SubscriptionType();
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_SubscriptionAmount(string hdnSubdur)
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                System.Data.DataSet ds = dbo.Select_SubscriptionAmount(hdnSubdur);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

    }
}