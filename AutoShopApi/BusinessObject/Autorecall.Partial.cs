namespace AutoShop.BusinessObjects
{
    public partial class Autorecall
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _recallid;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _automobileid;

        private string _status;

        private Automobile _automobile;

        private Recall _recall;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual Automobile Automobile
        {
            get
            {
                if ((this._automobile == null))
                {
                    this._automobile = AutoShop.BusinessObjects.Automobile.Load(this._automobileid);
                }
                return this._automobile;
            }
            set
            {
                _automobile = value;
            }
        }

        public virtual Recall Recall
        {
            get
            {
                if ((this._recall == null))
                {
                    this._recall = AutoShop.BusinessObjects.Recall.Load(this._recallid);
                }
                return this._recall;
            }
            set
            {
                _recall = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._recallid = null;
            this.Isactive = null;
            this._automobileid = null;
            this.Status = string.Empty;
            this.Automobile = null;
            this.Recall = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["RecallId"] != System.DBNull.Value))
            {
                this._recallid = ((System.Nullable<int>)(dr["RecallId"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["AutomobileId"] != System.DBNull.Value))
            {
                this._automobileid = ((System.Nullable<int>)(dr["AutomobileId"]));
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                this.Status = ((string)(dr["Status"]));
            }
        }

        public static AutorecallCollection Select_AutoRecalls_By_AutomobileId(System.Nullable<int> AutomobileId)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.Select_AutoRecalls_By_AutomobileId(AutomobileId);
                AutorecallCollection collection = new AutorecallCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Autorecall obj = new Autorecall();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static AutorecallCollection Select_AutoRecalls_By_RecallId(System.Nullable<int> RecallId)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.Select_AutoRecalls_By_RecallId(RecallId);
                AutorecallCollection collection = new AutorecallCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Autorecall obj = new Autorecall();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static AutorecallCollection GetAll()
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.AutoRecall_Select_All();
                AutorecallCollection collection = new AutorecallCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Autorecall obj = new Autorecall();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Autorecall Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.AutoRecall_Select_One(RowId);
                Autorecall obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Autorecall();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.AutoRecall_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                dbo.AutoRecall_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._recallid, this.Isactive, this._automobileid, this.Status);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                dbo.AutoRecall_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                dbo.AutoRecall_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._recallid, this.Isactive, this._automobileid, this.Status);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_AutoRecalls_By_ContactId(System.Nullable<int> contactId)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.Select_AutoRecalls_By_ContactId(contactId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet 
            Recall_Details(string year, string mak, string model, string style,string AutoId)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.GET_Recall_Details(year, mak, model, style, AutoId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet GET_Recall_Details_Expand(int RecallId)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.GET_Recall_Details_Expand(RecallId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_Bulletins_By_ContactId(System.Nullable<int> contactId)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.Select_Bulletins_By_ContactId(contactId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet GET_Bulletins_Details(string year, string mak, string model, string style)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.GET_Bulletins_Details(year, mak, model, style);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet GET_Bultn_Details_Expand(int BultId)
        {
            AutoShop.DAL.Autorecall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Autorecall();
                System.Data.DataSet ds = dbo.GET_Bultn_Details_Expand(BultId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}