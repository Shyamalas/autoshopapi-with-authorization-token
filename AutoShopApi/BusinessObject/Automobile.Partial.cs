using System.Data;
namespace AutoShop.BusinessObjects
{
    public partial class Automobile
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _contactiid;

        private System.Nullable<int> _mileage;

        private string _vinnumber;

        private System.Nullable<int> _year;

        private string _manufacturer;

        private string _model;

        private string _engine;

        private string _imageName;

        private AppointmentCollection _appointmentCollection;

        private Contact _contact;

        private AutorecallCollection _autorecallCollection;

        private QuoteCollection _quoteCollection;

        private ServicehistoryCollection _servicehistoryCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<int> Mileage
        {
            get
            {
                return _mileage;
            }
            set
            {
                _mileage = value;
            }
        }

        public virtual string Vinnumber
        {
            get
            {
                return _vinnumber;
            }
            set
            {
                _vinnumber = value;
            }
        }

        public virtual System.Nullable<int> Year
        {
            get
            {
                return _year;
            }
            set
            {
                _year = value;
            }
        }

        public virtual string Manufacturer
        {
            get
            {
                return _manufacturer;
            }
            set
            {
                _manufacturer = value;
            }
        }

        public virtual string Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value;
            }
        }

        public virtual string Engine
        {
            get
            {
                return _engine;
            }
            set
            {
                _engine = value;
            }
        }

        public virtual string ImageName
        {
            get
            {
                return _imageName;
            }
            set
            {
                _imageName = value;
            }
        }
        //public virtual AppointmentCollection AppointmentCollection
        //{
        //    get
        //    {
        //        if ((this._appointmentCollection == null))
        //        {
        //            _appointmentCollection = AutoShop.BusinessObjects.Appointment.Select_Appointments_By_AutomobileId(this.Rowid);
        //        }
        //        return this._appointmentCollection;
        //    }
        //}

        public virtual Contact Contact
        {
            get
            {
                if ((this._contact == null))
                {
                    this._contact = AutoShop.BusinessObjects.Contact.Load(this._contactiid);
                }
                return this._contact;
            }
            set
            {
                _contact = value;
            }
        }

        public virtual AutorecallCollection AutorecallCollection
        {
            get
            {
                if ((this._autorecallCollection == null))
                {
                    _autorecallCollection = AutoShop.BusinessObjects.Autorecall.Select_AutoRecalls_By_AutomobileId(this.Rowid);
                }
                return this._autorecallCollection;
            }
        }

        public virtual QuoteCollection QuoteCollection
        {
            get
            {
                if ((this._quoteCollection == null))
                {
                    _quoteCollection = AutoShop.BusinessObjects.Quote.Select_Quotes_By_AutomobildeId(this.Rowid);
                }
                return this._quoteCollection;
            }
        }

        public virtual ServicehistoryCollection ServicehistoryCollection
        {
            get
            {
                if ((this._servicehistoryCollection == null))
                {
                    _servicehistoryCollection = AutoShop.BusinessObjects.Servicehistory.Select_ServiceHistorys_By_AutomobildeId(this.Rowid);
                }
                return this._servicehistoryCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._contactiid = null;
            this.Mileage = null;
            this.Vinnumber = string.Empty;
            this.Year = null;
            this.Manufacturer = string.Empty;
            this.Model = string.Empty;
            this.Engine = string.Empty;
            this._appointmentCollection = null;
            this.Contact = null;
            this._autorecallCollection = null;
            this._quoteCollection = null;
            this._servicehistoryCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["ContactIid"] != System.DBNull.Value))
            {
                this._contactiid = ((System.Nullable<int>)(dr["ContactIid"]));
            }
            if ((dr["Mileage"] != System.DBNull.Value))
            {
                this.Mileage = ((System.Nullable<int>)(dr["Mileage"]));
            }
            if ((dr["VINNumber"] != System.DBNull.Value))
            {
                this.Vinnumber = ((string)(dr["VINNumber"]));
            }
            if ((dr["Year"] != System.DBNull.Value))
            {
                this.Year = ((System.Nullable<int>)(dr["Year"]));
            }
            if ((dr["Manufacturer"] != System.DBNull.Value))
            {
                this.Manufacturer = ((string)(dr["Manufacturer"]));
            }
            if ((dr["Model"] != System.DBNull.Value))
            {
                this.Model = ((string)(dr["Model"]));
            }
            if ((dr["Engine"] != System.DBNull.Value))
            {
                this.Engine = ((string)(dr["Engine"]));
            }
        }

        //public DataSet Select_Automobiles_By_ContactIid(System.Nullable<int> ContactIid)
        //{
        //    AutoShop.DAL.Automobile dbo = null;
        //    try
        //    {
        //        dbo = new AutoShop.DAL.Automobile();
        //        System.Data.DataSet ds = dbo.Select_Automobiles_By_ContactIid(ContactIid);
        //        //AutomobileCollection collection = new AutomobileCollection();
        //        //if (GlobalTools.IsSafeDataSet(ds))
        //        //{
        //        //    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
        //        //    {
        //        //        Automobile obj = new Automobile();
        //        //        obj.Fill(ds.Tables[0].Rows[i]);
        //        //        if ((obj != null))
        //        //        {
        //        //            collection.Add(obj);
        //        //        }
        //        //    }
        //        //}
        //        return ds;
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if ((dbo != null))
        //        {
        //            dbo.Dispose();
        //        }
        //    }
        //}

        public static AutomobileCollection Select_Automobiles_By_ContactIid(System.Nullable<int> ContactIid)
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                System.Data.DataSet ds = dbo.Select_Automobiles_By_ContactIid(ContactIid);
                AutomobileCollection collection = new AutomobileCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Automobile obj = new Automobile();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public DataSet GetAll()
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                System.Data.DataSet ds = dbo.Automobile_Select_All();
                //AutomobileCollection collection = new AutomobileCollection();
                //if (GlobalTools.IsSafeDataSet(ds))
                //{
                //    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                //    {
                //        Automobile obj = new Automobile();
                //        obj.Fill(ds.Tables[0].Rows[i]);
                //        if ((obj != null))
                //        {
                //            collection.Add(obj);
                //        }
                //    }
                //}
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Automobile Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                System.Data.DataSet ds = dbo.Automobile_Select_One(RowId);
                Automobile obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Automobile();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                System.Data.DataSet ds = dbo.Automobile_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public int? Insert(int contactId, string engineId, out int ReturnAutoId)
        {
            int? autoId;
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                autoId = dbo.Automobile_Insert(contactId, engineId, out ReturnAutoId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return autoId;
        }

        public int? Delete(System.Nullable<int> RowId, out int returnID)
        {
            int? value;
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                value = dbo.Automobile_Delete(RowId, out returnID);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return value;
        }

        public virtual void Update(System.Nullable<int> RowId, System.Nullable<int> Mileage)
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                dbo.Automobile_Update(RowId, Mileage);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update_AutoImg(System.Nullable<int> RowId, string ImageName)
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                dbo.Automobile_Image_Update(RowId, ImageName);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public int? Insert_Using_StyleId(int contactId, string styleId, out int ReturnAutoId)
        {
            int? autoId;
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                autoId = dbo.Automobile_Insert_Using_StyleId(contactId, styleId, out ReturnAutoId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return autoId;
        }

        public System.Data.DataSet Select_Automobiles_List_By_ContactIid_Mobile(int contectId)
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                System.Data.DataSet ds = dbo.Select_Automobiles_List_By_ContactIid_Mobile(contectId);
                
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }            
        }

        public System.Data.DataSet Get_Automobile_Datails_By_RowId(int RowId)
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                System.Data.DataSet ds = dbo.Get_Automobile_Datails_By_RowId(RowId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public int Add_NewAutoShop_To_Sel_Qt(int quoteId, Quote qut)
        {
            int autoId;
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                autoId = dbo.Add_NewAutoShop_To_Sel_Qt(quoteId, qut.MultiCount, qut.BusMultiId, qut.MultiMileage, qut.Zipcode, qut.ExpireDay);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return autoId;
        }

        public System.Data.DataSet Select_Automobiles_List_By_ContactIid_Style(int RowId)
        {
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                System.Data.DataSet ds = dbo.Select_Automobiles_List_By_ContactIid_Style(RowId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public int Automobile_Inactive(int RowId)
        {
            int autoId;
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                autoId = dbo.Automobile_Inactive(RowId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return autoId;
        }

        public int Insert_AutomobileImage(int AutoId, string image)
        {
            int autoId;
            AutoShop.DAL.Automobile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobile();
                autoId = dbo.Automobile_Insert_Image(AutoId, image);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return autoId;

        }
    }
}