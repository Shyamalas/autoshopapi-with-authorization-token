namespace AutoShop.BusinessObjects
{
    public partial class Contact
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _type;

        private string _firstname;

        private string _lastname;

        private string _emailaddress;

        private string _homephone;

        private string _cellphone;

        private string _workphone;

        private System.Nullable<bool> _isactive;

        private string _password;

        private System.Nullable<int> _loginid;

        private string _streetaddress;

        private string _city;

        private string _state;

        private string _photo;

        private System.Nullable<int> _zipcode;

        private string _activationCode;

        public System.Nullable<int> _isOnline;
      

        private AutomobileCollection _automobileCollection;

        private BusinesscontactCollection _businesscontactCollection;

        private BusinessratingCollection _businessratingCollection;

        private ContactpreferenceCollection _contactpreferenceCollection;

        private PaymentinfoCollection _paymentinfoCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public virtual string Firstname
        {
            get
            {
                return _firstname;
            }
            set
            {
                _firstname = value;
            }
        }

        public virtual string Lastname
        {
            get
            {
                return _lastname;
            }
            set
            {
                _lastname = value;
            }
        }

        public virtual string Emailaddress
        {
            get
            {
                return _emailaddress;
            }
            set
            {
                _emailaddress = value;
            }
        }

        public virtual string Homephone
        {
            get
            {
                return _homephone;
            }
            set
            {
                _homephone = value;
            }
        }

        public virtual string Cellphone
        {
            get
            {
                return _cellphone;
            }
            set
            {
                _cellphone = value;
            }
        }

        public virtual string Workphone
        {
            get
            {
                return _workphone;
            }
            set
            {
                _workphone = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        public virtual System.Nullable<int> Loginid
        {
            get
            {
                return _loginid;
            }
            set
            {
                _loginid = value;
            }
        }

        public virtual string Streetaddress
        {
            get
            {
                return _streetaddress;
            }
            set
            {
                _streetaddress = value;
            }
        }

        public virtual string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        public virtual string State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        public virtual string Photo
        {
            get
            {
                return _photo;
            }
            set
            {
                _photo = value;
            }
        }

        public virtual System.Nullable<int> Zipcode
        {
            get
            {
                return _zipcode;
            }
            set
            {
                _zipcode = value;
            }
        }

        public virtual string ActivationCode
        {
            get
            {
                return _activationCode;
            }
            set
            {
                _activationCode = value;
            }
        }

        public virtual int? IsOnline
        {
            get
            {
                return _isOnline;
            }
            set
            {
                _isOnline = value;
            }
        }
      
        public virtual AutomobileCollection AutomobileCollection
        {
            get
            {
                if ((this._automobileCollection == null))
                {
                    _automobileCollection = AutoShop.BusinessObjects.Automobile.Select_Automobiles_By_ContactIid(this.Rowid);
                }
                return this._automobileCollection;
            }
        }

        public virtual BusinesscontactCollection BusinesscontactCollection
        {
            get
            {
                if ((this._businesscontactCollection == null))
                {
                    _businesscontactCollection = AutoShop.BusinessObjects.Businesscontact.Select_BusinessContacts_By_ContactId(this.Rowid);
                }
                return this._businesscontactCollection;
            }
        }

        public virtual BusinessratingCollection BusinessratingCollection
        {
            get
            {
                if ((this._businessratingCollection == null))
                {
                    _businessratingCollection = AutoShop.BusinessObjects.Businessrating.Select_BusinessRatings_By_ContactId(this.Rowid);
                }
                return this._businessratingCollection;
            }
        }

        public virtual ContactpreferenceCollection ContactpreferenceCollection
        {
            get
            {
                if ((this._contactpreferenceCollection == null))
                {
                    _contactpreferenceCollection = AutoShop.BusinessObjects.Contactpreference.Select_ContactPreferences_By_ContactId(this.Rowid);
                }
                return this._contactpreferenceCollection;
            }
        }

        public virtual PaymentinfoCollection PaymentinfoCollection
        {
            get
            {
                if ((this._paymentinfoCollection == null))
                {
                    _paymentinfoCollection = AutoShop.BusinessObjects.Paymentinfo.Select_PaymentInfos_By_ContactId(this.Rowid);
                }
                return this._paymentinfoCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Type = string.Empty;
            this.Firstname = string.Empty;
            this.Lastname = string.Empty;
            this.Emailaddress = string.Empty;
            this.Homephone = string.Empty;
            this.Cellphone = string.Empty;
            this.Workphone = string.Empty;
            this.Isactive = null;
            this.Password = string.Empty;
            this.Loginid = null;
            this.Streetaddress = string.Empty;
            this.City = string.Empty;
            this.State = string.Empty;
            this.Zipcode = null;
            this.IsOnline = null;
            this.ActivationCode = string.Empty;
            this._automobileCollection = null;
            this._businesscontactCollection = null;
            this._businessratingCollection = null;
            this._contactpreferenceCollection = null;
            this._paymentinfoCollection = null;          
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Type"] != System.DBNull.Value))
            {
                this.Type = ((string)(dr["Type"]));
            }
            if ((dr["FirstName"] != System.DBNull.Value))
            {
                this.Firstname = ((string)(dr["FirstName"]));
            }
            if ((dr["LastName"] != System.DBNull.Value))
            {
                this.Lastname = ((string)(dr["LastName"]));
            }
            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                this.Emailaddress = ((string)(dr["EmailAddress"]));
            }
            if ((dr["HomePhone"] != System.DBNull.Value))
            {
                this.Homephone = ((string)(dr["HomePhone"]));
            }
            if ((dr["CellPhone"] != System.DBNull.Value))
            {
                this.Cellphone = ((string)(dr["CellPhone"]));
            }
            if ((dr["WorkPhone"] != System.DBNull.Value))
            {
                this.Workphone = ((string)(dr["WorkPhone"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["Password"] != System.DBNull.Value))
            {
                this.Password = ((string)(dr["Password"]));
            }
            if ((dr["LoginId"] != System.DBNull.Value))
            {
                this.Loginid = ((System.Nullable<int>)(dr["LoginId"]));
            }
            if ((dr["StreetAddress"] != System.DBNull.Value))
            {
                this.Streetaddress = ((string)(dr["StreetAddress"]));
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                this.City = ((string)(dr["City"]));
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                this.State = ((string)(dr["State"]));
            }
            if ((dr["ZipCode"] != System.DBNull.Value))
            {
                this.Zipcode = ((System.Nullable<int>)(dr["ZipCode"]));
            }
            if ((dr["IsOnline"] != System.DBNull.Value))
            {
                this.IsOnline = ((System.Nullable<int>)(dr["IsOnline"]));
            }            
        }

        public static ContactCollection GetAll()
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                System.Data.DataSet ds = dbo.Contact_Select_All();
                ContactCollection collection = new ContactCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Contact obj = new Contact();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Contact Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                System.Data.DataSet ds = dbo.Contact_Select_One(RowId);
                Contact obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Contact();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                System.Data.DataSet ds = dbo.Contact_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public int? Insert(Contact cont,out int ReturnContactId)
        {
            int? contId;
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                contId = dbo.Contact_Insert(this.Firstname, this.Lastname, this.Emailaddress, this.Type, this.Cellphone, this.Password, this.Streetaddress, this.City, this.State, this.Zipcode,this.ActivationCode,this.Photo, out ReturnContactId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return contId;
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                dbo.Contact_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                dbo.Contact_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Type, this.Firstname, this.Lastname, this.Emailaddress, this.Homephone, this.Cellphone, this.Workphone, this.Isactive, this.Password, this.Loginid, this.Streetaddress, this.City, this.State, this.Zipcode);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void UpdateContactSetOnline(string emailAddress)
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                dbo.SetContactOnline(emailAddress);
            }
            catch (System.Exception)
            {
                
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Nullable<int> GetContactIdByEmail(string Email, out int ReturnContactId)
        {
            int? contId;
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                contId = dbo.GetContactIdByEmail(Email, out ReturnContactId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return contId;
        }
        public string CheckEmailExists(string Email)
        {
            string rtnValue;
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                rtnValue = dbo.CheckEmailExists(Email).ToString();
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return rtnValue.ToString();
        }

        public System.Nullable<int> Check_UserActive(string Email, out int ReturnValue)
        {
            int? rtnValue;
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                rtnValue = dbo.Check_UserActive(Email, out ReturnValue);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return rtnValue;
        }

        public System.Nullable<int> GetReturnBusinessIdByEmail(string Email, out int ReturnBusinessId)
        {
            int? bussId;
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                bussId = dbo.GetReturnBusinessIdByEmail(Email, out ReturnBusinessId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return bussId;
        }

        public virtual System.Nullable<int> ContactSaveByAdmin(int cntid, int busid, string fname, string lname, string email, string password, string celPhone, out int returnEmail)
        {
            int? returnvalue;
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
               returnvalue =  dbo.ContactSaveByAdmin(cntid, busid, fname, lname, email, password, celPhone, out returnEmail);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return returnvalue;
        }

        public System.Data.DataSet GetB2CProfileByID(int RowId)
        {
            AutoShop.DAL.Contact dbo = null;
            System.Data.DataSet ds;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                ds = dbo.GetB2CProfileByID(RowId);          
      
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return ds;
        }

        public System.Data.DataSet GetB2BdetailByID(int? RowId)
        {
            AutoShop.DAL.Contact dbo = null;
            System.Data.DataSet ds;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                ds = dbo.GetB2BdetailByID(RowId);

            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return ds;
        }

        public virtual int ChangeB2CPassword(string EmailAddress, string CurrentPass, string NewPass, out int ReturnValue)
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                ReturnValue = dbo.ChangeB2CPassword(EmailAddress, CurrentPass, NewPass, out ReturnValue);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return ReturnValue;
        }

        public virtual bool CheckWorkingDay(string day, int busId)
        {
            bool value;
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                value = dbo.CheckWorkingDay(day,busId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return value;
        }
    }
}