﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Reflection;

namespace AutoShop.BusinessObjects.Common
{
    public class ConvertToDataTable<T> where T : class, new()
    {
        #region Converters

        /// <summary>
        /// Converts the Collection to a DataTable
        /// </summary>
        /// <returns>the DataTable from the Collection</returns>
        public DataTable ToDataTable()
        {
            T t = new T();
            DataTable dt = CreateTable(t);

            try //fill the table
            {
                dt = FillTable(dt, Execute(), false);
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }

        private Collection<T> Execute()
        {
            //Implement Execute methods and grab data or may reference another class method
            return null;
        }

        /// <summary>
        /// Converts a Collection into a DataTable
        /// </summary>
        /// <param name="Items">Collection to be converted</param>
        /// <returns>The DataTable with all the items in the collection</returns>
        public DataTable ToDataTable(Collection<T> Items)
        {
            DataTable dt = null;

            if (Items.Count != 0) //only create from items if it's not empty
            {
                dt = CreateTable(Items[0]); //create the Table from the collection inserted
            }
            else //if items collection is empty, create an empty data table with the column names
            {
                T t = new T();
                dt = CreateTable(t);
            }

            try //fill the Table
            {
                dt = FillTable(dt, Items, false);
            }
            catch (Exception)
            {
                throw;
            }

            return dt;
        }

        /// <summary>
        /// Converts to a DataTable allowing to get an exception or a "NO RESULTS" Table
        /// </summary>
        /// <param name="AllowExceptions">Whether to return an exception or a DataTable with no results</param>
        /// <returns></returns>
        public DataTable ToDataTable(bool AllowExceptions)
        {
            T t = new T();
            DataTable dt = CreateTable(t);

            try //fill the table
            {
                dt = FillTable(dt, Execute(), AllowExceptions);
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }

        /// <summary>
        /// Converts to a DataTable according to the parameters
        /// </summary>
        /// <param name="Items">Collection to be manipulated</param>
        /// <param name="AllowExceptions">Whether to return a DataTable with "NO RESULTS" or to return an Exception</param>
        /// <returns></returns>
        public DataTable ToDataTable(Collection<T> Items, bool AllowExceptions)
        {
            DataTable dt = null;

            if (Items.Count != 0) //only create from items if it's not empty
            {
                dt = CreateTable(Items[0]); //create the Table from the collection inserted
            }
            else //if items collection is empty, create an empty data table with the column names
            {
                T t = new T(); //instantiate the type
                dt = CreateTable(t);
            }

            try //fill the Table
            {
                dt = FillTable(dt, Items, AllowExceptions);
            }
            catch (Exception)
            {
                throw;
            }

            return dt;
        }

        #region Helper Methods

        /// <summary>
        /// Creates the Table with columns named the same as the properties of the T Generic Class
        /// </summary>
        /// <param name="genericItem">T Generic Class</param>
        /// <returns>The DataTable with each column</returns>
        private DataTable CreateTable(T genericItem)
        {
            Type type = genericItem.GetType();
            PropertyInfo[] properties = type.GetProperties(); //all the properties in the type

            //DataTable
            DataTable dt = new DataTable();
            foreach (PropertyInfo p in properties)
            {
                dt.Columns.Add(p.Name); //add all columns according to the property Name in the Type T
            }
            return dt;
        }

        /// <summary>
        /// Fills the Table with the data from the Collection
        /// </summary>
        /// <param name="table">DataTable to insert the data into</param>
        /// <param name="items">Collection of Items to be inserted into the DataTable</param>
        /// <param name="AllowExceptions">Whether to automatically handle emtpy tables</param>
        /// <returns>The Filled DataTable</returns>
        private DataTable FillTable(DataTable table, Collection<T> items, bool AllowExceptions)
        {
            if (items.Count != 0)
            {
                for (int i = 0; i < items.Count; i++)//Each element in the collection is added as a row
                {
                    T t1 = items[i];
                    Type type1 = t1.GetType();
                    object[] values = new object[type1.GetProperties().Length];

                    for (int l = 0; l < type1.GetProperties().Length; l++) //for each property, add the value for the member
                    {
                        PropertyInfo prop = type1.GetProperty(table.Columns[l].ColumnName);
                        values[l] = (object)prop.GetValue(t1, null); //to add the row, a collection of objects need to be created
                    }

                    table.Rows.Add(values); //add each row to the table
                }
            }

            else //if there are no items returned, either throw a new exception or add a NO RESULTS row
            {
                if (AllowExceptions) //if user wants to handle the empty table, throw an exception
                {
                    throw new Exception("No Results found, Empty DataTable not allowed, try the AllowExceptions overload instead");
                }
                else //create a new row with the default values of each property
                {
                    T t2 = new T();
                    Type type2 = t2.GetType();
                    PropertyInfo[] properties = type2.GetProperties();
                    object[] values = new object[type2.GetProperties().Length];
                    for (int i = 0; i < values.Length; i++)
                    {
                        Type t = properties[i].PropertyType; //create new type from the property
                        if (t == typeof(System.String)) values[i] = "No results found";
                        else values[i] = System.Activator.CreateInstance(t); //create an instance to use the default value
                    }
                    table.Rows.Add(values); //add the row
                }
            }
            return table;
        }

        #endregion Helper Methods

        #endregion Converters
    }
}