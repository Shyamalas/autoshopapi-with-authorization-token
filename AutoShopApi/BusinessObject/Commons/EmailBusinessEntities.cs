﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoShop.BusinessObjects.Commons
{
    public partial class EmailBusinessEntities
    {                    
       
    }    

    public partial class B2CAppointmentCancelEmail
    {
        public string ToAddress { get; set; }
        public string Subject { get; set; }
        public string AppointmentID { get; set; }        
        public string FirstName { get; set; }
        public string AutoShopName { get; set; }
        public string StreetName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string PhoneNumber { get; set; }
        public string Year { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
        public string ServiceType { get; set; }
        public string Service { get; set; }
        public string TableContent { get; set; }
        public string[] TabContent { get; set; }
        public string AppointmentDate { get; set; }
        public string AppointmentTime { get; set; }
        public string QuoteID { get; set; }
        public string Automobile { get; set; }

        public decimal Price{ get; set; }

    }
    
}
