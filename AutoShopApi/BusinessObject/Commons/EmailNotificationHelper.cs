﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace AutoShop.BusinessObjects.Commons
{
    public partial class EmailNotificationHelper
    {

        public void B2CWelcomeEmail(EmailHelperEntities EHE)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_User_Registration_activation_Mobile.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$Emailid$$", EHE.Emailid);
            myString = myString.Replace("$$ActivationCode$$", EHE.ActivationCode);
            myString = myString.Replace("$$FName$$", EHE.firstName);

            SendEmail(myString, EHE.toAddress, EHE.subject);
        }

        private string GenerateMailTable(string[] tabContent)
        {
            string content = string.Empty;
            if (tabContent != null)
            {
                content += "<tr><td style='border:1px black solid'>Service Action</td><td style='border:1px black solid'>Service Item</td></tr>";

                for (int i = 0; i < tabContent.Length; i++)
                {
                    int temp = tabContent[i].LastIndexOf(" (");
                    int lastIndex = tabContent[i].Length;

                    if (temp > -1)
                    {
                        content += "<tr><td style='border:1px black solid'>" + tabContent[i].Substring(0, temp) + "</td><td style='border:1px black solid'>" + tabContent[i].Substring(temp + 2, lastIndex - temp - 3) + "</td></tr>";
                    }
                    else
                    {
                        content += "<tr><td style='border:1px black solid'>Empty</td><td style='border:1px black solid'>" + tabContent[i].Substring(temp + 2, lastIndex - temp - 3) + "</td></tr>";
                    }
                }
            }

            return content;
        }

        public void B2CAppointmentCancelAutoshopEmail(B2CAppointmentCancelEmail BAC, ArrayList autoShopAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Appointment_Cancel_AutoshopMail.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$AppointmentID$$", BAC.AppointmentID);
            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$AutoShopName$$", BAC.AutoShopName);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$ServiceType$$", BAC.ServiceType);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);

            SendEmail(myString, autoShopAddress, BAC.Subject);
        }

        public void B2CAppointmentCancelEmail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Appointment_Cancel.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$AppointmentID$$", BAC.AppointmentID);
            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$AutoShopName$$", BAC.AutoShopName);
            myString = myString.Replace("$$StreetName$$", BAC.StreetName);
            myString = myString.Replace("$$City$$", BAC.City);
            myString = myString.Replace("$$State$$", BAC.State);
            myString = myString.Replace("$$Zipcode$$", BAC.Zipcode);
            myString = myString.Replace("$$PhoneNumber$$", BAC.PhoneNumber);
            myString = myString.Replace("$$Year$$", BAC.Year);

            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$ServiceType$$", BAC.ServiceType);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2CAppointmentNotificationEmail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Appointment_notification.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$AppointmentID$$", BAC.AppointmentID);
            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$AutoShopName$$", BAC.AutoShopName);
            myString = myString.Replace("$$StreetName$$", BAC.StreetName);
            myString = myString.Replace("$$City$$", BAC.City);
            myString = myString.Replace("$$State$$", BAC.State);
            myString = myString.Replace("$$Zipcode$$", BAC.Zipcode);
            myString = myString.Replace("$$PhoneNumber$$", BAC.PhoneNumber);
            myString = myString.Replace("$$Year$$", BAC.Year);

            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$ServiceType$$", BAC.ServiceType);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2BAppointmentNotificationEmail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_Appointment_alert_Notification.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$AppointmentID$$", BAC.AppointmentID);
            myString = myString.Replace("$$AutoShop$$", BAC.AutoShopName);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$ServiceType$$", BAC.ServiceType);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2CQuoteCancelMail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Quote_Cancel.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$QuoteID$$", BAC.QuoteID);
            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2CQuoteRequestEmail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Quote_Request_Notification.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$QuoteID$$", BAC.QuoteID);
            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$ServiceType$$", BAC.ServiceType);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2CQuoteResponseEmail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Quote_Acceptance_Notification.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$QuoteID$$", BAC.QuoteID);
            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$Service$$", BAC.ServiceType);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);
            myString = myString.Replace("$$AppointmentID$$", BAC.AppointmentID);

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2BQuoteResponseEmail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_Quote_Acceptance_Notification.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$QuoteID$$", BAC.QuoteID);
            myString = myString.Replace("$$AutoShop$$", BAC.AutoShopName);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$Service$$", BAC.ServiceType);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);
            myString = myString.Replace("$$AppointmentID$$", BAC.AppointmentID);

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2CAppointmentCompletionMail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Appointment_Completion.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$AutoShopName$$", BAC.AutoShopName);
            myString = myString.Replace("$$StreetName$$", BAC.StreetName);
            myString = myString.Replace("$$City$$", BAC.City);
            myString = myString.Replace("$$State$$", BAC.State);
            myString = myString.Replace("$$Zipcode$$", BAC.Zipcode);
            myString = myString.Replace("$$PhoneNumber$$", BAC.PhoneNumber);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);
            myString = myString.Replace("$$AppointmentID$$", BAC.AppointmentID);

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2CAppointmentConfirmMail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Appointment_Confirmation.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$AutoShopName$$", BAC.AutoShopName);
            myString = myString.Replace("$$StreetName$$", BAC.StreetName);
            myString = myString.Replace("$$City$$", BAC.City);
            myString = myString.Replace("$$State$$", BAC.State);
            myString = myString.Replace("$$Zipcode$$", BAC.Zipcode);
            myString = myString.Replace("$$PhoneNumber$$", BAC.PhoneNumber);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void SendForgotPassEmail(string username, string password, string sub)
        {
            ArrayList ToAddress = new ArrayList();
            ToAddress.Add(username);
            string body = "Your Username & Password :\n";
            body += "Username: " + username + "\n";
            body += "Password: " + password + "\n\n\n";
            body += "Best Regards,\n Admin, \n www.1800AutoShop.com";

            SendEmail(body, ToAddress, sub);
        }

        public void ActivationEmail(string firstname, string username, string sub)
        {
            ArrayList ToAddress = new ArrayList();
            ToAddress.Add(username);

            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_User_WelcomeEmail_After_Activation.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$FirstName$$", firstname);
            SendEmail(myString, ToAddress, sub);
        }

        private bool SendEmail(string Body, ArrayList ToAddress, string Sub)
        {

            string fromAddress = ConfigurationSettings.AppSettings["FromEmail"];
            string fromPassword = ConfigurationSettings.AppSettings["Password"];
            MailMessage Msg = new MailMessage();
            MailAddress fromMail = new MailAddress(fromAddress);
            Msg.From = fromMail;

            for (int i = 0; i < ToAddress.Count; i++)
            {
                Msg.To.Add(new MailAddress(ToAddress[i].ToString()));
                
            }

            //This is for testing purpose. Needs to be removed before production launch.
            Msg.Bcc.Add(new MailAddress("sneha@honeycombsoftwares.com"));

            Msg.Subject = Sub;
            Msg.Body = Body;
            Msg.IsBodyHtml = true;
            
            var smtp = new System.Net.Mail.SmtpClient();
            {

                smtp.Host = ConfigurationSettings.AppSettings["SMTP"];
                if (ConfigurationSettings.AppSettings["SMTPPort"] != "")
                    smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["SMTPPort"]);

                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                if (ConfigurationSettings.AppSettings["Password"] != "")
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);

                smtp.Timeout = 20000;
            }

            try
            {
                //smtp.SendAsync(Msg, state);
                smtp.Send(Msg);
                return true;
            }
            catch (System.Net.Mail.SmtpException smtpExc)
            {
                if (smtpExc.StatusCode == SmtpStatusCode.GeneralFailure)
                {

                }
                else
                {
                    throw smtpExc;
                }
                return false;
            }
        }
        
        public void B2BWelcomeEmail(EmailHelperEntities B2BEHE)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_Subscription.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$FirstName$$", B2BEHE.firstName);
            myString = myString.Replace("$$AutoShopName$$", B2BEHE.autoShopName);
            myString = myString.Replace("$$SubscriptionType$$", B2BEHE.subscriptionPlanName);
            myString = myString.Replace("$$Duration$$", B2BEHE.busSubscriptionDuration + " Months");

            SendEmail(myString, B2BEHE.toAddress, B2BEHE.subject);
        }

        public void B2BRegisterWelcomeEmail(EmailHelperEntities B2BRegiEHE)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_User_Registration_Email.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$FirstName$$", B2BRegiEHE.firstName);
            myString = myString.Replace("$$AutoShopName$$", B2BRegiEHE.autoShopName);

            SendEmail(myString, B2BRegiEHE.toAddress, B2BRegiEHE.subject);
        }

        public void B2BMypageWelcomEmail(EmailHelperEntities B2BMypageEHE)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_Profile_Update.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;

            myString = myString.Replace("$$FirstName$$", B2BMypageEHE.firstName);

            SendEmail(myString, B2BMypageEHE.toAddress, B2BMypageEHE.subject);

        }

        public void B2BQuoteUpdateEmail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath(("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Quote_Response_Notification.html")));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;

            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$Year$$", BAC.Year);
            myString = myString.Replace("$$Manufacturer$$", BAC.Manufacturer);
            myString = myString.Replace("$$Model$$", BAC.Model);
            myString = myString.Replace("$$Engine$$", BAC.Engine);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$QuoteID$$", BAC.QuoteID);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void B2C_Appointment_alert(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2C_Appointment_alert.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;

            myString = myString.Replace("$$FirstName$$", BAC.FirstName);
            myString = myString.Replace("$$AutoShopName$$", BAC.AutoShopName);
            myString = myString.Replace("$$StreetName$$", BAC.StreetName);
            myString = myString.Replace("$$City$$", BAC.City);
            myString = myString.Replace("$$State$$", BAC.State);
            myString = myString.Replace("$$Zipcode$$", BAC.Zipcode);
            myString = myString.Replace("$$PhoneNumber$$", BAC.PhoneNumber);
            myString = myString.Replace("$$Automobile$$", BAC.Automobile);
            myString = myString.Replace("$$ServiceType$$", BAC.ServiceType);
            myString = myString.Replace("$$Service$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$AppointmentDate$$", BAC.AppointmentDate);
            myString = myString.Replace("$$AppointmentTime$$", BAC.AppointmentTime);
            myString = myString.Replace("$$AppointmentID$$", BAC.AppointmentID);

            SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void PlanExpireMailToUser(string ExpDate, string FirstName, string Autoshop, string subject, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/PlanExpireMailToUser.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$ExpDate$$", ExpDate);
            myString = myString.Replace("$$FirstName$$", FirstName);
            myString = myString.Replace("$$Autoshop$$", Autoshop);

            SendEmail(myString, ToAddress, subject);
        }

        public void NotificationMailToUser(string ExpDate, string FirstName, string Autoshop, string subPlan, string subject, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_Subscription_Expiration_Notice.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$FirstName$$", FirstName);
            myString = myString.Replace("$$AutoShopName$$", Autoshop);
            myString = myString.Replace("$$expireddate$$", ExpDate);
            myString = myString.Replace("$$SubscriptionType$$", subPlan);

            SendEmail(myString, ToAddress, subject);
        }

        public bool B2BQuoteNotificationMail(B2CAppointmentCancelEmail BAC, ArrayList ToAddress)
        {
            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/B2B_Quote_Request_Notification.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;

            myString = myString.Replace("$$Autoshop$$", BAC.AutoShopName);
            myString = myString.Replace("$$AutoTitle$$", BAC.Automobile);
            myString = myString.Replace("$$ServiceName$$", BAC.Service);
            myString = myString.Replace("$$TableContent$$", GenerateMailTable(BAC.TabContent));
            myString = myString.Replace("$$QuoteID$$", BAC.QuoteID);

            return SendEmail(myString, ToAddress, BAC.Subject);
        }

        public void SendResetPasswordLinkEmail(string email, string firstName, string resetLink)
        {

            StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/" + ConfigurationSettings.AppSettings["RootDir"] + "EmailTemplates/Reset_Password_Link.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;

            myString = myString.Replace("$$FirstName$$", firstName);
            myString = myString.Replace("$$ResetLink$$", resetLink);
            
            
            ArrayList ToAddress = new ArrayList();
            ToAddress.Add(email);
            reader.Close();
            SendEmail(myString, ToAddress, "Reset Password");
        }
    }
}
