﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoShop.BusinessObjects//.Commons
{
	public enum AutoShopTypes
	{
		All = 0,
		RepairShop = 1,
		BodyShop = 2,
		TireShop = 3,
		Smogcheck = 4,
		Carwash = 5,
		Towing = 6,
		Transport = 7,
		SmogcheckOnly = 8,
		GlassShop = 9,
		Dealer = 10,
		Parts = 11,
		NewcarSales = 12,
		UsedcarSales = 13
	}
	public class CommonItems
	{
	}
}
