﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;

namespace AutoShop.BusinessObjects.Common
{
    public static class Extenders
    {
        public static DataTable ToDataTable<T>(IEnumerable<T> collection, string tableName)
        {
            DataTable tbl = ToDataTable(collection);
            tbl.TableName = tableName;
            return tbl;
        }

        public static DataTable ToDataTable<T>(IEnumerable<T> collection)
        {
            DataTable dt = new DataTable();
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();
            DataColumn dc = null;
            NullableConverter nc;
            //Create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type theType;
                if (IsNullableType(pi.PropertyType))
                {
                    nc = new NullableConverter(pi.PropertyType);
                    theType = nc.UnderlyingType;
                    nc = null;
                }
                else
                    theType = pi.PropertyType;
                dc = new DataColumn();
                dc.ColumnName = pi.Name;
                dc.DataType = theType;
                dt.Columns.Add(dc);
            }
            //Populate the table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    object o = pi.GetValue(item, null);
                    if (o == null)
                        dr[pi.Name] = DBNull.Value;
                    else
                        dr[pi.Name] = pi.GetValue(item, null);
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable(object collection)
        {
            DataTable dt = new DataTable();
            Type t = collection.GetType();// typeof();
            PropertyInfo[] pia = t.GetProperties();
            DataColumn dc = null;
            NullableConverter nc;
            //Create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type theType;
                if (IsNullableType(pi.PropertyType))
                {
                    nc = new NullableConverter(pi.PropertyType);
                    theType = nc.UnderlyingType;
                    nc = null;
                }
                else
                    theType = pi.PropertyType;
                dc = new DataColumn();
                dc.ColumnName = pi.Name;
                dc.DataType = theType;
                dt.Columns.Add(dc);
            }

            // FillData(pia, dt, collection);
            //Populate the table
            //foreach (object item in collection)
            //{
            DataRow dr = dt.NewRow();
            dr.BeginEdit();
            foreach (PropertyInfo pi in pia)
            {
                object o = pi.GetValue(collection, null);
                if (o == null)
                    dr[pi.Name] = DBNull.Value;
                else
                    dr[pi.Name] = pi.GetValue(collection, null);
            }
            dr.EndEdit();
            dt.Rows.Add(dr);
            //}
            return dt;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="childCollection"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable(object collection, System.Collections.ArrayList childCollection)
        {
            DataTable dt = new DataTable();
            Type t = collection.GetType();// typeof();
            PropertyInfo[] pia = t.GetProperties();
            DataColumn dc = null;
            NullableConverter nc;
            //Create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type theType;
                if (IsNullableType(pi.PropertyType))
                {
                    nc = new NullableConverter(pi.PropertyType);
                    theType = nc.UnderlyingType;
                    nc = null;
                }
                else
                    theType = pi.PropertyType;
                dc = new DataColumn();
                dc.ColumnName = pi.Name;
                dc.DataType = theType;
                dt.Columns.Add(dc);
            }

            // FillData(pia, dt, collection);
            //Populate the table
            //foreach (object item in collection)
            //{
            DataRow dr = dt.NewRow();
            dr.BeginEdit();
            foreach (PropertyInfo pi in pia)
            {
                object o = pi.GetValue(collection, null);
                if (o == null)
                    dr[pi.Name] = DBNull.Value;
                else
                    dr[pi.Name] = pi.GetValue(collection, null);
            }
            dr.EndEdit();
            dt.Rows.Add(dr);
            //}
            return dt;
        }

        private static bool IsNullableType(Type theType)
        {
            return (theType.IsGenericType && theType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        private static DataTable CreateDataTable(PropertyInfo[] properties)
        {
            DataTable dt = new DataTable();
            DataColumn dc = null;

            foreach (PropertyInfo pi in properties)
            {
                dc = new DataColumn();
                dc.ColumnName = pi.Name;
                dc.DataType = pi.PropertyType;

                dt.Columns.Add(dc);
            }

            return dt;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="dt"></param>
        /// <param name="o"></param>
        private static void FillData(PropertyInfo[] properties, DataTable dt, Object o)
        {
            DataRow dr = dt.NewRow();

            foreach (PropertyInfo pi in properties)
                dr[pi.Name] = pi.GetValue(o, null);

            dt.Rows.Add(dr);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable(Object[] array)
        {
            PropertyInfo[] properties = array.GetType().GetElementType().GetProperties();
            DataTable dt = CreateDataTable(properties);
            if (array.Length != 0)
            {
                foreach (object o in array)
                    FillData(properties, dt, o);
            }
            return dt;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        //public static DataTable ConvertToDataTable(Object array)
        //{
        //    PropertyInfo[] properties = array.GetType().GetElementType().GetProperties();
        //    DataTable dt = CreateDataTable(properties);
        //    FillData(properties, dt, array);
        //    return dt;
        //}

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataSource"></param>
        /// <param name="fieldName"></param>
        /// <param name="sortDirection"></param>
        public static void SortList<T>(List<T> dataSource, string fieldName, SortDirection sortDirection)
        {
            PropertyInfo propInfo = typeof(T).GetProperty(fieldName);
            Comparison<T> compare = delegate(T a, T b)
            {
                bool asc = sortDirection == SortDirection.Ascending;
                object valueA = asc ? propInfo.GetValue(a, null) : propInfo.GetValue(b, null);
                object valueB = asc ? propInfo.GetValue(b, null) : propInfo.GetValue(a, null);

                return valueA is IComparable ? ((IComparable)valueA).CompareTo(valueB) : 0;
            };
            dataSource.Sort(compare);
        }

        private static List<T> CreateSortList<T>(IEnumerable<T> dataSource,
                string fieldName, SortDirection sortDirection)
        {
            List<T> returnList = new List<T>();
            returnList.AddRange(dataSource);
            PropertyInfo propInfo = typeof(T).GetProperty(fieldName);
            Comparison<T> compare = delegate(T a, T b)
            {
                bool asc = sortDirection == SortDirection.Ascending;
                object valueA = asc ? propInfo.GetValue(a, null) : propInfo.GetValue(b, null);
                object valueB = asc ? propInfo.GetValue(b, null) : propInfo.GetValue(a, null);

                return valueA is IComparable ? ((IComparable)valueA).CompareTo(valueB) : 0;
            };
            returnList.Sort(compare);
            return returnList;
        }
    }
}