﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoShop.BusinessObjects.Entities;
using AutoShop.BusinessObjects.BusinessLogic;
using System.Data;
using System.Collections;
using System.IO;
using System.Configuration;

namespace AutoShop.BusinessObjects.Commons
{
    public partial class EmailSenderHelperClass
    {
        public const string DATE_FORMATE = "MM/dd/yyyy";
        EmailHelperEntities emailHelp = new EmailHelperEntities();

        #region B2CMypage

        public void AppointmentMailToCustomerBusObj(EmailHelperEntities emailHelperEntities, int AppointId, bool flag = false)
        {
            emailHelp = emailHelperEntities;

            B2CMyPageBL bus = new B2CMyPageBL();
            AppointmentInfo appInfo = new AppointmentInfo();
            BusinessInfo dsFullDetail = bus.GetBusinessDetailByID(emailHelp.busId);
            appInfo = bus.GetAppointmentByRowIdStyle(AppointId);
            string[] ServiceItem = null;

            if (string.IsNullOrEmpty(appInfo.ServiceItem))
            {
                ServiceItem = null;
            }
            else
            {
                if (appInfo.ServiceItem != "No records")
                {
                    ServiceItem = appInfo.ServiceItem.Split(',');
                }
            }

            B2CAppointmentCancelEmail B2CcanEmail = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();

            B2CcanEmail.AutoShopName = dsFullDetail.BusinessName;
            B2CcanEmail.StreetName = dsFullDetail.StreetAddress1;
            B2CcanEmail.City = dsFullDetail.City;
            B2CcanEmail.State = dsFullDetail.State;
            B2CcanEmail.Zipcode = Convert.ToString(dsFullDetail.Zipcode);
            B2CcanEmail.PhoneNumber = dsFullDetail.BusinessPhone1;
            B2CcanEmail.AppointmentID = appInfo.AppointmentUniqueId;
            B2CcanEmail.FirstName = emailHelp.firstName;
            B2CcanEmail.Year = appInfo.ManufacturYear.ToString();
            B2CcanEmail.Manufacturer = appInfo.Manufacturer;
            B2CcanEmail.Model = appInfo.Model;
            B2CcanEmail.Engine = appInfo.Style;
            B2CcanEmail.ServiceType = appInfo.Type;
            B2CcanEmail.Service = appInfo.Name;
            B2CcanEmail.TabContent = ServiceItem;
            B2CcanEmail.AppointmentDate = Convert.ToDateTime(appInfo.AppoinmentDate).Date.ToString(DATE_FORMATE);
            B2CcanEmail.AppointmentTime = Convert.ToDateTime(appInfo.AppoinmentDate).TimeOfDay.ToString();
            B2CcanEmail.Subject = emailHelp.subject + " " + B2CcanEmail.AppointmentID;

            EmailNotifyHelp.B2CAppointmentNotificationEmail(B2CcanEmail, emailHelp.toAddress);
        }

        public void AppointmentMailToAutoShopBusObj(EmailHelperEntities emailHelperEntities, int AppointId, bool falg = false)
        {
            emailHelp = emailHelperEntities;

            B2CMyPageBL bus = new B2CMyPageBL();
            AppointmentInfo appInfo = new AppointmentInfo();
            BusinessInfo dsFullDetail = bus.GetBusinessDetailByID(emailHelp.busId);
            appInfo = bus.GetAppointmentByRowIdStyle(AppointId);

            string[] ServiceItem = null;

            if (string.IsNullOrEmpty(appInfo.ServiceItem))
            {
                ServiceItem = null;
            }
            else
            {
                if (appInfo.ServiceItem != "No records")
                {
                    ServiceItem = appInfo.ServiceItem.Split(',');
                }
            }

            B2CAppointmentCancelEmail B2CcanEmail = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();

            emailHelp.toAddress.RemoveAt(0);
            // Changes By Kishan Prajapati on 14/12/2018
            if (dsFullDetail != null)
            {
                if (dsFullDetail.EmailAddress != null)
                {
                    emailHelp.toAddress.Add(dsFullDetail.EmailAddress);
                }
                else
                    emailHelp.toAddress.Add("dipal@honeycombsoftwares.com");
                B2CcanEmail.AutoShopName = dsFullDetail.BusinessName;
            }
            //End changes

            B2CcanEmail.AppointmentID = appInfo.AppointmentUniqueId;
            B2CcanEmail.Year = appInfo.ManufacturYear.ToString();
            B2CcanEmail.Manufacturer = appInfo.Manufacturer;
            B2CcanEmail.Model = appInfo.Model;
            B2CcanEmail.Engine = appInfo.Style;
            B2CcanEmail.ServiceType = appInfo.Type;
            B2CcanEmail.Service = appInfo.Name;
            B2CcanEmail.TabContent = ServiceItem;
            B2CcanEmail.AppointmentDate = Convert.ToDateTime(appInfo.AppoinmentDate).Date.ToString(DATE_FORMATE);
            B2CcanEmail.AppointmentTime = Convert.ToDateTime(appInfo.AppoinmentDate).TimeOfDay.ToString();
            B2CcanEmail.Subject = emailHelp.subjectB2B + " " + B2CcanEmail.AppointmentID;

            EmailNotifyHelp.B2BAppointmentNotificationEmail(B2CcanEmail, emailHelp.toAddress);
        }

        public void AppointmentCancelMail(EmailHelperEntities emailHelperEntities, string appointId)
        {
            emailHelp = emailHelperEntities;

            B2CMyPageBL bus = new B2CMyPageBL();
            BusinessInfo dsFullDetail = bus.GetBusinessDetailByID(emailHelp.busId);

            Appointment app = new Appointment();
            System.Data.DataSet dsApntByCntId = app.GET_Appointment_By_Automobile(emailHelp.currentUserId, emailHelp.styleId);

            string[] ServiceItem = null;

            string searchExpression = "RowId = " + appointId;
            DataRow[] dsApntRow = dsApntByCntId.Tables[0].Select(searchExpression);

            if (dsApntRow[0]["ServiceItem"] == null)
            {
                ServiceItem = null;
            }
            else
            {
                if (dsApntRow[0]["ServiceItem"].ToString() != "No records")
                {
                    ServiceItem = dsApntRow[0]["ServiceItem"].ToString().Split(',');
                }
            }

            B2CAppointmentCancelEmail ApptCanEmail = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();
            emailHelp.autoShopAddress.Add(dsFullDetail.EmailAddress);
            ApptCanEmail.AutoShopName = dsFullDetail.BusinessName;
            ApptCanEmail.StreetName = dsFullDetail.StreetAddress1;
            ApptCanEmail.City = dsFullDetail.City;
            ApptCanEmail.State = dsFullDetail.State;
            ApptCanEmail.Zipcode = Convert.ToString(dsFullDetail.Zipcode);
            ApptCanEmail.PhoneNumber = dsFullDetail.PHONE;
            ApptCanEmail.AppointmentID = dsApntRow[0]["AppointmentUniqueId"].ToString();
            ApptCanEmail.FirstName = emailHelp.firstName;
            ApptCanEmail.Year = dsApntRow[0]["ManufacturYear"].ToString();
            ApptCanEmail.Manufacturer = dsApntRow[0]["Manufacturer"].ToString();
            ApptCanEmail.Model = dsApntRow[0]["Model"].ToString();
            ApptCanEmail.Engine = dsApntRow[0]["Style"].ToString();
            ApptCanEmail.ServiceType = dsApntRow[0]["Type"].ToString();
            ApptCanEmail.Service = dsApntRow[0]["Name"].ToString();
            ApptCanEmail.TabContent = ServiceItem;
            ApptCanEmail.AppointmentDate = Convert.ToDateTime(dsApntRow[0]["AppoinmentDate"].ToString()).Date.ToString(DATE_FORMATE);
            ApptCanEmail.AppointmentTime = Convert.ToDateTime(dsApntRow[0]["AppoinmentDate"].ToString()).TimeOfDay.ToString();
            ApptCanEmail.Subject = emailHelp.subject + " " + ApptCanEmail.AppointmentID;

            EmailNotifyHelp.B2CAppointmentCancelAutoshopEmail(ApptCanEmail, emailHelp.autoShopAddress);
            EmailNotifyHelp.B2CAppointmentCancelEmail(ApptCanEmail, emailHelp.toAddress);
        }

        public void QuoteCancelMail(EmailHelperEntities emailHelperEntities, string selectedQuoteID)
        {
            emailHelp = emailHelperEntities;

            B2CMyPageBL bus = new B2CMyPageBL();
            QuoteInfo qtInfo = new QuoteInfo();
            AutoShop.DAL.Quoteresponse qr = new AutoShop.DAL.Quoteresponse();
            DataSet dssrItem = qr.Select_QuoteResponses_By_QuoteId_srItems(Convert.ToInt32(selectedQuoteID));
            qtInfo = bus.GetQuoteByRowId(Convert.ToInt32(selectedQuoteID));

            string[] ServiceItem = null;

            DataRow quoteDetailRow = dssrItem.Tables[0].Rows[0];

            if (string.IsNullOrEmpty(quoteDetailRow.ItemArray[2].ToString()))
            {
                ServiceItem = null;
            }
            else
            {
                if (quoteDetailRow.ItemArray[2].ToString() != "No records")
                {
                    ServiceItem = quoteDetailRow.ItemArray[2].ToString().Split(',');
                }
            }

            B2CAppointmentCancelEmail QuoteCanEmail = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();

            QuoteCanEmail.QuoteID = qtInfo.QuoteUniqueID;
            QuoteCanEmail.FirstName = emailHelperEntities.firstName;
            QuoteCanEmail.Year = qtInfo.MYear.ToString();
            QuoteCanEmail.Manufacturer = qtInfo.Make;
            QuoteCanEmail.Model = qtInfo.Model;
            QuoteCanEmail.Engine = qtInfo.Style;
            QuoteCanEmail.Service = qtInfo.ServiceName;
            QuoteCanEmail.TabContent = ServiceItem;
            QuoteCanEmail.Subject = emailHelperEntities.subject + " " + QuoteCanEmail.QuoteID;

            EmailNotifyHelp.B2CQuoteCancelMail(QuoteCanEmail, emailHelp.toAddress);
        }

        public void B2CQuoteRequestEmail(EmailHelperEntities emailHelperEntities, string quoteId)
        {
            emailHelp = emailHelperEntities;

            B2CMyPageBL Bl = new B2CMyPageBL();
            QuoteInfo qtInfo = new QuoteInfo();
            qtInfo = Bl.GetQuoteByRowId(Convert.ToInt32(quoteId));

            AutoShop.DAL.Quoteresponse qr = new AutoShop.DAL.Quoteresponse();
            string[] ServiceItem = null;

            DataSet ds = qr.Select_QuoteResponses_By_QuoteId_srItems(Convert.ToInt32(quoteId));
            if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ServiceItem"].ToString()))
            {
                ServiceItem = null;
            }
            else
            {
                if (ds.Tables[0].Rows[0]["ServiceItem"].ToString() != "No records")
                {
                    ServiceItem = ds.Tables[0].Rows[0]["ServiceItem"].ToString().Split(',');
                }
            }

            B2CAppointmentCancelEmail BAC = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();

            BAC.QuoteID = qtInfo.QuoteUniqueID;
            BAC.FirstName = emailHelp.firstName;
            BAC.Year = qtInfo.MYear.ToString(); 
            BAC.Manufacturer = qtInfo.Make;
            BAC.Model = qtInfo.Model; 
            BAC.Engine = qtInfo.Style; 
            BAC.ServiceType = ds.Tables[0].Rows[0]["ServiceName"].ToString();
            BAC.TabContent = ServiceItem;
            BAC.Subject = emailHelp.subject + " " + BAC.QuoteID;

            EmailNotifyHelp.B2CQuoteRequestEmail(BAC, emailHelp.toAddress);
        }

        public void B2CQuoteResponseEmail(EmailHelperEntities emailHelperEntities, string selectedQTid, int AppointId)
        {
            emailHelp = emailHelperEntities;

            B2CMyPageBL bus = new B2CMyPageBL();
            AppointmentInfo appInfo = new AppointmentInfo();
            AutoShop.DAL.Quoteresponse qr = new AutoShop.DAL.Quoteresponse();

            DataSet ds = qr.Select_QuoteResponses_By_QuoteId_srItems(Convert.ToInt32(selectedQTid));
            appInfo = bus.GetAppointmentByRowIdStyle(AppointId);

            string[] ServiceItem = null;

            if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ServiceItem"].ToString()))
            {
                ServiceItem = null;
            }
            else
            {
                if (ds.Tables[0].Rows[0]["ServiceItem"].ToString() != "No records")
                {
                    ServiceItem = ds.Tables[0].Rows[0]["ServiceItem"].ToString().Split(',');
                }
            }

            B2CAppointmentCancelEmail BAC = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();

            BAC.QuoteID = ds.Tables[0].Rows[0]["QuoteUniqueID"].ToString();
            BAC.FirstName = emailHelp.firstName;
            BAC.Year = ds.Tables[0].Rows[0]["MYear"].ToString();
            BAC.Manufacturer = ds.Tables[0].Rows[0]["Make"].ToString();
            BAC.Model = ds.Tables[0].Rows[0]["Model"].ToString();
            BAC.Engine = ds.Tables[0].Rows[0]["Style"].ToString();
            BAC.Service = ds.Tables[0].Rows[0]["ServiceName"].ToString();
            BAC.TabContent = ServiceItem;
            BAC.AppointmentDate = Convert.ToDateTime(appInfo.AppoinmentDate).Date.ToString(DATE_FORMATE);
            BAC.AppointmentTime = Convert.ToDateTime(appInfo.AppoinmentDate).TimeOfDay.ToString();
            BAC.AppointmentID = appInfo.AppointmentUniqueId;
            BAC.Subject = emailHelp.subject + " " + BAC.QuoteID;

            EmailNotifyHelp.B2CQuoteResponseEmail(BAC, emailHelp.toAddress);
        }

        #endregion

        #region B2BMypage

        public void B2CAppointmentCompletionMail(EmailHelperEntities emailHelperEntities, int AppointId)
        {
            B2CMyPageBL bus = new B2CMyPageBL();
            B2BAppointmentResponse busResponse = new B2BAppointmentResponse();
            busResponse = bus.B2CAppointment_Completion_Detail(AppointId);

            string[] ServiceItem = null;

            if (string.IsNullOrEmpty(busResponse.ServiceItem))
            {
                ServiceItem = null;
            }
            else
            {
                if (busResponse.ServiceItem != "No records")
                {
                    ServiceItem = busResponse.ServiceItem.Split(',');
                }
            }

            B2CAppointmentCancelEmail BAC = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();

            emailHelperEntities.toAddress.Add(busResponse.EmailAddress);
            BAC.FirstName = busResponse.FirstName;
            BAC.AutoShopName = busResponse.BusinessName;
            BAC.StreetName = busResponse.StreetAddress1;
            BAC.City = busResponse.City;
            BAC.State = busResponse.State;
            BAC.Zipcode = busResponse.Zipcode.ToString();
            BAC.PhoneNumber = busResponse.BusinessPhone1;
            BAC.Year = busResponse.ManufacturYear.ToString();
            BAC.Manufacturer = busResponse.Manufacturer;
            BAC.Model = busResponse.Model;
            BAC.Engine = busResponse.Style;
            BAC.TabContent = ServiceItem;
            BAC.AppointmentDate = Convert.ToDateTime(busResponse.AppoinmentDate).Date.ToString(DATE_FORMATE);
            BAC.AppointmentTime = Convert.ToDateTime(busResponse.StartTime).ToShortTimeString();
            BAC.AppointmentID = emailHelperEntities.subject.Substring(emailHelperEntities.subject.LastIndexOf('-') + 1).Trim();
            BAC.Subject = emailHelperEntities.subject;

            EmailNotifyHelp.B2CAppointmentCompletionMail(BAC, emailHelperEntities.toAddress);
        }

        public void B2CAppointmentConfirmMail(EmailHelperEntities emailHelperEntities, int AppointId)
        {
            B2CMyPageBL bus = new B2CMyPageBL();
            B2BAppointmentResponse busResponse = new B2BAppointmentResponse();
            busResponse = bus.B2CAppointment_Completion_Detail(AppointId);

            string[] ServiceItem = null;

            if (string.IsNullOrEmpty(busResponse.ServiceItem))
            {
                ServiceItem = null;
            }
            else
            {
                if (busResponse.ServiceItem != "No records")
                {
                    ServiceItem = busResponse.ServiceItem.Split(',');
                }
            }

            B2CAppointmentCancelEmail BAC = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();

            emailHelperEntities.toAddress.Add(busResponse.EmailAddress);
            BAC.FirstName = busResponse.FirstName;
            BAC.AutoShopName = busResponse.BusinessName;
            BAC.StreetName = busResponse.StreetAddress1;
            BAC.City = busResponse.City;
            BAC.State = busResponse.State;
            BAC.Zipcode = busResponse.Zipcode.ToString();
            BAC.PhoneNumber = busResponse.BusinessPhone1;
            BAC.Year = busResponse.ManufacturYear.ToString();
            BAC.Manufacturer = busResponse.Manufacturer;
            BAC.Model = busResponse.Model;
            BAC.Engine = busResponse.Style;
            BAC.TabContent = ServiceItem;
            BAC.AppointmentDate = Convert.ToDateTime(busResponse.AppoinmentDate).Date.ToString(DATE_FORMATE);
            BAC.AppointmentTime = Convert.ToDateTime(busResponse.AppoinmentDate).TimeOfDay.ToString();
            BAC.Subject = emailHelperEntities.subject;

            EmailNotifyHelp.B2CAppointmentConfirmMail(BAC, emailHelperEntities.toAddress);
        }

        public void B2BQuoteUpdateEmail(EmailHelperEntities emailHelperEntities, string quoteId)
        {
            emailHelp = emailHelperEntities;

            B2CMyPageBL Bl = new B2CMyPageBL();
            QuoteInfo qtInfo = new QuoteInfo();
            qtInfo = Bl.GetQuoteByRowId(Convert.ToInt32(quoteId));

            AutoShop.DAL.Quoteresponse qr = new AutoShop.DAL.Quoteresponse();
            string[] ServiceItem = null;

            DataSet ds = qr.Select_QuoteResponses_By_QuoteId_srItems(Convert.ToInt32(quoteId));
            if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ServiceItem"].ToString()))
            {
                ServiceItem = null;
            }
            else
            {
                if (ds.Tables[0].Rows[0]["ServiceItem"].ToString() != "No records")
                {
                    ServiceItem = ds.Tables[0].Rows[0]["ServiceItem"].ToString().Split(',');
                }
            }

            B2CAppointmentCancelEmail BAC = new B2CAppointmentCancelEmail();
            EmailNotificationHelper EmailNotifyHelp = new EmailNotificationHelper();
            emailHelp.toAddress.Add(qtInfo.EmailAddress);
            BAC.QuoteID = qtInfo.QuoteUniqueID;
            BAC.FirstName = qtInfo.CustomerName;
            BAC.Year = qtInfo.MYear.ToString();
            BAC.Manufacturer = qtInfo.Make;
            BAC.Model = qtInfo.Model;
            BAC.Engine = qtInfo.Style;
            BAC.ServiceType = qtInfo.ServiceName;
            BAC.TabContent = ServiceItem;
            BAC.Subject = emailHelp.subject + " " + BAC.QuoteID;

            EmailNotifyHelp.B2BQuoteUpdateEmail(BAC, emailHelp.toAddress);
        }

        #endregion
    }

    public class EmailHelperEntities
    {
        public int styleId { get; set; }
        public int currentUserId { get; set; }
        public int busId { get; set; }
        public string subject { get; set; }
        public string subjectB2B { get; set; }
        public string firstName { get; set; }
        public string Emailid { get; set; }

        public string ActivationCode { get; set; }
        public string autoShopName { get; set; }
        public string subscriptionPlanName { get; set; }
        public string busSubscriptionDuration { get; set; }
        //public string currentHost { get; set; }

        private ArrayList _toAddress;
        public ArrayList toAddress
        {
            get
            {
                if (_toAddress == null)
                {
                    _toAddress = new ArrayList();
                    return _toAddress;
                }
                else
                {
                    return _toAddress;
                }
            }

        }

        private ArrayList _autoShopAddress;
        public ArrayList autoShopAddress
        {
            get
            {
                if (_autoShopAddress == null)
                {
                    _autoShopAddress = new ArrayList();
                    return _autoShopAddress;
                }
                else
                {
                    return _autoShopAddress;
                }
            }

        }
    }
}
