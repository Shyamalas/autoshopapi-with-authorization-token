﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoShop.BusinessObjects.Commons
{
    [Serializable]
    public class UserSessionInfo
    {
        public string UserName { get; set; }
        public int? ContactID { get; set; }
        public string LoginRole { get; set; }
        public bool ShowMyAutoLink { get; set; }
        public int IsActive { get; set; }
        public string Firstname { get; set; }        
    }
}
