﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;

namespace AutoShop.BusinessObjects.Common
{
    public abstract class EntityObjectSource : DataTable
    {
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public abstract DataTable Fill(object parameter);

        /// <summary>
        /// Gets the data table.
        /// </summary>
        /// <param name=”list”>The list.</param>
        /// <returns></returns>
        protected DataTable GetDataTable(System.Collections.IList list, Type typ)
        {
            DataTable dt = new DataTable();
            PropertyInfo[] pi = typ.GetProperties();
            foreach (PropertyInfo p in pi)
                dt.Columns.Add(new DataColumn(p.Name, p.PropertyType));

            foreach (object obj in list)
            {
                object[] row = new object[pi.Length];
                int i = 0;
                foreach (PropertyInfo p in pi)
                    row[i++] = p.GetValue(obj, null);

                dt.Rows.Add(row);
            }
            return dt;
        }
    }

    //public class BindableSomethingCollection : EntityDataTable
    //{
    //    public override DataTable Fill(object parameter)
    //    {
    //        SomeService somesrv = new SomeService();
    //        return GetDataTable(somesrv.GetSomeCollection(parameter as SomeType), typeof(SomeContainedType));
    //    }
    //}
}