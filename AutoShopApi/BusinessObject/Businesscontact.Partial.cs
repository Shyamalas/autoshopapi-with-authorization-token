namespace AutoShop.BusinessObjects
{
    public partial class Businesscontact
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _businessid;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _contactid;

        private Business _business;

        private Contact _contact;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual Business Business
        {
            get
            {
                if ((this._business == null))
                {
                    this._business = AutoShop.BusinessObjects.Business.Load(this._businessid);
                }
                return this._business;
            }
            set
            {
                _business = value;
            }
        }

        public virtual Contact Contact
        {
            get
            {
                if ((this._contact == null))
                {
                    this._contact = AutoShop.BusinessObjects.Contact.Load(this._contactid);
                }
                return this._contact;
            }
            set
            {
                _contact = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._businessid = null;
            this.Isactive = null;
            this._contactid = null;
            this.Business = null;
            this.Contact = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this._businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["ContactId"] != System.DBNull.Value))
            {
                this._contactid = ((System.Nullable<int>)(dr["ContactId"]));
            }
        }

        public static BusinesscontactCollection Select_BusinessContacts_By_BusinessId(System.Nullable<int> BusinessId)
        {
            AutoShop.DAL.Businesscontact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscontact();
                System.Data.DataSet ds = dbo.Select_BusinessContacts_By_BusinessId(BusinessId);
                BusinesscontactCollection collection = new BusinesscontactCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesscontact obj = new Businesscontact();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinesscontactCollection Select_BusinessContacts_By_ContactId(System.Nullable<int> ContactId)
        {
            AutoShop.DAL.Businesscontact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscontact();
                System.Data.DataSet ds = dbo.Select_BusinessContacts_By_ContactId(ContactId);
                BusinesscontactCollection collection = new BusinesscontactCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesscontact obj = new Businesscontact();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinesscontactCollection GetAll()
        {
            AutoShop.DAL.Businesscontact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscontact();
                System.Data.DataSet ds = dbo.BusinessContact_Select_All();
                BusinesscontactCollection collection = new BusinesscontactCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesscontact obj = new Businesscontact();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Businesscontact Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Businesscontact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscontact();
                System.Data.DataSet ds = dbo.BusinessContact_Select_One(RowId);
                Businesscontact obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Businesscontact();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Businesscontact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscontact();
                System.Data.DataSet ds = dbo.BusinessContact_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Businesscontact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscontact();
                dbo.BusinessContact_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._businessid, this.Isactive, this._contactid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Businesscontact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscontact();
                dbo.BusinessContact_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Businesscontact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscontact();
                dbo.BusinessContact_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._businessid, this.Isactive, this._contactid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}