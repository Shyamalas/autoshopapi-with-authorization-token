namespace AutoShop.BusinessObjects
{
    public partial class Logintype
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<bool> _isactive;

        private string _logintypeid;

        private string _description;

        private LoginCollection _loginCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual string Logintypeid
        {
            get
            {
                return _logintypeid;
            }
            set
            {
                _logintypeid = value;
            }
        }

        public virtual string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public virtual LoginCollection LoginCollection
        {
            get
            {
                if ((this._loginCollection == null))
                {
                    _loginCollection = AutoShop.BusinessObjects.Login.Select_Logins_By_LoginTypeId(this.Rowid);
                }
                return this._loginCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Isactive = null;
            this.Logintypeid = string.Empty;
            this.Description = string.Empty;
            this._loginCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["LoginTypeID"] != System.DBNull.Value))
            {
                this.Logintypeid = ((string)(dr["LoginTypeID"]));
            }
            if ((dr["Description"] != System.DBNull.Value))
            {
                this.Description = ((string)(dr["Description"]));
            }
        }

        public static LogintypeCollection GetAll()
        {
            AutoShop.DAL.Logintype dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Logintype();
                System.Data.DataSet ds = dbo.LoginType_Select_All();
                LogintypeCollection collection = new LogintypeCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Logintype obj = new Logintype();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Logintype Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Logintype dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Logintype();
                System.Data.DataSet ds = dbo.LoginType_Select_One(RowId);
                Logintype obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Logintype();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Logintype dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Logintype();
                System.Data.DataSet ds = dbo.LoginType_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Logintype dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Logintype();
                dbo.LoginType_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Isactive, this.Logintypeid, this.Description);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Logintype dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Logintype();
                dbo.LoginType_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Logintype dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Logintype();
                dbo.LoginType_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Isactive, this.Logintypeid, this.Description);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}