namespace AutoShop.BusinessObjects
{
    public partial class Customerreview
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<char> _status;

        private System.Nullable<int> _qualityrating;

        private System.Nullable<int> _satisfactionrating;

        private System.Nullable<int> _customerservice;

        private System.Nullable<int> _appointmentid;

        private Appointment _appointment;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<char> Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual System.Nullable<int> Qualityrating
        {
            get
            {
                return _qualityrating;
            }
            set
            {
                _qualityrating = value;
            }
        }

        public virtual System.Nullable<int> Satisfactionrating
        {
            get
            {
                return _satisfactionrating;
            }
            set
            {
                _satisfactionrating = value;
            }
        }

        public virtual System.Nullable<int> Customerservice
        {
            get
            {
                return _customerservice;
            }
            set
            {
                _customerservice = value;
            }
        }

        public virtual Appointment Appointment
        {
            get
            {
                if ((this._appointment == null))
                {
                    this._appointment = AutoShop.BusinessObjects.Appointment.Load(this._appointmentid);
                }
                return this._appointment;
            }
            set
            {
                _appointment = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Status = null;
            this.Qualityrating = null;
            this.Satisfactionrating = null;
            this.Customerservice = null;
            this._appointmentid = null;
            this.Appointment = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                this.Status = ((System.Nullable<char>)(dr["Status"]));
            }
            if ((dr["QualityRating"] != System.DBNull.Value))
            {
                this.Qualityrating = ((System.Nullable<int>)(dr["QualityRating"]));
            }
            if ((dr["SatisfactionRating"] != System.DBNull.Value))
            {
                this.Satisfactionrating = ((System.Nullable<int>)(dr["SatisfactionRating"]));
            }
            if ((dr["CustomerService"] != System.DBNull.Value))
            {
                this.Customerservice = ((System.Nullable<int>)(dr["CustomerService"]));
            }
            if ((dr["AppointmentId"] != System.DBNull.Value))
            {
                this._appointmentid = ((System.Nullable<int>)(dr["AppointmentId"]));
            }
        }

        public static CustomerreviewCollection Select_CustomerReviews_By_AppointmentId(System.Nullable<int> AppointmentId)
        {
            AutoShop.DAL.Customerreview dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Customerreview();
                System.Data.DataSet ds = dbo.Select_CustomerReviews_By_AppointmentId(AppointmentId);
                CustomerreviewCollection collection = new CustomerreviewCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Customerreview obj = new Customerreview();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static CustomerreviewCollection GetAll()
        {
            AutoShop.DAL.Customerreview dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Customerreview();
                System.Data.DataSet ds = dbo.CustomerReview_Select_All();
                CustomerreviewCollection collection = new CustomerreviewCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Customerreview obj = new Customerreview();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Customerreview Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Customerreview dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Customerreview();
                System.Data.DataSet ds = dbo.CustomerReview_Select_One(RowId);
                Customerreview obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Customerreview();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Customerreview dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Customerreview();
                System.Data.DataSet ds = dbo.CustomerReview_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Customerreview dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Customerreview();
                dbo.CustomerReview_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Status, this.Qualityrating, this.Satisfactionrating, this.Customerservice, this._appointmentid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Customerreview dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Customerreview();
                dbo.CustomerReview_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Customerreview dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Customerreview();
                dbo.CustomerReview_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Status, this.Qualityrating, this.Satisfactionrating, this.Customerservice, this._appointmentid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}