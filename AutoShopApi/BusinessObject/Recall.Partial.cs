namespace AutoShop.BusinessObjects
{
    public partial class Recall
    {
        private System.Nullable<int> _rowid;
        
        private System.Nullable<int> _automobileid;
        
        private string _createdby;
        
        private System.Nullable<System.DateTime> _createddate;
        
        private string _modifiedby;
        
        private System.Nullable<System.DateTime> _modifieddate;
        
        private string _recallnumber;
        
        private string _manufacturerrecallnumber;
        
        private System.Nullable<System.DateTime> _notificationdate;
        
        private System.Nullable<System.DateTime> _manufacturedfromdate;
        
        private System.Nullable<System.DateTime> _manufacturedtodate;
        
        private string _componentdescription;
        
        private string _defectdescription;
        
        private string _consequence;
        
        private string _correctiveaction;
        
        public virtual System.Nullable<int> Rowid {
            get {
                return _rowid;
            }
            set {
                _rowid = value;
            }
        }
        
        public virtual System.Nullable<int> Automobileid {
            get {
                return _automobileid;
            }
            set {
                _automobileid = value;
            }
        }
        
        public virtual string Createdby {
            get {
                return _createdby;
            }
            set {
                _createdby = value;
            }
        }
        
        public virtual System.Nullable<System.DateTime> Createddate {
            get {
                return _createddate;
            }
            set {
                _createddate = value;
            }
        }
        
        public virtual string Modifiedby {
            get {
                return _modifiedby;
            }
            set {
                _modifiedby = value;
            }
        }
        
        public virtual System.Nullable<System.DateTime> Modifieddate {
            get {
                return _modifieddate;
            }
            set {
                _modifieddate = value;
            }
        }
        
        public virtual string Recallnumber {
            get {
                return _recallnumber;
            }
            set {
                _recallnumber = value;
            }
        }
        
        public virtual string Manufacturerrecallnumber {
            get {
                return _manufacturerrecallnumber;
            }
            set {
                _manufacturerrecallnumber = value;
            }
        }
        
        public virtual System.Nullable<System.DateTime> Notificationdate {
            get {
                return _notificationdate;
            }
            set {
                _notificationdate = value;
            }
        }
        
        public virtual System.Nullable<System.DateTime> Manufacturedfromdate {
            get {
                return _manufacturedfromdate;
            }
            set {
                _manufacturedfromdate = value;
            }
        }
        
        public virtual System.Nullable<System.DateTime> Manufacturedtodate {
            get {
                return _manufacturedtodate;
            }
            set {
                _manufacturedtodate = value;
            }
        }
        
        public virtual string Componentdescription {
            get {
                return _componentdescription;
            }
            set {
                _componentdescription = value;
            }
        }
        
        public virtual string Defectdescription {
            get {
                return _defectdescription;
            }
            set {
                _defectdescription = value;
            }
        }
        
        public virtual string Consequence {
            get {
                return _consequence;
            }
            set {
                _consequence = value;
            }
        }
        
        public virtual string Correctiveaction {
            get {
                return _correctiveaction;
            }
            set {
                _correctiveaction = value;
            }
        }
        
        private void Clean() {
            this.Rowid = null;
            this.Automobileid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Recallnumber = string.Empty;
            this.Manufacturerrecallnumber = string.Empty;
            this.Notificationdate = null;
            this.Manufacturedfromdate = null;
            this.Manufacturedtodate = null;
            this.Componentdescription = string.Empty;
            this.Defectdescription = string.Empty;
            this.Consequence = string.Empty;
            this.Correctiveaction = string.Empty;
        }
        
        private void Fill(System.Data.DataRow dr) {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value)) {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["AutomobileId"] != System.DBNull.Value)) {
                this.Automobileid = ((System.Nullable<int>)(dr["AutomobileId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value)) {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value)) {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value)) {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value)) {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["RecallNumber"] != System.DBNull.Value)) {
                this.Recallnumber = ((string)(dr["RecallNumber"]));
            }
            if ((dr["ManufacturerRecallNumber"] != System.DBNull.Value)) {
                this.Manufacturerrecallnumber = ((string)(dr["ManufacturerRecallNumber"]));
            }
            if ((dr["NotificationDate"] != System.DBNull.Value)) {
                this.Notificationdate = ((System.Nullable<System.DateTime>)(dr["NotificationDate"]));
            }
            if ((dr["ManufacturedFromDate"] != System.DBNull.Value)) {
                this.Manufacturedfromdate = ((System.Nullable<System.DateTime>)(dr["ManufacturedFromDate"]));
            }
            if ((dr["ManufacturedToDate"] != System.DBNull.Value)) {
                this.Manufacturedtodate = ((System.Nullable<System.DateTime>)(dr["ManufacturedToDate"]));
            }
            if ((dr["ComponentDescription"] != System.DBNull.Value)) {
                this.Componentdescription = ((string)(dr["ComponentDescription"]));
            }
            if ((dr["DefectDescription"] != System.DBNull.Value)) {
                this.Defectdescription = ((string)(dr["DefectDescription"]));
            }
            if ((dr["Consequence"] != System.DBNull.Value)) {
                this.Consequence = ((string)(dr["Consequence"]));
            }
            if ((dr["CorrectiveAction"] != System.DBNull.Value)) {
                this.Correctiveaction = ((string)(dr["CorrectiveAction"]));
            }
        }
        
        public static RecallCollection GetAll() {
            AutoShop.DAL.Recall dbo = null;
            try {
                dbo = new AutoShop.DAL.Recall();
                System.Data.DataSet ds = dbo.Recall_Select_All();
                RecallCollection collection = new RecallCollection();
                if (GlobalTools.IsSafeDataSet(ds)) {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1)) {
                        Recall obj = new Recall();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null)) {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception ) {
                throw;
            }
            finally {
                if ((dbo != null)) {
                    dbo.Dispose();
                }
            }
        }
        
        public static Recall Load(System.Nullable<int> RowId) {
            AutoShop.DAL.Recall dbo = null;
            try {
                dbo = new AutoShop.DAL.Recall();
                System.Data.DataSet ds = dbo.Recall_Select_One(RowId);
                Recall obj = null;
                if (GlobalTools.IsSafeDataSet(ds)) {
                    if ((ds.Tables[0].Rows.Count > 0)) {
                        obj = new Recall();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception ) {
                throw;
            }
            finally {
                if ((dbo != null)) {
                    dbo.Dispose();
                }
            }
        }
        
        public virtual void Load() {
            AutoShop.DAL.Recall dbo = null;
            try {
                dbo = new AutoShop.DAL.Recall();
                System.Data.DataSet ds = dbo.Recall_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds)) {
                    if ((ds.Tables[0].Rows.Count > 0)) {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception ) {
                throw;
            }
            finally {
                if ((dbo != null)) {
                    dbo.Dispose();
                }
            }
        }
        
        public virtual void Insert() {
            AutoShop.DAL.Recall dbo = null;
            try {
                dbo = new AutoShop.DAL.Recall();
                dbo.Recall_Insert(this.Automobileid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Recallnumber, this.Manufacturerrecallnumber, this.Notificationdate, this.Manufacturedfromdate, this.Manufacturedtodate, this.Componentdescription, this.Defectdescription, this.Consequence, this.Correctiveaction);
            }
            catch (System.Exception ) {
                throw;
            }
            finally {
                if ((dbo != null)) {
                    dbo.Dispose();
                }
            }
        }
        
        public virtual void Delete() {
            AutoShop.DAL.Recall dbo = null;
            try {
                dbo = new AutoShop.DAL.Recall();
                dbo.Recall_Delete(this.Rowid);
            }
            catch (System.Exception ) {
                throw;
            }
            finally {
                if ((dbo != null)) {
                    dbo.Dispose();
                }
            }
        }
        
        public virtual void Update() {
            AutoShop.DAL.Recall dbo = null;
            try {
                dbo = new AutoShop.DAL.Recall();
                dbo.Recall_Update(this.Rowid, this.Automobileid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Recallnumber, this.Manufacturerrecallnumber, this.Notificationdate, this.Manufacturedfromdate, this.Manufacturedtodate, this.Componentdescription, this.Defectdescription, this.Consequence, this.Correctiveaction);
            }
            catch (System.Exception ) {
                throw;
            }
            finally {
                if ((dbo != null)) {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Recall_Select_All()
        {
            AutoShop.DAL.Recall dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Recall();
                System.Data.DataSet ds = dbo.Recall_Select_All();

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}
