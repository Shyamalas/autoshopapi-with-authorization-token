namespace AutoShop.BusinessObjects
{
    public partial class Quoteresponse
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _quoteid;

        private System.Nullable<int> _businessid;

        private System.Nullable<decimal> _price;

        private string _comments;

        private Quote _quote;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<int> Businessid
        {
            get
            {
                return _businessid;
            }
            set
            {
                _businessid = value;
            }
        }

        public virtual System.Nullable<decimal> Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
            }
        }

        public virtual string Comments
        {
            get
            {
                return _comments;
            }
            set
            {
                _comments = value;
            }
        }

        public virtual Quote Quote
        {
            get
            {
                if ((this._quote == null))
                {
                    this._quote = AutoShop.BusinessObjects.Quote.Load(this._quoteid);
                }
                return this._quote;
            }
            set
            {
                _quote = value;
            }
        }

        public virtual System.Nullable<int> QuoteId
        {
            get
            {
                return _quoteid;
            }
            set
            {
                _quoteid = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._quoteid = null;
            this.Businessid = null;
            this.Price = null;
            this.Comments = string.Empty;
            this.Quote = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["QuoteId"] != System.DBNull.Value))
            {
                this._quoteid = ((System.Nullable<int>)(dr["QuoteId"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this.Businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
            if ((dr["Price"] != System.DBNull.Value))
            {
                this.Price = ((System.Nullable<decimal>)(dr["Price"]));
            }
            if ((dr["Comments"] != System.DBNull.Value))
            {
                this.Comments = ((string)(dr["Comments"]));
            }
        }

        public static QuoteresponseCollection Select_QuoteResponses_By_QuoteId(System.Nullable<int> QuoteId)
        {
            AutoShop.DAL.Quoteresponse dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quoteresponse();
                System.Data.DataSet ds = dbo.Select_QuoteResponses_By_QuoteId(QuoteId);
                QuoteresponseCollection collection = new QuoteresponseCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Quoteresponse obj = new Quoteresponse();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static QuoteresponseCollection GetAll()
        {
            AutoShop.DAL.Quoteresponse dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quoteresponse();
                System.Data.DataSet ds = dbo.QuoteResponse_Select_All();
                QuoteresponseCollection collection = new QuoteresponseCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Quoteresponse obj = new Quoteresponse();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Quoteresponse Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Quoteresponse dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quoteresponse();
                System.Data.DataSet ds = dbo.QuoteResponse_Select_One(RowId);
                Quoteresponse obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Quoteresponse();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Quoteresponse dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quoteresponse();
                System.Data.DataSet ds = dbo.QuoteResponse_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Quoteresponse dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quoteresponse();
                dbo.QuoteResponse_Insert(this.Businessid, this.Price, this.Comments,this.QuoteId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Quoteresponse dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quoteresponse();
                dbo.QuoteResponse_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Quoteresponse dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quoteresponse();
                dbo.QuoteResponse_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._quoteid, this.Businessid, this.Price, this.Comments);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}