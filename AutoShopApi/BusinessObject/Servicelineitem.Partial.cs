namespace AutoShop.BusinessObjects
{
    public partial class Servicelineitem
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _serviceitem;

        private System.Nullable<bool> _isactive;

        private ServiceservicelineitemCollection _serviceservicelineitemCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Serviceitem
        {
            get
            {
                return _serviceitem;
            }
            set
            {
                _serviceitem = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual ServiceservicelineitemCollection ServiceservicelineitemCollection
        {
            get
            {
                if ((this._serviceservicelineitemCollection == null))
                {
                    _serviceservicelineitemCollection = AutoShop.BusinessObjects.Serviceservicelineitem.Select_ServiceServiceLineItems_By_ServiceLineItemId(this.Rowid);
                }
                return this._serviceservicelineitemCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Serviceitem = string.Empty;
            this.Isactive = null;
            this._serviceservicelineitemCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["ServiceItem"] != System.DBNull.Value))
            {
                this.Serviceitem = ((string)(dr["ServiceItem"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
        }

        public static ServicelineitemCollection GetAll()
        {
            AutoShop.DAL.Servicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicelineitem();
                System.Data.DataSet ds = dbo.ServiceLineItem_Select_All();
                ServicelineitemCollection collection = new ServicelineitemCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Servicelineitem obj = new Servicelineitem();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Servicelineitem Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Servicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicelineitem();
                System.Data.DataSet ds = dbo.ServiceLineItem_Select_One(RowId);
                Servicelineitem obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Servicelineitem();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Servicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicelineitem();
                System.Data.DataSet ds = dbo.ServiceLineItem_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Servicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicelineitem();
                dbo.ServiceLineItem_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Serviceitem, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Servicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicelineitem();
                dbo.ServiceLineItem_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Servicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicelineitem();
                dbo.ServiceLineItem_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Serviceitem, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}