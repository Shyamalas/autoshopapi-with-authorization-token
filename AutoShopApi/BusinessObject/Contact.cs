using System;
using System.Collections.Generic;
using System.Text;

namespace AutoShop.BusinessObjects
{
    public partial class Contact
    {
        public virtual System.Data.DataSet Select_All_State()
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                System.Data.DataSet ds = dbo.Select_All_State();
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet AdminInfo(string email)
        {
            AutoShop.DAL.Contact dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contact();
                System.Data.DataSet ds = dbo.AdminInfo(email);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

    }
}