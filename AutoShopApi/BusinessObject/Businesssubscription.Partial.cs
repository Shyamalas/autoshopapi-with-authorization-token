namespace AutoShop.BusinessObjects
{
    public partial class Businesssubscription
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _subscriptionid;

        private System.Nullable<System.DateTime> _startdate;

        private System.Nullable<System.DateTime> _enddate;

        private System.Nullable<bool> _isactive;

        private string _comments;

        private System.Nullable<int> _businessid;

        private string _status;

        private Business _business;

        private Subscription _subscription;

        private PaymentCollection _paymentCollection;

        private System.Nullable<int> subscriptionid;

        private System.Nullable<int> busnessId;

        private System.Nullable<decimal> _subAmount;

        private System.Nullable<int> _discount;

        private System.Nullable<decimal> _paidAmount;

        private System.Nullable<bool> _autoRenewal;

        public virtual System.Nullable<decimal> SubAmount
        {
            get
            {
                return _subAmount;
            }
            set
            {
                _subAmount = value;
            }
        }

        public virtual System.Nullable<int> Discount
        {
            get
            {
                return _discount;
            }
            set
            {
                _discount = value;
            }
        }

        public virtual System.Nullable<decimal> PaidAmount
        {
            get
            {
                return _paidAmount;
            }
            set
            {
                _paidAmount = value;
            }
        }

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Startdate
        {
            get
            {
                return _startdate;
            }
            set
            {
                _startdate = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Enddate
        {
            get
            {
                return _enddate;
            }
            set
            {
                _enddate = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual string Comments
        {
            get
            {
                return _comments;
            }
            set
            {
                _comments = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual Business Business
        {
            get
            {
                if ((this._business == null))
                {
                    this._business = AutoShop.BusinessObjects.Business.Load(this._businessid);
                }
                return this._business;
            }
            set
            {
                _business = value;
            }
        }

        public virtual Subscription Subscription
        {
            get
            {
                if ((this._subscription == null))
                {
                    this._subscription = AutoShop.BusinessObjects.Subscription.Load(this._subscriptionid);
                }
                return this._subscription;
            }
            set
            {
                _subscription = value;
            }
        }

        public virtual PaymentCollection PaymentCollection
        {
            get
            {
                if ((this._paymentCollection == null))
                {
                    _paymentCollection = AutoShop.BusinessObjects.Payment.Select_Payments_By_BusinessSubscriptionId(this.Rowid);
                }
                return this._paymentCollection;
            }
        }

        public virtual System.Nullable<int> Subscriptionid
        {
            get
            {
                return subscriptionid;
            }
            set
            {
                subscriptionid = value;
            }
        }

        public virtual System.Nullable<int> BusnessId
        {
            get
            {
                return busnessId;
            }
            set
            {
                busnessId = value;
            }
        }

        public virtual System.Nullable<bool> AutoRenewal
        {
            get
            {
                return _autoRenewal;
            }
            set
            {
                _autoRenewal = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._subscriptionid = null;
            this.Startdate = null;
            this.Enddate = null;
            this.Isactive = null;
            this.Comments = string.Empty;
            this._businessid = null;
            this.Status = string.Empty;
            this.Business = null;
            this.Subscription = null;
            this._paymentCollection = null;
            this.subscriptionid = null;
            this.busnessId = null;
            this.AutoRenewal = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["SubscriptionId"] != System.DBNull.Value))
            {
                this._subscriptionid = ((System.Nullable<int>)(dr["SubscriptionId"]));
            }
            if ((dr["StartDate"] != System.DBNull.Value))
            {
                this.Startdate = ((System.Nullable<System.DateTime>)(dr["StartDate"]));
            }
            if ((dr["EndDate"] != System.DBNull.Value))
            {
                this.Enddate = ((System.Nullable<System.DateTime>)(dr["EndDate"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["Comments"] != System.DBNull.Value))
            {
                this.Comments = ((string)(dr["Comments"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this._businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                this.Status = ((string)(dr["Status"]));
            }
        }

        public static BusinesssubscriptionCollection Select_BusinessSubscriptions_By_BusinessId(System.Nullable<int> BusinessId)
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                System.Data.DataSet ds = dbo.Select_BusinessSubscriptions_By_BusinessId(BusinessId);
                BusinesssubscriptionCollection collection = new BusinesssubscriptionCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesssubscription obj = new Businesssubscription();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinesssubscriptionCollection Select_BusinessSubscriptions_By_SubscriptionId(System.Nullable<int> SubscriptionId)
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                System.Data.DataSet ds = dbo.Select_BusinessSubscriptions_By_SubscriptionId(SubscriptionId);
                BusinesssubscriptionCollection collection = new BusinesssubscriptionCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesssubscription obj = new Businesssubscription();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinesssubscriptionCollection GetAll()
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                System.Data.DataSet ds = dbo.BusinessSubscription_Select_All();
                BusinesssubscriptionCollection collection = new BusinesssubscriptionCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesssubscription obj = new Businesssubscription();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Businesssubscription Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                System.Data.DataSet ds = dbo.BusinessSubscription_Select_One(RowId);
                Businesssubscription obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Businesssubscription();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                System.Data.DataSet ds = dbo.BusinessSubscription_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                dbo.BusinessSubscription_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._subscriptionid, this.Startdate, this.Enddate, this.Isactive, this.Comments, this._businessid, this.Status);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                dbo.BusinessSubscription_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                dbo.BusinessSubscription_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._subscriptionid, this.Startdate, this.Enddate, this.Isactive, this.Comments, this._businessid, this.Status);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void InsertSubscription(AutoShop.BusinessObjects.Businesssubscription bus,AutoShop.BusinessObjects.Payment pay)
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                dbo.InsertBusSubscription(bus.Createdby, bus.Modifiedby, bus.Subscriptionid, bus.Startdate, bus.Enddate, bus.BusnessId, bus.Status, pay.Paymode, pay.Name, pay.Billingstreetname, pay.Billingcity, pay.BillingState, pay.Billingzipcode, pay.Transactionid, pay.PendingReason,bus.SubAmount,bus.Discount,bus.PaidAmount,bus.AutoRenewal);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Get_BusinessSubscripton_By_ContactId(System.Nullable<int> contactId)
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                System.Data.DataSet ds = dbo.Get_BusinessSubscripton_By_ContactId(contactId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void UpdateSubscriptionDetail(AutoShop.BusinessObjects.Businesssubscription bus, AutoShop.BusinessObjects.Payment pay, int businessSubscriptionid)
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                dbo.BusinessSubscription_UpdateSubscriptionDetail(bus.Createdby, bus.Modifiedby, bus.Subscriptionid, bus.Startdate, bus.Enddate, bus.BusnessId, bus.Status, pay.Paymode, pay.Name, pay.Billingstreetname, pay.Billingcity, pay.BillingState, pay.Billingzipcode, pay.Transactionid, pay.PendingReason, businessSubscriptionid, bus.PaidAmount,bus.AutoRenewal);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void updateIsActiveBusinessSubscription(int isActive, int busSubId)
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                dbo.updateIsActiveBusinessSubscription(isActive,busSubId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual int CheckTrialPlan(int busID, out int returnCount)
        {
            AutoShop.DAL.Businesssubscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesssubscription();
                returnCount = dbo.CheckTrialPlan(busID, out returnCount);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return returnCount;
        }
    }
}