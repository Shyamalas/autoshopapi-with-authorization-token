namespace AutoShop.BusinessObjects
{
    public partial class Businessimage
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _businessid;

        private string _name;

        private string _image;

        private int? _imageNo;

        private Business _business;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
            }
        }

        public virtual System.Nullable<int> ImageNo
        {
            get
            {
                return _imageNo;
            }
            set
            {
                _imageNo = value;
            }
        }

        public virtual Business Business
        {
            get
            {
                if ((this._business == null))
                {
                    this._business = AutoShop.BusinessObjects.Business.Load(this._businessid);
                }
                return this._business;
            }
            set
            {
                _business = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Isactive = null;
            this._businessid = null;
            this.Name = string.Empty;
            this.Image = string.Empty;
            this.Business = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this._businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
            if ((dr["Name"] != System.DBNull.Value))
            {
                this.Name = ((string)(dr["Name"]));
            }
            if ((dr["Image"] != System.DBNull.Value))
            {
                this.Image = ((string)(dr["Image"]));
            }
        }

        public static BusinessimageCollection Select_BusinessImages_By_BusinessId(System.Nullable<int> BusinessId)
        {
            AutoShop.DAL.Businessimage dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessimage();
                System.Data.DataSet ds = dbo.Select_BusinessImages_By_BusinessId(BusinessId);
                BusinessimageCollection collection = new BusinessimageCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessimage obj = new Businessimage();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinessimageCollection GetAll()
        {
            AutoShop.DAL.Businessimage dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessimage();
                System.Data.DataSet ds = dbo.BusinessImage_Select_All();
                BusinessimageCollection collection = new BusinessimageCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessimage obj = new Businessimage();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Businessimage Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Businessimage dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessimage();
                System.Data.DataSet ds = dbo.BusinessImage_Select_One(RowId);
                Businessimage obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Businessimage();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Businessimage dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessimage();
                System.Data.DataSet ds = dbo.BusinessImage_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Businessimage dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessimage();
                dbo.BusinessImage_Insert(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Isactive, this._businessid, this.Name, this.Image);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Businessimage dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessimage();
                dbo.BusinessImage_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Businessimage dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessimage();
                dbo.BusinessImage_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Isactive, this._businessid, this.Name, this.Image);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}