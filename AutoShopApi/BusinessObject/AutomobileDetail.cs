﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoShop.BusinessObjects
{
    public partial class AutomobileDetail
    {
        public int RowId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int ContactId { get; set; }
        public int Mileage { get; set; }
        public string VINNumber { get; set; }
        public int Year { get; set; }
        public int MakeId { get; set; }
        public string MakeName { get; set; }
        public int ModelId { get; set; }
        public string ModelName { get; set; }
        public int SubModelId { get; set; }
        public string SubModelName { get; set; }
        public int BaseVehicleId { get; set; }
        public int EngineId { get; set; }
        public string EngineName { get; set; }
        public int Cylinders { get; set; }
        public int DriveTypeId { get; set; }
        public string DriveType { get; set; }
        public int BodyStyleId { get; set; }
        public int DoorCount { get; set; }
        public string Type { get; set; }
        public int AxleTypeId { get; set; }
        public int CabTypeId { get; set; }
        public int TransmissionId { get; set; }
        public string ControlType { get; set; }
        public string ElectronicControl { get; set; }
        public string ManufacturerCode { get; set; }
        public int Speed { get; set; }
        public int VehicleID { get; set; }
        public bool IsActive { get; set; }
        public string ImageName { get; set; }
        public string Description { get; set; } //API is sending Engine name in this field


        public int? sp_AutomobileDetail_Insert_Using_StyleId(int contactId, AutomobileDetail objAutomobileDetails, out int ReturnAutoId)
        {
            int? autoId;
            DAL.AutomobileDetailDAL dbo = new DAL.AutomobileDetailDAL();
            try
            {
                autoId = dbo.AutomobileDetail_Insert_Using_StyleId(contactId,objAutomobileDetails.Mileage,objAutomobileDetails.VINNumber, objAutomobileDetails.Year, objAutomobileDetails.MakeId, objAutomobileDetails.MakeName,
                    objAutomobileDetails.ModelId, objAutomobileDetails.ModelName, objAutomobileDetails.SubModelId, objAutomobileDetails.SubModelName,
                    objAutomobileDetails.BaseVehicleId, objAutomobileDetails.EngineId, objAutomobileDetails.Description, objAutomobileDetails.Cylinders,
                    objAutomobileDetails.DriveTypeId, objAutomobileDetails.DriveType, objAutomobileDetails.BodyStyleId,
                    objAutomobileDetails.DoorCount, objAutomobileDetails.Type, objAutomobileDetails.AxleTypeId, objAutomobileDetails.CabTypeId, objAutomobileDetails.TransmissionId,
                    objAutomobileDetails.ControlType, objAutomobileDetails.ElectronicControl, objAutomobileDetails.ManufacturerCode,objAutomobileDetails.Speed,objAutomobileDetails.VehicleID,
                    objAutomobileDetails.IsActive,objAutomobileDetails.ImageName, out ReturnAutoId);
            }
            catch (Exception ex)
            {
                throw;
            }
           
            return autoId;
        }

    }

}



