using System.Collections.Generic;
namespace AutoShop.BusinessObjects
{
    public partial class Address
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _streetaddress1;

        private string _streetaddress2;

        private string _city;

        private string _state;

        private System.Nullable<int> _zipcode;

        private System.Nullable<int> _zipcodesuffix;

        private string _country;

        private BusinessCollection _businessCollection;

        private BusinessaddressCollection _businessaddressCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Streetaddress1
        {
            get
            {
                return _streetaddress1;
            }
            set
            {
                _streetaddress1 = value;
            }
        }

        public virtual string Streetaddress2
        {
            get
            {
                return _streetaddress2;
            }
            set
            {
                _streetaddress2 = value;
            }
        }

        public virtual string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        public virtual string State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        public virtual System.Nullable<int> Zipcode
        {
            get
            {
                return _zipcode;
            }
            set
            {
                _zipcode = value;
            }
        }

        public virtual System.Nullable<int> Zipcodesuffix
        {
            get
            {
                return _zipcodesuffix;
            }
            set
            {
                _zipcodesuffix = value;
            }
        }

        public virtual string Country
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value;
            }
        }

        public virtual BusinessCollection BusinessCollection
        {
            get
            {
                if ((this._businessCollection == null))
                {
                    _businessCollection = new BusinessCollection();
                }
                return this._businessCollection;
            }
        }

        public virtual BusinessaddressCollection BusinessaddressCollection
        {
            get
            {
                if ((this._businessaddressCollection == null))
                {
                    _businessaddressCollection = new BusinessaddressCollection();
                }
                return this._businessaddressCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Streetaddress1 = string.Empty;
            this.Streetaddress2 = string.Empty;
            this.City = string.Empty;
            this.State = string.Empty;
            this.Zipcode = null;
            this.Zipcodesuffix = null;
            this.Country = string.Empty;
            this._businessCollection = null;
            this._businessaddressCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["StreetAddress1"] != System.DBNull.Value))
            {
                this.Streetaddress1 = ((string)(dr["StreetAddress1"]));
            }
            if ((dr["StreetAddress2"] != System.DBNull.Value))
            {
                this.Streetaddress2 = ((string)(dr["StreetAddress2"]));
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                this.City = ((string)(dr["City"]));
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                this.State = ((string)(dr["State"]));
            }
            if ((dr["Zipcode"] != System.DBNull.Value))
            {
                this.Zipcode = ((System.Nullable<int>)(dr["Zipcode"]));
            }
            if ((dr["ZipcodeSuffix"] != System.DBNull.Value))
            {
                this.Zipcodesuffix = ((System.Nullable<int>)(dr["ZipcodeSuffix"]));
            }
            if ((dr["Country"] != System.DBNull.Value))
            {
                this.Country = ((string)(dr["Country"]));
            }
        }

        public static AddressCollection GetAll()
        {
            AutoShop.DAL.Address dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Address();
                System.Data.DataSet ds = dbo.Address_Select_All();
                AddressCollection collection = new AddressCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Address obj = new Address();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Address Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Address dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Address();
                System.Data.DataSet ds = dbo.Address_Select_One(RowId);
                Address obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Address();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Address dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Address();
                System.Data.DataSet ds = dbo.Address_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Address dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Address();
                dbo.Address_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Streetaddress1, this.Streetaddress2, this.City, this.State, this.Zipcode, this.Zipcodesuffix, this.Country);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual int Insert_RowId()
        {
            AutoShop.DAL.Address dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Address();
                int RowId = dbo.Address_Insert_RowId(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Streetaddress1, this.Streetaddress2, this.City, this.State, this.Zipcode, this.Zipcodesuffix, this.Country);
                return RowId;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Address dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Address();
                dbo.Address_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Address dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Address();
                dbo.Address_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Streetaddress1, this.Streetaddress2, this.City, this.State, this.Zipcode, this.Zipcodesuffix, this.Country);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet GetZipCodes(int zipcode)
        {
            List<string> zipcodes = new List<string>();
            AutoShop.DAL.Address add = new AutoShop.DAL.Address();
            try
            {
                System.Data.DataSet ds = add.AutoComplete_By_ZipCode(zipcode);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((zipcodes != null))
                {
                    zipcodes.Clear();
                }
            }
            
        }
    }
}