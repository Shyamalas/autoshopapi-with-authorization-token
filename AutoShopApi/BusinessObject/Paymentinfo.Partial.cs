namespace AutoShop.BusinessObjects
{
    public partial class Paymentinfo
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _contactid;

        private System.Nullable<int> _cardnumber;

        private string _cardtype;

        private System.Nullable<int> _securitycode;

        private System.Nullable<System.DateTime> _cardexpirationdate;

        private string _name;

        private string _billingstreetname;

        private string _billingcity;

        private System.Nullable<char> _billingstate;

        private System.Nullable<int> _billingzipcode;

        private PaymentCollection _paymentCollection;

        private Contact _contact;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<int> Cardnumber
        {
            get
            {
                return _cardnumber;
            }
            set
            {
                _cardnumber = value;
            }
        }

        public virtual string Cardtype
        {
            get
            {
                return _cardtype;
            }
            set
            {
                _cardtype = value;
            }
        }

        public virtual System.Nullable<int> Securitycode
        {
            get
            {
                return _securitycode;
            }
            set
            {
                _securitycode = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Cardexpirationdate
        {
            get
            {
                return _cardexpirationdate;
            }
            set
            {
                _cardexpirationdate = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Billingstreetname
        {
            get
            {
                return _billingstreetname;
            }
            set
            {
                _billingstreetname = value;
            }
        }

        public virtual string Billingcity
        {
            get
            {
                return _billingcity;
            }
            set
            {
                _billingcity = value;
            }
        }

        public virtual System.Nullable<char> Billingstate
        {
            get
            {
                return _billingstate;
            }
            set
            {
                _billingstate = value;
            }
        }

        public virtual System.Nullable<int> Billingzipcode
        {
            get
            {
                return _billingzipcode;
            }
            set
            {
                _billingzipcode = value;
            }
        }

        public virtual PaymentCollection PaymentCollection
        {
            get
            {
                if ((this._paymentCollection == null))
                {
                    _paymentCollection = AutoShop.BusinessObjects.Payment.Select_Payments_By_PaymentInfoId(this.Rowid);
                }
                return this._paymentCollection;
            }
        }

        public virtual Contact Contact
        {
            get
            {
                if ((this._contact == null))
                {
                    this._contact = AutoShop.BusinessObjects.Contact.Load(this._contactid);
                }
                return this._contact;
            }
            set
            {
                _contact = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._contactid = null;
            this.Cardnumber = null;
            this.Cardtype = string.Empty;
            this.Securitycode = null;
            this.Cardexpirationdate = null;
            this.Name = string.Empty;
            this.Billingstreetname = string.Empty;
            this.Billingcity = string.Empty;
            this.Billingstate = null;
            this.Billingzipcode = null;
            this._paymentCollection = null;
            this.Contact = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["ContactId"] != System.DBNull.Value))
            {
                this._contactid = ((System.Nullable<int>)(dr["ContactId"]));
            }
            if ((dr["CardNumber"] != System.DBNull.Value))
            {
                this.Cardnumber = ((System.Nullable<int>)(dr["CardNumber"]));
            }
            if ((dr["CardType"] != System.DBNull.Value))
            {
                this.Cardtype = ((string)(dr["CardType"]));
            }
            if ((dr["SecurityCode"] != System.DBNull.Value))
            {
                this.Securitycode = ((System.Nullable<int>)(dr["SecurityCode"]));
            }
            if ((dr["CardExpirationDate"] != System.DBNull.Value))
            {
                this.Cardexpirationdate = ((System.Nullable<System.DateTime>)(dr["CardExpirationDate"]));
            }
            if ((dr["Name"] != System.DBNull.Value))
            {
                this.Name = ((string)(dr["Name"]));
            }
            if ((dr["BillingStreetName"] != System.DBNull.Value))
            {
                this.Billingstreetname = ((string)(dr["BillingStreetName"]));
            }
            if ((dr["BillingCity"] != System.DBNull.Value))
            {
                this.Billingcity = ((string)(dr["BillingCity"]));
            }
            if ((dr["BillingState"] != System.DBNull.Value))
            {
                this.Billingstate = ((System.Nullable<char>)(dr["BillingState"]));
            }
            if ((dr["BillingZipCode"] != System.DBNull.Value))
            {
                this.Billingzipcode = ((System.Nullable<int>)(dr["BillingZipCode"]));
            }
        }

        public static PaymentinfoCollection Select_PaymentInfos_By_ContactId(System.Nullable<int> ContactId)
        {
            AutoShop.DAL.Paymentinfo dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Paymentinfo();
                System.Data.DataSet ds = dbo.Select_PaymentInfos_By_ContactId(ContactId);
                PaymentinfoCollection collection = new PaymentinfoCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Paymentinfo obj = new Paymentinfo();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static PaymentinfoCollection GetAll()
        {
            AutoShop.DAL.Paymentinfo dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Paymentinfo();
                System.Data.DataSet ds = dbo.PaymentInfo_Select_All();
                PaymentinfoCollection collection = new PaymentinfoCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Paymentinfo obj = new Paymentinfo();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Paymentinfo Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Paymentinfo dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Paymentinfo();
                System.Data.DataSet ds = dbo.PaymentInfo_Select_One(RowId);
                Paymentinfo obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Paymentinfo();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Paymentinfo dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Paymentinfo();
                System.Data.DataSet ds = dbo.PaymentInfo_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Paymentinfo dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Paymentinfo();
                dbo.PaymentInfo_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._contactid, this.Cardnumber, this.Cardtype, this.Securitycode, this.Cardexpirationdate, this.Name, this.Billingstreetname, this.Billingcity, this.Billingstate, this.Billingzipcode);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Paymentinfo dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Paymentinfo();
                dbo.PaymentInfo_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Paymentinfo dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Paymentinfo();
                dbo.PaymentInfo_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._contactid, this.Cardnumber, this.Cardtype, this.Securitycode, this.Cardexpirationdate, this.Name, this.Billingstreetname, this.Billingcity, this.Billingstate, this.Billingzipcode);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}