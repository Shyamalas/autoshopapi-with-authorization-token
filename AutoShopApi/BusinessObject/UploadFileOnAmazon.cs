﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoShop.BusinessObjects
{
    public class UploadFileOnAmazon
    {
        public void UploadFile(byte[] imageBytes, string CurrentPath)
        {
            AutoShop.DAL.UploadFileOnAmazon dbo = new DAL.UploadFileOnAmazon();
            dbo.UploadFile(imageBytes, CurrentPath);
        }
    }
}
