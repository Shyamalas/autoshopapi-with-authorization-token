namespace AutoShop.BusinessObjects
{
    public partial class Businesscoupon
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<System.DateTime> _startdate;

        private System.Nullable<System.DateTime> _enddate;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _businessid;

        private string _name;

        private string _descriptions;

        private string _coupon;

        private System.Nullable<int> _couponno;

        private Business _business;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Startdate
        {
            get
            {
                return _startdate;
            }
            set
            {
                _startdate = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Enddate
        {
            get
            {
                return _enddate;
            }
            set
            {
                _enddate = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Descriptions
        {
            get
            {
                return _descriptions;
            }
            set
            {
                _descriptions = value;
            }
        }

        public virtual string Coupon
        {
            get
            {
                return _coupon;
            }
            set
            {
                _coupon = value;
            }
        }

        public virtual System.Nullable<int> CouponNo
        {
            get
            {
                return _couponno;
            }
            set
            {
                _couponno = value;
            }
        }

        public virtual Business Business
        {
            get
            {
                if ((this._business == null))
                {
                    this._business = AutoShop.BusinessObjects.Business.Load(this._businessid);
                }
                return this._business;
            }
            set
            {
                _business = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Startdate = null;
            this.Enddate = null;
            this.Isactive = null;
            this._businessid = null;
            this.Name = string.Empty;
            this.Descriptions = string.Empty;
            this.Coupon = string.Empty;
            this.Business = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["StartDate"] != System.DBNull.Value))
            {
                this.Startdate = ((System.Nullable<System.DateTime>)(dr["StartDate"]));
            }
            if ((dr["EndDate"] != System.DBNull.Value))
            {
                this.Enddate = ((System.Nullable<System.DateTime>)(dr["EndDate"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this._businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
            if ((dr["Name"] != System.DBNull.Value))
            {
                this.Name = ((string)(dr["Name"]));
            }
            if ((dr["Descriptions"] != System.DBNull.Value))
            {
                this.Descriptions = ((string)(dr["Descriptions"]));
            }
            if ((dr["Coupon"] != System.DBNull.Value))
            {
                this.Coupon = ((string)(dr["Coupon"]));
            }
        }

        public static BusinesscouponCollection Select_BusinessCoupons_By_BusinessId(System.Nullable<int> BusinessId)
        {
            AutoShop.DAL.Businesscoupon dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscoupon();
                System.Data.DataSet ds = dbo.Select_BusinessCoupons_By_BusinessId(BusinessId);
                BusinesscouponCollection collection = new BusinesscouponCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesscoupon obj = new Businesscoupon();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinesscouponCollection GetAll()
        {
            AutoShop.DAL.Businesscoupon dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscoupon();
                System.Data.DataSet ds = dbo.BusinessCoupon_Select_All();
                BusinesscouponCollection collection = new BusinesscouponCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesscoupon obj = new Businesscoupon();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Businesscoupon Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Businesscoupon dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscoupon();
                System.Data.DataSet ds = dbo.BusinessCoupon_Select_One(RowId);
                Businesscoupon obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Businesscoupon();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Businesscoupon dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscoupon();
                System.Data.DataSet ds = dbo.BusinessCoupon_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Businesscoupon dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscoupon();
                dbo.BusinessCoupon_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Startdate, this.Enddate, this.Isactive, this._businessid, this.Name, this.Descriptions, this.Coupon);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Businesscoupon dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscoupon();
                dbo.BusinessCoupon_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Businesscoupon dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesscoupon();
                dbo.BusinessCoupon_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Startdate, this.Enddate, this.Isactive, this._businessid, this.Name, this.Descriptions, this.Coupon);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}