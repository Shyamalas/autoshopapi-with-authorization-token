namespace AutoShop.BusinessObjects
{
    public partial class Automobilemanufacturer
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _manufacturer;

        private System.Nullable<int> _yearid;

        private string _description;

        private System.Nullable<bool> _isactive;

        private Automobileyear _automobileyear;

        private AutomobilemodelCollection _automobilemodelCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Manufacturer
        {
            get
            {
                return _manufacturer;
            }
            set
            {
                _manufacturer = value;
            }
        }

        public virtual string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual Automobileyear Automobileyear
        {
            get
            {
                if ((this._automobileyear == null))
                {
                    this._automobileyear = AutoShop.BusinessObjects.Automobileyear.Load(this._yearid);
                }
                return this._automobileyear;
            }
            set
            {
                _automobileyear = value;
            }
        }

        public virtual AutomobilemodelCollection AutomobilemodelCollection
        {
            get
            {
                if ((this._automobilemodelCollection == null))
                {
                    _automobilemodelCollection = AutoShop.BusinessObjects.Automobilemodel.Select_AutomobileModels_By_ManufacturerId(this.Rowid);
                }
                return this._automobilemodelCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Manufacturer = string.Empty;
            this._yearid = null;
            this.Description = string.Empty;
            this.Isactive = null;
            this.Automobileyear = null;
            this._automobilemodelCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Manufacturer"] != System.DBNull.Value))
            {
                this.Manufacturer = ((string)(dr["Manufacturer"]));
            }
            if ((dr["YearId"] != System.DBNull.Value))
            {
                this._yearid = ((System.Nullable<int>)(dr["YearId"]));
            }
            if ((dr["Description"] != System.DBNull.Value))
            {
                this.Description = ((string)(dr["Description"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
        }

        public static AutomobilemanufacturerCollection Select_AutomobileManufacturers_By_YearId(System.Nullable<int> YearId)
        {
            AutoShop.DAL.Automobilemanufacturer dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemanufacturer();
                System.Data.DataSet ds = dbo.Select_AutomobileManufacturers_By_YearId(YearId);
                AutomobilemanufacturerCollection collection = new AutomobilemanufacturerCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Automobilemanufacturer obj = new Automobilemanufacturer();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static AutomobilemanufacturerCollection GetAll()
        {
            AutoShop.DAL.Automobilemanufacturer dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemanufacturer();
                System.Data.DataSet ds = dbo.AutomobileManufacturer_Select_All();
                AutomobilemanufacturerCollection collection = new AutomobilemanufacturerCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Automobilemanufacturer obj = new Automobilemanufacturer();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Automobilemanufacturer Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Automobilemanufacturer dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemanufacturer();
                System.Data.DataSet ds = dbo.AutomobileManufacturer_Select_One(RowId);
                Automobilemanufacturer obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Automobilemanufacturer();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Automobilemanufacturer dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemanufacturer();
                System.Data.DataSet ds = dbo.AutomobileManufacturer_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Automobilemanufacturer dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemanufacturer();
                dbo.AutomobileManufacturer_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Manufacturer, this._yearid, this.Description, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Automobilemanufacturer dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemanufacturer();
                dbo.AutomobileManufacturer_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Automobilemanufacturer dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemanufacturer();
                dbo.AutomobileManufacturer_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Manufacturer, this._yearid, this.Description, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}