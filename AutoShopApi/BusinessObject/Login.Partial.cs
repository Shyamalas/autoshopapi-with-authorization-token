namespace AutoShop.BusinessObjects
{
    public partial class Login
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _loginname;

        private System.Nullable<int> _logintypeid;

        private string _screenname;

        private string _password;

        private System.Nullable<bool> _isactive;

        private Logintype _logintype;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Loginname
        {
            get
            {
                return _loginname;
            }
            set
            {
                _loginname = value;
            }
        }

        public virtual string Screenname
        {
            get
            {
                return _screenname;
            }
            set
            {
                _screenname = value;
            }
        }

        public virtual string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual Logintype Logintype
        {
            get
            {
                if ((this._logintype == null))
                {
                    this._logintype = AutoShop.BusinessObjects.Logintype.Load(this._logintypeid);
                }
                return this._logintype;
            }
            set
            {
                _logintype = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Loginname = string.Empty;
            this._logintypeid = null;
            this.Screenname = string.Empty;
            this.Password = string.Empty;
            this.Isactive = null;
            this.Logintype = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["LoginName"] != System.DBNull.Value))
            {
                this.Loginname = ((string)(dr["LoginName"]));
            }
            if ((dr["LoginTypeId"] != System.DBNull.Value))
            {
                this._logintypeid = ((System.Nullable<int>)(dr["LoginTypeId"]));
            }
            if ((dr["ScreenName"] != System.DBNull.Value))
            {
                this.Screenname = ((string)(dr["ScreenName"]));
            }
            if ((dr["Password"] != System.DBNull.Value))
            {
                this.Password = ((string)(dr["Password"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
        }

        public static LoginCollection Select_Logins_By_LoginTypeId(System.Nullable<int> LoginTypeId)
        {
            AutoShop.DAL.Login dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Login();
                System.Data.DataSet ds = dbo.Select_Logins_By_LoginTypeId(LoginTypeId);
                LoginCollection collection = new LoginCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Login obj = new Login();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static LoginCollection GetAll()
        {
            AutoShop.DAL.Login dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Login();
                System.Data.DataSet ds = dbo.Login_Select_All();
                LoginCollection collection = new LoginCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Login obj = new Login();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Login Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Login dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Login();
                System.Data.DataSet ds = dbo.Login_Select_One(RowId);
                Login obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Login();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Login dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Login();
                System.Data.DataSet ds = dbo.Login_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Login dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Login();
                dbo.Login_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Loginname, this._logintypeid, this.Screenname, this.Password, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Login dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Login();
                dbo.Login_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Login dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Login();
                dbo.Login_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Loginname, this._logintypeid, this.Screenname, this.Password, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}