namespace AutoShop.BusinessObjects
{
    public partial class Holiday
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<System.DateTime> _holidaydate;

        private string _holidaydescription;

        private System.Nullable<bool> _isactive;

        private BusinessholidayCollection _businessholidayCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Holidaydate
        {
            get
            {
                return _holidaydate;
            }
            set
            {
                _holidaydate = value;
            }
        }

        public virtual string Holidaydescription
        {
            get
            {
                return _holidaydescription;
            }
            set
            {
                _holidaydescription = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual BusinessholidayCollection BusinessholidayCollection
        {
            get
            {
                if ((this._businessholidayCollection == null))
                {
                    _businessholidayCollection = AutoShop.BusinessObjects.Businessholiday.Select_BusinessHolidays_By_HolidayId(this.Rowid);
                }
                return this._businessholidayCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Holidaydate = null;
            this.Holidaydescription = string.Empty;
            this.Isactive = null;
            this._businessholidayCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["HolidayDate"] != System.DBNull.Value))
            {
                this.Holidaydate = ((System.Nullable<System.DateTime>)(dr["HolidayDate"]));
            }
            if ((dr["HolidayDescription"] != System.DBNull.Value))
            {
                this.Holidaydescription = ((string)(dr["HolidayDescription"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
        }

        public static HolidayCollection GetAll()
        {
            AutoShop.DAL.Holiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Holiday();
                System.Data.DataSet ds = dbo.Holiday_Select_All();
                HolidayCollection collection = new HolidayCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Holiday obj = new Holiday();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Holiday Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Holiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Holiday();
                System.Data.DataSet ds = dbo.Holiday_Select_One(RowId);
                Holiday obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Holiday();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Holiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Holiday();
                System.Data.DataSet ds = dbo.Holiday_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Holiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Holiday();
                dbo.Holiday_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Holidaydate, this.Holidaydescription, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Holiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Holiday();
                dbo.Holiday_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Holiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Holiday();
                dbo.Holiday_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Holidaydate, this.Holidaydescription, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}