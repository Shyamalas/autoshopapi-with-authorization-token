namespace AutoShop.BusinessObjects
{
    public partial class Businessrating
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _qualityrating;

        private System.Nullable<int> _customerservicerating;

        private string _notes;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _contactid;

        private Contact _contact;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<int> Qualityrating
        {
            get
            {
                return _qualityrating;
            }
            set
            {
                _qualityrating = value;
            }
        }

        public virtual System.Nullable<int> Customerservicerating
        {
            get
            {
                return _customerservicerating;
            }
            set
            {
                _customerservicerating = value;
            }
        }

        public virtual string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual Contact Contact
        {
            get
            {
                if ((this._contact == null))
                {
                    this._contact = AutoShop.BusinessObjects.Contact.Load(this._contactid);
                }
                return this._contact;
            }
            set
            {
                _contact = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Qualityrating = null;
            this.Customerservicerating = null;
            this.Notes = string.Empty;
            this.Isactive = null;
            this._contactid = null;
            this.Contact = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["QualityRating"] != System.DBNull.Value))
            {
                this.Qualityrating = ((System.Nullable<int>)(dr["QualityRating"]));
            }
            if ((dr["CustomerServiceRating"] != System.DBNull.Value))
            {
                this.Customerservicerating = ((System.Nullable<int>)(dr["CustomerServiceRating"]));
            }
            if ((dr["Notes"] != System.DBNull.Value))
            {
                this.Notes = ((string)(dr["Notes"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["ContactId"] != System.DBNull.Value))
            {
                this._contactid = ((System.Nullable<int>)(dr["ContactId"]));
            }
        }

        public static BusinessratingCollection Select_BusinessRatings_By_ContactId(System.Nullable<int> ContactId)
        {
            AutoShop.DAL.Businessrating dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessrating();
                System.Data.DataSet ds = dbo.Select_BusinessRatings_By_ContactId(ContactId);
                BusinessratingCollection collection = new BusinessratingCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessrating obj = new Businessrating();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinessratingCollection GetAll()
        {
            AutoShop.DAL.Businessrating dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessrating();
                System.Data.DataSet ds = dbo.BusinessRating_Select_All();
                BusinessratingCollection collection = new BusinessratingCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessrating obj = new Businessrating();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Businessrating Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Businessrating dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessrating();
                System.Data.DataSet ds = dbo.BusinessRating_Select_One(RowId);
                Businessrating obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Businessrating();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Businessrating dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessrating();
                System.Data.DataSet ds = dbo.BusinessRating_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Businessrating dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessrating();
                dbo.BusinessRating_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Qualityrating, this.Customerservicerating, this.Notes, this.Isactive, this._contactid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Businessrating dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessrating();
                dbo.BusinessRating_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Businessrating dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessrating();
                dbo.BusinessRating_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Qualityrating, this.Customerservicerating, this.Notes, this.Isactive, this._contactid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}