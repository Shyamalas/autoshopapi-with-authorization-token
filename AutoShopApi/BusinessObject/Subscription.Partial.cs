namespace AutoShop.BusinessObjects
{
    public partial class Subscription
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _type;

        private System.Nullable<long> _amount;

        private System.Nullable<int> _validationdays;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _frequency;

        private BusinesssubscriptionCollection _businesssubscriptionCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public virtual System.Nullable<long> Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        public virtual System.Nullable<int> Validationdays
        {
            get
            {
                return _validationdays;
            }
            set
            {
                _validationdays = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual System.Nullable<int> Frequency
        {
            get
            {
                return _frequency;
            }
            set
            {
                _frequency = value;
            }
        }

        public virtual BusinesssubscriptionCollection BusinesssubscriptionCollection
        {
            get
            {
                if ((this._businesssubscriptionCollection == null))
                {
                    _businesssubscriptionCollection = AutoShop.BusinessObjects.Businesssubscription.Select_BusinessSubscriptions_By_SubscriptionId(this.Rowid);
                }
                return this._businesssubscriptionCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Type = string.Empty;
            this.Amount = null;
            this.Validationdays = null;
            this.Isactive = null;
            this.Frequency = null;
            this._businesssubscriptionCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Type"] != System.DBNull.Value))
            {
                this.Type = ((string)(dr["Type"]));
            }
            if ((dr["Amount"] != System.DBNull.Value))
            {
                this.Amount = ((System.Nullable<long>)(dr["Amount"]));
            }
            if ((dr["ValidationDays"] != System.DBNull.Value))
            {
                this.Validationdays = ((System.Nullable<int>)(dr["ValidationDays"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["Frequency"] != System.DBNull.Value))
            {
                this.Frequency = ((System.Nullable<int>)(dr["Frequency"]));
            }
        }

        public static SubscriptionCollection GetAll()
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                System.Data.DataSet ds = dbo.Subscription_Select_All();
                SubscriptionCollection collection = new SubscriptionCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Subscription obj = new Subscription();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Subscription Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                System.Data.DataSet ds = dbo.Subscription_Select_One(RowId);
                Subscription obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Subscription();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                System.Data.DataSet ds = dbo.Subscription_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                dbo.Subscription_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Type, this.Amount, this.Validationdays, this.Isactive, this.Frequency);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                dbo.Subscription_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Subscription dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Subscription();
                dbo.Subscription_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Type, this.Amount, this.Validationdays, this.Isactive, this.Frequency);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}