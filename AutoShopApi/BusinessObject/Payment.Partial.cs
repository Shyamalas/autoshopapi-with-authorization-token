namespace AutoShop.BusinessObjects
{
    public partial class Payment
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _contactid;

        private System.Nullable<int> _cardnumber;

        private string _cardtype;

        private System.Nullable<int> _securitycode;

        private System.Nullable<System.DateTime> _cardexpirationdate;

        private string _name;

        private string _billingstreetname;

        private string _billingcity;

        private System.Nullable<char> _billingstate;

        private System.Nullable<int> _billingzipcode;

        private string _transactionid;

        private string _transactionxml;

        private string _status;

        private System.Nullable<int> _businesssubscriptionid;

        private System.Nullable<int> _paymentinfoid;

        private Businesssubscription _businesssubscription;

        private Paymentinfo _paymentinfo;

        private string _payMode;

        private string _billingState;

        private string _pendingreason;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<int> Contactid
        {
            get
            {
                return _contactid;
            }
            set
            {
                _contactid = value;
            }
        }

        public virtual System.Nullable<int> Cardnumber
        {
            get
            {
                return _cardnumber;
            }
            set
            {
                _cardnumber = value;
            }
        }

        public virtual string Cardtype
        {
            get
            {
                return _cardtype;
            }
            set
            {
                _cardtype = value;
            }
        }

        public virtual System.Nullable<int> Securitycode
        {
            get
            {
                return _securitycode;
            }
            set
            {
                _securitycode = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Cardexpirationdate
        {
            get
            {
                return _cardexpirationdate;
            }
            set
            {
                _cardexpirationdate = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Billingstreetname
        {
            get
            {
                return _billingstreetname;
            }
            set
            {
                _billingstreetname = value;
            }
        }

        public virtual string Billingcity
        {
            get
            {
                return _billingcity;
            }
            set
            {
                _billingcity = value;
            }
        }

        public virtual System.Nullable<char> Billingstate
        {
            get
            {
                return _billingstate;
            }
            set
            {
                _billingstate = value;
            }
        }

        public virtual System.Nullable<int> Billingzipcode
        {
            get
            {
                return _billingzipcode;
            }
            set
            {
                _billingzipcode = value;
            }
        }

        public virtual string Transactionid
        {
            get
            {
                return _transactionid;
            }
            set
            {
                _transactionid = value;
            }
        }

        public virtual string Transactionxml
        {
            get
            {
                return _transactionxml;
            }
            set
            {
                _transactionxml = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual string Paymode
        {
            get
            {
                return _payMode;
            }
            set
            {
                _payMode = value;
            }
        }

        public virtual string BillingState
        {
            get
            {
                return _billingState;
            }
            set
            {
                _billingState = value;
            }
        }


        public virtual string PendingReason
        {
            get
            {
                return _pendingreason;
            }
            set
            {
                _pendingreason = value;
            }
        }


        public virtual Businesssubscription Businesssubscription
        {
            get
            {
                if ((this._businesssubscription == null))
                {
                    this._businesssubscription = AutoShop.BusinessObjects.Businesssubscription.Load(this._businesssubscriptionid);
                }
                return this._businesssubscription;
            }
            set
            {
                _businesssubscription = value;
            }
        }

        public virtual Paymentinfo Paymentinfo
        {
            get
            {
                if ((this._paymentinfo == null))
                {
                    this._paymentinfo = AutoShop.BusinessObjects.Paymentinfo.Load(this._paymentinfoid);
                }
                return this._paymentinfo;
            }
            set
            {
                _paymentinfo = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Contactid = null;
            this.Cardnumber = null;
            this.Cardtype = string.Empty;
            this.Securitycode = null;
            this.Cardexpirationdate = null;
            this.Name = string.Empty;
            this.Billingstreetname = string.Empty;
            this.Billingcity = string.Empty;
            this.Billingstate = null;
            this.Billingzipcode = null;
            this.Transactionid = null;
            this.Transactionxml = string.Empty;
            this.Status = string.Empty;
            this._businesssubscriptionid = null;
            this._paymentinfoid = null;
            this.Businesssubscription = null;
            this.Paymentinfo = null;
            this.BillingState = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["ContactId"] != System.DBNull.Value))
            {
                this.Contactid = ((System.Nullable<int>)(dr["ContactId"]));
            }
            if ((dr["CardNumber"] != System.DBNull.Value))
            {
                this.Cardnumber = ((System.Nullable<int>)(dr["CardNumber"]));
            }
            if ((dr["CardType"] != System.DBNull.Value))
            {
                this.Cardtype = ((string)(dr["CardType"]));
            }
            if ((dr["SecurityCode"] != System.DBNull.Value))
            {
                this.Securitycode = ((System.Nullable<int>)(dr["SecurityCode"]));
            }
            if ((dr["CardExpirationDate"] != System.DBNull.Value))
            {
                this.Cardexpirationdate = ((System.Nullable<System.DateTime>)(dr["CardExpirationDate"]));
            }
            if ((dr["Name"] != System.DBNull.Value))
            {
                this.Name = ((string)(dr["Name"]));
            }
            if ((dr["BillingStreetName"] != System.DBNull.Value))
            {
                this.Billingstreetname = ((string)(dr["BillingStreetName"]));
            }
            if ((dr["BillingCity"] != System.DBNull.Value))
            {
                this.Billingcity = ((string)(dr["BillingCity"]));
            }
            if ((dr["BillingState"] != System.DBNull.Value))
            {
                this.Billingstate = ((System.Nullable<char>)(dr["BillingState"]));
            }
            if ((dr["BillingZipCode"] != System.DBNull.Value))
            {
                this.Billingzipcode = ((System.Nullable<int>)(dr["BillingZipCode"]));
            }
            if ((dr["TransactionId"] != System.DBNull.Value))
            {
                this.Transactionid = ((string)(dr["TransactionId"]));
            }
            if ((dr["TransactionXML"] != System.DBNull.Value))
            {
                this.Transactionxml = ((string)(dr["TransactionXML"]));
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                this.Status = ((string)(dr["Status"]));
            }
            if ((dr["BusinessSubscriptionId"] != System.DBNull.Value))
            {
                this._businesssubscriptionid = ((System.Nullable<int>)(dr["BusinessSubscriptionId"]));
            }
            if ((dr["PaymentInfoId"] != System.DBNull.Value))
            {
                this._paymentinfoid = ((System.Nullable<int>)(dr["PaymentInfoId"]));
            }
        }

        public static PaymentCollection Select_Payments_By_BusinessSubscriptionId(System.Nullable<int> BusinessSubscriptionId)
        {
            AutoShop.DAL.Payment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Payment();
                System.Data.DataSet ds = dbo.Select_Payments_By_BusinessSubscriptionId(BusinessSubscriptionId);
                PaymentCollection collection = new PaymentCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Payment obj = new Payment();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static PaymentCollection Select_Payments_By_PaymentInfoId(System.Nullable<int> PaymentInfoId)
        {
            AutoShop.DAL.Payment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Payment();
                System.Data.DataSet ds = dbo.Select_Payments_By_PaymentInfoId(PaymentInfoId);
                PaymentCollection collection = new PaymentCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Payment obj = new Payment();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static PaymentCollection GetAll()
        {
            AutoShop.DAL.Payment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Payment();
                System.Data.DataSet ds = dbo.Payment_Select_All();
                PaymentCollection collection = new PaymentCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Payment obj = new Payment();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Payment Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Payment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Payment();
                System.Data.DataSet ds = dbo.Payment_Select_One(RowId);
                Payment obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Payment();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Payment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Payment();
                System.Data.DataSet ds = dbo.Payment_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Payment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Payment();
                dbo.Payment_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Contactid, this.Cardnumber, this.Cardtype, this.Securitycode, this.Cardexpirationdate, this.Name, this.Billingstreetname, this.Billingcity, this.Billingstate, this.Billingzipcode, this.Transactionid, this.Transactionxml, this.Status, this._businesssubscriptionid, this._paymentinfoid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Payment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Payment();
                dbo.Payment_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Payment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Payment();
                dbo.Payment_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Contactid, this.Cardnumber, this.Cardtype, this.Securitycode, this.Cardexpirationdate, this.Name, this.Billingstreetname, this.Billingcity, this.Billingstate, this.Billingzipcode, this.Transactionid, this.Transactionxml, this.Status, this._businesssubscriptionid, this._paymentinfoid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}