namespace AutoShop.BusinessObjects
{
    public partial class Automobileengine
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _engine;

        private System.Nullable<int> _modelid;

        private string _description;

        private System.Nullable<bool> _isactive;

        private Automobilemodel _automobilemodel;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Engine
        {
            get
            {
                return _engine;
            }
            set
            {
                _engine = value;
            }
        }

        public virtual string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual Automobilemodel Automobilemodel
        {
            get
            {
                if ((this._automobilemodel == null))
                {
                    this._automobilemodel = AutoShop.BusinessObjects.Automobilemodel.Load(this._modelid);
                }
                return this._automobilemodel;
            }
            set
            {
                _automobilemodel = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Engine = string.Empty;
            this._modelid = null;
            this.Description = string.Empty;
            this.Isactive = null;
            this.Automobilemodel = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Engine"] != System.DBNull.Value))
            {
                this.Engine = ((string)(dr["Engine"]));
            }
            if ((dr["ModelId"] != System.DBNull.Value))
            {
                this._modelid = ((System.Nullable<int>)(dr["ModelId"]));
            }
            if ((dr["Description"] != System.DBNull.Value))
            {
                this.Description = ((string)(dr["Description"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
        }

        public static AutomobileengineCollection Select_AutomobileEngines_By_ModelId(System.Nullable<int> ModelId)
        {
            AutoShop.DAL.Automobileengine dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileengine();
                System.Data.DataSet ds = dbo.Select_AutomobileEngines_By_ModelId(ModelId);
                AutomobileengineCollection collection = new AutomobileengineCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Automobileengine obj = new Automobileengine();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static AutomobileengineCollection GetAll()
        {
            AutoShop.DAL.Automobileengine dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileengine();
                System.Data.DataSet ds = dbo.AutomobileEngine_Select_All();
                AutomobileengineCollection collection = new AutomobileengineCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Automobileengine obj = new Automobileengine();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Automobileengine Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Automobileengine dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileengine();
                System.Data.DataSet ds = dbo.AutomobileEngine_Select_One(RowId);
                Automobileengine obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Automobileengine();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Automobileengine dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileengine();
                System.Data.DataSet ds = dbo.AutomobileEngine_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Automobileengine dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileengine();
                dbo.AutomobileEngine_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Engine, this._modelid, this.Description, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Automobileengine dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileengine();
                dbo.AutomobileEngine_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Automobileengine dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileengine();
                dbo.AutomobileEngine_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Engine, this._modelid, this.Description, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}