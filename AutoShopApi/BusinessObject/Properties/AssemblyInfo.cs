﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AutoShop.BusinessObjects")]
[assembly: AssemblyDescription("AutoShop BusinessObjects")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("AutoShop")]
[assembly: AssemblyProduct("AutoShop")]
[assembly: AssemblyCopyright("AutoShop")]
[assembly: AssemblyTrademark("AutoShop")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("834b7513-0c43-4a7b-ad4a-7f055c48707c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
