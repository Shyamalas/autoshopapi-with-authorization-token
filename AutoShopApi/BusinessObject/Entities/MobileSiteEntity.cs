﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoShop.BusinessObjects.Entities
{
    class MobileSiteEntity
    {
    }
    [Serializable]
    public partial class AppointmentInfoM
    {
        public int AppointId { get; set; }
        public string AppoinmentDate { get; set; }
        public string BusName { get; set; }
        public string ServiceName { get; set; }
        public string ServiceType { get; set; }
        public string AutoTitle { get; set; }
        public int StyleId { get; set; }
        public int BussId { get; set; }
        public int RecallId { get; set; }
        public string Status { get; set; }
        public string AppointmentUniqueId { get; set; }
        public int ManufacturYear { get; set; }
        public int Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public string AppMonthYear { get; set; }
        public string AppDay { get; set; }
        public string AppTime { get; set; }
        public string bNameTemp { get; set; }
        public string FulAutoDetails { get; set; }
    }

    public partial class ServiceHistoryDetailsInfoM
    {
        public string BusinessName { get; set; }
        public string AutoTitle { get; set; }
        public string ServiceName { get; set; }
        public string ServiceType { get; set; }
        public string ServiceItem { get; set; }
        public string Date { get; set; }
        public string Mileage { get; set; }
        public int AutomobildeId { get; set; }
        public int ServiceId { get; set; }
        public int RepairTypeId { get; set; }
        public int RecallId { get; set; }
    }

    public partial class RecomandedServiceInfoM
    {
        public int RowId { get; set; }
        public string Name { get; set; }
        public int Mileage { get; set; }
        public int StyleId { get; set; }
    }

    public partial class RepairTypesM
    {
        public int ID { get; set; }
        public string RepairTypeName { get; set; }
    }

    [Serializable]
    public partial class QuoteInfoM
    {
        public int RowId { get; set; }
        public int SerID { get; set; }
        public string SerName { get; set; }
        public string SerType { get; set; }

        public int QuoteId { get; set; }
        public int MYear { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public string ServiceName { get; set; }
        public string Status { get; set; }
        public string AutoTitle { get; set; }
        public string CreatedDate { get; set; }
        public string IdCss { get; set; }
        public string QuoteUniqueID { get; set; }

        public int QuoteRequestId { get; set; }
        public string BusinessName { get; set; }
        public int Price { get; set; }
        public int AutoId { get; set; }
        public int BusId { get; set; }
        public int SrvcId { get; set; }
        public int ReprId { get; set; }
        public string AppDisplay { get; set; }
        public string Comments { get; set; }
        public int StyleId { get; set; }
        public string ServiceItem { get; set; }

        public string CustomerName { get; set; }
        public string UserComments { get; set; }
        public string ModifiedDate { get; set; }
        public string EmailAddress { get; set; }

        public string QuoteTitle { get; set; }
        public string lineItems { get; set; }

        public decimal priceDC { get; set; }
    }

    [Serializable]
    public partial class AutomobileInfoM
    {
        public int RowId { get; set; }
        public int StyleID { get; set; }
        public int Year { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public string TransmissionCode { get; set; }
        public string DriveType { get; set; }
        public string EngineCode { get; set; }
        public string ModifiedDate { get; set; }
        public int Mileage { get; set; }
        public string ImageSrc { get; set; }
        public int EdmundsStyleId { get; set; }
        public string AutomobileListText { get; set; }
        public string AutomobileText
        {
            get
            {
                return string.Format("{0} {1} {2}", Year, Manufacturer, Model);
            }
        }
        public string ImageName { get; set; }
    }

    public partial class ServiceLineItemsM
    {
        public string ServiceItemDescription { get; set; }
        public string ServiceItem { get; set; }
        public string lineItems { get; set; }
        public string Actions { get; set; }
        public string Item { get; set; }
    }

    public partial class HoursInfoM
    {
        public int RowId { get; set; }
        public string TimeIntervals { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }

    [Serializable]
    public partial class BusinessInfoM
    {
        public int RowId { get; set; }
        public string BusinessName { get; set; }
        public string bNameTemp { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public int ZIP { get; set; }
        public int ZIP_4 { get; set; }
        public string PHONE { get; set; }
        public string AutoType { get; set; }
        public float Miles { get; set; }        
        public string StreetAddress1 { get; set; }
        public string BusinessPhone1 { get; set; }
        public string BusinessPhone2 { get; set; }
        public string State { get; set; }
        public int Zipcode { get; set; }
        public string EmailAddress { get; set; }        
        public string FulAutoDetails { get; set; }
    }

    public partial class BusinessSearchM
    {
        public int RowId { get; set; }
        public string BusinessName { get; set; }
        public string bNameTemp { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public int ZIP { get; set; }
        public int ZIP_4 { get; set; }
        public string PHONE { get; set; }
        public string AutoType { get; set; }
        public float Miles { get; set; }
        public string FulAutoDetails { get; set; }
    }

    public partial class RecallInfoM
    {
        public int ManufacturYear { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public int AutoId { get; set; }

        public int RowId { get; set; }
        public string ComponentDescription { get; set; }
    }

    public partial class ServBulletineM
    {
        public int ManufacturYear { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public int AutoId { get; set; }

        public int RowId { get; set; }
        public string ComponentDescription { get; set; }
    }    
}
