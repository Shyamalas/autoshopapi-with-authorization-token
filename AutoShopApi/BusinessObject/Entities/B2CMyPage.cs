﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoShop.BusinessObjects.Entities
{
    class B2CMyPage
    {
    }

    [Serializable]
    public partial class AutomobileInfo
    {
        public int RowId { get; set; }
        public int StyleID { get; set; }
        public int Year { get; set; }
        public int MakeId { get; set; }
        public string Manufacturer { get; set; }
        public int ModelId { get; set; }
        public string Model { get; set; }
        public int SubModelId { get; set; }
        public int BaseVehicleId { get; set; }
        public string Style { get; set; }
        public string TransmissionCode { get; set; }
        public string IntervalType { get; set; }
        public string DriveType { get; set; }
        public int DriveTypeId { get; set; }
        public int EngineId { get; set; }
        public string EngineCode { get; set; }
        public string ModifiedDate { get; set; }

        public string CreatedDate { get; set; }
        public int Mileage { get; set; }
        public string ImageSrc { get; set; }
        public int EdmundsStyleId { get; set; }
        public string AutomobileListText { get; set; }
        public int DriveTypeID { get; set; }
        public string AutomobileText
        {
            get
            {
                //return string.Format("{0},{1},{2},{3}", Year, Manufacturer, Model, Style);
                return string.Format("{0} {1} {2}", Year, Manufacturer, Model);
            }
        }
        public string ImageName { get; set; }

    }

    public partial class RecomandedServiceInfo
    {
        public int RowId { get; set; }
        public string Name { get; set; }
        public int Mileage { get; set; }
        public int StyleId { get; set; }
    }

    public partial class RepairTypes
    {
        public int ID { get; set; }
        public string RepairTypeName { get; set; }
        public string AutoshopType { get; set; }
        public string IDwithType
         
        {
            get
            {
                return string.Format("{0},{1}", ID, AutoshopType);
            }
        
        }
    }

    [Serializable]
    public partial class AppointmentInfo
    {
        public int StyleId { get; set; }
        public string AutoTitle { get; set; }

        public int RowId { get; set; }
        public int ManufacturYear { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public string AppoinmentDate { get; set; }
        public string BusinessName { get; set; }
        public string bNameTemp { get; set; }
        public string Name { get; set; }//ServiceName
        public string Type { get; set; }//ServiceType        
        public int AutoId { get; set; }
        public int BussId { get; set; }
        public int RecallId { get; set; }
        public string Status { get; set; }
        public string DateCheck { get; set; }
        public string AppointmentUniqueId { get; set; }
        public string ServiceItem { get; set; }
        public string AppMonthYear { get; set; }
        public string AppDay { get; set; }
        public string AppTime { get; set; }
        public string FulAutoDetails { get; set; }

        #region for Get_Appointments_By_BusId_Style

        public int AppointId { get; set; }
        public string Customer { get; set; }
        public string IsConfirmed { get; set; }
        public string StatusCheck { get; set; }

        #endregion
    }

    public partial class RecallInfo
    {
        public int ManufacturYear { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public int AutoId { get; set; }

        public int RowId { get; set; }
        public string ComponentDescription { get; set; }
    }

    public partial class ServBulletine
    {
        public int ManufacturYear { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public int AutoId { get; set; }

        public int RowId { get; set; }
        public string ComponentDescription { get; set; }
    }

    [Serializable]
    public partial class QuoteInfo
    {
        public int QuoteId { get; set; }
        public int MYear { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public string ServiceName { get; set; }
        public string Status { get; set; }
        public string AutoTitle { get; set; }
        public string CreatedDate { get; set; }
        public string IdCss { get; set; }
        public string QuoteUniqueID { get; set; }

        public int QuoteRequestId { get; set; }
        public string BusinessName { get; set; }
        public int Price { get; set; }
        public int AutoId { get; set; }
        public int BusId { get; set; }
        public int SrvcId { get; set; }
        public int ReprId { get; set; }
        public string AppDisplay { get; set; }
        public string Comments { get; set; }
        public int StyleId { get; set; }
        public string ServiceItem { get; set; }

        public string CustomerName { get; set; }
        public string UserComments { get; set; }
        public string ModifiedDate { get; set; }
        public string EmailAddress { get; set; }

        public string QuoteTitle { get; set; }
        public string lineItems { get; set; }

        public decimal priceDC { get; set; }
    }


    [Serializable]
    public partial class QuoteDetails
    {
        public int QuoteId { get; set; }
        public int MYear { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public string ServiceName { get; set; }
        public string Status { get; set; }
        public string AutoTitle { get; set; }
        public string QuoteUniqueID { get; set; }
        public int StyleId { get; set; }
        public string ServiceItem { get; set; }
        public string UserComments { get; set; }
        public string ModifiedDate { get; set; }
        public decimal Price { get; set; }
        public string priceStr
        {
            get
            {
              return Price.ToString("N2"); 
            }
        }
        public string Comments { get; set; }
        public string AppoinmentDate { get; set; }        
    }


    public partial class B2CUserInfo
    {
        public int ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ZipCode { get; set; }
        public string Photo{ get; set; }
    }

    public partial class ServiceLineItems
    {
        public string ServiceItemDescription { get; set; }
        public string ServiceItem { get; set; }
        public string lineItems { get; set; }
        public string Actions { get; set; }
        public string Item { get; set; }
    }

    public partial class BusinessSearch
    {
        public int RowId { get; set; }
        public string BusinessName { get; set; }
        public string bNameTemp { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public int ZIP { get; set; }
        public int ZIP_4 { get; set; }
        public string PHONE { get; set; }
        public string AutoType { get; set; }
        public float Miles { get; set; }
        public string FulAutoDetails { get; set; }
    }

    [Serializable]
    public partial class BusinessInfo
    {
        public int RowId { get; set; }
        public string BusinessName { get; set; }
        public string bNameTemp { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public int ZIP { get; set; }
        public int ZIP_4 { get; set; }
        public string PHONE { get; set; }
        public string AutoType { get; set; }
        public float Miles { get; set; }

        public string Createdby { get; set; }
        public System.DateTime Createddate { get; set; }
        public string Modifiedby { get; set; }
        public System.DateTime Modifieddate { get; set; }
        public string Businesstype { get; set; }
        public int Addressid { get; set; }
        public string StreetAddress1 { get; set; }
        public string BusinessPhone1 { get; set; }
        public string BusinessPhone2 { get; set; }
        public string State { get; set; }
        public int Zipcode { get; set; }
        public string EmailAddress { get; set; }
        public string Faxnumber { get; set; }
        public bool Memberofaaa { get; set; }
        public string Memberofaaastr { get; set; }
        public string MemberofAAAdisp { get; set; }
        public bool StarCertified { get; set; }
        public string StarCertifiedstr { get; set; }
        public string StarCertifieddisp { get; set; }
        public bool Memberoface { get; set; }
        public string Memberofacestr { get; set; }
        public string MemberofACEdisp { get; set; }
        public bool Memberofase { get; set; }
        public string Memberofasestr { get; set; }
        public string MemberofASEdisp { get; set; }
        public bool Memberofbbb { get; set; }
        public string Memberofbbbstr { get; set; }
        public string MemberofBBBdisp { get; set; }
        public string Url { get; set; }
        public bool Isactive { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public int NaicsCode { get; set; }
        public string NaicsDescription { get; set; }
        public int SicCode { get; set; }
        public string SicDescription { get; set; }
        public string Image { get; set; }
        public string BusinessHour { get; set; }
        public bool RepairShop { get; set; }
        public string RepairShopstr { get; set; }
        public string Quotedisp { get; set; }
        public string RepairShopdisp { get; set; }
        public bool BodyShop { get; set; }
        public string BodyShopstr { get; set; }
        public string BodyShopdisp { get; set; }
        public bool TireShop { get; set; }
        public string TireShopstr { get; set; }
        public string TireShopdisp { get; set; }
        public bool CarWash { get; set; }
        public string CarWashstr { get; set; }
        public string CarWashdisp { get; set; }
        public bool Transport { get; set; }
        public string Transportstr { get; set; }
        public string Transportdisp { get; set; }
        public bool Towing { get; set; }
        public string Towingstr { get; set; }
        public string Towingdisp { get; set; }
        public bool SmogCheck { get; set; }
        public string SmogCheckstr { get; set; }
        public string SmogCheckdisp { get; set; }
        public bool GlassShop { get; set; }
        public string GlassShopstr { get; set; }
        public string GlassShopdisp { get; set; }
        public bool Dealer { get; set; }
        public string Dealerstr { get; set; }
        public string Dealerdisp { get; set; }
        public bool Parts { get; set; }
        public string Partsstr { get; set; }
        public string Partsdisp { get; set; }
        public bool NewCarSales { get; set; }
        public string NewCarSalesstr { get; set; }
        public string NewCarSalesdisp { get; set; }
        public bool UsedCarSales { get; set; }
        public string UsedCarSalesstr { get; set; }
        public string UsedCarSalesdisp { get; set; }
        public int Rating { get; set; }
        public string IsFavorite { get; set; }
        public string IsFavoriteClass { get; set; }
        public string ServiceOffered { get; set; }
        public string Specialities { get; set; }
        public string ContactName { get; set; }
        public string Email { get; set; }
        public string IsOnline { get; set; }
        public string Coupon { get; set; }
        public string Appointment { get; set; }
        public string AutoManufacturer { get; set; }
        public string Chat { get; set; }
        public bool IsDealer { get; set; }
        public string Quote { get; set; }
        public string FulAutoDetails { get; set; }
    }

    public partial class QuoteResponseBLL
    {
        public int QuoteId { get; set; }
        public int QuoteRequestId { get; set; }
        public string AppDisplay { get; set; }
        public string AutoTile { get; set; }
        public string BusinessName { get; set; }
        public string Comments { get; set; }
        public int Price { get; set; }
        public int AutoId { get; set; }
        public int BusId { get; set; }
        public string QuoteUniqueID { get; set; }
        public int ReprId { get; set; }
        public int SrvcId { get; set; }
        public string ServiceName { get; set; }
        public string ServiceItem { get; set; }
        public string Status { get; set; }
        public int StyleId { get; set; }
        public int MYear { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
    }

    public partial class StateInfo
    {
        public string StateName { get; set; }
        public string StateCode { get; set; }
    }

    public partial class HoursInfo
    {
        public int RowId { get; set; }
        public string TimeIntervals { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }

    public partial class AppointmentDetailsInfo
    {
        public string BusinessName { get; set; }
        public int RowId { get; set; }
        public string Image { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public int ZIP { get; set; }
        public int ZIP4 { get; set; }
        public string PHONE { get; set; }
        public string State { get; set; }
        public string ServiceName { get; set; }
        public string ServiceType { get; set; }
        public string MemberofAAAdisp { get; set; }
        public string MemberofAAAstr { get; set; }
        public string MemberofACEdisp { get; set; }
        public string MemberofACEstr { get; set; }
        public string MemberofASEdisp { get; set; }
        public string MemberofASEstr { get; set; }
        public string MemberofBBBdisp { get; set; }
        public string MemberofBBBstr { get; set; }
        public string AppoinmentDate { get; set; }
        public string AppoinmentTime { get; set; }
        public int Year { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public int Mileage { get; set; }
        public string ImagePath { get; set; }
        public string ModifiedDate { get; set; }
        public int StyleID { get; set; }
        public string AutomobileText { get; set; }
        public int EdmundsStyleId { get; set; }
        public string EngineCode { get; set; }
        public string DriveType { get; set; }
        public string TransmissionCode { get; set; }
        public int ServiceID { get; set; }
        public string CustName { get; set; }
        public string CustPhone { get; set; }
        public string CustEmail { get; set; }
        public string AppointmentUniqueId { get; set; }
        public string BusinessHour { get; set; }
        public string lineItems { get; set; }
        public string lineItemsPrnt { get; set; }
        public string ImageSrc { get; set; }
        public int Rating { get; set; }
        public string hidelineItems { get; set; }
        public string AppMonthYear { get; set; }
        public string AppDay { get; set; }
        public string AppTime { get; set; }
    }

    public partial class ServiceHistoryDetailsInfo
    {
        public string BusinessName { get; set; }
        public string AutoTitle { get; set; }
        public string ServiceName { get; set; }
        public string ServiceType { get; set; }
        public string ServiceItem { get; set; }
        public string Date { get; set; }
        public string Mileage { get; set; }
        public int AutomobildeId { get; set; }
        public int ServiceId { get; set; }
        public int RepairTypeId { get; set; }
        public int RecallId { get; set; }
    }

    public partial class CityInfo
    {
        public string City { get; set; }
        public int ZipCode { get; set; }
    }

    public partial class B2BAppointmentResponse
    {
        public string FirstName { get; set; }
        public string BusinessName { get; set; }
        public string StreetAddress1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zipcode { get; set; }
        public string BusinessPhone1 { get; set; }
        public int ManufacturYear { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Style { get; set; }
        public string Type { get; set; }
        public string AppoinmentDate { get; set; }
        public string StartTime { get; set; }
        public string ServiceItem { get; set; }
        public string EmailAddress { get; set; }
    }

    public partial class SubscriptionInfo
    {
        public int RowId { get; set; }
        public string Type { get; set; }
        public int ValidationDays { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public int Discount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal AmountDis
        {
            get
            {
                return Amount - (Amount * Discount / 100);
            }
        }

        public string SubscriptionTypeText
        {
            get
            {
                return Type + " | " + ValidationDays + " Month" + " | " + " $ " + AmountDis.ToString("N2");
            }
        }

    }

    public partial class AutomobileManufacturerInfo
    {
        public string Manufacturer { get; set; }
    }

    public partial class B2BProfileInfo
    {
        #region for FillBusinessDetails

        public int BusinessId { get; set; }
        public int ContactId { get; set; }
        public string AutoShopName { get; set; }
        public string AutoShopType { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string AutoShopAddress1 { get; set; }
        public string AutoShopAddress2 { get; set; }
        public string AutoShopCity { get; set; }
        public string AutoShopState { get; set; }
        public string AutoShopZipCode { get; set; }
        public string ServiceCenterPh { get; set; }
        public string ContactPhNo { get; set; }
        public string FaxNo { get; set; }
        public string Website { get; set; }
        public string ServiceOffered { get; set; }
        public string Specialities { get; set; }
        public string CouponName { get; set; }
        public System.DateTime CouponStartDate { get; set; }
        public System.DateTime CouponEndDate { get; set; }
        public bool CouponActiveFlag { get; set; }
        public bool RepairShop { get; set; }
        public bool BodyShop { get; set; }
        public bool TyreShop { get; set; }
        public bool CarWash { get; set; }
        public bool Transport { get; set; }
        public bool Towing { get; set; }
        public bool SmogCheck { get; set; }
        public bool SmogCheckOnly { get; set; }
        public bool GlassShop { get; set; }
        public bool Dealer { get; set; }
        public bool Parts { get; set; }
        public bool NewCarSales { get; set; }
        public bool UsedCarSales { get; set; }
        public bool MemAAA { get; set; }
        public bool MemACE { get; set; }
        public bool MemASE { get; set; }
        public bool MemBBB { get; set; }
        public string Manufacturer { get; set; }
        public bool StarCertified { get; set; }

        public bool AutoTransport { get; set; }

        #endregion

        #region for FillServiceOperations

        public bool IsWorkingDay { get; set; }
        public System.DateTime StartTime { get; set; }
        public System.DateTime EndTime { get; set; }
        public int WeekDayId { get; set; }

        #endregion

        #region for FillCoupons

        public int CouponNo { get; set; }
        public bool CouponActive { get; set; }
        public System.DateTime CouponStart { get; set; }
        public System.DateTime CouponEnd { get; set; }
        public string CouponImage { get; set; }

        #endregion

        #region for FillAutoshopImages

        public int AutoShopImageNo { get; set; }
        public bool ImageActiveFlag { get; set; }
        public string AutoShopImage { get; set; }

        #endregion
    }

    public partial class B2BServiceOprations
    {
        public bool IsWorkingDay { get; set; }
        public System.DateTime StartTime { get; set; }
        public System.DateTime EndTime { get; set; }
        public int WeekDayId { get; set; }
    }

    public partial class B2BCoupons
    {
        public int CouponNo { get; set; }
        public bool CouponActive { get; set; }
        public System.DateTime CouponStart { get; set; }
        public System.DateTime CouponEnd { get; set; }
        public string CouponImage { get; set; }
        public string CouponName { get; set; }
    }

    public partial class B2BAutoshopImage
    {
        public int AutoShopImageNo { get; set; }
        public bool ImageActiveFlag { get; set; }
        public string AutoShopImage { get; set; }
    }

    public partial class BusinessSubscriptonInfo
    {
        public string BillingStreetName { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public int BillingZipCode { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
        public double Amount { get; set; }
        public int ValidationDays { get; set; }
        public int RowId { get; set; }
        public System.DateTime EndDate { get; set; }
        public int BSRowId { get; set; }
        public bool Coupon { get; set; }
        public bool Appointment { get; set; }
        public bool Chat { get; set; }
        public bool AutoRenewal { get; set; }
    }

    public partial class BusinessRegisterInfo
    {
        public int RowId { get; set; }
        public string BusinessName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string AddressDetail { get; set; }
        public int Zipcode { get; set; }
        public string phone { get; set; }
    }

    public partial class BusinessCoupons
    {
        public int RowId { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public int BusinessId { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public string Coupon { get; set; }
    }

    [Serializable]
    public partial class AutoshopDeal
    {
        public int RowId { get; set; }
        public string Coupon { get; set; }
        public string Name { get; set; }        
        public string EndDate { get; set; }
        public int BusinessId { get; set; }        
    }
}
