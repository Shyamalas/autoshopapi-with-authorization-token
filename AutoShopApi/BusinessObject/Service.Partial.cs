namespace AutoShop.BusinessObjects
{
    public partial class Service
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _name;

        private System.Nullable<bool> _isactive;

        private string _type;

        private AppointmentCollection _appointmentCollection;

        private QuoteCollection _quoteCollection;

        private ServicehistoryCollection _servicehistoryCollection;

        private ServiceservicelineitemCollection _serviceservicelineitemCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        //public virtual AppointmentCollection AppointmentCollection
        //{
        //    get
        //    {
        //        if ((this._appointmentCollection == null))
        //        {
        //            _appointmentCollection = AutoShop.BusinessObjects.Appointment.Select_Appointments_By_ServiceId(this.Rowid);
        //        }
        //        return this._appointmentCollection;
        //    }
        //}

        public virtual QuoteCollection QuoteCollection
        {
            get
            {
                if ((this._quoteCollection == null))
                {
                    _quoteCollection = AutoShop.BusinessObjects.Quote.Select_Quotes_By_ServiceId(this.Rowid);
                }
                return this._quoteCollection;
            }
        }

        public virtual ServicehistoryCollection ServicehistoryCollection
        {
            get
            {
                if ((this._servicehistoryCollection == null))
                {
                    _servicehistoryCollection = AutoShop.BusinessObjects.Servicehistory.Select_ServiceHistorys_By_ServiceId(this.Rowid);
                }
                return this._servicehistoryCollection;
            }
        }

        public virtual ServiceservicelineitemCollection ServiceservicelineitemCollection
        {
            get
            {
                if ((this._serviceservicelineitemCollection == null))
                {
                    _serviceservicelineitemCollection = AutoShop.BusinessObjects.Serviceservicelineitem.Select_ServiceServiceLineItems_By_ServiceId(this.Rowid);
                }
                return this._serviceservicelineitemCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Name = string.Empty;
            this.Isactive = null;
            this.Type = string.Empty;
            this._appointmentCollection = null;
            this._quoteCollection = null;
            this._servicehistoryCollection = null;
            this._serviceservicelineitemCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Name"] != System.DBNull.Value))
            {
                this.Name = ((string)(dr["Name"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["Type"] != System.DBNull.Value))
            {
                this.Type = ((string)(dr["Type"]));
            }
        }

        public static ServiceCollection GetAll()
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                System.Data.DataSet ds = dbo.Service_Select_All();
                ServiceCollection collection = new ServiceCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Service obj = new Service();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Service Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                System.Data.DataSet ds = dbo.Service_Select_One(RowId);
                Service obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Service();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                System.Data.DataSet ds = dbo.Service_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                dbo.Service_Insert(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Name, this.Isactive, this.Type);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                dbo.Service_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                dbo.Service_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Name, this.Isactive, this.Type);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_Recommanded_Services_By_AutoId(int AutoId)
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                System.Data.DataSet ds = dbo.Select_Recommanded_Services_By_AutoId(AutoId);
                
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_Recommanded_Services_By_AutoId_Style(int AutoId)
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                System.Data.DataSet ds = dbo.Select_Recommanded_Services_By_AutoId_Style(AutoId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_Recommanded_Services_By_StyleId(int styleID, int contactID)
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                System.Data.DataSet ds = dbo.Select_Recommanded_Services_By_StyleId(styleID, contactID);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_Repair_Types()
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                System.Data.DataSet ds = dbo.Select_Repair_Types();

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_ServiceLines_By_ServiceId(int serviceId)
        {
            AutoShop.DAL.Service dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Service();
                System.Data.DataSet ds = dbo.Select_ServiceLines_By_ServiceId(serviceId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }        
    }
}