﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using AutoShop.BusinessObjects.Entities;
using System.Linq;
using AutoShop.DAL;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Web.Configuration;

namespace AutoShop.BusinessObjects.BusinessLogic
{
    public class B2CMyPageBL
    {
        public AutoShop.DAL.Automobile AutoDL;
        public AutoShop.DAL.Service SrvDL;
        public AutoShop.DAL.Appointment App;
        public AutoShop.DAL.Autorecall Rcal;
        public AutoShop.DAL.Quote QT;
        public AutoShop.DAL.Contact Cnt;
        public AutoShop.DAL.Quoteresponse QTRes;
        public AutoShop.DAL.SearchBusiness SRB;
        public AutoShop.DAL.Business BS;
        public AutoShop.DAL.Businesshours BH;
        public AutoShop.DAL.Servicehistory SRH;
        public AutoShop.DAL.Subscription subscription;
        public AutoShop.DAL.Automobilemanufacturer Automobilemanufacturer;
        public AutoShop.DAL.BusinessProfile BSProfile;
        public AutoShop.DAL.Businesssubscription BSsubscription;
        public AutoShop.DAL.Appointment Appointment;
        public AutoShop.DAL.Quote Quotes;
        public AutoShop.DAL.Businesscoupon BSCoupons;
        public AutoShop.DAL.Deal DL;

        public B2CMyPageBL()
        {
            if (AutoDL == null)
            {
                AutoDL = new AutoShop.DAL.Automobile();
            }
            if (SrvDL == null)
            {
                SrvDL = new AutoShop.DAL.Service();
            }
            if (App == null)
            {
                App = new AutoShop.DAL.Appointment();
            }
            if (Rcal == null)
            {
                Rcal = new AutoShop.DAL.Autorecall();
            }
            if (QT == null)
            {
                QT = new AutoShop.DAL.Quote();
            }
            if (Cnt == null)
            {
                Cnt = new AutoShop.DAL.Contact();
            }
            if (QTRes == null)
            {
                QTRes = new AutoShop.DAL.Quoteresponse();
            }
            if (SRB == null)
            {
                SRB = new AutoShop.DAL.SearchBusiness();
            }
            if (BS == null)
            {
                BS = new AutoShop.DAL.Business();
            }
            if (BH == null)
            {
                BH = new AutoShop.DAL.Businesshours();
            }
            if (SRH == null)
            {
                SRH = new AutoShop.DAL.Servicehistory();
            }
            if (subscription == null)
            {
                subscription = new AutoShop.DAL.Subscription();
            }
            if (Automobilemanufacturer == null)
            {
                Automobilemanufacturer = new AutoShop.DAL.Automobilemanufacturer();
            }
            if (BSProfile == null)
            {
                BSProfile = new AutoShop.DAL.BusinessProfile();
            }
            if (BSsubscription == null)
            {
                BSsubscription = new AutoShop.DAL.Businesssubscription();
            }
            if (Appointment == null)
            {
                Appointment = new AutoShop.DAL.Appointment();
            }
            if (Quotes == null)
            {
                Quotes = new AutoShop.DAL.Quote();
            }
            if (BSCoupons == null)
            {
                BSCoupons = new AutoShop.DAL.Businesscoupon();
            }
            if (DL == null)
            {
                DL = new AutoShop.DAL.Deal();
            }
        }

        public List<AutomobileInfo> GetAutomobiles(int ContactId)
        {
            try
            {
                int i = 0;

                System.Data.DataSet ds = AutoDL.Select_Automobiles_List_By_ContactIid_Style(ContactId);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageSrc"].ToString()) && string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageName"].ToString()))
                    {
                        ds.Tables[0].Rows[i]["ImageSrc"] = WebConfigurationManager.AppSettings["AutoImagePath"];
                    }
                    else
                    {
                        //if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageName"].ToString()))
                        //{
                        //    ds.Tables[0].Rows[i]["ImageSrc"] = WebConfigurationManager.AppSettings["RepositoryPath"] + ds.Tables[0].Rows[i]["ImageName"].ToString();
                        //}
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageName"].ToString()))
                        {
                            ds.Tables[0].Rows[i]["ImageSrc"] = ds.Tables[0].Rows[i]["ImageName"].ToString();
                        }
                    }
                    i++;
                }

                List<AutomobileInfo> collection = new List<AutomobileInfo>();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AutomobileInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RecomandedServiceInfo> GetRecommandedServices(int AutoId)
        {
            try
            {
                System.Data.DataSet ds = SrvDL.Select_Recommanded_Services_By_AutoId_Style(AutoId);
                List<RecomandedServiceInfo> collection = new List<RecomandedServiceInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RecomandedServiceInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RepairTypes> GetRepairTypes()
        {
            try
            {
                System.Data.DataSet ds = SrvDL.Select_Repair_Types();
                List<RepairTypes> collection = new List<RepairTypes>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RepairTypes>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RepairTypes> GetRepairTypesByAutoType(string AutoType)
        {
            try
            {
                System.Data.DataSet ds = SrvDL.Select_Repair_Types_By_AutoshopType(AutoType);
                List<RepairTypes> collection = new List<RepairTypes>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RepairTypes>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RecomandedServiceInfo> GetServicesByRepairTypeId(int repairTypeId)
        {
            try
            {
                System.Data.DataSet ds = SrvDL.Select_Services_By_RepairTypeId(repairTypeId);
                List<RecomandedServiceInfo> collection = new List<RecomandedServiceInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RecomandedServiceInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AppointmentInfo> GetAppointments(int contactId)
        {
            try
            {
                System.Data.DataSet ds = App.Select_Appointments_By_ContactId_Style(contactId);
                List<AppointmentInfo> collection = new List<AppointmentInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AppointmentInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AppointmentInfo> GetAppointmentsByAutomobile(int contactId, int styleId)
        {
            try
            {
                System.Data.DataSet ds = App.GET_Appointment_By_Automobile(contactId, styleId);
                List<AppointmentInfo> collection = new List<AppointmentInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AppointmentInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AppointmentInfo> GetServiceDue(int contactId)
        {
            try
            {
                System.Data.DataSet ds = App.Select_ServiceDueList_By_ContactId_Style(contactId);
                List<AppointmentInfo> collection = new List<AppointmentInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AppointmentInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RecallInfo> GetRecalls(int contactId)
        {
            try
            {
                System.Data.DataSet ds = Rcal.Select_AutoRecalls_By_ContactId(contactId);
                List<RecallInfo> collection = new List<RecallInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RecallInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RecallInfo> GETRecallDetails(string year, string make, string model, string style, string autoId)
        {
            try
            {
                System.Data.DataSet ds = Rcal.GET_Recall_Details(year, make, model, style, autoId);
                List<RecallInfo> collection = new List<RecallInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RecallInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<ServBulletine> GetBulletine(int contactId)
        {
            try
            {
                System.Data.DataSet ds = Rcal.Select_Bulletins_By_ContactId(contactId);
                List<ServBulletine> collection = new List<ServBulletine>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<ServBulletine>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<ServBulletine> GetBulletineDetails(ServBulletine bulDtl)
        {
            try
            {
                System.Data.DataSet ds = Rcal.GET_Bulletins_Details(bulDtl.ManufacturYear.ToString(), bulDtl.Manufacturer, bulDtl.Model, bulDtl.Style);
                List<ServBulletine> collection = new List<ServBulletine>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<ServBulletine>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfo> GetRequestedQuotesByContactId(int contactId)
        {
            try
            {
                System.Data.DataSet ds = QT.Select_Quotes_By_ContactId(contactId);
                List<QuoteInfo> collection = new List<QuoteInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfo> GetRequestedQuotesByContactIdFilterByBusId(int contactId, int busId)
        {
            try
            {
                System.Data.DataSet ds = QT.Select_Quotes_By_ContactId_fltBusId(contactId, busId);
                List<QuoteInfo> collection = new List<QuoteInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public B2CUserInfo GetB2CProfileByID(int RowId)
        {
            try
            {
                System.Data.DataSet ds = Cnt.GetB2CProfileByID(RowId);
                B2CUserInfo bsInfo = new B2CUserInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    //collection = ds.Tables[0].ToCollection<B2CUserInfo>();

                    bsInfo = FillProfile(ds.Tables[0].Rows[0]);
                }
                return bsInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public B2CUserInfo GetB2BdetailByID(int RowId)
        {
            try
            {
                System.Data.DataSet ds = Cnt.GetB2BdetailByID(RowId);
                B2CUserInfo bsInfo = new B2CUserInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    //collection = ds.Tables[0].ToCollection<B2CUserInfo>();

                    bsInfo = FillProfile(ds.Tables[0].Rows[0]);
                }
                return bsInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfo> GetQuoteResponseById(int QuoteId)
        {
            try
            {
                System.Data.DataSet ds = QTRes.Select_QuoteResponses_By_QuoteId(QuoteId);
                List<QuoteInfo> collection = new List<QuoteInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfo> GetQuoteResponseByIdsrItems(int QuoteId)
        {
            try
            {
                System.Data.DataSet ds = QTRes.Select_QuoteResponses_By_QuoteId_srItems(QuoteId);
                List<QuoteInfo> collection = new List<QuoteInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<ServiceLineItems> GetServiceLinesByServiceId(int serviceId)
        {
            try
            {
                System.Data.DataSet ds = SrvDL.Select_ServiceLines_By_ServiceId(serviceId);
                List<ServiceLineItems> collection = new List<ServiceLineItems>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<ServiceLineItems>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<BusinessSearch> BusinessSearchFromMypage(string zipcode, string autoTypes)
        {
            try
            {
                System.Data.DataSet ds = SRB.BusinessSearchFromMypage(zipcode, autoTypes);
                List<BusinessSearch> collection = new List<BusinessSearch>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessSearch>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<BusinessSearch> BusinessSearchFromMypageQuote(string zipcode, int miles)
        {
            try
            {
                System.Data.DataSet ds = SRB.BusinessSearchFromMypage_Quote(zipcode, miles);
                List<BusinessSearch> collection = new List<BusinessSearch>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessSearch>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<BusinessSearch> BusinessSearchFromMypageQuoteFilter(string zipcode, int miles, int quoteId)
        {
            try
            {
                System.Data.DataSet ds = SRB.BusinessSearchFromMypage_Quote_Filter(zipcode, miles, quoteId);
                List<BusinessSearch> collection = new List<BusinessSearch>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessSearch>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<BusinessInfo> GETDealerList(string Make, string zipcode)
        {
            try
            {
                System.Data.DataSet ds = BS.Select_Dealer_List(Make, zipcode);
                List<BusinessInfo> collection = new List<BusinessInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public void B2CProfile_Update(B2CUserInfo usrInfo)
        {
            Cnt.B2CProfile_Update(usrInfo.ContactId
                    , usrInfo.FirstName
                    , usrInfo.LastName
                    , usrInfo.CellPhone
                    , usrInfo.StreetAddress
                    , usrInfo.City
                    , usrInfo.State
                    , usrInfo.ZipCode, usrInfo.Photo);
        }

        public BusinessInfo GetBusinessDetailByID(int RowId)
        {
            try
            {
                System.Data.DataSet ds = BS.Get_Businesss_FullDetails_By_BusId(RowId);
                BusinessInfo obj = new BusinessInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    obj = FillBusiness(ds.Tables[0].Rows[0]);
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteResponseBLL> GetQuoteResponseBysrItem(int srItem)
        {
            try
            {
                System.Data.DataSet ds = QTRes.Select_QuoteResponses_By_QuoteId_srItems(srItem);
                List<QuoteResponseBLL> collection = new List<QuoteResponseBLL>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteResponseBLL>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteResponseBLL> GetQuoteResponseByQuoteId(int qtId)
        {
            try
            {
                System.Data.DataSet ds = QTRes.Select_QuoteResponses_By_QuoteId(qtId);
                List<QuoteResponseBLL> collection = new List<QuoteResponseBLL>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteResponseBLL>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<Appointment> GetAppointByAutomobile(int autoId, int style)
        {
            try
            {
                System.Data.DataSet ds = App.GET_Appointment_By_Automobile(autoId, style);
                List<AutoShop.BusinessObjects.Appointment> collection = new List<AutoShop.BusinessObjects.Appointment>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AutoShop.BusinessObjects.Appointment>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<StateInfo> Select_All_State()
        {
            try
            {
                System.Data.DataSet ds = Cnt.Select_All_State();
                List<StateInfo> collection = new List<StateInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<StateInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<HoursInfo> GetBusinessHoursToday(int busId, string day)
        {
            try
            {
                System.Data.DataSet ds = BH.Select_BusinessHourss_By_BusinessIdAndDay_Today(busId, day);
                List<HoursInfo> collection = new List<HoursInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<HoursInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<HoursInfo> GetBusinessHours(int busId, string day)
        {
            try
            {
                System.Data.DataSet ds = BH.Select_BusinessHourss_By_BusinessIdAndDay(busId, day);
                List<HoursInfo> collection = new List<HoursInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<HoursInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<HoursInfo> GetBusinessHoursByBusId(int busId, string day)
        {
            try
            {
                System.Data.DataSet ds = BH.GetBusinessHours(busId, day);
                List<HoursInfo> collection = new List<HoursInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<HoursInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<BusinessInfo> GetFavoriteAutoshopListContactId(int contactId)
        {
            try
            {
                int i = 0;
                System.Data.DataSet ds = SRB.Get_FavoriteAutoshopList_By_ContactId(contactId);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Image"].ToString()))
                    {
                        ds.Tables[0].Rows[i]["Image"] = WebConfigurationManager.AppSettings["DefaultPath"];
                    }
                    i++;
                }

                List<BusinessInfo> collection = new List<BusinessInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AppointmentDetailsInfo> GetAppointmentDetails(int RowId)
        {
            int i = 0;
            try
            {
                System.Data.DataSet ds = App.Get_AppointmentDetails_By_RowId(RowId);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageSrc"].ToString()) && string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageName"].ToString()))
                    {
                        ds.Tables[0].Rows[i]["ImageSrc"] = WebConfigurationManager.AppSettings["DefaultPath"];
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageName"].ToString()))
                        {
                            ds.Tables[0].Rows[i]["ImageSrc"] = WebConfigurationManager.AppSettings["GetRepositoryPath"] + ds.Tables[0].Rows[i]["ImageName"].ToString();
                        }
                    }
                    i++;
                }

                List<AppointmentDetailsInfo> collection = new List<AppointmentDetailsInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AppointmentDetailsInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<ServiceHistoryDetailsInfo> GetServiceHistoryByAutoId(int AutoId)
        {
            try
            {
                System.Data.DataSet ds = SRH.Select_ServiceHistory_By_AutomobileId(AutoId);
                List<ServiceHistoryDetailsInfo> srHistoryList = new List<ServiceHistoryDetailsInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    srHistoryList = ds.Tables[0].ToCollection<ServiceHistoryDetailsInfo>();
                }
                return srHistoryList;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<CityInfo> SelectCityByState(string stateCode)
        {
            try
            {
                System.Data.DataSet ds = Cnt.Select_City_By_State(stateCode);
                List<CityInfo> collection = new List<CityInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<CityInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public ServiceHistoryDetailsInfo GetServiceHistoryDetails(int AutoId, int ServiceId, int RepairTypeId, int RecallId)
        {
            try
            {
                System.Data.DataSet ds = SRH.GetServiceHistory_Details(AutoId, ServiceId, RepairTypeId, RecallId);
                ServiceHistoryDetailsInfo obj = new ServiceHistoryDetailsInfo();

                if (GlobalTools.IsSafeDataSet(ds) && ds.Tables[0].Rows.Count != 0)
                {
                    obj = FillServiceDetails(ds.Tables[0].Rows[0]);
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AppointmentInfo> GetAppointmentsByBusIdStyle(int BusinessId, string TodayDate, string IsConfirmed)
        {
            try
            {
                System.Data.DataSet ds = Appointment.Get_Appointments_By_BusId_Style(BusinessId, TodayDate, IsConfirmed);
                List<AppointmentInfo> Appinfo = new List<AppointmentInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    Appinfo = ds.Tables[0].ToCollection<AppointmentInfo>();
                }
                return Appinfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private B2CUserInfo FillProfile(System.Data.DataRow dr)
        {
            B2CUserInfo bsInfo = new B2CUserInfo();

            if ((dr["FirstName"] != System.DBNull.Value))
            {
                bsInfo.FirstName = dr["FirstName"].ToString();
            }
            if ((dr["LastName"] != System.DBNull.Value))
            {
                bsInfo.LastName = dr["LastName"].ToString();
            }
            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                bsInfo.EmailAddress = dr["EmailAddress"].ToString();
            }
            if ((dr["CellPhone"] != System.DBNull.Value))
            {
                bsInfo.CellPhone = dr["CellPhone"].ToString();
            }
            if ((dr["StreetAddress"] != System.DBNull.Value))
            {
                bsInfo.StreetAddress = dr["StreetAddress"].ToString();
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                bsInfo.City = dr["City"].ToString();
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                bsInfo.State = dr["State"].ToString();
            }
            if ((dr["ZipCode"] != System.DBNull.Value))
            {
                bsInfo.ZipCode = Convert.ToInt32(dr["ZipCode"].ToString());
            }
            if ((dr["Photo"] != System.DBNull.Value))
            {
                bsInfo.Photo = dr["Photo"].ToString();
            }


            return bsInfo;
        }

        private BusinessInfo FillBusiness(System.Data.DataRow dr)
        {
            BusinessInfo bus = new BusinessInfo();
            if ((dr["BusinessName"] != System.DBNull.Value))
            {
                bus.BusinessName = dr["BusinessName"].ToString();
            }
            if ((dr["BusinessPhone1"] != System.DBNull.Value))
            {
                bus.BusinessPhone1 = dr["BusinessPhone1"].ToString();
            }
            if ((dr["StreetAddress1"] != System.DBNull.Value))
            {
                bus.StreetAddress1 = dr["StreetAddress1"].ToString();
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                bus.City = dr["City"].ToString();
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                bus.State = dr["State"].ToString();
            }
            if ((dr["Zipcode"] != System.DBNull.Value))
            {
                bus.Zipcode = Convert.ToInt32(dr["Zipcode"].ToString());
            }
            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                bus.EmailAddress = dr["EmailAddress"].ToString();
            }
            if ((dr["AutoType"] != System.DBNull.Value))
            {
                bus.AutoType = dr["AutoType"].ToString();
            }
            if ((dr["FulAutoDetails"] != System.DBNull.Value))
            {
                bus.FulAutoDetails = dr["FulAutoDetails"].ToString();
            }
            return bus;
        }

        private ServiceHistoryDetailsInfo FillServiceDetails(System.Data.DataRow dr)
        {
            ServiceHistoryDetailsInfo obj = new ServiceHistoryDetailsInfo();

            if ((dr["BusinessName"] != System.DBNull.Value))
            {
                obj.BusinessName = dr["BusinessName"].ToString();
            }
            if ((dr["AutoTitle"] != System.DBNull.Value))
            {
                obj.AutoTitle = dr["AutoTitle"].ToString();
            }
            if ((dr["ServiceName"] != System.DBNull.Value))
            {
                obj.ServiceName = dr["ServiceName"].ToString();
            }
            if ((dr["ServiceType"] != System.DBNull.Value))
            {
                obj.ServiceType = dr["ServiceType"].ToString();
            }
            if ((dr["ServiceItem"] != System.DBNull.Value))
            {
                obj.ServiceItem = dr["ServiceItem"].ToString();
            }

            return obj;
        }

        public AppointmentInfo GetAppointmentByRowIdStyle(int AppointId)
        {
            try
            {
                System.Data.DataSet ds = Appointment.Get_Appointment_By_RowId_Style(AppointId);
                AppointmentInfo obj = new AppointmentInfo();

                if (GlobalTools.IsSafeDataSet(ds) && ds.Tables[0].Rows.Count != 0)
                {
                    obj = FillGetAppointmentByRowIdStyle(ds.Tables[0].Rows[0]);
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private AppointmentInfo FillGetAppointmentByRowIdStyle(System.Data.DataRow dr)
        {
            AppointmentInfo aapoinInfo = new AppointmentInfo();

            if ((dr["AppointmentUniqueId"] != System.DBNull.Value))
            {
                aapoinInfo.AppointmentUniqueId = dr["AppointmentUniqueId"].ToString();
            }
            if ((dr["ManufacturYear"] != System.DBNull.Value))
            {
                aapoinInfo.ManufacturYear = Convert.ToInt32(dr["ManufacturYear"].ToString());
            }
            if ((dr["Manufacturer"] != System.DBNull.Value))
            {
                aapoinInfo.Manufacturer = dr["Manufacturer"].ToString();
            }
            if ((dr["Model"] != System.DBNull.Value))
            {
                aapoinInfo.Model = dr["Model"].ToString();
            }
            if ((dr["Style"] != System.DBNull.Value))
            {
                aapoinInfo.Style = dr["Style"].ToString();
            }
            if ((dr["Name"] != System.DBNull.Value))
            {
                aapoinInfo.Name = dr["Name"].ToString();
            }
            if ((dr["Type"] != System.DBNull.Value))
            {
                aapoinInfo.Type = dr["Type"].ToString();
            }
            if ((dr["Customer"] != System.DBNull.Value))
            {
                aapoinInfo.Customer = dr["Customer"].ToString();
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                aapoinInfo.Status = dr["Status"].ToString();
            }
            if ((dr["IsConfirmed"] != System.DBNull.Value))
            {
                aapoinInfo.IsConfirmed = dr["IsConfirmed"].ToString();
            }
            if ((dr["AppoinmentDate"] != System.DBNull.Value))
            {
                aapoinInfo.AppoinmentDate = dr["AppoinmentDate"].ToString();
            }
            if ((dr["AutoTitle"] != System.DBNull.Value))
            {
                aapoinInfo.AutoTitle = dr["AutoTitle"].ToString();
            }
            if ((dr["ServiceItem"] != System.DBNull.Value))
            {
                aapoinInfo.ServiceItem = dr["ServiceItem"].ToString();
            }

            return aapoinInfo;
        }

        public QuoteInfo GetQuoteByRowId(int RowId)
        {
            try
            {
                System.Data.DataSet ds = QT.Get_Quote_By_RowId(RowId);
                QuoteInfo qtInfo = new QuoteInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    qtInfo = FillQuote(ds.Tables[0].Rows[0]);
                }
                return qtInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private QuoteInfo FillQuote(System.Data.DataRow dr)
        {
            QuoteInfo queInfo = new QuoteInfo();

            if ((dr["QuoteUniqueID"] != System.DBNull.Value))
            {
                queInfo.QuoteUniqueID = dr["QuoteUniqueID"].ToString();
            }
            if ((dr["MYear"] != System.DBNull.Value))
            {
                queInfo.MYear = Convert.ToInt32(dr["MYear"].ToString());
            }
            if ((dr["Make"] != System.DBNull.Value))
            {
                queInfo.Make = dr["Make"].ToString();
            }
            if ((dr["Model"] != System.DBNull.Value))
            {
                queInfo.Model = dr["Model"].ToString();
            }
            if ((dr["Style"] != System.DBNull.Value))
            {
                queInfo.Style = dr["Style"].ToString();
            }
            if ((dr["ServiceName"] != System.DBNull.Value))
            {
                queInfo.ServiceName = dr["ServiceName"].ToString();
            }
            if ((dr["CustomerName"] != System.DBNull.Value))
            {
                queInfo.CustomerName = dr["CustomerName"].ToString();
            }
            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                queInfo.EmailAddress = dr["EmailAddress"].ToString();
            }

            return queInfo;
        }

        public B2BAppointmentResponse B2CAppointment_Completion_Detail(int AppointId)
        {
            B2BAppointmentResponse busInfo = new B2BAppointmentResponse();
            try
            {
                System.Data.DataSet ds = BS.B2CAppointment_Completion_Detail(AppointId);

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    busInfo = FillAppointCompletion(ds.Tables[0].Rows[0]);
                }
                return busInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private B2BAppointmentResponse FillAppointCompletion(System.Data.DataRow dr)
        {
            B2BAppointmentResponse busInfo = new B2BAppointmentResponse();

            if ((dr["FirstName"] != System.DBNull.Value))
            {
                busInfo.FirstName = dr["FirstName"].ToString();
            }
            if ((dr["BusinessName"] != System.DBNull.Value))
            {
                busInfo.BusinessName = dr["BusinessName"].ToString();
            }
            if ((dr["StreetAddress1"] != System.DBNull.Value))
            {
                busInfo.StreetAddress1 = dr["StreetAddress1"].ToString();
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                busInfo.City = dr["City"].ToString();
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                busInfo.State = dr["State"].ToString();
            }
            if ((dr["Zipcode"] != System.DBNull.Value))
            {
                busInfo.Zipcode = Convert.ToInt32(dr["Zipcode"].ToString());
            }
            if ((dr["BusinessPhone1"] != System.DBNull.Value))
            {
                busInfo.BusinessPhone1 = dr["BusinessPhone1"].ToString();
            }
            if ((dr["ManufacturYear"] != System.DBNull.Value))
            {
                busInfo.ManufacturYear = Convert.ToInt32(dr["ManufacturYear"].ToString());
            }
            if ((dr["Manufacturer"] != System.DBNull.Value))
            {
                busInfo.Manufacturer = dr["Manufacturer"].ToString();
            }
            if ((dr["Model"] != System.DBNull.Value))
            {
                busInfo.Model = dr["Model"].ToString();
            }
            if ((dr["Style"] != System.DBNull.Value))
            {
                busInfo.Style = dr["Style"].ToString();
            }
            if ((dr["Type"] != System.DBNull.Value))
            {
                busInfo.Type = dr["Type"].ToString();
            }
            if ((dr["AppoinmentDate"] != System.DBNull.Value))
            {
                busInfo.AppoinmentDate = dr["AppoinmentDate"].ToString();
            }
            if ((dr["StartTime"] != System.DBNull.Value))
            {
                busInfo.StartTime = dr["StartTime"].ToString();
            }
            if ((dr["ServiceItem"] != System.DBNull.Value))
            {
                busInfo.ServiceItem = dr["ServiceItem"].ToString();
            }
            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                busInfo.EmailAddress = dr["EmailAddress"].ToString();
            }

            return busInfo;
        }

        public List<BusinessSearch> GetFavoriteAutoshopByType(int contactId, string autoTypes)
        {
            try
            {
                System.Data.DataSet ds = BS.GetFavorite_Autoshop_By_Type(contactId, autoTypes);
                List<BusinessSearch> collection = new List<BusinessSearch>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessSearch>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        #region B2BMyPage
        public List<SubscriptionInfo> Select_SubscriptionType()
        {
            try
            {
                System.Data.DataSet ds = subscription.Select_SubscriptionType();
                List<SubscriptionInfo> collection = new List<SubscriptionInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<SubscriptionInfo>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AutomobileManufacturerInfo> SearchAutoManufacturer()
        {
            try
            {
                System.Data.DataSet ds = Automobilemanufacturer.SearchAutoManufacturer();
                List<AutomobileManufacturerInfo> Automobilemanucollection = new List<AutomobileManufacturerInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    Automobilemanucollection = ds.Tables[0].ToCollection<AutomobileManufacturerInfo>();
                }
                return Automobilemanucollection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public ArrayList GetB2BProfileByID(int RowId)
        {
            try
            {
                System.Data.DataSet ds = BSProfile.Get_BusinessProfile_By_ContactId(RowId);
                B2BProfileInfo bsInfo = new B2BProfileInfo();
                B2BServiceOprations B2BServiceOpn = new B2BServiceOprations();
                B2BCoupons BSCpn = new B2BCoupons();
                B2BAutoshopImage BSAutoshopImg = new B2BAutoshopImage();

                ArrayList list = new ArrayList();

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //if (GlobalTools.IsSafeDataSet(ds))
                    //{
                    //if (ds.Tables[i].Rows.Count != 0)
                    //{
                    if (i == 0)
                    {
                        bsInfo = FillBusinessProfile(ds.Tables[i].Rows[0]);
                        list.Add(bsInfo);
                    }
                    if (i == 1)
                    {
                        List<B2BServiceOprations> B2BServiceOpnList = new List<B2BServiceOprations>();

                        for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                        {
                            B2BServiceOpnList.Add(FillB2BServiceOprations(ds.Tables[i].Rows[j]));
                        }
                        list.Add(B2BServiceOpnList);
                    }
                    if (i == 2)
                    {
                        List<B2BCoupons> B2BCoupnsList = new List<B2BCoupons>();

                        for (int k = 0; k < ds.Tables[2].Rows.Count; k++)
                        {
                            B2BCoupnsList.Add(FillB2BCoupons(ds.Tables[i].Rows[k]));
                        }
                        list.Add(B2BCoupnsList);
                    }
                    if (i == 3)
                    {
                        List<B2BAutoshopImage> B2BAutoshopImageList = new List<B2BAutoshopImage>();

                        for (int l = 0; l < ds.Tables[3].Rows.Count; l++)
                        {
                            B2BAutoshopImageList.Add(FillB2BAutoshopImage(ds.Tables[i].Rows[l]));
                        }
                        list.Add(B2BAutoshopImageList);
                    }

                    // list.Add(bsInfo);
                    //}
                    //}
                }

                //if (GlobalTools.IsSafeDataSet(ds))
                //{
                //    //collection = ds.Tables[0].ToCollection<B2CUserInfo>();

                //    bsInfo = FillBusinessProfile(ds.Tables[0].Rows[0]);
                //}
                return list;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        /* public BusinessSubscriptonInfo GetBusinessSubscriptonByContactId(int ContactId)
         {
             try
             {
                 System.Data.DataSet ds = BSsubscription.Get_BusinessSubscripton_By_ContactId(ContactId);
                 BusinessSubscriptonInfo bssubscriptionInfo = new BusinessSubscriptonInfo();

                 if (GlobalTools.IsSafeDataSet(ds))
                 {
                     bssubscriptionInfo = FillBusinessSubscripton(ds.Tables[0].Rows[0]);
                 }
                 return bssubscriptionInfo;
             }
             catch (System.Exception)
             {
                 throw;
             }
         }*/

        public List<QuoteInfo> quoteinfo(int BusinessId)
        {
            try
            {
                System.Data.DataSet ds = Quotes.Select_Quotes_By_BusinessId(BusinessId);
                List<QuoteInfo> quoinfo = new List<QuoteInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    quoinfo = ds.Tables[0].ToCollection<QuoteInfo>();
                }
                return quoinfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteDetails> quoteDetails(int QuoteId)
        {
            try
            {
                System.Data.DataSet ds = Quotes.Select_Quotes_By_QuoteId(QuoteId);
                List<QuoteDetails> quoDetails = new List<QuoteDetails>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    quoDetails = ds.Tables[0].ToCollection<QuoteDetails>();
                }
                return quoDetails;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfo> quoteHeader(int BusinessId)
        {
            try
            {
                System.Data.DataSet ds = Quotes.Select_Quotes_QuoteHeader(BusinessId);
                List<QuoteInfo> quoheader = new List<QuoteInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    quoheader = ds.Tables[0].ToCollection<QuoteInfo>();
                }
                return quoheader;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private B2BProfileInfo FillBusinessProfile(System.Data.DataRow dr)
        {
            B2BProfileInfo bsinfoprofile = new B2BProfileInfo();

            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                bsinfoprofile.BusinessId = Convert.ToInt32(dr["BusinessId"].ToString());
            }
            if ((dr["ContactId"] != System.DBNull.Value))
            {
                bsinfoprofile.ContactId = Convert.ToInt32(dr["ContactId"].ToString());
            }
            if ((dr["AutoShopName"] != System.DBNull.Value))
            {
                bsinfoprofile.AutoShopName = dr["AutoShopName"].ToString();
            }
            if ((dr["AutoShopType"] != System.DBNull.Value))
            {
                bsinfoprofile.AutoShopType = dr["AutoShopType"].ToString();
            }
            if ((dr["ContactFirstName"] != System.DBNull.Value))
            {
                bsinfoprofile.ContactFirstName = dr["ContactFirstName"].ToString();
            }
            if ((dr["ContactLastName"] != System.DBNull.Value))
            {
                bsinfoprofile.ContactLastName = dr["ContactLastName"].ToString();
            }
            if ((dr["AutoShopAddress1"] != System.DBNull.Value))
            {
                bsinfoprofile.AutoShopAddress1 = dr["AutoShopAddress1"].ToString();
            }
            if ((dr["AutoShopAddress2"] != System.DBNull.Value))
            {
                bsinfoprofile.AutoShopAddress2 = dr["AutoShopAddress2"].ToString();
            }
            if ((dr["AutoShopCity"] != System.DBNull.Value))
            {
                bsinfoprofile.AutoShopCity = dr["AutoShopCity"].ToString();
            }
            if ((dr["AutoShopState"] != System.DBNull.Value))
            {
                bsinfoprofile.AutoShopState = dr["AutoShopState"].ToString();
            }
            if ((dr["AutoShopZipCode"] != System.DBNull.Value))
            {
                bsinfoprofile.AutoShopZipCode = dr["AutoShopZipCode"].ToString();
            }
            if ((dr["ServiceCenterPh"] != System.DBNull.Value))
            {
                bsinfoprofile.ServiceCenterPh = dr["ServiceCenterPh"].ToString();
            }
            if ((dr["ContactPhNo"] != System.DBNull.Value))
            {
                bsinfoprofile.ContactPhNo = dr["ContactPhNo"].ToString();
            }
            if ((dr["FaxNo"] != System.DBNull.Value))
            {
                bsinfoprofile.FaxNo = dr["FaxNo"].ToString();
            }
            if ((dr["Website"] != System.DBNull.Value))
            {
                bsinfoprofile.Website = dr["Website"].ToString();
            }
            if ((dr["ServiceOffered"] != System.DBNull.Value))
            {
                bsinfoprofile.ServiceOffered = dr["ServiceOffered"].ToString();
            }
            if ((dr["Specialities"] != System.DBNull.Value))
            {
                bsinfoprofile.Specialities = dr["Specialities"].ToString();
            }
            if ((dr["CouponName"] != System.DBNull.Value))
            {
                bsinfoprofile.CouponName = dr["CouponName"].ToString();
            }
            if ((dr["CouponStartDate"] != System.DBNull.Value))
            {
                bsinfoprofile.CouponStartDate = Convert.ToDateTime(dr["CouponStartDate"].ToString());
            }
            if ((dr["CouponEndDate"] != System.DBNull.Value))
            {
                bsinfoprofile.CouponEndDate = Convert.ToDateTime(dr["CouponEndDate"].ToString());
            }
            if ((dr["CouponActiveFlag"] != System.DBNull.Value))
            {
                bsinfoprofile.CouponActiveFlag = Convert.ToBoolean(dr["CouponActiveFlag"].ToString());
            }
            if ((dr["RepairShop"] != System.DBNull.Value))
            {
                bsinfoprofile.RepairShop = Convert.ToBoolean(dr["RepairShop"].ToString());
            }
            if ((dr["BodyShop"] != System.DBNull.Value))
            {
                bsinfoprofile.BodyShop = Convert.ToBoolean(dr["BodyShop"].ToString());
            }
            if ((dr["TyreShop"] != System.DBNull.Value))
            {
                bsinfoprofile.TyreShop = Convert.ToBoolean(dr["TyreShop"].ToString());
            }
            if ((dr["CarWash"] != System.DBNull.Value))
            {
                bsinfoprofile.CarWash = Convert.ToBoolean(dr["CarWash"].ToString());
            }
            if ((dr["Transport"] != System.DBNull.Value))
            {
                bsinfoprofile.Transport = Convert.ToBoolean(dr["Transport"].ToString());
            }
            if ((dr["Towing"] != System.DBNull.Value))
            {
                bsinfoprofile.Towing = Convert.ToBoolean(dr["Towing"].ToString());
            }
            if ((dr["SmogCheck"] != System.DBNull.Value))
            {
                bsinfoprofile.SmogCheck = Convert.ToBoolean(dr["SmogCheck"].ToString());
            }
            if ((dr["SmogCheckOnly"] != System.DBNull.Value))
            {
                bsinfoprofile.SmogCheckOnly = Convert.ToBoolean(dr["SmogCheckOnly"].ToString());
            }
            if ((dr["GlassShop"] != System.DBNull.Value))
            {
                bsinfoprofile.GlassShop = Convert.ToBoolean(dr["GlassShop"].ToString());
            }
            if ((dr["Dealer"] != System.DBNull.Value))
            {
                bsinfoprofile.Dealer = Convert.ToBoolean(dr["Dealer"].ToString());
            }
            if ((dr["Parts"] != System.DBNull.Value))
            {
                bsinfoprofile.Parts = Convert.ToBoolean(dr["Parts"].ToString());
            }
            if ((dr["NewCarSales"] != System.DBNull.Value))
            {
                bsinfoprofile.NewCarSales = Convert.ToBoolean(dr["NewCarSales"].ToString());
            }
            if ((dr["UsedCarSales"] != System.DBNull.Value))
            {
                bsinfoprofile.UsedCarSales = Convert.ToBoolean(dr["UsedCarSales"].ToString());
            }
            if ((dr["MemAAA"] != System.DBNull.Value))
            {
                bsinfoprofile.MemAAA = Convert.ToBoolean(dr["MemAAA"].ToString());
            }
            if ((dr["MemACE"] != System.DBNull.Value))
            {
                bsinfoprofile.MemACE = Convert.ToBoolean(dr["MemACE"].ToString());
            }
            if ((dr["MemASE"] != System.DBNull.Value))
            {
                bsinfoprofile.MemASE = Convert.ToBoolean(dr["MemASE"].ToString());
            }
            if ((dr["MemBBB"] != System.DBNull.Value))
            {
                bsinfoprofile.MemBBB = Convert.ToBoolean(dr["MemBBB"].ToString());
            }
            if ((dr["Manufacturer"] != System.DBNull.Value))
            {
                bsinfoprofile.Manufacturer = dr["Manufacturer"].ToString();
            }
            if ((dr["StarCertified"] != System.DBNull.Value))
            {
                bsinfoprofile.StarCertified = Convert.ToBoolean(dr["StarCertified"].ToString());
            }

            return bsinfoprofile;
        }

        private B2BServiceOprations FillB2BServiceOprations(System.Data.DataRow dr)
        {
            B2BServiceOprations B2Bservice = new B2BServiceOprations();

            if ((dr["IsWorkingDay"] != System.DBNull.Value))
            {
                B2Bservice.IsWorkingDay = Convert.ToBoolean(dr["IsWorkingDay"].ToString());
            }
            if ((dr["StartTime"] != System.DBNull.Value))
            {
                B2Bservice.StartTime = Convert.ToDateTime(dr["StartTime"].ToString());
            }
            if ((dr["EndTime"] != System.DBNull.Value))
            {
                B2Bservice.EndTime = Convert.ToDateTime(dr["EndTime"].ToString());
            }
            if ((dr["WeekDayId"] != System.DBNull.Value))
            {
                B2Bservice.WeekDayId = Convert.ToInt32(dr["WeekDayId"].ToString());
            }

            return B2Bservice;
        }

        private B2BCoupons FillB2BCoupons(System.Data.DataRow dr)
        {
            B2BCoupons B2BCpn = new B2BCoupons();

            if ((dr["CouponNo"] != System.DBNull.Value))
            {
                B2BCpn.CouponNo = Convert.ToInt32(dr["CouponNo"].ToString());
            }
            if ((dr["CouponName"] != System.DBNull.Value))
            {
                B2BCpn.CouponName = dr["CouponName"].ToString();
            }
            if ((dr["CouponActive"] != System.DBNull.Value))
            {
                B2BCpn.CouponActive = Convert.ToBoolean(dr["CouponActive"].ToString());
            }
            if ((dr["CouponStart"] != System.DBNull.Value))
            {
                B2BCpn.CouponStart = Convert.ToDateTime(dr["CouponStart"].ToString());
            }
            if ((dr["CouponEnd"] != System.DBNull.Value))
            {
                B2BCpn.CouponEnd = Convert.ToDateTime(dr["CouponEnd"].ToString());
            }
            if ((dr["CouponImage"] != System.DBNull.Value))
            {
                B2BCpn.CouponImage = dr["CouponImage"].ToString();
            }

            return B2BCpn;
        }

        private B2BAutoshopImage FillB2BAutoshopImage(System.Data.DataRow dr)
        {
            B2BAutoshopImage B2BAutoshopImg = new B2BAutoshopImage();

            if ((dr["AutoShopImageNo"] != System.DBNull.Value))
            {
                B2BAutoshopImg.AutoShopImageNo = Convert.ToInt32(dr["AutoShopImageNo"].ToString());
            }
            if ((dr["ImageActiveFlag"] != System.DBNull.Value))
            {
                B2BAutoshopImg.ImageActiveFlag = Convert.ToBoolean(dr["ImageActiveFlag"].ToString());
            }
            if ((dr["AutoShopImage"] != System.DBNull.Value))
            {
                B2BAutoshopImg.AutoShopImage = dr["AutoShopImage"].ToString();
            }

            return B2BAutoshopImg;
        }

        public BusinessSubscriptonInfo GetBusinessSubscriptonByContactId(int ContactId)
        {
            try
            {
                System.Data.DataSet ds = BSsubscription.Get_BusinessSubscripton_By_ContactId(ContactId);
                BusinessSubscriptonInfo bssubscriptionInfo = new BusinessSubscriptonInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        bssubscriptionInfo = FillBusinessSubscripton(ds.Tables[0].Rows[0]);
                    }
                    else
                    {
                        bssubscriptionInfo = null;
                    }
                }
                return bssubscriptionInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        /*        public List<QuoteInfo> quoteinfo(int BusinessId)*/

        private BusinessSubscriptonInfo FillBusinessSubscripton(System.Data.DataRow dr)
        {
            BusinessSubscriptonInfo BSBusinessSubscripton = new BusinessSubscriptonInfo();

            if ((dr["BillingStreetName"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.BillingStreetName = dr["BillingStreetName"].ToString();
            }
            if ((dr["BillingCity"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.BillingCity = dr["BillingCity"].ToString();
            }
            if ((dr["BillingState"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.BillingState = dr["BillingState"].ToString();
            }
            if ((dr["BillingZipCode"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.BillingZipCode = Convert.ToInt32(dr["BillingZipCode"].ToString());
            }
            if ((dr["Type"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.Type = dr["Type"].ToString();
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.IsActive = Convert.ToBoolean(dr["IsActive"].ToString());
            }
            if ((dr["Amount"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.Amount = Convert.ToDouble(dr["Amount"].ToString());
            }
            if ((dr["ValidationDays"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.ValidationDays = Convert.ToInt32(dr["ValidationDays"].ToString());
            }
            if ((dr["RowId"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.RowId = Convert.ToInt32(dr["RowId"].ToString());
            }
            if ((dr["EndDate"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.EndDate = Convert.ToDateTime(dr["EndDate"].ToString());
            }
            if ((dr["BSRowId"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.BSRowId = Convert.ToInt32(dr["BSRowId"].ToString());
            }
            if ((dr["Coupon"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.Coupon = Convert.ToBoolean(dr["Coupon"].ToString());
            }
            if ((dr["Appointment"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.Appointment = Convert.ToBoolean(dr["Appointment"].ToString());
            }
            if ((dr["Chat"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.Chat = Convert.ToBoolean(dr["Chat"].ToString());
            }
            if ((dr["AutoRenewal"] != System.DBNull.Value))
            {
                BSBusinessSubscripton.AutoRenewal = Convert.ToBoolean(dr["AutoRenewal"].ToString());
            }
            return BSBusinessSubscripton;
        }

        public SubscriptionInfo SubscriptInfo(string subscriberType)
        {
            try
            {
                System.Data.DataSet ds = subscription.Select_SubscriptionDuration(subscriberType);
                SubscriptionInfo subscriptionInfo = new SubscriptionInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if (ds.Tables[0].Rows.Count != 0)
                        subscriptionInfo = FillSubscriptionInfo(ds.Tables[0].Rows[0]);
                }
                return subscriptionInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private SubscriptionInfo FillSubscriptionInfo(System.Data.DataRow dr)
        {
            SubscriptionInfo SubscriptionInfo = new SubscriptionInfo();

            if ((dr["RowId"] != System.DBNull.Value))
            {
                SubscriptionInfo.RowId = Convert.ToInt32(dr["RowId"].ToString());
            }
            if ((dr["ValidationDays"] != System.DBNull.Value))
            {
                SubscriptionInfo.ValidationDays = Convert.ToInt32(dr["ValidationDays"].ToString());
            }

            return SubscriptionInfo;
        }

        public SubscriptionInfo SubscriptionInfoAmount(string subDuration)
        {
            try
            {
                System.Data.DataSet ds = subscription.Select_SubscriptionAmount(subDuration);
                SubscriptionInfo subscriptionInfo = new SubscriptionInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if (ds.Tables[0].Rows.Count != 0)
                        subscriptionInfo = FillSubscriptionAmount(ds.Tables[0].Rows[0]);
                }
                return subscriptionInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private SubscriptionInfo FillSubscriptionAmount(System.Data.DataRow dr)
        {
            SubscriptionInfo SubscriptionInfo = new SubscriptionInfo();

            if ((dr["Amount"] != System.DBNull.Value))
            {
                SubscriptionInfo.Amount = Convert.ToDecimal(dr["Amount"].ToString());
            }
            if ((dr["Description"] != System.DBNull.Value))
            {
                SubscriptionInfo.Description = dr["Description"].ToString();
            }
            if ((dr["Discount"] != System.DBNull.Value))
            {
                SubscriptionInfo.Discount = Convert.ToInt32(dr["Discount"].ToString());
            }


            return SubscriptionInfo;
        }

        public QuoteInfo quoteInfo(int RowId, int BusinessId)
        {
            try
            {
                System.Data.DataSet ds = Quotes.QuoteB2B_Select_By_RowID(RowId, BusinessId);
                QuoteInfo quoteinfo = new QuoteInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    quoteinfo = FillQuoteB2BSelectByRowID(ds.Tables[0].Rows[0]);
                }
                return quoteinfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private QuoteInfo FillQuoteB2BSelectByRowID(System.Data.DataRow dr)
        {
            QuoteInfo queInfo = new QuoteInfo();

            if ((dr["CustomerName"] != System.DBNull.Value))
            {
                queInfo.CustomerName = dr["CustomerName"].ToString();
            }
            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                queInfo.EmailAddress = dr["EmailAddress"].ToString();
            }
            if ((dr["AutoTitle"] != System.DBNull.Value))
            {
                queInfo.AutoTitle = dr["AutoTitle"].ToString();
            }
            if ((dr["ServiceItem"] != System.DBNull.Value))
            {
                queInfo.ServiceItem = dr["ServiceItem"].ToString();
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                queInfo.Status = dr["Status"].ToString();
            }
            if ((dr["Price"] != System.DBNull.Value))
            {
                queInfo.priceDC = Convert.ToDecimal(dr["Price"].ToString());
            }
            if ((dr["UserComments"] != System.DBNull.Value))
            {
                queInfo.UserComments = dr["UserComments"].ToString();
            }
            if ((dr["Comments"] != System.DBNull.Value))
            {
                queInfo.Comments = dr["Comments"].ToString();
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                queInfo.ModifiedDate = dr["ModifiedDate"].ToString();
            }
            if ((dr["MYear"] != System.DBNull.Value))
            {
                queInfo.MYear = Convert.ToInt32(dr["MYear"].ToString());
            }
            if ((dr["Make"] != System.DBNull.Value))
            {
                queInfo.Make = dr["Make"].ToString();
            }
            if ((dr["Model"] != System.DBNull.Value))
            {
                queInfo.Model = dr["Model"].ToString();
            }
            if ((dr["Style"] != System.DBNull.Value))
            {
                queInfo.Make = dr["Style"].ToString();
            }

            return queInfo;
        }

        public BusinessInfo GetSelectBusinesssByRowId(int BusinessId)
        {
            try
            {
                System.Data.DataSet ds = BS.Select_Businesss_By_RowId(BusinessId);
                BusinessInfo BSInfo = new BusinessInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    BSInfo = FillSelectBusinesssByRowId(ds.Tables[0].Rows[0]);
                }
                return BSInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private BusinessInfo FillSelectBusinesssByRowId(System.Data.DataRow dr)
        {
            BusinessInfo BusInfo = new BusinessInfo();

            if ((dr["BusinessName"] != System.DBNull.Value))
            {
                BusInfo.BusinessName = dr["BusinessName"].ToString();
            }
            if ((dr["BusinessPhone1"] != System.DBNull.Value))
            {
                BusInfo.BusinessPhone1 = dr["BusinessPhone1"].ToString();
            }
            if ((dr["StreetAddress1"] != System.DBNull.Value))
            {
                BusInfo.StreetAddress1 = dr["StreetAddress1"].ToString();
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                BusInfo.City = dr["City"].ToString();
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                BusInfo.State = dr["State"].ToString();
            }
            if ((dr["Zipcode"] != System.DBNull.Value))
            {
                BusInfo.Zipcode = Convert.ToInt32(dr["Zipcode"].ToString());
            }
            return BusInfo;
        }
        #endregion

        #region B2BRegister
        public List<BusinessRegisterInfo> BusinessRegiInfo(string busName, int zipcode)
        {
            try
            {
                System.Data.DataSet ds = BS.B2BRegSearchList(busName, zipcode);
                List<BusinessRegisterInfo> BSRegiInfo = new List<BusinessRegisterInfo>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    BSRegiInfo = ds.Tables[0].ToCollection<BusinessRegisterInfo>();
                }
                return BSRegiInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public B2BProfileInfo BusinessRegidter(int busId)
        {
            try
            {
                System.Data.DataSet ds = BS.GetB2BDetailsByBusinessId(busId);
                B2BProfileInfo B2BRegiInfo = new B2BProfileInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    B2BRegiInfo = FillB2BDetailsByBusinessId(ds.Tables[0].Rows[0]);
                }
                return B2BRegiInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private B2BProfileInfo FillB2BDetailsByBusinessId(System.Data.DataRow dr)
        {
            B2BProfileInfo B2BRegiInfo = new B2BProfileInfo();

            if ((dr["AutoShopName"] != System.DBNull.Value))
            {
                B2BRegiInfo.AutoShopName = dr["AutoShopName"].ToString();
            }
            if ((dr["AutoShopAddress1"] != System.DBNull.Value))
            {
                B2BRegiInfo.AutoShopAddress1 = dr["AutoShopAddress1"].ToString();
            }
            if ((dr["AutoShopAddress2"] != System.DBNull.Value))
            {
                B2BRegiInfo.AutoShopAddress2 = dr["AutoShopAddress2"].ToString();
            }
            if ((dr["AutoShopCity"] != System.DBNull.Value))
            {
                B2BRegiInfo.AutoShopCity = dr["AutoShopCity"].ToString();
            }
            if ((dr["AutoShopState"] != System.DBNull.Value))
            {
                B2BRegiInfo.AutoShopState = dr["AutoShopState"].ToString();
            }
            if ((dr["AutoShopZipCode"] != System.DBNull.Value))
            {
                B2BRegiInfo.AutoShopZipCode = dr["AutoShopZipCode"].ToString();
            }
            if ((dr["ServiceCenterPh"] != System.DBNull.Value))
            {
                B2BRegiInfo.ServiceCenterPh = dr["ServiceCenterPh"].ToString();
            }
            if ((dr["FaxNo"] != System.DBNull.Value))
            {
                B2BRegiInfo.FaxNo = dr["FaxNo"].ToString();
            }
            if ((dr["Website"] != System.DBNull.Value))
            {
                B2BRegiInfo.Website = dr["Website"].ToString();
            }
            if ((dr["ServiceOffered"] != System.DBNull.Value))
            {
                B2BRegiInfo.ServiceOffered = dr["ServiceOffered"].ToString();
            }
            if ((dr["Specialities"] != System.DBNull.Value))
            {
                B2BRegiInfo.Specialities = dr["Specialities"].ToString();
            }
            if ((dr["RepairShop"] != System.DBNull.Value))
            {
                B2BRegiInfo.RepairShop = Convert.ToBoolean(dr["RepairShop"].ToString());
            }
            if ((dr["BodyShop"] != System.DBNull.Value))
            {
                B2BRegiInfo.BodyShop = Convert.ToBoolean(dr["BodyShop"].ToString());
            }
            if ((dr["TyreShop"] != System.DBNull.Value))
            {
                B2BRegiInfo.TyreShop = Convert.ToBoolean(dr["TyreShop"].ToString());
            }
            if ((dr["SmogCheck"] != System.DBNull.Value))
            {
                B2BRegiInfo.SmogCheck = Convert.ToBoolean(dr["SmogCheck"].ToString());
            }
            if ((dr["CarWash"] != System.DBNull.Value))
            {
                B2BRegiInfo.CarWash = Convert.ToBoolean(dr["CarWash"].ToString());
            }
            if ((dr["Towing"] != System.DBNull.Value))
            {
                B2BRegiInfo.Towing = Convert.ToBoolean(dr["Towing"].ToString());
            }
            if ((dr["AutoTransport"] != System.DBNull.Value))
            {
                B2BRegiInfo.AutoTransport = Convert.ToBoolean(dr["AutoTransport"].ToString());
            }
            return B2BRegiInfo;
        }
        #endregion

        #region AutoDetailMypage
        public List<Businesscoupon> BSCouponsInfo(int BusId)
        {
            try
            {
                System.Data.DataSet ds = BSCoupons.Select_BusinessCoupons_By_BusinessId(BusId);
                List<Businesscoupon> BSCouponInfo = new List<Businesscoupon>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    BSCouponInfo = ds.Tables[0].ToCollection<Businesscoupon>();
                }
                return BSCouponInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }
        #endregion

        public int GetCurrentBusinessId(int contactId, out int ReturnBusId)
        {
            try
            {
                return BS.GetCurrentBusinessId(contactId, out ReturnBusId);
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public BusinessInfo GetAdressbyBusId(int contactId)
        {
            try
            {
                System.Data.DataSet ds = BS.Get_Business_Adress(contactId);
                BusinessInfo BSInfo = new BusinessInfo();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    BSInfo = FillBusBillAddress(ds.Tables[0].Rows[0]);
                }
                return BSInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private BusinessInfo FillBusBillAddress(System.Data.DataRow dr)
        {
            BusinessInfo BusInfo = new BusinessInfo();
            if ((dr["ContactName"] != System.DBNull.Value))
            {
                BusInfo.ContactName = dr["ContactName"].ToString();
            }
            if ((dr["StreetAddress1"] != System.DBNull.Value))
            {
                BusInfo.StreetAddress1 = dr["StreetAddress1"].ToString();
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                BusInfo.City = dr["City"].ToString();
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                BusInfo.State = dr["State"].ToString();
            }
            if ((dr["Zipcode"] != System.DBNull.Value))
            {
                BusInfo.Zipcode = Convert.ToInt32(dr["Zipcode"].ToString());
            }
            return BusInfo;
        }

        public List<AutoshopDeal> GetDealList(string searchText, string autoshopType)
        {
            try
            {
                System.Data.DataSet ds = DL.GetAutoshopDealList(searchText, autoshopType);
                List<AutoshopDeal> collection = new List<AutoshopDeal>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AutoshopDeal>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        //public int GetUserLocationByRowId(int RowId)
        //{
        //    try
        //    {
        //        System.Data.DataSet ds = QT.Get_UserLocation_By_RowId(RowId);

        //        return ds;
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //}
    }

    public static class MyExtensionClass
    {
        public static List<T> ToCollection<T>(this DataTable dt)
        {
            List<T> lst = new System.Collections.Generic.List<T>();
            Type tClass = typeof(T);
            PropertyInfo[] pClass = tClass.GetProperties();
            List<DataColumn> dc = dt.Columns.Cast<DataColumn>().ToList();
            T cn;
            foreach (DataRow item in dt.Rows)
            {
                cn = (T)Activator.CreateInstance(tClass);
                foreach (PropertyInfo pc in pClass)
                {
                    // Can comment try catch block. 
                    try
                    {
                        DataColumn d = dc.Find(c => c.ColumnName == pc.Name);
                        if (d != null)
                            pc.SetValue(cn, item[pc.Name], null);
                    }
                    catch
                    {
                    }
                }
                lst.Add(cn);
            }
            return lst;
        }
    }




}
