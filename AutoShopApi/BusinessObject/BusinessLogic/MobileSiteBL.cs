﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Configuration;
using AutoShop.BusinessObjects.Entities;
using System.Web.Configuration;

namespace AutoShop.BusinessObjects.BusinessLogic
{
    public class MobileSiteBL
    {
        public AutoShop.DAL.Appointment Appointment;
        public AutoShop.DAL.Servicehistory Servicehistory;
        public AutoShop.DAL.Service Service;
        public AutoShop.DAL.Quote Quote;
        public AutoShop.DAL.Quoteresponse Quoteresponse;
        public AutoShop.DAL.Automobile Automobile;
        public AutoShop.DAL.Businesshours Businesshours;
        public AutoShop.DAL.Business Business;
        public AutoShop.DAL.SearchBusiness SRB;
        public AutoShop.DAL.Businesscoupon BSCoupons;
        public AutoShop.DAL.Autorecall Rcal;

        public MobileSiteBL()
        {
            if (Appointment == null)
            {
                Appointment = new DAL.Appointment();
            }
            if (Servicehistory == null)
            {
                Servicehistory = new DAL.Servicehistory();
            }
            if (Service == null)
            {
                Service = new DAL.Service();
            }
            if (Quote == null)
            {
                Quote = new DAL.Quote();
            }
            if (Quoteresponse == null)
            {
                Quoteresponse = new DAL.Quoteresponse();
            }
            if (Automobile == null)
            {
                Automobile = new DAL.Automobile();
            }
            if (Businesshours == null)
            {
                Businesshours = new DAL.Businesshours();
            }
            if (Business == null)
            {
                Business = new DAL.Business();
            }
            if (SRB == null)
            {
                SRB = new AutoShop.DAL.SearchBusiness();
            }
            if (BSCoupons == null)
            {
                BSCoupons = new AutoShop.DAL.Businesscoupon();
            }
            if (Rcal == null)
            {
                Rcal = new AutoShop.DAL.Autorecall();
            }
        }

        public List<AppointmentInfoM> GetAppointmentByAutoId(int AutoId)
        {
            try
            {
                System.Data.DataSet ds = Appointment.Get_Appointments_By_AutoId(AutoId);
                List<AppointmentInfoM> collection = new List<AppointmentInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AppointmentInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<ServiceHistoryDetailsInfoM> GetServiceHistoryByAutoId(int AutoId)
        {
            try
            {
                System.Data.DataSet ds = Servicehistory.Select_ServiceHistory_By_AutomobileId(AutoId);
                List<ServiceHistoryDetailsInfoM> srHistoryList = new List<ServiceHistoryDetailsInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    srHistoryList = ds.Tables[0].ToCollection<ServiceHistoryDetailsInfoM>();
                }
                return srHistoryList;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RecomandedServiceInfoM> GetRecommandedServices(int AutoId)
        {
            try
            {
                System.Data.DataSet ds = Service.Select_Recommanded_Services_By_AutoId_Style(AutoId);
                List<RecomandedServiceInfoM> collection = new List<RecomandedServiceInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RecomandedServiceInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RepairTypesM> GetRepairTypes()
        {
            try
            {
                System.Data.DataSet ds = Service.Select_Repair_Types();
                List<RepairTypesM> collection = new List<RepairTypesM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RepairTypesM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RecomandedServiceInfoM> GetServicesByRepairTypeId(int repairTypeId)
        {
            try
            {
                System.Data.DataSet ds = Service.Select_Services_By_RepairTypeId(repairTypeId);
                List<RecomandedServiceInfoM> collection = new List<RecomandedServiceInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RecomandedServiceInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }
        
        public QuoteInfoM GetQuoteDetailsMail(int RowId, int SerId)
        {
            try
            {
                System.Data.DataSet ds = Quote.GetQuoteDetailsForMail(RowId, SerId);
                QuoteInfoM qutInfo = new QuoteInfoM();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    qutInfo = FillQuote(ds.Tables[0].Rows[0]);
                }
                return qutInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private QuoteInfoM FillQuote(System.Data.DataRow dr)
        {
            QuoteInfoM quoteInfo = new QuoteInfoM();

            if ((dr["RowId"] != System.DBNull.Value))
            {
                quoteInfo.RowId = Convert.ToInt32(dr["RowId"].ToString());
            }
            if ((dr["SerID"] != System.DBNull.Value))
            {
                quoteInfo.SerID = Convert.ToInt32(dr["SerID"].ToString());
            }

            return quoteInfo;
        }

        public List<QuoteInfoM> GetQuoteResponseById(int QuoteId)
        {
            try
            {
                System.Data.DataSet ds = Quoteresponse.Select_QuoteResponses_By_QuoteId(QuoteId);
                List<QuoteInfoM> collection = new List<QuoteInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfoM> GetQuoteResponseByIdsrItems(int QuoteId)
        {
            try
            {
                System.Data.DataSet ds = Quoteresponse.Select_QuoteResponses_By_QuoteId_srItems(QuoteId);
                List<QuoteInfoM> collection = new List<QuoteInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfoM> GetRequestedQuotesByContactId(int contactId)
        {
            try
            {
                System.Data.DataSet ds = Quote.Select_Quotes_By_ContactId(contactId);
                List<QuoteInfoM> collection = new List<QuoteInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AutomobileInfoM> GetAutomobiles(int ContactId)
        {
            try
            {
                int i = 0;

                System.Data.DataSet ds = Automobile.Select_Automobiles_List_By_ContactIid_Style(ContactId);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageSrc"].ToString()) && string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageName"].ToString()))
                    {
                        ds.Tables[0].Rows[i]["ImageSrc"] = WebConfigurationManager.AppSettings["DefaultPath"];
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ImageName"].ToString()))
                        {
                            ds.Tables[0].Rows[i]["ImageSrc"] = WebConfigurationManager.AppSettings["GetRepositoryPathMobile"] + ds.Tables[0].Rows[i]["ImageName"].ToString();
                        }
                    }
                    i++;
                }

                List<AutomobileInfoM> collection = new List<AutomobileInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AutomobileInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RepairTypesM> GetRepairTypesByAutoType(string AutoType)
        {
            try
            {
                System.Data.DataSet ds = Service.Select_Repair_Types_By_AutoshopType(AutoType);
                List<RepairTypesM> collection = new List<RepairTypesM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RepairTypesM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<ServiceLineItemsM> GetServiceLinesByServiceId(int serviceId)
        {
            try
            {
                System.Data.DataSet ds = Service.Select_ServiceLines_By_ServiceId(serviceId);
                List<ServiceLineItemsM> collection = new List<ServiceLineItemsM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<ServiceLineItemsM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<HoursInfoM> GetBusinessHoursToday(int busId, string day)
        {
            try
            {
                System.Data.DataSet ds = Businesshours.Select_BusinessHourss_By_BusinessIdAndDay_Today(busId, day);
                List<HoursInfoM> collection = new List<HoursInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<HoursInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<HoursInfoM> GetBusinessHours(int busId, string day)
        {
            try
            {
                System.Data.DataSet ds = Businesshours.Select_BusinessHourss_By_BusinessIdAndDay(busId, day);
                List<HoursInfoM> collection = new List<HoursInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<HoursInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<HoursInfoM> GetBusinessHoursByBusId(int busId, string day)
        {
            try
            {
                System.Data.DataSet ds = Businesshours.GetBusinessHours(busId, day);
                List<HoursInfoM> collection = new List<HoursInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<HoursInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public BusinessInfoM GetSelectBusinesssByRowId(int BusinessId)
        {
            try
            {
                System.Data.DataSet ds = Business.Select_Businesss_By_RowId(BusinessId);
                BusinessInfoM BSInfo = new BusinessInfoM();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    BSInfo = FillSelectBusinesssByRowId(ds.Tables[0].Rows[0]);
                }
                return BSInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private BusinessInfoM FillSelectBusinesssByRowId(System.Data.DataRow dr)
        {
            BusinessInfoM BusInfo = new BusinessInfoM();

            if ((dr["BusinessName"] != System.DBNull.Value))
            {
                BusInfo.BusinessName = dr["BusinessName"].ToString();
            }
            if ((dr["BusinessPhone1"] != System.DBNull.Value))
            {
                BusInfo.BusinessPhone1 = dr["BusinessPhone1"].ToString();
            }
            if ((dr["StreetAddress1"] != System.DBNull.Value))
            {
                BusInfo.StreetAddress1 = dr["StreetAddress1"].ToString();
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                BusInfo.City = dr["City"].ToString();
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                BusInfo.State = dr["State"].ToString();
            }
            if ((dr["Zipcode"] != System.DBNull.Value))
            {
                BusInfo.Zipcode = Convert.ToInt32(dr["Zipcode"].ToString());
            }
            return BusInfo;
        }

        public BusinessInfoM GetBusinessDetailByID(int RowId)
        {
            try
            {
                System.Data.DataSet ds = Business.Get_Businesss_FullDetails_By_BusId(RowId);
                BusinessInfoM obj = new BusinessInfoM();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    obj = FillBusiness(ds.Tables[0].Rows[0]);
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private BusinessInfoM FillBusiness(System.Data.DataRow dr)
        {
            BusinessInfoM bus = new BusinessInfoM();
            if ((dr["BusinessName"] != System.DBNull.Value))
            {
                bus.BusinessName = dr["BusinessName"].ToString();
            }
            if ((dr["BusinessPhone1"] != System.DBNull.Value))
            {
                bus.BusinessPhone1 = dr["BusinessPhone1"].ToString();
            }
            if ((dr["StreetAddress1"] != System.DBNull.Value))
            {
                bus.StreetAddress1 = dr["StreetAddress1"].ToString();
            }
            if ((dr["City"] != System.DBNull.Value))
            {
                bus.City = dr["City"].ToString();
            }
            if ((dr["State"] != System.DBNull.Value))
            {
                bus.State = dr["State"].ToString();
            }
            if ((dr["Zipcode"] != System.DBNull.Value))
            {
                bus.Zipcode = Convert.ToInt32(dr["Zipcode"].ToString());
            }
            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                bus.EmailAddress = dr["EmailAddress"].ToString();
            }
            if ((dr["AutoType"] != System.DBNull.Value))
            {
                bus.AutoType = dr["AutoType"].ToString();
            }
            if ((dr["FulAutoDetails"] != System.DBNull.Value))
            {
                bus.FulAutoDetails = dr["FulAutoDetails"].ToString();
            }
            return bus;
        }

        public List<BusinessSearchM> BusinessSearchFromMypage(string zipcode, string autoTypes)
        {
            try
            {
                System.Data.DataSet ds = SRB.BusinessSearchFromMypage(zipcode, autoTypes);
                List<BusinessSearchM> collection = new List<BusinessSearchM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessSearchM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AppointmentInfoM> GetAppointments(int contactId)
        {
            try
            {
                System.Data.DataSet ds = Appointment.Select_Appointments_By_ContactId_Style(contactId);
                List<AppointmentInfoM> collection = new List<AppointmentInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AppointmentInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<AppointmentInfoM> GetAppointmentsByAutomobile(int contactId, int styleId)
        {
            try
            {
                System.Data.DataSet ds = Appointment.GET_Appointment_By_Automobile(contactId, styleId);
                List<AppointmentInfoM> collection = new List<AppointmentInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<AppointmentInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<Businesscoupon> BSCouponsInfo(int BusId)
        {
            try
            {
                System.Data.DataSet ds = BSCoupons.Select_BusinessCoupons_By_BusinessId(BusId);
                List<Businesscoupon> BSCouponInfo = new List<Businesscoupon>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    BSCouponInfo = ds.Tables[0].ToCollection<Businesscoupon>();
                }
                return BSCouponInfo;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfoM> GetRequestedQuotesByContactIdFilterByBusId(int contactId, int busId)
        {
            try
            {
                System.Data.DataSet ds = Quote.Select_Quotes_By_ContactId_fltBusId(contactId, busId);
                List<QuoteInfoM> collection = new List<QuoteInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<QuoteInfoM> GetRequestedQuotesByAutoId(int AutoId)
        {
            try
            {
                System.Data.DataSet ds = Quote.Select_Quotes_By_AutoId(AutoId);
                List<QuoteInfoM> collection = new List<QuoteInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<QuoteInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<BusinessSearchM> BusinessSearchFromMypageQuote(string zipcode, int miles)
        {
            try
            {
                System.Data.DataSet ds = SRB.BusinessSearchFromMypage_Quote(zipcode, miles);
                List<BusinessSearchM> collection = new List<BusinessSearchM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessSearchM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<BusinessSearchM> BusinessSearchFromMypageQuoteFilter(string zipcode, int miles, int quoteId)
        {
            try
            {
                System.Data.DataSet ds = SRB.BusinessSearchFromMypage_Quote_Filter(zipcode, miles, quoteId);
                List<BusinessSearchM> collection = new List<BusinessSearchM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessSearchM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RecallInfoM> GetRecalls(int AutoId)
        {
            try
            {
                System.Data.DataSet ds = Rcal.Select_AutoRecalls_By_AutoId(AutoId);
                List<RecallInfoM> collection = new List<RecallInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RecallInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<RecallInfoM> GETRecallDetails(RecallInfoM rclInfo)
        {
            try
            {
                System.Data.DataSet ds = Rcal.GET_Recall_Details(rclInfo.ManufacturYear.ToString(), rclInfo.Manufacturer, rclInfo.Model, rclInfo.Style, rclInfo.AutoId.ToString());
                List<RecallInfoM> collection = new List<RecallInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<RecallInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<ServBulletineM> GetBulletine(int AutoId)
        {
            try
            {
                System.Data.DataSet ds = Rcal.Select_Bulletins_By_AutoId(AutoId);
                List<ServBulletineM> collection = new List<ServBulletineM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<ServBulletineM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<ServBulletineM> GetBulletineDetails(ServBulletineM bulDtl)
        {
            try
            {
                System.Data.DataSet ds = Rcal.GET_Bulletins_Details(bulDtl.ManufacturYear.ToString(), bulDtl.Manufacturer, bulDtl.Model, bulDtl.Style);
                List<ServBulletineM> collection = new List<ServBulletineM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<ServBulletineM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<BusinessInfoM> GETDealerList(string Make, string zipcode)
        {
            try
            {
                System.Data.DataSet ds = Business.Select_Dealer_List(Make, zipcode);
                List<BusinessInfoM> collection = new List<BusinessInfoM>();

                if (GlobalTools.IsSafeDataSet(ds))
                {
                    collection = ds.Tables[0].ToCollection<BusinessInfoM>();
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public DataSet GetNewQuoteEmailAddress(int QuoteId)
        {
            try
            {
                DataSet ds = Quote.Get_NewQuote_EmailAddress(QuoteId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }    
}
