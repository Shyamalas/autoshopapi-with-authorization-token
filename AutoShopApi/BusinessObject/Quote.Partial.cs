namespace AutoShop.BusinessObjects
{
    public partial class Quote
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _automobildeid;

        private System.Nullable<int> _businessid;

        private System.Nullable<int> _repairTypeid;

        private string _status;

        private System.Nullable<int> _miles;

        private string _comments;

        private System.Nullable<int> _serviceid;

        private Automobile _automobile;

        private Service _service;

        private QuoteresponseCollection _quoteresponseCollection;

        private System.Nullable<double> _mileage;

        private System.Nullable<int> _zipcode;

        private string _busMultiId;

        private string _multiMileage;

        private System.Nullable<int> _multiCount;

        private int _expireDay;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual System.Nullable<int> Miles
        {
            get
            {
                return _miles;
            }
            set
            {
                _miles = value;
            }
        }

        public virtual string Comments
        {
            get
            {
                return _comments;
            }
            set
            {
                _comments = value;
            }
        }

        public virtual System.Nullable<int> AutoMobileId
        {
            get
            {
                return _automobildeid;
            }
            set
            {
                _automobildeid = value;
            }
        }

        public virtual System.Nullable<int> BusinessId
        {
            get
            {
                return _businessid;
            }
            set
            {
                _businessid = value;
            }
        }

        public virtual System.Nullable<int> RepairTypeId
        {
            get
            {
                return _repairTypeid;
            }
            set
            {
                _repairTypeid = value;
            }
        }

        public virtual System.Nullable<int> ServiceId
        {
            get
            {
                return _serviceid;
            }
            set
            {
                _serviceid = value;
            }
        }

        public virtual System.Nullable<int> Zipcode
        {
            get
            {
                return _zipcode;
            }
            set
            {
                _zipcode = value;
            }
        }

        public virtual System.Nullable<double> Mileage
        {
            get
            {
                return _mileage;
            }
            set
            {
                _mileage = value;
            }
        }

        public virtual string BusMultiId
        {
            get
            {
                return _busMultiId;
            }
            set
            {
                _busMultiId = value;
            }
        }

        public virtual string MultiMileage
        {
            get
            {
                return _multiMileage;
            }
            set
            {
                _multiMileage = value;
            }
        }

        public virtual System.Nullable<int> MultiCount
        {
            get
            {
                return _multiCount;
            }
            set
            {
                _multiCount = value;
            }
        }

        public virtual Automobile Automobile
        {
            get
            {
                if ((this._automobile == null))
                {
                    this._automobile = AutoShop.BusinessObjects.Automobile.Load(this._automobildeid);
                }
                return this._automobile;
            }
            set
            {
                _automobile = value;
            }
        }

        public virtual Service Service
        {
            get
            {
                if ((this._service == null))
                {
                    this._service = AutoShop.BusinessObjects.Service.Load(this._serviceid);
                }
                return this._service;
            }
            set
            {
                _service = value;
            }
        }

        public virtual QuoteresponseCollection QuoteresponseCollection
        {
            get
            {
                if ((this._quoteresponseCollection == null))
                {
                    _quoteresponseCollection = AutoShop.BusinessObjects.Quoteresponse.Select_QuoteResponses_By_QuoteId(this.Rowid);
                }
                return this._quoteresponseCollection;
            }
        }

        public virtual int ExpireDay
        {
            get
            {
                return _expireDay;
            }
            set
            {
                _expireDay = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._automobildeid = null;
            this.Status = string.Empty;
            this.Miles = null;
            this.Comments = string.Empty;
            this._serviceid = null;
            this.Automobile = null;
            this.Service = null;
            this._quoteresponseCollection = null;
            this.Zipcode = null;
            this.Mileage = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["AutomobildeId"] != System.DBNull.Value))
            {
                this._automobildeid = ((System.Nullable<int>)(dr["AutomobildeId"]));
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                this.Status = ((string)(dr["Status"]));
            }
            if ((dr["Miles"] != System.DBNull.Value))
            {
                this.Miles = ((System.Nullable<int>)(dr["Miles"]));
            }
            if ((dr["Comments"] != System.DBNull.Value))
            {
                this.Comments = ((string)(dr["Comments"]));
            }
            if ((dr["ServiceId"] != System.DBNull.Value))
            {
                this._serviceid = ((System.Nullable<int>)(dr["ServiceId"]));
            }
        }

        public static QuoteCollection Select_Quotes_By_AutomobildeId(System.Nullable<int> AutomobildeId)
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                System.Data.DataSet ds = dbo.Select_Quotes_By_AutomobildeId(AutomobildeId);
                QuoteCollection collection = new QuoteCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Quote obj = new Quote();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static QuoteCollection Select_Quotes_By_ServiceId(System.Nullable<int> ServiceId)
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                System.Data.DataSet ds = dbo.Select_Quotes_By_ServiceId(ServiceId);
                QuoteCollection collection = new QuoteCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Quote obj = new Quote();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static QuoteCollection GetAll()
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                System.Data.DataSet ds = dbo.Quote_Select_All();
                QuoteCollection collection = new QuoteCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Quote obj = new Quote();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Quote Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                System.Data.DataSet ds = dbo.Quote_Select_One(RowId);
                Quote obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Quote();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                System.Data.DataSet ds = dbo.Quote_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual int Insert(Quote qut,out int returnId)
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                returnId = dbo.Quote_Insert(qut.AutoMobileId, qut.BusinessId, qut.ServiceId, qut.RepairTypeId, qut.Zipcode, qut.Mileage, qut.Comments, qut.ExpireDay,out returnId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return returnId;
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                dbo.Quote_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                dbo.Quote_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._automobildeid, this.Status, this.Miles, this.Comments, this._serviceid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual int InsertMultiple(Quote qut, out int returnId)
        {
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                returnId = dbo.Quote_InsertMultiple(qut.AutoMobileId, qut.BusMultiId, qut.ServiceId, qut.RepairTypeId, qut.Zipcode, qut.MultiMileage, qut.Comments, MultiCount, qut.ExpireDay,out returnId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return returnId;
        }

        public int Send_Existing_Quote(int QuoteId, int BusinessId, double Mileage, int Zipcode, int expDay)
        {
            int value;
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                value = dbo.Send_Existing_Quote(QuoteId, BusinessId, Mileage,Zipcode, expDay);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return value;
        }

        public int Inactive_Quotes_By_QuoteId(int quotId)
        {
            int value;
            AutoShop.DAL.Quote dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Quote();
                value = dbo.Inactive_Quotes_By_QuoteId(quotId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return value;
        }
    }
}