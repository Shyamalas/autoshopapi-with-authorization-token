namespace AutoShop.BusinessObjects
{
    public partial class Weekdays
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _weekday;

        private System.Nullable<bool> _isactive;

        private BusinesshoursCollection _businesshoursCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Weekday
        {
            get
            {
                return _weekday;
            }
            set
            {
                _weekday = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual BusinesshoursCollection BusinesshoursCollection
        {
            get
            {
                if ((this._businesshoursCollection == null))
                {
                    _businesshoursCollection = AutoShop.BusinessObjects.Businesshours.Select_BusinessHourss_By_WeekDayId(this.Rowid);
                }
                return this._businesshoursCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Weekday = string.Empty;
            this.Isactive = null;
            this._businesshoursCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["WeekDay"] != System.DBNull.Value))
            {
                this.Weekday = ((string)(dr["WeekDay"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
        }

        public static WeekdaysCollection GetAll()
        {
            AutoShop.DAL.Weekdays dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Weekdays();
                System.Data.DataSet ds = dbo.WeekDays_Select_All();
                WeekdaysCollection collection = new WeekdaysCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Weekdays obj = new Weekdays();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Weekdays Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Weekdays dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Weekdays();
                System.Data.DataSet ds = dbo.WeekDays_Select_One(RowId);
                Weekdays obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Weekdays();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Weekdays dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Weekdays();
                System.Data.DataSet ds = dbo.WeekDays_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Weekdays dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Weekdays();
                dbo.WeekDays_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Weekday, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Weekdays dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Weekdays();
                dbo.WeekDays_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Weekdays dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Weekdays();
                dbo.WeekDays_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Weekday, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}