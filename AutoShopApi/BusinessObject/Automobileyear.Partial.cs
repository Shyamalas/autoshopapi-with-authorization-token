namespace AutoShop.BusinessObjects
{
    public partial class Automobileyear
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _year;

        private string _description;

        private System.Nullable<bool> _isactive;

        private AutomobilemanufacturerCollection _automobilemanufacturerCollection;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<int> Year
        {
            get
            {
                return _year;
            }
            set
            {
                _year = value;
            }
        }

        public virtual string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual AutomobilemanufacturerCollection AutomobilemanufacturerCollection
        {
            get
            {
                if ((this._automobilemanufacturerCollection == null))
                {
                    _automobilemanufacturerCollection = AutoShop.BusinessObjects.Automobilemanufacturer.Select_AutomobileManufacturers_By_YearId(this.Rowid);
                }
                return this._automobilemanufacturerCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Year = null;
            this.Description = string.Empty;
            this.Isactive = null;
            this._automobilemanufacturerCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Year"] != System.DBNull.Value))
            {
                this.Year = ((System.Nullable<int>)(dr["Year"]));
            }
            if ((dr["Description"] != System.DBNull.Value))
            {
                this.Description = ((string)(dr["Description"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
        }

        public static AutomobileyearCollection GetAll()
        {
            AutoShop.DAL.Automobileyear dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileyear();
                System.Data.DataSet ds = dbo.AutomobileYear_Select_All();
                AutomobileyearCollection collection = new AutomobileyearCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Automobileyear obj = new Automobileyear();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Automobileyear Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Automobileyear dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileyear();
                System.Data.DataSet ds = dbo.AutomobileYear_Select_One(RowId);
                Automobileyear obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Automobileyear();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Automobileyear dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileyear();
                System.Data.DataSet ds = dbo.AutomobileYear_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Automobileyear dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileyear();
                dbo.AutomobileYear_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Year, this.Description, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Automobileyear dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileyear();
                dbo.AutomobileYear_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Automobileyear dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobileyear();
                dbo.AutomobileYear_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Year, this.Description, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}