using System;
using System.Collections.Generic;
using System.Text;
namespace AutoShop.BusinessObjects
{
    [Serializable]
    public partial class Business
    {
        public virtual void UpdateIsPrimaryBusContact(int busId, int cntId)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.UpdateIsPrimaryBusContact(busId, cntId);

            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert_BusinessNote(string noteDate, string noteDesc,int BusId)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.Insert_BusinessNote(noteDate, noteDesc, BusId);

            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Search_BusinessNote(string fromDate, string toDate, int BusId)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.Search_BusinessNote(fromDate, toDate, BusId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet Select_BusinessNote_By_BusId(int AutoId)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.Select_BusinessNote_By_BusId(AutoId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}