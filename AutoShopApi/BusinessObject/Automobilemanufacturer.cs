using System;
using System.Collections.Generic;
using System.Text;

namespace AutoShop.BusinessObjects
{
    public partial class Automobilemanufacturer
    {
        public virtual System.Data.DataSet SearchAutoManufacturer()
        {
            AutoShop.DAL.Automobilemanufacturer dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemanufacturer();
                System.Data.DataSet ds = dbo.SearchAutoManufacturer();
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}