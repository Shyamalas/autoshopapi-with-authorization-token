﻿using System;
using System.Collections.Generic;
using System.Text;
namespace AutoShop.BusinessObjects
{
    public partial class BusinessProfile
    {
        public virtual System.Data.DataSet Get_BusinessProfile_By_ContactId(System.Nullable<int> RowId)
        {
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                System.Data.DataSet ds = dbo.Get_BusinessProfile_By_ContactId(RowId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual int Submit_Business_Profile(AutoShop.BusinessObjects.Business bsn, AutoShop.BusinessObjects.Address adr, AutoShop.BusinessObjects.Contact cnt, List<AutoShop.BusinessObjects.Businesshours> bsnHrs)//, List<AutoShop.BusinessObjects.Businesscoupon> bsnCpns)
        {
            string operHrs = "$";
            //string businesscoupon = "$";
            
            foreach (Businesshours bsnhr in bsnHrs)
            {
                operHrs = operHrs + bsnhr.Weekdays.Rowid + "|" + bsnhr.Isbusinessopen + "|" + bsnhr.Starttime + "|" + bsnhr.Endtime + "$";
            }

            //foreach (Businesscoupon bsnc in bsnCpns)
            //{
            //    businesscoupon = businesscoupon + bsnc.CouponNo + "|" + bsnc.Name + "|" + bsnc.Isactive + "|" + bsnc.Startdate + "#" + bsnc.Enddate + "#" + bsnc.Coupon + "$";
            //}

            int returnFlag;
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                returnFlag = dbo.Submit_Business_Profile(cnt.Rowid, bsn.Rowid, bsn.Businessname, bsn.Businesstype, cnt.Firstname, cnt.Lastname, adr.Streetaddress1, adr.City, adr.State,
                                                         adr.Zipcode, bsn.Businessphone1, cnt.Cellphone, bsn.Faxnumber, bsn.Url, bsn.ServiceOffered, bsn.Specialities,
                                                         bsn.Modifiedby,bsn.Memberofaaa, bsn.Memberoface, bsn.Memberofase, bsn.Memberofbbb, operHrs,bsn.IsDealer,bsn.AutoManufacturer,bsn.StarCertified);//,businesscoupon);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return returnFlag;
        }

        public virtual int Submit_Business_Coupon(AutoShop.BusinessObjects.Businesscoupon bsnCpn, int businessId)
        {
            int returnFlag;
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                returnFlag = dbo.Submit_Business_Coupon(bsnCpn.Modifiedby,bsnCpn.Startdate,bsnCpn.Enddate,bsnCpn.Isactive,businessId,bsnCpn.Name,bsnCpn.Coupon,bsnCpn.CouponNo);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return returnFlag;
        }

        public virtual int Submit_Autoshop_Image(AutoShop.BusinessObjects.Businessimage bsnimage, int businessId, string modifiedBy)
        {
            int returnFlag;
            //string autoshopimages = "$";
            //foreach (Businessimage bsnimg in bsnimageList)
            //{
            //    autoshopimages = autoshopimages + bsnimg.ImageNo + "|" + bsnimg.Isactive + "|" + bsnimg.Name + "|" + bsnimg.Image + "$";
            //}
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                returnFlag = dbo.Submit_Autoshop_Image(modifiedBy, bsnimage.Isactive, businessId, bsnimage.Name, bsnimage.Image, bsnimage.ImageNo);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return 0;
        }

        public virtual System.Data.DataSet Get_BusinessProfile_By_BusinessId(int RowId)
        {
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                System.Data.DataSet ds = dbo.Get_BusinessProfile_By_BusinessId(RowId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
 
        }

        public virtual System.Data.DataSet Get_PaymentHistory_By_BusinessId(int RowId)
        {
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                System.Data.DataSet ds = dbo.Get_PaymentHistory_By_BusinessId(RowId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

        }

        public virtual System.Data.DataSet GetServiceCenterImagesForAdmin_By_BusinessId(int RowId)
        {
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                System.Data.DataSet ds = dbo.GetServiceCenterImagesForAdmin_By_BusinessId(RowId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet GetCouponForAdmin_By_BusinessId(int RowId)
        {
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                System.Data.DataSet ds = dbo.GetCouponForAdmin_By_BusinessId(RowId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Data.DataSet GetHoursOFOPerationForAdmin_By_BusinessId(int RowId)
        {
            AutoShop.DAL.BusinessProfile dbo = null;
            try
            {
                dbo = new AutoShop.DAL.BusinessProfile();
                System.Data.DataSet ds = dbo.GetHoursOFOPerationForAdmin_By_BusinessId(RowId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void UpdateServiceCenterByAdmin(AutoShop.BusinessObjects.Business bsn, AutoShop.BusinessObjects.Address adr, List<AutoShop.BusinessObjects.Businesshours> bsnHrs)
        {
             AutoShop.DAL.BusinessProfile dbo = null;
             string operHrs = "$";
             //string businesscoupon = "$";

             foreach (Businesshours bsnhr in bsnHrs)
             {
                 operHrs = operHrs + bsnhr.Weekdays.Rowid + "|" + bsnhr.Isbusinessopen + "|" + bsnhr.Starttime + "|" + bsnhr.Endtime + "$";
             }
             try
             {
                 dbo = new AutoShop.DAL.BusinessProfile();
                 dbo.UpdateServiceCenterByAdmin(bsn.Rowid, bsn.Modifiedby, bsn.Businessname, bsn.Businesstype, adr.Streetaddress1, adr.City, adr.State, adr.Zipcode,
                     bsn.Businessphone1, bsn.Faxnumber, bsn.Url, bsn.ServiceOffered, bsn.Specialities, bsn.Memberofaaa,
                     bsn.Memberoface, bsn.Memberofase, bsn.Memberofbbb, operHrs, bsn.IsDealer, bsn.AutoManufacturer, bsn.StarCertified, bsn.Isactive);
             }
             catch (System.Exception ex)
             {
                 throw ex;
             }
             finally
             {
                 if ((dbo != null))
                 {
                     dbo.Dispose();
                 }
             }
        }
    }
}