namespace AutoShop.BusinessObjects
{
    public partial class Businesshours
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<System.DateTime> _starttime;

        private System.Nullable<System.DateTime> _endtime;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _businessid;

        private System.Nullable<int> _weekdayid;

        private System.Nullable<bool> _isbusinessopen;

        private Business _business;

        private Weekdays _weekdays;

        private int _weekDayId;

        private System.Nullable<int> _ContactId;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Starttime
        {
            get
            {
                return _starttime;
            }
            set
            {
                _starttime = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Endtime
        {
            get
            {
                return _endtime;
            }
            set
            {
                _endtime = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual System.Nullable<bool> Isbusinessopen
        {
            get
            {
                return _isbusinessopen;
            }
            set
            {
                _isbusinessopen = value;
            }
        }

        public virtual Business Business
        {
            get
            {
                if ((this._business == null))
                {
                    this._business = AutoShop.BusinessObjects.Business.Load(this._businessid);
                }
                return this._business;
            }
            set
            {
                _business = value;
            }
        }

        public virtual Weekdays Weekdays
        {
            get
            {
                if ((this._weekdays == null))
                {
                    this._weekdays = AutoShop.BusinessObjects.Weekdays.Load(this._weekdayid);
                }
                return this._weekdays;
            }
            set
            {
                _weekdays = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Starttime = null;
            this.Endtime = null;
            this.Isactive = null;
            this._businessid = null;
            this._weekdayid = null;
            this.Isbusinessopen = null;
            this.Business = null;
            this.Weekdays = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["StartTime"] != System.DBNull.Value))
            {
                this.Starttime = ((System.Nullable<System.DateTime>)(dr["StartTime"]));
            }
            if ((dr["EndTime"] != System.DBNull.Value))
            {
                this.Endtime = ((System.Nullable<System.DateTime>)(dr["EndTime"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this._businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
            if ((dr["WeekDayId"] != System.DBNull.Value))
            {
                this._weekdayid = ((System.Nullable<int>)(dr["WeekDayId"]));
            }
            if ((dr["IsBusinessOpen"] != System.DBNull.Value))
            {
                this.Isbusinessopen = ((System.Nullable<bool>)(dr["IsBusinessOpen"]));
            }
        }

        public static BusinesshoursCollection Select_BusinessHourss_By_BusinessId(System.Nullable<int> BusinessId)
        {
            AutoShop.DAL.Businesshours dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesshours();
                System.Data.DataSet ds = dbo.Select_BusinessHourss_By_BusinessId(BusinessId);
                BusinesshoursCollection collection = new BusinesshoursCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesshours obj = new Businesshours();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinesshoursCollection Select_BusinessHourss_By_WeekDayId(System.Nullable<int> WeekDayId)
        {
            AutoShop.DAL.Businesshours dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesshours();
                System.Data.DataSet ds = dbo.Select_BusinessHourss_By_WeekDayId(WeekDayId);
                BusinesshoursCollection collection = new BusinesshoursCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesshours obj = new Businesshours();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinesshoursCollection GetAll()
        {
            AutoShop.DAL.Businesshours dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesshours();
                System.Data.DataSet ds = dbo.BusinessHours_Select_All();
                BusinesshoursCollection collection = new BusinesshoursCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businesshours obj = new Businesshours();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Businesshours Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Businesshours dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesshours();
                System.Data.DataSet ds = dbo.BusinessHours_Select_One(RowId);
                Businesshours obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Businesshours();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Businesshours dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesshours();
                System.Data.DataSet ds = dbo.BusinessHours_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Businesshours dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesshours();
                dbo.BusinessHours_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Starttime, this.Endtime, this.Isactive, this._businessid, this._weekdayid, this.Isbusinessopen);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Businesshours dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesshours();
                dbo.BusinessHours_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Businesshours dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businesshours();
                dbo.BusinessHours_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Starttime, this.Endtime, this.Isactive, this._businessid, this._weekdayid, this.Isbusinessopen);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
        public virtual System.Nullable<int> ContactId
        {
            get
            {
                return _ContactId;
            }
            set
            {
                _ContactId = value;
            }
        }
    }
}