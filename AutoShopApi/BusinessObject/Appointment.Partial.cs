using System;
namespace AutoShop.BusinessObjects
{
    public partial class Appointment
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<System.DateTime> _starttime;

        private System.Nullable<System.DateTime> _endtime;

        private string _status;

        private System.Nullable<int> _automobileid;

        private System.Nullable<int> _businessid;

        private System.Nullable<int> _serviceid;

        private Automobile _automobile;

        private Business _business;

        private Service _service;

        private CustomerreviewCollection _customerreviewCollection;

        public DateTime AppoinmentDate { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public int AutoMobileIdIn { get; set; }

        public int BusinessIdIn { get; set; }

        public int ServiceIdIn { get; set; }

        //public int AutoRecallId { get; set; }

        public int RecallId { get; set; }

        public int RepairTypeIdIn { get; set; }

        public int QuoteId { get; set; }

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Starttime
        {
            get
            {
                return _starttime;
            }
            set
            {
                _starttime = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Endtime
        {
            get
            {
                return _endtime;
            }
            set
            {
                _endtime = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual Automobile Automobile
        {
            get
            {
                if ((this._automobile == null))
                {
                    this._automobile = AutoShop.BusinessObjects.Automobile.Load(this._automobileid);
                }
                return this._automobile;
            }
            set
            {
                _automobile = value;
            }
        }

        public virtual Business Business
        {
            get
            {
                if ((this._business == null))
                {
                    this._business = AutoShop.BusinessObjects.Business.Load(this._businessid);
                }
                return this._business;
            }
            set
            {
                _business = value;
            }
        }

        public virtual Service Service
        {
            get
            {
                if ((this._service == null))
                {
                    this._service = AutoShop.BusinessObjects.Service.Load(this._serviceid);
                }
                return this._service;
            }
            set
            {
                _service = value;
            }
        }

        public virtual CustomerreviewCollection CustomerreviewCollection
        {
            get
            {
                if ((this._customerreviewCollection == null))
                {
                    _customerreviewCollection = AutoShop.BusinessObjects.Customerreview.Select_CustomerReviews_By_AppointmentId(this.Rowid);
                }
                return this._customerreviewCollection;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Starttime = null;
            this.Endtime = null;
            this.Status = string.Empty;
            this._automobileid = null;
            this._businessid = null;
            this._serviceid = null;
            this.Automobile = null;
            this.Business = null;
            this.Service = null;
            this._customerreviewCollection = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["StartTime"] != System.DBNull.Value))
            {
                this.Starttime = ((System.Nullable<System.DateTime>)(dr["StartTime"]));
            }
            if ((dr["EndTime"] != System.DBNull.Value))
            {
                this.Endtime = ((System.Nullable<System.DateTime>)(dr["EndTime"]));
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                this.Status = ((string)(dr["Status"]));
            }
            if ((dr["AutomobileId"] != System.DBNull.Value))
            {
                this._automobileid = ((System.Nullable<int>)(dr["AutomobileId"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this._businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
            if ((dr["ServiceId"] != System.DBNull.Value))
            {
                this._serviceid = ((System.Nullable<int>)(dr["ServiceId"]));
            }
        }

        public static AppointmentCollection Select_Appointments_By_AutomobileId(System.Nullable<int> AutomobileId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Select_Appointments_By_AutomobileId(AutomobileId);
                AppointmentCollection collection = new AppointmentCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Appointment obj = new Appointment();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static AppointmentCollection Select_Appointments_By_BusinessId(System.Nullable<int> BusinessId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Select_Appointments_By_BusinessId(BusinessId);
                AppointmentCollection collection = new AppointmentCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Appointment obj = new Appointment();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static AppointmentCollection Select_Appointments_By_ServiceId(System.Nullable<int> ServiceId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Select_Appointments_By_ServiceId(ServiceId);
                AppointmentCollection collection = new AppointmentCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Appointment obj = new Appointment();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static AppointmentCollection GetAll()
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Appointment_Select_All();
                AppointmentCollection collection = new AppointmentCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Appointment obj = new Appointment();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Appointment Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Appointment_Select_One(RowId);
                Appointment obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Appointment();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Appointment_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
        
        public virtual int Insert(AutoShop.BusinessObjects.Appointment appoin,out int returnId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                returnId = dbo.Appointment_Insert(appoin.AppoinmentDate, appoin.StartTime, appoin.AutoMobileIdIn, appoin.BusinessIdIn, appoin.ServiceIdIn, appoin.RecallId, appoin.RepairTypeIdIn, out returnId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return returnId;
        }

        public virtual void Delete(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                dbo.Appointment_Delete(RowId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update(AutoShop.BusinessObjects.Appointment appoin)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                dbo.Appointment_Update(appoin.Rowid,appoin.AppoinmentDate, appoin.StartTime, appoin.BusinessIdIn);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Select_ServiceDueList_By_ContactId(System.Nullable<int> ContactId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Select_ServiceDueList_By_ContactId(ContactId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Get_Appointments_By_BusId(System.Nullable<int> BusinessId, string TodayDate, string IsConfirmed)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Get_Appointments_By_BusId(BusinessId, TodayDate, IsConfirmed);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Get_Appointment_By_RowId(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Get_Appointment_By_RowId(RowId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update_Appointment_By_RowId(System.Nullable<int> RowId, string IsConfirmed, string status)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                dbo.Update_Appointment_By_RowId(RowId, IsConfirmed, status);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Select_ServiceDueList_By_ContactId_Style(System.Nullable<int> ContactId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Select_ServiceDueList_By_ContactId_Style(ContactId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Get_Appointments_By_BusId_Style(System.Nullable<int> BusinessId, string TodayDate, string IsConfirmed)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Get_Appointments_By_BusId_Style(BusinessId, TodayDate, IsConfirmed);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Get_Appointment_By_RowId_Style(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.Get_Appointment_By_RowId_Style(RowId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet GET_Appointment_By_Automobile(int ContactId,int StyleId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                System.Data.DataSet ds = dbo.GET_Appointment_By_Automobile(ContactId, StyleId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual int Quote_Appointment_Insert(AutoShop.BusinessObjects.Appointment appoin, out int returnId)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                dbo.Quote_Appointment_Insert(appoin.AppoinmentDate, appoin.StartTime, appoin.AutoMobileIdIn, appoin.BusinessIdIn, appoin.ServiceIdIn, appoin.RepairTypeIdIn, appoin.QuoteId, out returnId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }

            return returnId;
        }

        public bool CheckAppointmentExists(int AutoId, DateTime AppoinmentDate, int BusinessId, DateTime StartTime)
        {
            AutoShop.DAL.Appointment dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Appointment();
                bool ds = dbo.CheckAppointmentExists(AutoId, AppoinmentDate, BusinessId, StartTime);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

    }
}