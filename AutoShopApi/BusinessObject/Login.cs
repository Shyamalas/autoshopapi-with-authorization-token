using System;
using System.Collections.Generic;
using System.Text;
namespace AutoShop.BusinessObjects
{
    public partial class Login
    {
        public virtual System.Data.DataSet GetLoginDetailByContactId(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Login dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Login();
                System.Data.DataSet ds = dbo.Get_LoginDetail_ByContactId(RowId);
                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}