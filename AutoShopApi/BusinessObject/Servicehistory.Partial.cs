namespace AutoShop.BusinessObjects
{
    public partial class Servicehistory
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _status;

        private System.Nullable<int> _miles;

        private System.Nullable<int> _automobildeid;

        private System.Nullable<int> _serviceid;

        private Automobile _automobile;

        private Service _service;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual System.Nullable<int> Miles
        {
            get
            {
                return _miles;
            }
            set
            {
                _miles = value;
            }
        }

        public virtual Automobile Automobile
        {
            get
            {
                if ((this._automobile == null))
                {
                    this._automobile = AutoShop.BusinessObjects.Automobile.Load(this._automobildeid);
                }
                return this._automobile;
            }
            set
            {
                _automobile = value;
            }
        }

        public virtual Service Service
        {
            get
            {
                if ((this._service == null))
                {
                    this._service = AutoShop.BusinessObjects.Service.Load(this._serviceid);
                }
                return this._service;
            }
            set
            {
                _service = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Status = string.Empty;
            this.Miles = null;
            this._automobildeid = null;
            this._serviceid = null;
            this.Automobile = null;
            this.Service = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                this.Status = ((string)(dr["Status"]));
            }
            if ((dr["Miles"] != System.DBNull.Value))
            {
                this.Miles = ((System.Nullable<int>)(dr["Miles"]));
            }
            if ((dr["AutomobildeId"] != System.DBNull.Value))
            {
                this._automobildeid = ((System.Nullable<int>)(dr["AutomobildeId"]));
            }
            if ((dr["ServiceId"] != System.DBNull.Value))
            {
                this._serviceid = ((System.Nullable<int>)(dr["ServiceId"]));
            }
        }

        public static ServicehistoryCollection Select_ServiceHistorys_By_AutomobildeId(System.Nullable<int> AutomobildeId)
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                System.Data.DataSet ds = dbo.Select_ServiceHistorys_By_AutomobildeId(AutomobildeId);
                ServicehistoryCollection collection = new ServicehistoryCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Servicehistory obj = new Servicehistory();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static ServicehistoryCollection Select_ServiceHistorys_By_ServiceId(System.Nullable<int> ServiceId)
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                System.Data.DataSet ds = dbo.Select_ServiceHistorys_By_ServiceId(ServiceId);
                ServicehistoryCollection collection = new ServicehistoryCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Servicehistory obj = new Servicehistory();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static ServicehistoryCollection GetAll()
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                System.Data.DataSet ds = dbo.ServiceHistory_Select_All();
                ServicehistoryCollection collection = new ServicehistoryCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Servicehistory obj = new Servicehistory();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Servicehistory Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                System.Data.DataSet ds = dbo.ServiceHistory_Select_One(RowId);
                Servicehistory obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Servicehistory();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                System.Data.DataSet ds = dbo.ServiceHistory_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                dbo.ServiceHistory_Insert(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Status, this.Miles, this._automobildeid, this._serviceid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                dbo.ServiceHistory_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                dbo.ServiceHistory_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Status, this.Miles, this._automobildeid, this._serviceid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public System.Data.DataSet Select_ServiceHistory_By_AutomobileId(System.Nullable<int> AutoId)
        {
            AutoShop.DAL.Servicehistory dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Servicehistory();
                System.Data.DataSet ds = dbo.Select_ServiceHistory_By_AutomobileId(AutoId);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}