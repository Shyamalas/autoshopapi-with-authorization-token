namespace AutoShop.BusinessObjects
{
    public partial class Contactpreference
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _preferenceid;

        private System.Nullable<int> _contactid;

        private Contact _contact;

        private Preference _preference;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual Contact Contact
        {
            get
            {
                if ((this._contact == null))
                {
                    this._contact = AutoShop.BusinessObjects.Contact.Load(this._contactid);
                }
                return this._contact;
            }
            set
            {
                _contact = value;
            }
        }

        public virtual Preference Preference
        {
            get
            {
                if ((this._preference == null))
                {
                    this._preference = AutoShop.BusinessObjects.Preference.Load(this._preferenceid);
                }
                return this._preference;
            }
            set
            {
                _preference = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._preferenceid = null;
            this._contactid = null;
            this.Contact = null;
            this.Preference = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["PreferenceId"] != System.DBNull.Value))
            {
                this._preferenceid = ((System.Nullable<int>)(dr["PreferenceId"]));
            }
            if ((dr["ContactId"] != System.DBNull.Value))
            {
                this._contactid = ((System.Nullable<int>)(dr["ContactId"]));
            }
        }

        public static ContactpreferenceCollection Select_ContactPreferences_By_ContactId(System.Nullable<int> ContactId)
        {
            AutoShop.DAL.Contactpreference dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contactpreference();
                System.Data.DataSet ds = dbo.Select_ContactPreferences_By_ContactId(ContactId);
                ContactpreferenceCollection collection = new ContactpreferenceCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Contactpreference obj = new Contactpreference();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static ContactpreferenceCollection Select_ContactPreferences_By_PreferenceId(System.Nullable<int> PreferenceId)
        {
            AutoShop.DAL.Contactpreference dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contactpreference();
                System.Data.DataSet ds = dbo.Select_ContactPreferences_By_PreferenceId(PreferenceId);
                ContactpreferenceCollection collection = new ContactpreferenceCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Contactpreference obj = new Contactpreference();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static ContactpreferenceCollection GetAll()
        {
            AutoShop.DAL.Contactpreference dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contactpreference();
                System.Data.DataSet ds = dbo.ContactPreference_Select_All();
                ContactpreferenceCollection collection = new ContactpreferenceCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Contactpreference obj = new Contactpreference();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Contactpreference Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Contactpreference dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contactpreference();
                System.Data.DataSet ds = dbo.ContactPreference_Select_One(RowId);
                Contactpreference obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Contactpreference();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Contactpreference dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contactpreference();
                System.Data.DataSet ds = dbo.ContactPreference_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Contactpreference dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contactpreference();
                dbo.ContactPreference_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._preferenceid, this._contactid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Contactpreference dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contactpreference();
                dbo.ContactPreference_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Contactpreference dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Contactpreference();
                dbo.ContactPreference_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._preferenceid, this._contactid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}