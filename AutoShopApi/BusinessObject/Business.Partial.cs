using System;
using System.Collections.Generic;
using System.Web.Configuration;
namespace AutoShop.BusinessObjects
{

    public partial class Business
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _businessname;

        private string _businesstype;

        private System.Nullable<int> _addressid;

        private string _businessphone1;

        private string _businessphone2;

        private string _cellphone;

        private string _faxnumber;

        private System.Nullable<bool> _memberofaaa;

        private string _memberofaaastr;

        private string _memberofAAAdisp;

        private bool _starCertified;

        private string _starCertifiedstr;

        private string _starCertifieddisp;

        private System.Nullable<bool> _memberoface;

        private string _memberofacestr;

        private string _memberofACEdisp;

        private System.Nullable<bool> _memberofase;

        private string _memberofasestr;

        private string _memberofASEdisp;

        private System.Nullable<bool> _memberofbbb;

        private string _memberofbbbstr;

        private string _memberofBBBdisp;

        private string _url;

        private bool _isactive;

        private System.Nullable<bool> _isactiveDisp;

        private System.Nullable<float> _longitude;

        private System.Nullable<float> _latitude;

        private System.Nullable<int> _naicsCode;

        private string _naicsDescription;

        private System.Nullable<int> _sicCode;

        private string _sicDescription;

        private AppointmentCollection _appointmentCollection;

        private Address _address;

        private BusinessaddressCollection _businessaddressCollection;

        private BusinesscontactCollection _businesscontactCollection;

        private BusinesscouponCollection _businesscouponCollection;

        private BusinessholidayCollection _businessholidayCollection;

        private BusinesshoursCollection _businesshoursCollection;

        private BusinessimageCollection _businessimageCollection;

        private BusinesssubscriptionCollection _businesssubscriptionCollection;

        private string _image;

        private string _businessHour;

        private string _addressDetail;

        private string _city;

        private string _statecode;

        private System.Nullable<int> _zip;

        private System.Nullable<int> _zip4;

        private string _phone;

        private System.Nullable<bool> _repairShop;

        private string _repairShopstr;

        private string _quotedisp;

        private string _repairShopdisp;

        private System.Nullable<bool> _bodyShop;

        private string _bodyShopstr;

        private string _bodyShopdisp;

        private System.Nullable<bool> _tireShop;

        private string _tireShopstr;

        private string _tireShopdisp;

        private System.Nullable<bool> _carWash;

        private string _carWashstr;

        private string _carWashdisp;

        private System.Nullable<bool> _transport;

        private string __transportstr;

        private string __transportdisp;

        private System.Nullable<bool> _towing;

        private string _towingstr;

        private string _towingdisp;

        private System.Nullable<bool> _smogCheck;

        private string _smogCheckstr;

        private string _smogCheckdisp;

        private System.Nullable<bool> _glassShop;

        private string _glassShopstr;

        private string _glassShopdisp;

        private System.Nullable<bool> _dealer;

        private string _dealerstr;

        private string _dealerdisp;

        private System.Nullable<bool> _parts;

        private string _partsstr;

        private string _partsdisp;

        private System.Nullable<bool> _newCarSales;

        private string _newCarSalesstr;

        private string _newCarSalesdisp;

        private System.Nullable<bool> _usedCarSales;

        private string _usedCarSalesstr;

        private string _usedCarSalesdisp;

        private System.Nullable<int> _rating;

        private string _isFavorite;

        private string _isFavoriteTxt;

        private string _isFavoriteClass;

        private string _serviceOffered;

        private string _specialities;

        private string _contactName;

        private string _email;

        private string _isOnline;

        private string _coupon;

        private string _appointment;

        private string _autoManufacturer;

        private string _chat;

        private bool _isDealer;





        public virtual bool IsDealer
        {
            get
            {
                return _isDealer;
            }
            set
            {
                _isDealer = value;
            }
        }

        public virtual string AutoManufacturer
        {
            get
            {
                return _autoManufacturer;
            }
            set
            {
                _autoManufacturer = value;
            }
        }


        public virtual string Chat
        {
            get
            {
                return _chat;
            }
            set
            {
                _chat = value;
            }
        }

        public virtual string Appointment
        {
            get
            {
                return _appointment;
            }
            set
            {
                _appointment = value;
            }
        }

        public virtual string Coupon
        {
            get
            {
                return _coupon;
            }
            set
            {
                _coupon = value;
            }
        }

        public virtual string Online
        {
            get
            {
                return _isOnline;
            }
            set
            {
                _isOnline = value;
            }
        }

        public virtual string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Businessname
        {
            get
            {
                return _businessname;
            }
            set
            {
                _businessname = value;
            }
        }

        public virtual string Businesstype
        {
            get
            {
                return _businesstype;
            }
            set
            {
                _businesstype = value;
            }
        }

        public virtual System.Nullable<int> Addressid
        {
            get
            {
                return _addressid;
            }
            set
            {
                _addressid = value;
            }
        }

        public virtual string Businessphone1
        {
            get
            {
                return _businessphone1;
            }
            set
            {
                _businessphone1 = value;
            }
        }

        public virtual string Businessphone2
        {
            get
            {
                return _businessphone2;
            }
            set
            {
                _businessphone2 = value;
            }
        }

        public virtual string Cellphone
        {
            get
            {
                return _cellphone;
            }
            set
            {
                _cellphone = value;
            }
        }

        public virtual string Faxnumber
        {
            get
            {
                return _faxnumber;
            }
            set
            {
                _faxnumber = value;
            }
        }

        public virtual System.Nullable<bool> Memberofaaa
        {
            get
            {
                return _memberofaaa;
            }
            set
            {
                _memberofaaa = value;
            }
        }

        public virtual string Memberofaaastr
        {
            get
            {
                return _memberofaaastr;
            }
            set
            {
                _memberofaaastr = value;
            }
        }

        public virtual string MemberofAAAdisp
        {
            get
            {
                return _memberofAAAdisp;
            }
            set
            {
                _memberofAAAdisp = value;
            }
        }

        public virtual bool StarCertified
        {
            get
            {
                return _starCertified;
            }
            set
            {
                _starCertified = value;
            }
        }

        public virtual string StarCertifiedstr
        {
            get
            {
                return _starCertifiedstr;
            }
            set
            {
                _starCertifiedstr = value;
            }
        }

        public virtual string StarCertifieddisp
        {
            get
            {
                return _starCertifieddisp;
            }
            set
            {
                _starCertifieddisp = value;
            }
        }

        public virtual System.Nullable<bool> Memberoface
        {
            get
            {
                return _memberoface;
            }
            set
            {
                _memberoface = value;
            }
        }

        public virtual string Memberofacestr
        {
            get
            {
                return _memberofacestr;
            }
            set
            {
                _memberofacestr = value;
            }
        }

        public virtual string MemberofACEdisp
        {
            get
            {
                return _memberofACEdisp;
            }
            set
            {
                _memberofACEdisp = value;
            }
        }

        public virtual System.Nullable<bool> Memberofase
        {
            get
            {
                return _memberofase;
            }
            set
            {
                _memberofase = value;
            }
        }

        public virtual string Memberofasestr
        {
            get
            {
                return _memberofasestr;
            }
            set
            {
                _memberofasestr = value;
            }
        }

        public virtual string MemberofASEdisp
        {
            get
            {
                return _memberofASEdisp;
            }
            set
            {
                _memberofASEdisp = value;
            }
        }

        public virtual System.Nullable<bool> Memberofbbb
        {
            get
            {
                return _memberofbbb;
            }
            set
            {
                _memberofbbb = value;
            }
        }

        public virtual string Memberofbbbstr
        {
            get
            {
                return _memberofbbbstr;
            }
            set
            {
                _memberofbbbstr = value;
            }
        }

        public virtual string MemberofBBBdisp
        {
            get
            {
                return _memberofBBBdisp;
            }
            set
            {
                _memberofBBBdisp = value;
            }
        }

        public virtual string Url
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
            }
        }

        public virtual bool Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual System.Nullable<bool> IsactiveDisp
        {
            get
            {
                return _isactiveDisp;
            }
            set
            {
                _isactiveDisp = value;
            }
        }

        public virtual System.Nullable<float> Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
            }
        }

        public virtual System.Nullable<float> Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
            }
        }

        public virtual System.Nullable<int> NaicsCode
        {
            get
            {
                return _naicsCode;
            }
            set
            {
                _naicsCode = value;
            }
        }

        public virtual string NaicsDescription
        {
            get
            {
                return _naicsDescription;
            }
            set
            {
                _naicsDescription = value;
            }
        }

        public virtual System.Nullable<int> SicCode
        {
            get
            {
                return _sicCode;
            }
            set
            {
                _sicCode = value;
            }
        }

       
        public virtual string SicDescription
        {
            get
            {
                return _sicDescription;
            }
            set
            {
                _sicDescription = value;
            }
        }

        public virtual string ContactName
        {
            get
            {
                return _contactName;
            }
            set
            {
                _contactName = value;
            }
        }

        //public virtual AppointmentCollection AppointmentCollection
        //{
        //    get
        //    {
        //        if ((this._appointmentCollection == null))
        //        {
        //            _appointmentCollection = AutoShop.BusinessObjects.Appointment.Select_Appointments_By_BusinessId(this.Rowid);
        //        }
        //        return this._appointmentCollection;
        //    }
        //}

        public virtual Address Address
        {
            get
            {
                if ((this._address == null))
                {
                    this._address = AutoShop.BusinessObjects.Address.Load(this._rowid);
                }
                return this._address;
            }
            set
            {
                _address = value;
            }
        }

        public virtual BusinessaddressCollection BusinessaddressCollection
        {
            get
            {
                if ((this._businessaddressCollection == null))
                {
                    _businessaddressCollection = AutoShop.BusinessObjects.Businessaddress.Select_BusinessAddresss_By_BusinessId(this.Rowid);
                }
                return this._businessaddressCollection;
            }
        }

        public virtual BusinesscontactCollection BusinesscontactCollection
        {
            get
            {
                if ((this._businesscontactCollection == null))
                {
                    _businesscontactCollection = AutoShop.BusinessObjects.Businesscontact.Select_BusinessContacts_By_BusinessId(this.Rowid);
                }
                return this._businesscontactCollection;
            }
        }

        public virtual BusinesscouponCollection BusinesscouponCollection
        {
            get
            {
                if ((this._businesscouponCollection == null))
                {
                    _businesscouponCollection = AutoShop.BusinessObjects.Businesscoupon.Select_BusinessCoupons_By_BusinessId(this.Rowid);
                }
                return this._businesscouponCollection;
            }
        }

        public virtual BusinessholidayCollection BusinessholidayCollection
        {
            get
            {
                if ((this._businessholidayCollection == null))
                {
                    _businessholidayCollection = AutoShop.BusinessObjects.Businessholiday.Select_BusinessHolidays_By_BusinessId(this.Rowid);
                }
                return this._businessholidayCollection;
            }
        }

        public virtual BusinesshoursCollection BusinesshoursCollection
        {
            get
            {
                if ((this._businesshoursCollection == null))
                {
                    _businesshoursCollection = AutoShop.BusinessObjects.Businesshours.Select_BusinessHourss_By_BusinessId(this.Rowid);
                }
                return this._businesshoursCollection;
            }
        }

        public virtual BusinessimageCollection BusinessimageCollection
        {
            get
            {
                if ((this._businessimageCollection == null))
                {
                    _businessimageCollection = AutoShop.BusinessObjects.Businessimage.Select_BusinessImages_By_BusinessId(this.Rowid);
                }
                return this._businessimageCollection;
            }
        }

        public virtual BusinesssubscriptionCollection BusinesssubscriptionCollection
        {
            get
            {
                if ((this._businesssubscriptionCollection == null))
                {
                    _businesssubscriptionCollection = AutoShop.BusinessObjects.Businesssubscription.Select_BusinessSubscriptions_By_BusinessId(this.Rowid);
                }
                return this._businesssubscriptionCollection;
            }
        }

        public virtual string Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
            }
        }

        public virtual string BusinessHour
        {
            get
            {
                return _businessHour;
            }
            set
            {
                _businessHour = value;
            }
        }

        public virtual string AddressDetail
        {
            get
            {
                return _addressDetail;
            }
            set
            {
                _addressDetail = value;
            }
        }

        public virtual string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        public virtual string Statecode
        {
            get
            {
                return _statecode;
            }
            set
            {
                _statecode = value;
            }
        }

        public virtual System.Nullable<int> Zip
        {
            get
            {
                return _zip;
            }
            set
            {
                _zip = value;
            }
        }

        public virtual System.Nullable<int> Zip4
        {
            get
            {
                return _zip4;
            }
            set
            {
                _zip4 = value;
            }
        }

        public virtual string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }

        public virtual System.Nullable<bool> RepairShop
        {
            get
            {
                return _repairShop;
            }
            set
            {
                _repairShop = value;
            }
        }

        public virtual string RepairShopstr
        {
            get
            {
                return _repairShopstr;
            }
            set
            {
                _repairShopstr = value;
            }
        }

        public virtual string Quotedisp
        {
            get
            {
                return _quotedisp;
            }
            set
            {
                _quotedisp = value;
            }
        }

        public virtual string RepairShopdisp
        {
            get
            {
                return _repairShopdisp;
            }
            set
            {
                _repairShopdisp = value;
            }
        }

        public virtual System.Nullable<bool> BodyShop
        {
            get
            {
                return _bodyShop;
            }
            set
            {
                _bodyShop = value;
            }
        }

        public virtual string BodyShopstr
        {
            get
            {
                return _bodyShopstr;
            }
            set
            {
                _bodyShopstr = value;
            }
        }

        public virtual string BodyShopdisp
        {
            get
            {
                return _bodyShopdisp;
            }
            set
            {
                _bodyShopdisp = value;
            }
        }

        public virtual System.Nullable<bool> TireShop
        {
            get
            {
                return _tireShop;
            }
            set
            {
                _tireShop = value;
            }
        }

        public virtual string TireShopstr
        {
            get
            {
                return _tireShopstr;
            }
            set
            {
                _tireShopstr = value;
            }
        }

        public virtual string TireShopdisp
        {
            get
            {
                return _tireShopdisp;
            }
            set
            {
                _tireShopdisp = value;
            }
        }

        public virtual System.Nullable<bool> CarWash
        {
            get
            {
                return _carWash;
            }
            set
            {
                _carWash = value;
            }
        }

        public virtual string CarWashstr
        {
            get
            {
                return _carWashstr;
            }
            set
            {
                _carWashstr = value;
            }
        }

        public virtual string CarWashdisp
        {
            get
            {
                return _carWashdisp;
            }
            set
            {
                _carWashdisp = value;
            }
        }

        public virtual System.Nullable<bool> Transport
        {
            get
            {
                return _transport;
            }
            set
            {
                _transport = value;
            }
        }

        public virtual string Transportstr
        {
            get
            {
                return __transportstr;
            }
            set
            {
                __transportstr = value;
            }
        }

        public virtual string Transportdisp
        {
            get
            {
                return __transportdisp;
            }
            set
            {
                __transportdisp = value;
            }
        }

        public virtual System.Nullable<bool> Towing
        {
            get
            {
                return _towing;
            }
            set
            {
                _towing = value;
            }
        }

        public virtual string Towingstr
        {
            get
            {
                return _towingstr;
            }
            set
            {
                _towingstr = value;
            }
        }

        public virtual string Towingdisp
        {
            get
            {
                return _towingdisp;
            }
            set
            {
                _towingdisp = value;
            }
        }

        public virtual System.Nullable<bool> SmogCheck
        {
            get
            {
                return _smogCheck;
            }
            set
            {
                _smogCheck = value;
            }
        }

        public virtual string SmogCheckstr
        {
            get
            {
                return _smogCheckstr;
            }
            set
            {
                _smogCheckstr = value;
            }
        }

        public virtual string SmogCheckdisp
        {
            get
            {
                return _smogCheckdisp;
            }
            set
            {
                _smogCheckdisp = value;
            }
        }

        public virtual System.Nullable<bool> GlassShop
        {
            get
            {
                return _glassShop;
            }
            set
            {
                _glassShop = value;
            }
        }

        public virtual string GlassShopstr
        {
            get
            {
                return _glassShopstr;
            }
            set
            {
                _glassShopstr = value;
            }
        }

        public virtual string GlassShopdisp
        {
            get
            {
                return _glassShopdisp;
            }
            set
            {
                _glassShopdisp = value;
            }
        }

        public virtual System.Nullable<bool> Dealer
        {
            get
            {
                return _dealer;
            }
            set
            {
                _dealer = value;
            }
        }

        public virtual string Dealerstr
        {
            get
            {
                return _dealerstr;
            }
            set
            {
                _dealerstr = value;
            }
        }

        public virtual string Dealerdisp
        {
            get
            {
                return _dealerdisp;
            }
            set
            {
                _dealerdisp = value;
            }
        }

        public virtual System.Nullable<bool> Parts
        {
            get
            {
                return _parts;
            }
            set
            {
                _parts = value;
            }
        }

        public virtual string Partsstr
        {
            get
            {
                return _partsstr;
            }
            set
            {
                _partsstr = value;
            }
        }

        public virtual string Partsdisp
        {
            get
            {
                return _partsdisp;
            }
            set
            {
                _partsdisp = value;
            }
        }

        public virtual System.Nullable<bool> NewCarSales
        {
            get
            {
                return _newCarSales;
            }
            set
            {
                _newCarSales = value;
            }
        }

        public virtual string NewCarSalesstr
        {
            get
            {
                return _newCarSalesstr;
            }
            set
            {
                _newCarSalesstr = value;
            }
        }

        public virtual string NewCarSalesdisp
        {
            get
            {
                return _newCarSalesdisp;
            }
            set
            {
                _newCarSalesdisp = value;
            }
        }

        public virtual System.Nullable<bool> UsedCarSales
        {
            get
            {
                return _usedCarSales;
            }
            set
            {
                _usedCarSales = value;
            }
        }

        public virtual string UsedCarSalesstr
        {
            get
            {
                return _usedCarSalesstr;
            }
            set
            {
                _usedCarSalesstr = value;
            }
        }

        public virtual string UsedCarSalesdisp
        {
            get
            {
                return _usedCarSalesdisp;
            }
            set
            {
                _usedCarSalesdisp = value;
            }
        }

        public virtual System.Nullable<int> Rating
        {
            get
            {
                return _rating;
            }
            set
            {
                _rating = value;
            }
        }

        public virtual string IsFavorite
        {
            get
            {
                return _isFavorite;
            }
            set
            {
                _isFavorite = value;
            }
        }

        public virtual string IsFavoriteTxt
        {
            get
            {
                return _isFavoriteTxt;
            }
            set
            {
                _isFavoriteTxt = value;
            }
        }

        public virtual string IsFavoriteClass
        {
            get
            {
                return _isFavoriteClass;
            }
            set
            {
                _isFavoriteClass = value;
            }
        }

        public virtual string ServiceOffered
        {
            get
            {
                return _serviceOffered;
            }
            set
            {
                _serviceOffered = value;
            }
        }

        public virtual string Specialities
        {
            get
            {
                return _specialities;
            }
            set
            {
                _specialities = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Businessname = string.Empty;
            this.Businesstype = string.Empty;
            this.Addressid = null;
            this.Businessphone1 = string.Empty;
            this.Businessphone2 = string.Empty;
            this.Cellphone = string.Empty;
            this.Faxnumber = string.Empty;
            this.Memberofaaa = null;
            this.Memberofaaastr = null;
            this.MemberofAAAdisp = string.Empty;
            this.StarCertified = false;
            this.StarCertifiedstr = string.Empty;
            this.StarCertifieddisp = string.Empty;
            this.Memberoface = null;
            this.Memberofacestr = null;
            this.MemberofACEdisp = string.Empty;
            this.Memberofase = null;
            this.Memberofasestr = null;
            this.MemberofASEdisp = string.Empty;
            this.Memberofbbb = null;
            this.Memberofbbbstr = null;
            this.MemberofBBBdisp = string.Empty;
            this.Url = string.Empty;
            this.Isactive = false;
            this.IsactiveDisp = null;
            this.Longitude = null;
            this.Latitude = null;
            this.NaicsCode = null;
            this.NaicsDescription = string.Empty;
            this.SicCode = null;
            this.SicDescription = string.Empty;
            this._appointmentCollection = null;
            this.Address = null;
            this._businessaddressCollection = null;
            this._businesscontactCollection = null;
            this._businesscouponCollection = null;
            this._businessholidayCollection = null;
            this._businesshoursCollection = null;
            this._businessimageCollection = null;
            this._businesssubscriptionCollection = null;
            this.Image = string.Empty;
            this.BusinessHour = string.Empty;
            this.AddressDetail = null;
            this.City = string.Empty;
            this.Statecode = string.Empty;
            this.Zip = null;
            this.Zip4 = null;
            this.Phone = string.Empty;
            this.RepairShop = null;
            this.RepairShopstr = string.Empty;
            this.Quotedisp = string.Empty;
            this.RepairShopdisp = string.Empty;
            this.BodyShop = null;
            this.BodyShopstr = string.Empty;
            this.BodyShopdisp = string.Empty;
            this.TireShop = null;
            this.TireShopstr = string.Empty;
            this.TireShopdisp = string.Empty;
            this.CarWash = null;
            this.CarWashstr = string.Empty;
            this.CarWashdisp = string.Empty;
            this.Transport = null;
            this.Transportstr = string.Empty;
            this.Transportdisp = string.Empty;
            this.Towing = null;
            this.Towingstr = string.Empty;
            this.Towingdisp = string.Empty;
            this.SmogCheck = null;
            this.SmogCheckstr = string.Empty;
            this.SmogCheckdisp = string.Empty;
            this.GlassShop = null;
            this.GlassShopstr = string.Empty;
            this.GlassShopdisp = string.Empty;
            this.Dealer = null;
            this.Dealerstr = string.Empty;
            this.Dealerdisp = string.Empty;
            this.Parts = null;
            this.Partsstr = string.Empty;
            this.Partsdisp = string.Empty;
            this.NewCarSales = null;
            this.NewCarSalesstr = string.Empty;
            this.NewCarSalesdisp = string.Empty;
            this.UsedCarSales = null;
            this.UsedCarSalesstr = string.Empty;
            this.UsedCarSalesdisp = string.Empty;
            this.Rating = null;
            this.IsFavorite = string.Empty;
            this.IsFavoriteTxt = string.Empty;
            this.IsFavoriteClass = string.Empty;
            this.ServiceOffered = string.Empty;
            this.Specialities = string.Empty;
            this.Online = string.Empty;
            this.Email = string.Empty;
            this.Coupon = string.Empty;
            this.Appointment = string.Empty;
            this.Chat = string.Empty;

        }

        private void Fill(System.Data.DataRow dr)
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("MemberofAAA", "/images/Member/AAAdelt.jpg");
            dictionary.Add("MemberofACE", "/images/Member/ACEdelt.jpg");
            dictionary.Add("MemberofASE", "/images/Member/ASEdelt.jpg");
            dictionary.Add("MemberofBBB", "/images/Member/BBBdelt.jpg");
            dictionary.Add("StarCertified", "/images/Member/strcrt.jpg");

            Dictionary<string, string> dictService = new Dictionary<string, string>();
            dictService.Add("RepairShop", "Auto Repair");
            dictService.Add("BodyShop", "Body Shop");
            dictService.Add("TireShop", "Tire Shop");
            dictService.Add("CarWash", "Car Wash");
            dictService.Add("Transport", "Transport Service");
            dictService.Add("Towing", "Towing Services");
            dictService.Add("SmogCheck", "Smog Check");
            dictService.Add("GlassShop", "Glass Shop");
            dictService.Add("Dealer", "Dealer");
            dictService.Add("Parts", "Parts");
            dictService.Add("NewCarSales", "New Car Sales");
            dictService.Add("UsedCarSales", "Used Car Sales");

            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            //if ((dr["CreatedBy"] != System.DBNull.Value))
            //{
            //    this.Createdby = ((string)(dr["CreatedBy"]));
            //}
            //if ((dr["CreatedDate"] != System.DBNull.Value))
            //{
            //    this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            //}
            //if ((dr["ModifiedBy"] != System.DBNull.Value))
            //{
            //    this.Modifiedby = ((string)(dr["ModifiedBy"]));
            //}
            //if ((dr["ModifiedDate"] != System.DBNull.Value))
            //{
            //    this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            //}
            if ((dr["BusinessName"] != System.DBNull.Value))
            {
                this.Businessname = ((string)(dr["BusinessName"]));
            }
            //if ((dr["BusinessType"] != System.DBNull.Value))
            //{
            //    this.Businesstype = ((string)(dr["BusinessType"]));
            //}
            //if ((dr["AddressId"] != System.DBNull.Value))
            //{
            //    int resu = 0;
            //    resu = System.Convert.ToInt32(dr["AddressId"]);
            //    this.Addressid = ((System.Nullable<int>)(resu));
            //}
            //if ((dr["BusinessPhone1"] != System.DBNull.Value))
            //{
            //    this.Businessphone1 = ((string)(dr["BusinessPhone1"]));
            //}
            //if ((dr["BusinessPhone2"] != System.DBNull.Value))
            //{
            //    this.Businessphone2 = ((string)(dr["BusinessPhone2"]));
            //}
            //if ((dr["CellPhone"] != System.DBNull.Value))
            //{
            //    this.Cellphone = ((string)(dr["CellPhone"]));
            //}
            //if ((dr["FaxNumber"] != System.DBNull.Value))
            //{
            //    this.Faxnumber = ((string)(dr["FaxNumber"]));
            //}
            if ((dr["MemberofAAA"] != System.DBNull.Value))
            {
                this.Memberofaaa = ((System.Nullable<bool>)(dr["MemberofAAA"]));

                if (this.Memberofaaa == true)
                {
                    this.Memberofaaastr = dictionary["MemberofAAA"];
                }
                else
                {
                    this.MemberofAAAdisp = "none";
                }
            }
            else
            {
                this.MemberofAAAdisp = "none";
            }

            //vikas 

            if ((dr["StarCertified"] != System.DBNull.Value))
            {
                this.StarCertified = ((bool)(dr["StarCertified"]));

                if (this.StarCertified == true)
                {
                    this.StarCertifiedstr = dictionary["StarCertified"];
                }
                else
                {
                    this.StarCertifieddisp = "none";
                }
            }
            else
            {
                this.StarCertifieddisp = "none";
            }

            //

            if ((dr["MemberofACE"] != System.DBNull.Value))
            {
                this.Memberoface = ((System.Nullable<bool>)(dr["MemberofACE"]));

                if (this.Memberoface == true)
                {
                    this.Memberofacestr = dictionary["MemberofACE"];
                }
                else
                {
                    this.MemberofACEdisp = "none";
                }
            }
            else
            {
                this.MemberofACEdisp = "none";
            }

            if ((dr["MemberofASE"] != System.DBNull.Value))
            {
                this.Memberofase = ((System.Nullable<bool>)(dr["MemberofASE"]));

                if (this.Memberofase == true)
                {
                    this.Memberofasestr = dictionary["MemberofASE"];
                }
                else
                {
                    this.MemberofASEdisp = "none";
                }
            }
            else
            {
                this.MemberofASEdisp = "none";
            }

            if ((dr["MemberofBBB"] != System.DBNull.Value))
            {
                this.Memberofbbb = ((System.Nullable<bool>)(dr["MemberofBBB"]));

                if (this.Memberofbbb == true)
                {
                    this.Memberofbbbstr = dictionary["MemberofBBB"];
                }
                else
                {
                    this.MemberofBBBdisp = "none";
                }
            }
            else
            {
                this.MemberofBBBdisp = "none";
            }
            //if ((dr["URL"] != System.DBNull.Value))
            //{
            //    this.Url = ((string)(dr["URL"]));
            //}
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((bool)(dr["IsActive"]));
            }           
            if ((dr["Longitude"] != System.DBNull.Value))
            {
                float result = 0;
                float.TryParse(dr["Longitude"].ToString(), out result);
                this.Longitude = (result);
            }
            else
            {
                float result = 0;
                float.TryParse("-117.453765", out result);
                this.Longitude = (result);
            }
            if ((dr["Latitude"] != System.DBNull.Value))
            {
                //this.Latitude = ((System.Nullable<float>)(dr["Latitude"]));
                float result = 0;
                float.TryParse(dr["Latitude"].ToString(), out result);
                this.Latitude = (result);
            }
            else
            {
                float result = 0;
                float.TryParse("34.132960", out result);
                this.Latitude = (result);
            }
            //if ((dr["NAICS_Code"] != System.DBNull.Value))
            //{
            //    this.NaicsCode = ((System.Nullable<int>)(dr["NAICS_Code"]));
            //}
            //if ((dr["NAICS_Description"] != System.DBNull.Value))
            //{
            //    this.NaicsDescription = ((string)(dr["NAICS_Description"]));
            //}
            //if ((dr["SIC_Code"] != System.DBNull.Value))
            //{
            //    this.SicCode = ((System.Nullable<int>)(dr["SIC_Code"]));
            //}
            //if ((dr["SIC_Description"] != System.DBNull.Value))
            //{
            //    this.SicDescription = ((string)(dr["SIC_Description"]));
            //}
            if ((dr["Image"] != System.DBNull.Value))
            {
                this.Image = ((string)(dr["Image"]));
            }
            else
            {
                //this.Image = "/images/Logo/noimage.jpg";
                this.Image = WebConfigurationManager.AppSettings["DefaultPath"];
            }

            if ((dr["BusinessHour"] != System.DBNull.Value))
            {
                this.BusinessHour = ((string)(dr["BusinessHour"]));
            }
            else
            {
                //this.BusinessHour = "Not found";
                this.BusinessHour = "";
            }

            if ((dr["Address"] != System.DBNull.Value))
            {
                this.AddressDetail = ((string)(dr["Address"]));
            }

            if ((dr["City"] != System.DBNull.Value))
            {
                this.City = ((string)(dr["City"]));
            }
            if ((dr["StateCode"] != System.DBNull.Value))
            {
                this.Statecode = ((string)(dr["StateCode"]));
            }
            if ((dr["ZIP"] != System.DBNull.Value))
            {
                this.Zip = ((System.Nullable<int>)(dr["ZIP"]));
            }
            if ((dr["ZIP_4"] != System.DBNull.Value))
            {
                this.Zip4 = ((System.Nullable<int>)(dr["ZIP_4"]));
            }
            if ((dr["PHONE"] != System.DBNull.Value))
            {
                this.Phone = ((string)(dr["PHONE"]));
            }

            if ((dr["RepairShop"] != System.DBNull.Value))
            {
                this.RepairShop = ((System.Nullable<bool>)(dr["RepairShop"]));

                if (this.RepairShop == true)
                {
                    this.RepairShopstr = dictService["RepairShop"];
                }
                else
                {
                    this.RepairShopdisp = "none";
                }
            }
            else
            {
                this.RepairShopdisp = "none";
            }

            if ((dr["RepairShop"] != System.DBNull.Value))
            {
                this.RepairShop = ((System.Nullable<bool>)(dr["RepairShop"]));

                if (this.RepairShop == true)
                {
                    if ((dr["Quote"] != System.DBNull.Value))
                    {
                        if ((string)(dr["Quote"]) == "block")
                        {
                            this.Quotedisp = "block";
                        }
                        else
                        {
                            this.Quotedisp = "none";
                        }
                    }
                    else
                    {
                        this.Quotedisp = "none";
                    }
                }
                else
                {
                    this.Quotedisp = "none";
                }
            }
            else
            {
                this.Quotedisp = "none";
            }

            if ((dr["BodyShop"] != System.DBNull.Value))
            {
                this.BodyShop = ((System.Nullable<bool>)(dr["BodyShop"]));

                if (this.BodyShop == true)
                {
                    this.BodyShopstr = dictService["BodyShop"];
                }
                else
                {
                    this.BodyShopdisp = "none";
                }
            }
            else
            {
                this.BodyShopdisp = "none";
            }

            if ((dr["TireShop"] != System.DBNull.Value))
            {
                this.TireShop = ((System.Nullable<bool>)(dr["TireShop"]));

                if (this.TireShop == true)
                {
                    this.TireShopstr = dictService["TireShop"];
                }
                else
                {
                    this.TireShopdisp = "none";
                }
            }
            else
            {
                this.TireShopdisp = "none";
            }

            if ((dr["CarWash"] != System.DBNull.Value))
            {
                this.CarWash = ((System.Nullable<bool>)(dr["CarWash"]));

                if (this.CarWash == true)
                {
                    this.CarWashstr = dictService["CarWash"];
                }
                else
                {
                    this.CarWashdisp = "none";
                }
            }
            else
            {
                this.CarWashdisp = "none";
            }

            if ((dr["Transport"] != System.DBNull.Value))
            {
                this.Transport = ((System.Nullable<bool>)(dr["Transport"]));

                if (this.Transport == true)
                {
                    this.Transportstr = dictService["Transport"];
                }
                else
                {
                    this.Transportdisp = "none";
                }
            }
            else
            {
                this.Transportdisp = "none";
            }

            if ((dr["Towing"] != System.DBNull.Value))
            {
                this.Towing = ((System.Nullable<bool>)(dr["Towing"]));

                if (this.Towing == true)
                {
                    this.Towingstr = dictService["Towing"];
                }
                else
                {
                    this.Towingdisp = "none";
                }
            }
            else
            {
                this.Towingdisp = "none";
            }

            if ((dr["SmogCheck"] != System.DBNull.Value))
            {
                this.SmogCheck = ((System.Nullable<bool>)(dr["SmogCheck"]));

                if (this.SmogCheck == true)
                {
                    this.SmogCheckstr = dictService["SmogCheck"];
                }
                else
                {
                    this.SmogCheckdisp = "none";
                }
            }
            else
            {
                this.SmogCheckdisp = "none";
            }

            if ((dr["GlassShop"] != System.DBNull.Value))
            {
                this.GlassShop = ((System.Nullable<bool>)(dr["GlassShop"]));

                if (this.GlassShop == true)
                {
                    this.GlassShopstr = dictService["GlassShop"];
                }
                else
                {
                    this.GlassShopdisp = "none";
                }
            }
            else
            {
                this.GlassShopdisp = "none";
            }

            if ((dr["Dealer"] != System.DBNull.Value))
            {
                this.Dealer = ((System.Nullable<bool>)(dr["Dealer"]));

                if (this.Dealer == true)
                {
                    this.Dealerstr = dictService["Dealer"];
                }
                else
                {
                    this.Dealerdisp = "none";
                }
            }
            else
            {
                this.Dealerdisp = "none";
            }

            if ((dr["Parts"] != System.DBNull.Value))
            {
                this.Parts = ((System.Nullable<bool>)(dr["Parts"]));

                if (this.Parts == true)
                {
                    this.Partsstr = dictService["Parts"];
                }
                else
                {
                    this.Partsdisp = "none";
                }
            }
            else
            {
                this.Partsdisp = "none";
            }

            if ((dr["NewCarSales"] != System.DBNull.Value))
            {
                this.NewCarSales = ((System.Nullable<bool>)(dr["NewCarSales"]));

                if (this.NewCarSales == true)
                {
                    this.NewCarSalesstr = dictService["NewCarSales"];
                }
                else
                {
                    this.NewCarSalesdisp = "none";
                }
            }
            else
            {
                this.NewCarSalesdisp = "none";
            }

            if ((dr["UsedCarSales"] != System.DBNull.Value))
            {
                this.UsedCarSales = ((System.Nullable<bool>)(dr["UsedCarSales"]));

                if (this.UsedCarSales == true)
                {
                    this.UsedCarSalesstr = dictService["UsedCarSales"];
                }
                else
                {
                    this.UsedCarSalesdisp = "none";
                }
            }
            else
            {
                this.UsedCarSalesdisp = "none";
            }

            if ((dr["Rating"] != System.DBNull.Value))
            {
                this.Rating = ((System.Nullable<int>)(dr["Rating"]));
            }
            else
            {
                this.Rating = 0;
            }

            if ((dr["ISFavorite"] != System.DBNull.Value))
            {
                this.IsFavorite = ((string)(dr["ISFavorite"]));

                if ((dr["ISFavorite"].ToString() == "iconFavRemoved"))
                {
                    this.IsFavoriteTxt = "Add in Favorites";
                }
                else
                {
                    this.IsFavoriteTxt = "Remove from Favorites";
                }
            }

            if ((dr["ISFavoriteClass"] != System.DBNull.Value))
            {
                this.IsFavoriteClass = ((string)(dr["ISFavoriteClass"]));
            }

            if ((dr["ServiceOffered"] != System.DBNull.Value))
            {
                this.ServiceOffered = ((string)(dr["ServiceOffered"]));
            }

            if ((dr["Specialities"] != System.DBNull.Value))
            {
                this.Specialities = ((string)(dr["Specialities"]));
            }

            if ((dr["Online"] != System.DBNull.Value))
            {
                this.Online = ((string)(dr["Online"]));
            }

            if ((dr["EmailAddress"] != System.DBNull.Value))
            {
                this.Email = ((string)(dr["EmailAddress"]));
            }

            if ((dr["Coupon"] != System.DBNull.Value))
            {
                this.Coupon = ((string)(dr["Coupon"]));
            }

            if ((dr["Appointment"] != System.DBNull.Value))
            {
                this.Appointment = ((string)(dr["Appointment"]));

                if ((dr["CarWash"] != System.DBNull.Value))
                {
                    this.CarWash = ((System.Nullable<bool>)(dr["CarWash"]));

                    if (this.CarWash == true)
                    {
                        this.Appointment = "none";
                    }
                }
            }

            if ((dr["Chat"] != System.DBNull.Value))
            {
                this.Chat = ((string)(dr["Chat"]));
            }

        }

        public static BusinessCollection Select_Businesss_By_RowId(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.Select_Businesss_By_RowId(RowId);
                BusinessCollection collection = new BusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Business obj = new Business();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinessCollection GetAll()
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.Business_Select_All();
                BusinessCollection collection = new BusinessCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Business obj = new Business();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Business Load(System.Nullable<int> RowID)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.Business_Select_One(RowID);
                Business obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Business();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }


        public virtual void Load()
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.Business_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.Business_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Businessname, this.Businesstype, this.Addressid, this.Businessphone1, this.Businessphone2, this.Cellphone, this.Faxnumber, this.Memberofaaa, this.Memberoface, this.Memberofase, this.Memberofbbb, this.Url, this.Isactive, this.Longitude, this.Latitude, this.NaicsCode, this.NaicsDescription, this.SicCode, this.SicDescription);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.Business_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.Business_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Businessname, this.Businesstype, this.Addressid, this.Businessphone1, this.Businessphone2, this.Cellphone, this.Faxnumber, this.Memberofaaa, this.Memberoface, this.Memberofase, this.Memberofbbb, this.Url, this.Isactive, this.Longitude, this.Latitude, this.NaicsCode, this.NaicsDescription, this.SicCode, this.SicDescription);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual System.Nullable<int> InsertBusinessUser(AutoShop.BusinessObjects.Contact cnt,int businessID, out int returnUserId)
        {
            int? businessId;
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                businessId = dbo.Insert_BusinessUser(businessID, cnt.Emailaddress, cnt.Password, cnt.Firstname, cnt.Lastname, cnt.Cellphone, cnt.Photo,"admin", "admin", out returnUserId);
            }
            catch (System.Exception ex)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return businessId;
        }

        public virtual System.Nullable<int> Insertwith_Address(AutoShop.BusinessObjects.Business bus, AutoShop.BusinessObjects.Address adr, AutoShop.BusinessObjects.Contact cnt, out int returnBusinessID)
        {
            int? businessId;
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                businessId = dbo.Business_InsertwithAddress(adr.Streetaddress1, adr.City, adr.State, adr.Zipcode, bus.Businessphone1, bus.Businessname, bus.Businesstype, bus.Faxnumber, bus.Url, cnt.Emailaddress, cnt.Password, cnt.Firstname, cnt.Lastname, bus.Businessphone2, cnt.Photo, adr.Country, bus.Modifiedby, bus.Createdby, bus.IsDealer, bus.AutoManufacturer, bus.StarCertified, out returnBusinessID);
            }
            catch (System.Exception ex)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            return businessId;
        }

        public void Updatewith_Address(AutoShop.BusinessObjects.Business bus, AutoShop.BusinessObjects.Address adr, AutoShop.BusinessObjects.Contact cnt)
        {

            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.Business_UpdateWithAddress(adr.Streetaddress1, adr.City, adr.State, adr.Zipcode, bus.Businessphone1, bus.Businessname, bus.RepairShop.Value, bus.BodyShop.Value, bus.TireShop.Value, bus.CarWash.Value, bus.SmogCheck.Value, bus.Faxnumber, bus.Url, cnt.Firstname, cnt.Lastname, cnt.Cellphone, bus.IsDealer, bus.AutoManufacturer, bus.StarCertified, cnt.Rowid.Value);
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }
        public virtual void Insert_FavouriteDetail(int contactId, int businessId)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.Insert_FavoriteDetail(contactId, businessId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            //return value;
        }

        //public virtual System.Data.DataSet Load_BusinessDetail(int RowId, int contactId)
        // {
        //     AutoShop.DAL.Business dbo = null;
        //     try
        //     {
        //         dbo = new AutoShop.DAL.Business();
        //         System.Data.DataSet ds = dbo.Business_Select_Detail(RowId,contactId);

        //         return ds;
        //     }
        //     catch (System.Exception)
        //     {
        //         throw;
        //     }
        //     finally
        //     {
        //         if ((dbo != null))
        //         {
        //             dbo.Dispose();
        //         }
        //     }
        // }


        //public static Business Load1(System.Nullable<int> RowId,int contactId)
        //{
        //    AutoShop.DAL.Business dbo = null;
        //    try
        //    {
        //        dbo = new AutoShop.DAL.Business();
        //        System.Data.DataSet ds = dbo.Business_Select_One(RowId, contactId);
        //        Business obj = null;
        //        if (GlobalTools.IsSafeDataSet(ds))
        //        {
        //            if ((ds.Tables[0].Rows.Count > 0))
        //            {
        //                obj = new Business();
        //                obj.Fill(ds.Tables[0].Rows[0]);
        //            }
        //        }
        //        return obj;
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if ((dbo != null))
        //        {
        //            dbo.Dispose();
        //        }
        //    }
        //}

        public System.Data.DataSet Select_Dealer_List(string ManufacturerName, string zipcode)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.Select_Dealer_List(ManufacturerName, zipcode);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete_Favorite_Bussiness_By_ContactId(int BusId, int ContId)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.Delete_Favorite_Bussiness_By_ContactId(BusId, ContId);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Business LoadWithContactID(System.Nullable<int> RowID, int contactId)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.Business_Select_OnewithContactId(RowID, contactId);
                Business obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Business();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                //System.Data.DataSet dsDetail = obj;
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual BusinessCollection B2BListing(int PageIndex, int PageSize, string searchValue, string autoShopType,string searchType, out int TotalRecord)
        {
            AutoShop.DAL.Business dbo = null;
            BusinessCollection busCollection = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.B2BListing(PageIndex, PageSize, searchValue, autoShopType, searchType, out TotalRecord);
                busCollection = new BusinessCollection();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    busCollection.Add(B2BData(ds, i));
                }
                return busCollection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        private Business B2BData(System.Data.DataSet ds, int i)
        {
            Business busB2Bdata = new Business();
          
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["IsActive"].ToString()))
            {
                busB2Bdata.Isactive = Convert.ToBoolean(ds.Tables[0].Rows[i]["IsActive"].ToString());
            }
            //else if (string.IsNullOrEmpty(ds.Tables[0].Rows[i]["IsActive"].ToString()))
            //{
            //    busB2Bdata.Isactive = Convert.ToBoolean(0);
            //    busB2Bdata.IsactiveDisp = Convert.ToBoolean(0);
            //}
            //else
            //{
            //    busB2Bdata.Isactive = Convert.ToBoolean(0);
            //    busB2Bdata.IsactiveDisp = Convert.ToBoolean(0);
            //}
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["RowId"].ToString()))
            {
                busB2Bdata.Rowid = Convert.ToInt32(ds.Tables[0].Rows[i]["RowId"].ToString());
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["BusinessName"].ToString()))
            {
                busB2Bdata.Businessname = ds.Tables[0].Rows[i]["BusinessName"].ToString();
            }
            else
            {
                busB2Bdata.Businessname = "";
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["RepairShop"].ToString()))
            {
                if (ds.Tables[0].Rows[i]["RepairShop"].ToString() == "True")
                {
                    busB2Bdata.Businesstype = "Repair Shop";
                }
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["BodyShop"].ToString()))
            {
                if (ds.Tables[0].Rows[i]["BodyShop"].ToString() == "True")
                {
                    busB2Bdata.Businesstype = "Body Shop";
                }
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["TireShop"].ToString()))
            {
                if (ds.Tables[0].Rows[i]["TireShop"].ToString() == "True")
                {
                    busB2Bdata.Businesstype = "Tire Shop";
                }
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["SmogCheck"].ToString()))
            {
                if (ds.Tables[0].Rows[i]["SmogCheck"].ToString() == "True")
                {
                    busB2Bdata.Businesstype = "Smog Check";
                }
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["CarWash"].ToString()))
            {
                if (ds.Tables[0].Rows[i]["CarWash"].ToString() == "True")
                {
                    busB2Bdata.Businesstype = "Car Wash";
                }
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Towing"].ToString()))
            {
                if (ds.Tables[0].Rows[i]["Towing"].ToString() == "True")
                {
                    busB2Bdata.Businesstype = "Towing";
                }
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Transport"].ToString()))
            {
                if (ds.Tables[0].Rows[i]["Transport"].ToString() == "True")
                {
                    busB2Bdata.Businesstype = "Transport";
                }
            }
            if (string.IsNullOrEmpty(busB2Bdata._businesstype))
            {
                busB2Bdata.Businesstype = "";
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ContactName"].ToString()))
            {
                busB2Bdata.ContactName = ds.Tables[0].Rows[i]["ContactName"].ToString();
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["City"].ToString()))
            {
                busB2Bdata.City = ds.Tables[0].Rows[i]["City"].ToString();
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["StateName"].ToString()))
            {
                busB2Bdata.Statecode = ds.Tables[0].Rows[i]["StateName"].ToString();
            }
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Zipcode"].ToString()))
            {
                busB2Bdata.AddressDetail = ds.Tables[0].Rows[i]["Zipcode"].ToString();
            }


            return busB2Bdata;
        }

        public virtual void update(bool chk, int cntId)
        {
            AutoShop.DAL.Business bus = new DAL.Business();
            bus.UpdateIsActiveBusContact(chk, cntId);
        }

        public System.Data.DataSet B2BRegSearchList(string busName, int zipcode)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                System.Data.DataSet ds = dbo.B2BRegSearchList(busName, zipcode);

                return ds;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void RegisterBusiness(AutoShop.BusinessObjects.Contact cnt, int busID)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.RegisterBusiness(cnt.Emailaddress, cnt.Password, cnt.Firstname, cnt.Lastname, busID);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
            //return value;
        }

        public virtual void Business_ActiveInactive(int BusId, bool IsActive)
        {
            AutoShop.DAL.Business dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Business();
                dbo.Business_ActiveInactive(BusId, IsActive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        //public virtual System.Nullable<int> Insertwith_AddressBilling(AutoShop.BusinessObjects.Business bus, AutoShop.BusinessObjects.Address adr, AutoShop.BusinessObjects.Contact cnt, AutoShop.BusinessObjects.Payment pay, out int returnBusinessID)
        //{
        //    int? businessId;
        //    AutoShop.DAL.Business dbo = null;
        //    try
        //    {
        //        dbo = new AutoShop.DAL.Business();
        //        businessId = dbo.Insertwith_AddressBilling(adr.Streetaddress1, adr.City, adr.State, adr.Zipcode, bus.Businessphone1, bus.Businessname, bus.Businesstype, bus.Faxnumber, bus.Url, cnt.Emailaddress, cnt.Password, cnt.Firstname, cnt.Lastname, bus.Businessphone2, adr.Country, bus.Modifiedby, bus.Createdby, bus.IsDealer, bus.AutoManufacturer, bus.StarCertified,pay.Billingstreetname,pay.Billingcity,pay.BillingState,pay.Billingzipcode, out  returnBusinessID);
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if ((dbo != null))
        //        {
        //            dbo.Dispose();
        //        }
        //    }
        //    return businessId;
        //}

    }
}