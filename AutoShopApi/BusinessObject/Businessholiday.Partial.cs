namespace AutoShop.BusinessObjects
{
    public partial class Businessholiday
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _status;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _businessid;

        private System.Nullable<int> _holidayid;

        private Business _business;

        private Holiday _holiday;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual Business Business
        {
            get
            {
                if ((this._business == null))
                {
                    this._business = AutoShop.BusinessObjects.Business.Load(this._businessid);
                }
                return this._business;
            }
            set
            {
                _business = value;
            }
        }

        public virtual Holiday Holiday
        {
            get
            {
                if ((this._holiday == null))
                {
                    this._holiday = AutoShop.BusinessObjects.Holiday.Load(this._holidayid);
                }
                return this._holiday;
            }
            set
            {
                _holiday = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Status = string.Empty;
            this.Isactive = null;
            this._businessid = null;
            this._holidayid = null;
            this.Business = null;
            this.Holiday = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Status"] != System.DBNull.Value))
            {
                this.Status = ((string)(dr["Status"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this._businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
            if ((dr["HolidayId"] != System.DBNull.Value))
            {
                this._holidayid = ((System.Nullable<int>)(dr["HolidayId"]));
            }
        }

        public static BusinessholidayCollection Select_BusinessHolidays_By_BusinessId(System.Nullable<int> BusinessId)
        {
            AutoShop.DAL.Businessholiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessholiday();
                System.Data.DataSet ds = dbo.Select_BusinessHolidays_By_BusinessId(BusinessId);
                BusinessholidayCollection collection = new BusinessholidayCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessholiday obj = new Businessholiday();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinessholidayCollection Select_BusinessHolidays_By_HolidayId(System.Nullable<int> HolidayId)
        {
            AutoShop.DAL.Businessholiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessholiday();
                System.Data.DataSet ds = dbo.Select_BusinessHolidays_By_HolidayId(HolidayId);
                BusinessholidayCollection collection = new BusinessholidayCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessholiday obj = new Businessholiday();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinessholidayCollection GetAll()
        {
            AutoShop.DAL.Businessholiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessholiday();
                System.Data.DataSet ds = dbo.BusinessHoliday_Select_All();
                BusinessholidayCollection collection = new BusinessholidayCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessholiday obj = new Businessholiday();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Businessholiday Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Businessholiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessholiday();
                System.Data.DataSet ds = dbo.BusinessHoliday_Select_One(RowId);
                Businessholiday obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Businessholiday();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Businessholiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessholiday();
                System.Data.DataSet ds = dbo.BusinessHoliday_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Businessholiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessholiday();
                dbo.BusinessHoliday_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Status, this.Isactive, this._businessid, this._holidayid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Businessholiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessholiday();
                dbo.BusinessHoliday_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Businessholiday dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessholiday();
                dbo.BusinessHoliday_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Status, this.Isactive, this._businessid, this._holidayid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}