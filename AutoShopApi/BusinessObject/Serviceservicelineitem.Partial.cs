namespace AutoShop.BusinessObjects
{
    public partial class Serviceservicelineitem
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _servicelineitemid;

        private System.Nullable<bool> _isactive;

        private System.Nullable<int> _serviceid;

        private Service _service;

        private Servicelineitem _servicelineitem;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual Service Service
        {
            get
            {
                if ((this._service == null))
                {
                    this._service = AutoShop.BusinessObjects.Service.Load(this._serviceid);
                }
                return this._service;
            }
            set
            {
                _service = value;
            }
        }

        public virtual Servicelineitem Servicelineitem
        {
            get
            {
                if ((this._servicelineitem == null))
                {
                    this._servicelineitem = AutoShop.BusinessObjects.Servicelineitem.Load(this._servicelineitemid);
                }
                return this._servicelineitem;
            }
            set
            {
                _servicelineitem = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._servicelineitemid = null;
            this.Isactive = null;
            this._serviceid = null;
            this.Service = null;
            this.Servicelineitem = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["ServiceLineItemId"] != System.DBNull.Value))
            {
                this._servicelineitemid = ((System.Nullable<int>)(dr["ServiceLineItemId"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
            if ((dr["ServiceId"] != System.DBNull.Value))
            {
                this._serviceid = ((System.Nullable<int>)(dr["ServiceId"]));
            }
        }

        public static ServiceservicelineitemCollection Select_ServiceServiceLineItems_By_ServiceId(System.Nullable<int> ServiceId)
        {
            AutoShop.DAL.Serviceservicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Serviceservicelineitem();
                System.Data.DataSet ds = dbo.Select_ServiceServiceLineItems_By_ServiceId(ServiceId);
                ServiceservicelineitemCollection collection = new ServiceservicelineitemCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Serviceservicelineitem obj = new Serviceservicelineitem();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static ServiceservicelineitemCollection Select_ServiceServiceLineItems_By_ServiceLineItemId(System.Nullable<int> ServiceLineItemId)
        {
            AutoShop.DAL.Serviceservicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Serviceservicelineitem();
                System.Data.DataSet ds = dbo.Select_ServiceServiceLineItems_By_ServiceLineItemId(ServiceLineItemId);
                ServiceservicelineitemCollection collection = new ServiceservicelineitemCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Serviceservicelineitem obj = new Serviceservicelineitem();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static ServiceservicelineitemCollection GetAll()
        {
            AutoShop.DAL.Serviceservicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Serviceservicelineitem();
                System.Data.DataSet ds = dbo.ServiceServiceLineItem_Select_All();
                ServiceservicelineitemCollection collection = new ServiceservicelineitemCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Serviceservicelineitem obj = new Serviceservicelineitem();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Serviceservicelineitem Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Serviceservicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Serviceservicelineitem();
                System.Data.DataSet ds = dbo.ServiceServiceLineItem_Select_One(RowId);
                Serviceservicelineitem obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Serviceservicelineitem();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Serviceservicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Serviceservicelineitem();
                System.Data.DataSet ds = dbo.ServiceServiceLineItem_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Serviceservicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Serviceservicelineitem();
                dbo.ServiceServiceLineItem_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._servicelineitemid, this.Isactive, this._serviceid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Serviceservicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Serviceservicelineitem();
                dbo.ServiceServiceLineItem_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Serviceservicelineitem dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Serviceservicelineitem();
                dbo.ServiceServiceLineItem_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._servicelineitemid, this.Isactive, this._serviceid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}