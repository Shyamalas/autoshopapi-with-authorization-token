namespace AutoShop.BusinessObjects
{
    public partial class Automobilemodel
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _model;

        private System.Nullable<int> _manufacturerid;

        private string _description;

        private System.Nullable<bool> _isactive;

        private AutomobileengineCollection _automobileengineCollection;

        private Automobilemanufacturer _automobilemanufacturer;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value;
            }
        }

        public virtual string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual AutomobileengineCollection AutomobileengineCollection
        {
            get
            {
                if ((this._automobileengineCollection == null))
                {
                    _automobileengineCollection = AutoShop.BusinessObjects.Automobileengine.Select_AutomobileEngines_By_ModelId(this.Rowid);
                }
                return this._automobileengineCollection;
            }
        }

        public virtual Automobilemanufacturer Automobilemanufacturer
        {
            get
            {
                if ((this._automobilemanufacturer == null))
                {
                    this._automobilemanufacturer = AutoShop.BusinessObjects.Automobilemanufacturer.Load(this._manufacturerid);
                }
                return this._automobilemanufacturer;
            }
            set
            {
                _automobilemanufacturer = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this.Model = string.Empty;
            this._manufacturerid = null;
            this.Description = string.Empty;
            this.Isactive = null;
            this._automobileengineCollection = null;
            this.Automobilemanufacturer = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["Model"] != System.DBNull.Value))
            {
                this.Model = ((string)(dr["Model"]));
            }
            if ((dr["ManufacturerId"] != System.DBNull.Value))
            {
                this._manufacturerid = ((System.Nullable<int>)(dr["ManufacturerId"]));
            }
            if ((dr["Description"] != System.DBNull.Value))
            {
                this.Description = ((string)(dr["Description"]));
            }
            if ((dr["IsActive"] != System.DBNull.Value))
            {
                this.Isactive = ((System.Nullable<bool>)(dr["IsActive"]));
            }
        }

        public static AutomobilemodelCollection Select_AutomobileModels_By_ManufacturerId(System.Nullable<int> ManufacturerId)
        {
            AutoShop.DAL.Automobilemodel dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemodel();
                System.Data.DataSet ds = dbo.Select_AutomobileModels_By_ManufacturerId(ManufacturerId);
                AutomobilemodelCollection collection = new AutomobilemodelCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Automobilemodel obj = new Automobilemodel();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static AutomobilemodelCollection GetAll()
        {
            AutoShop.DAL.Automobilemodel dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemodel();
                System.Data.DataSet ds = dbo.AutomobileModel_Select_All();
                AutomobilemodelCollection collection = new AutomobilemodelCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Automobilemodel obj = new Automobilemodel();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Automobilemodel Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Automobilemodel dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemodel();
                System.Data.DataSet ds = dbo.AutomobileModel_Select_One(RowId);
                Automobilemodel obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Automobilemodel();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Automobilemodel dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemodel();
                System.Data.DataSet ds = dbo.AutomobileModel_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Automobilemodel dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemodel();
                dbo.AutomobileModel_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Model, this._manufacturerid, this.Description, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Automobilemodel dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemodel();
                dbo.AutomobileModel_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Automobilemodel dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Automobilemodel();
                dbo.AutomobileModel_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this.Model, this._manufacturerid, this.Description, this.Isactive);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}