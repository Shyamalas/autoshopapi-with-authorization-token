namespace AutoShop.BusinessObjects
{
    public partial class Businessaddress
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private System.Nullable<int> _addressid;

        private System.Nullable<int> _businessid;

        private Address _address;

        private Business _business;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual Address Address
        {
            get
            {
                if ((this._address == null))
                {
                    this._address = AutoShop.BusinessObjects.Address.Load(this._addressid);
                }
                return this._address;
            }
            set
            {
                _address = value;
            }
        }

        public virtual Business Business
        {
            get
            {
                if ((this._business == null))
                {
                    this._business = AutoShop.BusinessObjects.Business.Load(this._businessid);
                }
                return this._business;
            }
            set
            {
                _business = value;
            }
        }

        private void Clean()
        {
            this.Rowid = null;
            this.Createdby = string.Empty;
            this.Createddate = null;
            this.Modifiedby = string.Empty;
            this.Modifieddate = null;
            this._addressid = null;
            this._businessid = null;
            this.Address = null;
            this.Business = null;
        }

        private void Fill(System.Data.DataRow dr)
        {
            this.Clean();
            if ((dr["RowId"] != System.DBNull.Value))
            {
                this.Rowid = ((System.Nullable<int>)(dr["RowId"]));
            }
            if ((dr["CreatedBy"] != System.DBNull.Value))
            {
                this.Createdby = ((string)(dr["CreatedBy"]));
            }
            if ((dr["CreatedDate"] != System.DBNull.Value))
            {
                this.Createddate = ((System.Nullable<System.DateTime>)(dr["CreatedDate"]));
            }
            if ((dr["ModifiedBy"] != System.DBNull.Value))
            {
                this.Modifiedby = ((string)(dr["ModifiedBy"]));
            }
            if ((dr["ModifiedDate"] != System.DBNull.Value))
            {
                this.Modifieddate = ((System.Nullable<System.DateTime>)(dr["ModifiedDate"]));
            }
            if ((dr["AddressId"] != System.DBNull.Value))
            {
                this._addressid = ((System.Nullable<int>)(dr["AddressId"]));
            }
            if ((dr["BusinessId"] != System.DBNull.Value))
            {
                this._businessid = ((System.Nullable<int>)(dr["BusinessId"]));
            }
        }

        public static BusinessaddressCollection Select_BusinessAddresss_By_AddressId(System.Nullable<int> AddressId)
        {
            AutoShop.DAL.Businessaddress dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessaddress();
                System.Data.DataSet ds = dbo.Select_BusinessAddresss_By_AddressId(AddressId);
                BusinessaddressCollection collection = new BusinessaddressCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessaddress obj = new Businessaddress();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinessaddressCollection Select_BusinessAddresss_By_BusinessId(System.Nullable<int> BusinessId)
        {
            AutoShop.DAL.Businessaddress dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessaddress();
                System.Data.DataSet ds = dbo.Select_BusinessAddresss_By_BusinessId(BusinessId);
                BusinessaddressCollection collection = new BusinessaddressCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessaddress obj = new Businessaddress();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static BusinessaddressCollection GetAll()
        {
            AutoShop.DAL.Businessaddress dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessaddress();
                System.Data.DataSet ds = dbo.BusinessAddress_Select_All();
                BusinessaddressCollection collection = new BusinessaddressCollection();
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    for (int i = 0; (i < ds.Tables[0].Rows.Count); i = (i + 1))
                    {
                        Businessaddress obj = new Businessaddress();
                        obj.Fill(ds.Tables[0].Rows[i]);
                        if ((obj != null))
                        {
                            collection.Add(obj);
                        }
                    }
                }
                return collection;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public static Businessaddress Load(System.Nullable<int> RowId)
        {
            AutoShop.DAL.Businessaddress dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessaddress();
                System.Data.DataSet ds = dbo.BusinessAddress_Select_One(RowId);
                Businessaddress obj = null;
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        obj = new Businessaddress();
                        obj.Fill(ds.Tables[0].Rows[0]);
                    }
                }
                return obj;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Load()
        {
            AutoShop.DAL.Businessaddress dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessaddress();
                System.Data.DataSet ds = dbo.BusinessAddress_Select_One(this.Rowid);
                if (GlobalTools.IsSafeDataSet(ds))
                {
                    if ((ds.Tables[0].Rows.Count > 0))
                    {
                        this.Fill(ds.Tables[0].Rows[0]);
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Insert()
        {
            AutoShop.DAL.Businessaddress dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessaddress();
                dbo.BusinessAddress_Insert(this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._addressid, this._businessid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Delete()
        {
            AutoShop.DAL.Businessaddress dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessaddress();
                dbo.BusinessAddress_Delete(this.Rowid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }

        public virtual void Update()
        {
            AutoShop.DAL.Businessaddress dbo = null;
            try
            {
                dbo = new AutoShop.DAL.Businessaddress();
                dbo.BusinessAddress_Update(this.Rowid, this.Createdby, this.Createddate, this.Modifiedby, this.Modifieddate, this._addressid, this._businessid);
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                if ((dbo != null))
                {
                    dbo.Dispose();
                }
            }
        }
    }
}