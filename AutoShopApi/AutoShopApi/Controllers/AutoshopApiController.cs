﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APIServiceLibrary;
using Newtonsoft.Json;

namespace AutoShopApi.Controllers
{
    public class AutoshopApiController : ApiController
    {
        AutoShopApi.Dataservice ds = new AutoShopApi.Dataservice();
        [Authorize]
        [HttpPost]
        [Route("api/Search/GetSearchResults/{securityToken}")]
        public IHttpActionResult GetBusinessListByZipCodeTypesMiles(string securityToken, [FromBody] AutoShopApi.Models.Business Business) 
        {
            try
            {
                string responsestr = ds.GetBusinessListByZipCodeTypesMiles(securityToken, Business.Zipcode, Business.Miles, Business.AutoShopTypes, Business.ContactID);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Search/RefineSearch/{securityToken}")]
        public IHttpActionResult GetBusinessListByUpdateSearch(string securityToken, [FromBody] AutoShopApi.Models.Business Business)
        {
            try
            {
                string responsestr = ds.GetBusinessListByUpdateSearch(securityToken, Business.Zipcode, Business.Miles, Business.AutoShopTypes, Business.WeekdayID, Business.ContactID);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Register/RegisterUser/{securityToken}")]
        public IHttpActionResult NewUserRegistration(string securityToken, [FromBody] AutoShopApi.Models.Contact_Model Contact) 
        {
            try
            {
                string responsestr = ds.NewUserRegistration(securityToken, Contact);
                return SuccessMsg(JsonConvert.SerializeObject(responsestr));
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Favorite/GetMyFavoriteAutoShops/{securityToken}")]
        public IHttpActionResult GetMyFavoriteAutoShops(string securityToken, [FromBody] string EmailAddress)
        {
            try
            {
                string responsestr = ds.GetMyFavoriteAutoShops(securityToken, EmailAddress);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Favorite/DeleteFavoriteAutoShop/{securityToken}")]
        public IHttpActionResult DeleteFavoriteAutoShop(string securityToken, [FromBody] AutoShopApi.Models.Business Business)
        {
            try
            {
                string responsestr = ds.DeleteFavoriteAutoShop(securityToken, Business.Emailaddress, Business.BusinessID);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Login/GetMyProfile/{securityToken}")]
        public IHttpActionResult GetMyProfile(string securityToken, [FromBody] string EmailAddress)
        {
            try
            {
                string responsestr = ds.GetMyProfile(securityToken, EmailAddress);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Login/UpdateMyProfile/{securityToken}/{EmailAddress}/{FirstName}/{LastName}/{StreetAddress}/{City}/{State}/{Zipcode}/{Cellphone}")]
        public IHttpActionResult UpdateMyProfile(string securityToken, [FromBody] string EmailAddress, string FirstName, string LastName, string StreetAddress, string City, string State, int Zipcode, string Cellphone)
        {
            try
            {
                string responsestr = ds.UpdateMyProfile(securityToken, EmailAddress, FirstName, LastName, StreetAddress, City, State, Zipcode, Cellphone);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }


        //[HttpPost]
        //[Route("api/Login/Login/{securityToken}")]
        //public IHttpActionResult Login(string securityToken, [FromBody] AutoShopApi.Models.Login Login)
        //{
        //    try
        //    {
        //        string responsestr = ds.LoginAuth(securityToken, Login.Emailaddress, Login.OldPassword);
        //        return SuccessMsg(JsonConvert.SerializeObject(responsestr));
        //    }
        //    catch (Exception ex)
        //    {
        //        return FailureMsg(ex.Message);
        //    }
        //}

        [Authorize]
        [HttpPost]
        [Route("api/Login/ChangePassword/{securityToken}")]
        public IHttpActionResult ChangePassword(string securityToken, [FromBody] AutoShopApi.Models.Login Login)
        {
            try
            {
                string responsestr = ds.ChangePassword(securityToken, Login.Emailaddress, Login.OldPassword, Login.NewPassword);
                return SuccessMsg(JsonConvert.SerializeObject(responsestr));
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Login/ForgotPassword/{securityToken}")]
        public IHttpActionResult ForgotPassword(string securityToken, [FromBody] AutoShopApi.Models.Login Login)
        {
            try
            {
                string responsestr = ds.ForgotPassword(securityToken, Login.Emailaddress);
                return SuccessMsg(JsonConvert.SerializeObject(responsestr));
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Login/UpdatePassword/{securityToken}")]
        public IHttpActionResult UpdatePassword(string securityToken, [FromBody] AutoShopApi.Models.Login Login)
        {
            try
            {
                string responsestr = ds.UpdatePassword(securityToken, Login.Emailaddress, Login.NewPassword, Login.ResetToken);
                return SuccessMsg(JsonConvert.SerializeObject(responsestr));
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Register/BusinessRegistrationSearch/{securityToken}")]
        public IHttpActionResult BusinessRegistrationSearch(string securityToken, [FromBody] AutoShopApi.Models.Business Business)// string BusinessName, string ZipCode)
        {
            try
            {
                string responsestr = ds.GetB2BRegSearchList(securityToken, Business.BusinessName, Business.Zipcode);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/Register/GetSubscriptionList/{securityToken}")]
        public IHttpActionResult GetSubscriptionList(string securityToken)
        {
            try
            {
                string responsestr = ds.GetSubscription(securityToken);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Register/CheckUserExists/{securityToken}")]
        public IHttpActionResult CheckUserExists(string securityToken, [FromBody] string EmailAddress)
        {
            try
            {
                string responsestr = ds.CheckEmailExists(securityToken, EmailAddress);
                return SuccessMsg(JsonConvert.SerializeObject(responsestr));
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/Register/GetStateList/{securityToken}")]
        public IHttpActionResult GetStateList(string securityToken)
        {
            try
            {
                string responsestr = ds.GetAllStates(securityToken);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Search/GetZipcodeAutoComplete/{securityToken}")]
        public IHttpActionResult GetZipcodeAutoComplete(string securityToken, [FromBody] int Zipcode)
        {
            try
            {
                string responsestr = ds.GetZipCode(securityToken, Zipcode);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Search/GetAutoshopDetails/{securityToken}")]
        public IHttpActionResult GetAutoshopDetails(string securityToken, [FromBody] AutoShopApi.Models.Business Bus)
        {
            try
            {
                string responsestr = ds.GetAutoshopDetails(securityToken, Bus.BusinessID, Bus.ContactID);
                return SuccessMsg(responsestr);
            }
            catch (Exception ex)
            {
                return FailureMsg(ex.Message);
            }
        }

        private IHttpActionResult SuccessMsg(string result)
        {
            var Response = new ResponseMsg();
            object JsonObj = JsonSerilizerHelper.DeserializeJsonFromString(result);
            Response.Data = JsonObj;
            Response.Status = HttpStatusCode.OK;
            Response.Message = "Successful";
            return new HttpActionResult(Response);
        }

        private IHttpActionResult FailureMsg(string result)
        {
            var Response = new ResponseMsg();
            object JsonObj = JsonSerilizerHelper.DeserializeJsonFromString(JsonConvert.SerializeObject(result));
            Response.Status = HttpStatusCode.ExpectationFailed;
            Response.Message = "Failure";
            Response.Data = JsonObj;
            return new HttpActionResult(Response);
        }
    }
}