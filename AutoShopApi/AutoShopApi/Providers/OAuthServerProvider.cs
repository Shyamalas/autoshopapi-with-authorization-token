﻿
using System.Collections.Generic;
using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Web.Security;

namespace AutoShopApi.Providers
{
    public class OAuthServerProvider : OAuthAuthorizationServerProvider
    {

        #region[GrantResourceOwnerCredentials]
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var username = context.UserName;
            var password = FormsAuthentication.HashPasswordForStoringInConfigFile(context.Password, "MD5"); 

            AutoShopApi.Dataservice ds = new AutoShopApi.Dataservice();
            string responsestr = ds.LoginAuth(string.Empty,username, password);

            if (responsestr == "Valid User")
            {
                context.Validated(new ClaimsIdentity(context.Options.AuthenticationType));
            }

            else if (responsestr == "Unauthorized User")
            {
                context.SetError("invalid_grant", "The user name or password is incorrect or user may inactive.");
            }
           
        }
        #endregion

        #region[ValidateClientAuthentication]
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (string.IsNullOrEmpty(context.Parameters.Get("device_type")))
            {
                if (!string.IsNullOrEmpty(context.Parameters.Get("username")) && !string.IsNullOrEmpty(context.Parameters.Get("password")))
                {
                    context.Validated();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(context.Parameters.Get("username")) && !string.IsNullOrEmpty(context.Parameters.Get("password"))
                    && !string.IsNullOrEmpty(context.Parameters.Get("device_type")))
                {
                    context.Validated();
                }
            }
        }
        #endregion

        #region[TokenEndpoint]
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
        #endregion

        //#region[CreateProperties]
        //public static AuthenticationProperties CreateProperties(string userName)
        //{
        //    IDictionary<string, string> data = new Dictionary<string, string>
        //    {
        //        { "userName", userName }
        //    };
        //    return new AuthenticationProperties(data);
        //}
        //#endregion

    }
}