﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoShopApi.Models
{
    public class Business
    {
        private int _zipcode;

        private int _miles;

        private string _autoshoptypes;
        private string _weekdayid;
        private string _businessname;
        private string _emailaddress;
        private int _contactid;
        private int _busid;
        public virtual int Zipcode
        {
            get
            {
                return _zipcode;
            }
            set
            {
                _zipcode = value;
            }
        }
        public virtual int Miles
        {
            get
            {
                return _miles;
            }
            set
            {
                _miles = value;
            }
        }

        public virtual string AutoShopTypes
        {
            get
            {
                return _autoshoptypes;
            }
            set
            {
                _autoshoptypes = value;
            }
        }

        public virtual string Emailaddress
        {
            get
            {
                return _emailaddress;
            }
            set
            {
                _emailaddress = value;
            }
        }

        public virtual string WeekdayID
        {
            get
            {
                return _weekdayid;
            }
            set
            {
                _weekdayid = value;
            }
        }

        public virtual string BusinessName
        {
            get
            {
                return _businessname;
            }
            set
            {
                _businessname = value;
            }
        }

        public virtual int ContactID
        {
            get
            {
                return _contactid;
            }
            set
            {
                _contactid = value;
            }
        }

        public virtual int BusinessID
        {
            get
            {
                return _busid;
            }
            set
            {
                _busid = value;
            }
        }
    }
}