﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoShopApi.Models
{
    public class Contact_Model
    {
        private System.Nullable<int> _rowid;

        private string _createdby;

        private System.Nullable<System.DateTime> _createddate;

        private string _modifiedby;

        private System.Nullable<System.DateTime> _modifieddate;

        private string _type;

        private string _firstname;

        private string _lastname;

        private string _emailaddress;

        private string _homephone;

        private string _cellphone;

        private string _workphone;

        private System.Nullable<bool> _isactive;

        private string _password;

        private System.Nullable<int> _loginid;

        private string _streetaddress;

        private string _city;

        private string _state;

        private string _photo;

        private System.Nullable<int> _zipcode;

        private string _activationCode;

        public System.Nullable<int> _isOnline;

        public virtual System.Nullable<int> Rowid
        {
            get
            {
                return _rowid;
            }
            set
            {
                _rowid = value;
            }
        }

        public virtual string Createdby
        {
            get
            {
                return _createdby;
            }
            set
            {
                _createdby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                _createddate = value;
            }
        }

        public virtual string Modifiedby
        {
            get
            {
                return _modifiedby;
            }
            set
            {
                _modifiedby = value;
            }
        }

        public virtual System.Nullable<System.DateTime> Modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                _modifieddate = value;
            }
        }

        public virtual string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public virtual string Firstname
        {
            get
            {
                return _firstname;
            }
            set
            {
                _firstname = value;
            }
        }

        public virtual string Lastname
        {
            get
            {
                return _lastname;
            }
            set
            {
                _lastname = value;
            }
        }

        public virtual string Emailaddress
        {
            get
            {
                return _emailaddress;
            }
            set
            {
                _emailaddress = value;
            }
        }

        public virtual string Homephone
        {
            get
            {
                return _homephone;
            }
            set
            {
                _homephone = value;
            }
        }

        public virtual string Cellphone
        {
            get
            {
                return _cellphone;
            }
            set
            {
                _cellphone = value;
            }
        }

        public virtual string Workphone
        {
            get
            {
                return _workphone;
            }
            set
            {
                _workphone = value;
            }
        }

        public virtual System.Nullable<bool> Isactive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = value;
            }
        }

        public virtual string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        public virtual System.Nullable<int> Loginid
        {
            get
            {
                return _loginid;
            }
            set
            {
                _loginid = value;
            }
        }

        public virtual string Streetaddress
        {
            get
            {
                return _streetaddress;
            }
            set
            {
                _streetaddress = value;
            }
        }

        public virtual string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        public virtual string State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        public virtual string Photo
        {
            get
            {
                return _photo;
            }
            set
            {
                _photo = value;
            }
        }

        public virtual System.Nullable<int> Zipcode
        {
            get
            {
                return _zipcode;
            }
            set
            {
                _zipcode = value;
            }
        }

        public virtual string ActivationCode
        {
            get
            {
                return _activationCode;
            }
            set
            {
                _activationCode = value;
            }
        }

        public virtual int? IsOnline
        {
            get
            {
                return _isOnline;
            }
            set
            {
                _isOnline = value;
            }
        }

    }
}