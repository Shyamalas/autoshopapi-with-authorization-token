﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoShopApi.Models
{
    public class Login
    {
        private string _oldpassword;

        private string _newpassword;

        private string _emailaddress;

        private string _resettoken;
        public virtual string OldPassword
        {
            get
            {
                return _oldpassword;
            }
            set
            {
                _oldpassword = value;
            }
        }

        public virtual string NewPassword
        {
            get
            {
                return _newpassword;
            }
            set
            {
                _newpassword = value;
            }
        }

        public virtual string Emailaddress
        {
            get
            {
                return _emailaddress;
            }
            set
            {
                _emailaddress = value;
            }
        }

        public virtual string ResetToken
        {
            get
            {
                return _resettoken;
            }
            set
            {
                _resettoken = value;
            }
        }
    }
}