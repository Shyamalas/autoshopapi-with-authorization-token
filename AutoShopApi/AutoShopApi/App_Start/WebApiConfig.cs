﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AutoShopApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
               name: "ApiByName",
               routeTemplate: "api/{controller}/{action}/{actionname}",
               defaults: null,
               constraints: new { actionname = @"^[a-z]+$" }
           );
            config.Routes.MapHttpRoute(
               name: "ApiByAction",
               routeTemplate: "api/{controller}/{action}",
               defaults: new { action = "Get" }
           );

            config.Routes.MapHttpRoute(
                name: "ApiByactionName",
                routeTemplate: "api/{controller}/{action}/{actionname}",
                defaults: new { action = @"^[a-z]+$" },
                constraints: new { actionname = @"^[a-z]+$" }
            );
        }
    }
}
