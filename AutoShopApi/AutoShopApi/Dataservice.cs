﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using APIServiceLibrary;
using System.Configuration;
using System.Data;
using System.Reflection;
using AutoShop.BusinessObjects;
using AutoShop.BusinessObjects.Entities;
using System.Web.Security;
using System.Text.RegularExpressions;

namespace AutoShopApi
{
    public class Dataservice
    {
        ILogger _Logger;

        public Dataservice()
        {
            _Logger = new LoggingAdapters();
        }

        private string ConvertAutoShopTypes(string autoShopTypes)
        {
            char[] delimiters = { ' ', ',', '.' };
            string[] arrTypes = autoShopTypes.Split(delimiters);
            List<int> typei = new List<int>();
            foreach (string s in arrTypes)
            {
                switch (s.ToUpper())
                {
                    case "REPAIRSHOP":
                        typei.Add(1);
                        break;
                    case "BODYSHOP":
                        typei.Add(2);
                        break;
                    case "TIRESHOP":
                        typei.Add(3);
                        break;
                    case "SMOGCHECK":
                        typei.Add(4);
                        break;
                    case "CARWASH":
                        typei.Add(5);
                        break;
                    case "TOWING":
                        typei.Add(6);
                        break;
                    case "TRANSPORT":
                        typei.Add(7);
                        break;
                    case "SMOGCHECKONLY":
                        typei.Add(8);
                        break;
                    case "GLASSSHOP":
                        typei.Add(9);
                        break;
                    case "DEALER":
                        typei.Add(10);
                        break;
                    case "PARTS":
                        typei.Add(11);
                        break;
                    case "NEWCARSALES":
                        typei.Add(12);
                        break;
                    case "USEDCARSALES":
                        typei.Add(13);
                        break;
                    case "ALL":
                        typei.Add(14);
                        break;
                    default:
                        typei.Add(14);
                        break;
                }

            }
            return String.Join(",", typei.Distinct().ToArray());
        }
        private bool ValidateSecurityTokenKey(string SecurityTokenKey)
        {
            try
            {
                string[] listsec = ConfigurationManager.AppSettings["SecurityTokenKeys"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return listsec.Contains(SecurityTokenKey);
            }
            catch (Exception ex)
            {
                _Logger.Log("Error in Validating Security Token key", ex);
                throw ex;
            }

        }

        public string GetBusinessListByZipCodeTypesMiles(string SecurityTokenKey, int Zipcode, int Radius, string AutoShopTypes, int ContactId)
        {
            string jsonstr = string.Empty;
            try
            {
                // Pass ContactId to get the favourite Autoshops. Null will return all Autoshops. 				
                _Logger.Log("GetBusinessListByZipCodeTypesMiles: Zipcode [" + Zipcode + "] Radius [" + Radius + "] AutoShopTypes [" + AutoShopTypes + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " GetBusinessListByZipCodeTypesMiles "));

                AutoShop.DAL.SearchBusiness dbo = new AutoShop.DAL.SearchBusiness();
                DataSet ds = new DataSet();
                ds = dbo.SearchBusinessByZipCodeMilesType(Zipcode, Radius, AutoShopTypes, ContactId);

                ds.DataSetName = "BusinessSearch";
                ds.Tables[0].TableName = "Business";
                ds.Tables[1].TableName = "BusinessHours";

                DataRelation drrelate = new DataRelation("Relate", ds.Tables["Business"].Columns["BusinessId"], ds.Tables["BusinessHours"].Columns["BusinessId"]);
                drrelate.Nested = true;
                ds.Relations.Add(drrelate);

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("GetBusinessListByZipCodeTypesMiles Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetBusinessListByZipCodeTypesMiles Output: [" + jsonstr + "]");
            }
        }

        public string GetBusinessListByUpdateSearch(string SecurityTokenKey, int Zipcode, System.Nullable<int> Miles, string AutoShopTypes, string WeekdayID, int ContactID)
        {
            string jsonstr = string.Empty;
            try
            {
                // Pass ContactId to get the favourite Autoshops. Null will return all Autoshops. 				
                _Logger.Log("GetBusinessListByUpdateSearch: Zipcode [" + Zipcode + "]  Miles [" + Miles + "] AutoShopTypes [" + AutoShopTypes + "] WeekdayID [" + WeekdayID + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " GetBusinessListByUpdateSearch "));

                AutoShop.DAL.SearchBusiness dbo = new AutoShop.DAL.SearchBusiness();
                DataSet ds = new DataSet();
                ds = dbo.BusinessRevisedSearch(Zipcode, Miles, AutoShopTypes, WeekdayID, ContactID);
                ds.DataSetName = "RevisedSearch";
                ds.Tables[0].TableName = "Business";
                ds.Tables[1].TableName = "BusinessHours";

                DataRelation drrelate = new DataRelation("Relate", ds.Tables["Business"].Columns["BusinessId"], ds.Tables["BusinessHours"].Columns["BusinessId"]);
                drrelate.Nested = true;
                ds.Relations.Add(drrelate);

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("GetBusinessListByUpdateSearch Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetBusinessListByUpdateSearch Output: [" + jsonstr + "]");
            }
        }


        public string NewUserRegistration(string SecurityTokenKey, AutoShopApi.Models.Contact_Model Contact)
        {
            int? ContactId = 0;
            string jsonstr = string.Empty;
            try
            {
                _Logger.Log("NewUserRegistration: [" + Contact + "]");
                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " NewUserRegistration "));

                AutoShop.DAL.Contact dbo = new AutoShop.DAL.Contact();
                if (!dbo.CheckEmailExists(Contact.Emailaddress))
                {
                    AutoShop.DAL.Contact contactDal = new AutoShop.DAL.Contact();

                    ContactId = contactDal.Contact_Insert(Contact.Firstname, Contact.Lastname, Contact.Emailaddress, Contact.Type, Contact.Cellphone, EncryptDecryptMD5(Contact.Password), Contact.Streetaddress, Contact.City, Contact.State, Contact.Zipcode, Contact.ActivationCode, Contact.Photo, out int ReturnContactId);
                    // Send welcome Email
                    jsonstr = (ContactId != 0) ? "True" : "False";
                }
                else
                {
                    jsonstr = "Email Already Exists.";
                }
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("NewUserRegistration Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("NewUserRegistration EmailAddress [" + Contact.Emailaddress + " ] Output: [" + ContactId + "]");
            }
        }

        public string GetMyProfile(string SecurityTokenKey, string EmailAddress)
        {
            string jsonstr = string.Empty;
            int ContactId;
            try
            {
                _Logger.Log("GetMyProfile: EmailAddress [" + EmailAddress + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " GetMyProfile "));

                DataSet ds = new DataSet();
                AutoShop.DAL.Contact dbo = new AutoShop.DAL.Contact();

                dbo.GetContactIdByEmail(EmailAddress, out ContactId);
                ds = dbo.GetB2CProfileByID(ContactId);
                ds.DataSetName = "UserProfile";
                ds.Tables[0].TableName = "User";

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("GetMyProfile Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetMyProfile Output: [" + jsonstr + "]");
            }
        }

        public string UpdateMyProfile(string SecurityTokenKey, string EmailAddress, string FirstName, string LastName, string StreetAddress, string City, string State, int Zipcode, string Cellphone)
        {
            string xmlstr = string.Empty;
            int ContactId;
            try
            {
                _Logger.Log("GetMyProfile: EmailAddress [" + EmailAddress + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " UpdateProfile "));

                DataSet ds = new DataSet();

                Contact Con = new Contact();
                Con.GetContactIdByEmail(EmailAddress, out ContactId);

                Con.Rowid = ContactId;
                Con.Loginid = ContactId;
                Con.Emailaddress = EmailAddress;
                Con.Firstname = FirstName;
                Con.Lastname = LastName;
                Con.Streetaddress = StreetAddress;
                Con.City = City;
                Con.State = State;
                Con.Zipcode = Zipcode;
                Con.Cellphone = Cellphone;
                Con.Isactive = true;
                Con.Createdby = "Admin";
                Con.Createddate = System.DateTime.Now.ToLocalTime();  // Need to retain old value.
                Con.Modifiedby = "Admin";
                Con.Modifieddate = System.DateTime.Now.ToLocalTime();
                Con.Type = "Web";
                Con.Update();

                return Convert.ToString(true);
            }
            catch (Exception ex)
            {
                _Logger.Log("GetMyProfile Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetMyProfile Output: [" + xmlstr + "]");
            }
        }

        public string ChangePassword(string SecurityTokenKey, string EmailAddress, string OldPassword, string NewPassword)
        {
            int Status = 0;
            try
            {
                _Logger.Log("ChangePassword: EmailAddress [" + EmailAddress + "]");
                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security key {0} Received for {1}", SecurityTokenKey, " ChangePassword "));

                AutoShop.DAL.Contact dbo = new AutoShop.DAL.Contact();
                dbo.ChangeB2CPassword(EmailAddress, EncryptDecryptMD5(OldPassword), EncryptDecryptMD5(NewPassword), out Status);
                return Convert.ToBoolean(Status) ? "Password updated Successfully" : "Cannot Update password";
            }
            catch (Exception ex)
            {
                _Logger.Log("ChangePassword Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("ChangePassword completed!!!");
            }
        }

        public string LoginAuth(string SecurityTokenKey, string EmailAddress, string Password)
        {
            bool Status;
            try
            {
                _Logger.Log("Login Authorization: EmailAddress [" + EmailAddress + "]");

                //if (!ValidateSecurityTokenKey(SecurityTokenKey))
                //    throw new Exception(String.Format("Cannot Establish Response Invalid Security key {0} Received for {1}", SecurityTokenKey, " Login Authorization "));

                AutoShop.DAL.Contact dbo = new AutoShop.DAL.Contact();
                Status = dbo.LoginAuth(EmailAddress, EncryptDecryptMD5(Password));
                return Status ? "Valid User" : "Unauthorized User";
            }
            catch (Exception ex)
            {
                _Logger.Log("Login Authorization Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("Login Authorization completed!!!");
            }
        }

        public string ForgotPassword(string SecurityTokenKey, string EmailAddress)
        {
            int Status = 0;
            try
            {
                _Logger.Log("ForgotPassword: EmailAddress [" + EmailAddress + "]");
                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security key {0} Received for {1}", SecurityTokenKey, " ForgotPassword "));

                string ResetToken = Guid.NewGuid().ToString();
                AutoShop.DAL.Contact dbo = new AutoShop.DAL.Contact();
                dbo.LoginUpdate(EmailAddress, ResetToken, out Status);

                return Convert.ToBoolean(Status) ? "Reset Link created with Reset Token" : "Cannot create RestLink with Reset Token";
            }
            catch (Exception ex)
            {
                _Logger.Log("ForgotPassword Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("ForgotPassword completed!!!");
            }
        }

        public string UpdatePassword(string SecurityTokenKey, string EmailAddress, string NewPassword, string ResetToken)
        {
            int Status = 0;
            try
            {
                _Logger.Log("UpdatePassword: EmailAddress [" + EmailAddress + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security key {0} Received for {1}", SecurityTokenKey, " UpdatePassword "));

                AutoShop.DAL.Contact dbo = new AutoShop.DAL.Contact();
                dbo.UpdatePassword(EmailAddress, ResetToken, EncryptDecryptMD5(NewPassword), out Status);
                return Convert.ToBoolean(Status) ? "New Password Updated" : "Cannot update new Password";
            }
            catch (Exception ex)
            {
                _Logger.Log("UpdatePassword Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("UpdatePassword completed!!!");
            }
        }


        public string GetB2BRegSearchList(string SecurityTokenKey, string BusinessName, int ZipCode)
        {
            string jsonstr = string.Empty;
            try
            {
                _Logger.Log("GetB2BRegSearchList: Business Name [" + BusinessName + "] ZipCode [" + ZipCode + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security key {0} Received for {1}", SecurityTokenKey, " GetB2BRegSearchList "));

                AutoShop.DAL.Business BS = new AutoShop.DAL.Business();
                System.Data.DataSet ds = BS.B2BRegSearchList(BusinessName, ZipCode);

                ds.DataSetName = "BusinessSearch";
                ds.Tables[0].TableName = "Business";

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("GetB2BRegSearchList Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetB2BRegSearchList completed!!!");
            }
        }

        public string GetSubscription(string SecurityTokenKey)
        {
            string jsonstr = string.Empty;
            try
            {
                _Logger.Log("GetSubscription...");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security key {0} Received for {1}", SecurityTokenKey, " GetSubscription "));

                AutoShop.DAL.Subscription BS = new AutoShop.DAL.Subscription();
                System.Data.DataSet ds = BS.Select_SubscriptionType();

                ds.DataSetName = "SubscriptionList";
                ds.Tables[0].TableName = "Subscription";

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("GetSubscription Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetSubscription completed!!!");
            }
        }

        public string GetMyFavoriteAutoShops(string SecurityTokenKey, string EmailAddress)
        {
            string jsonstr = string.Empty;
            try
            {
                _Logger.Log("GetMyFavoriteAutoShops: EmailAddress [" + EmailAddress + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " GetMyFavoriteAutoShops "));

                int ContactId;

                Contact Con = new Contact();
                Con.GetContactIdByEmail(EmailAddress, out ContactId);

                SearchBusinessCollection SBCol = new SearchBusinessCollection();
                SBCol = SearchBusiness.Get_FavoriteAutoshopList_By_ContactId(ContactId);

                DataTable dt = AutoShop.BusinessObjects.Common.Extenders.ToDataTable(SBCol);

                dt.TableName = "Business";
                DataSet ds = new DataSet("BusinessList");
                ds.Tables.Add(dt);

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("GetMyFavoriteAutoShops Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetMyFavoriteAutoShops Output: [" + jsonstr + "]");
            }
        }

        public string DeleteFavoriteAutoShop(string SecurityTokenKey, string EmailAddress, int BusId)
        {
            string xmlstr = string.Empty;
            try
            {
                _Logger.Log("DeleteFavoriteAutoShop: EmailAddress [" + EmailAddress + "] BusinessId [" + BusId + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " DeleteFavoriteAutoShop "));

                int ContactId;

                Contact Con = new Contact();
                Con.GetContactIdByEmail(EmailAddress, out ContactId);

                Business Bus = new Business();
                Bus.Delete_Favorite_Bussiness_By_ContactId(BusId, ContactId);

                return Convert.ToString(true);
            }
            catch (Exception ex)
            {
                _Logger.Log("DeleteFavoriteAutoShop Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("DeleteFavoriteAutoShop Output: [" + xmlstr + "]");
            }
        }

        public string CheckEmailExists(string SecurityTokenKey, string EmailAddress)
        {
            bool Status;
            string rtnValue = string.Empty;
            try
            {
                _Logger.Log("CheckEmailExists: EmailAddress [" + EmailAddress + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " CheckEmailExists "));

                AutoShop.DAL.Contact dbo = new AutoShop.DAL.Contact();
                Status = dbo.CheckEmailExists(EmailAddress);
                rtnValue = Convert.ToBoolean(Status) ? Convert.ToString(true) : Convert.ToString(false);
                return rtnValue;
            }
            catch (Exception ex)
            {
                _Logger.Log("CheckEmailExists Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("CheckEmailExists Output: [" + rtnValue + "]");
            }
        }

        public string GetAllStates(string SecurityTokenKey)
        {
            string jsonstr = string.Empty;
            try
            {
                _Logger.Log("GetAllStates...");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security key {0} Received for {1}", SecurityTokenKey, " GetAllStates "));

                AutoShop.DAL.Contact dbo = new AutoShop.DAL.Contact();
                System.Data.DataSet ds = dbo.Select_All_State();

                ds.DataSetName = "StateList";
                ds.Tables[0].TableName = "State";

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;

            }
            catch (Exception ex)
            {
                _Logger.Log("GetAllStates Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetAllStates completed!!!");
            }
        }

        
        public string GetZipCode(string SecurityTokenKey, int ZipCode)
        {
            string jsonstr = string.Empty;
            try
            {
                _Logger.Log("GetZipCode...[" + ZipCode.ToString() + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security key {0} Received for {1}", SecurityTokenKey, " GetZipCode "));

                AutoShop.DAL.Address dbo = new AutoShop.DAL.Address();
                System.Data.DataSet ds = dbo.AutoComplete_By_ZipCode(ZipCode);

                ds.DataSetName = "ZipCodes";
                ds.Tables[0].TableName = "ZipCode";

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("GetZipCode Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetZipCode completed!!!");
            }
        }

        public string GetAutoshopDetails(string SecurityTokenKey, int BusinessId, int ContactId)
        {
            string jsonstr = string.Empty;
            try
            {
                _Logger.Log("GetAutoshopDetails: BusinessID [" + BusinessId + "] ContactId [" + ContactId + "]");

                if (!ValidateSecurityTokenKey(SecurityTokenKey))
                    throw new Exception(String.Format("Cannot Establish Response Invalid Security  key {0} Received for {1}", SecurityTokenKey, " GetAutoshopDetails "));

                AutoShop.DAL.SearchBusiness dbo = new AutoShop.DAL.SearchBusiness();
                DataSet ds = new DataSet();

                ds = dbo.GetAutoshopDetails(BusinessId, ContactId);
                ds.DataSetName = "Business";
                ds.Tables[0].TableName = "BusinessDetail";
                ds.Tables[1].TableName = "BusinessHours";
                ds.Tables[2].TableName = "BusinessCoupon";
                ds.Tables[3].TableName = "BusinessImage";

                DataRelation relateHours = new DataRelation("Relatehr", ds.Tables["BusinessDetail"].Columns["BusinessId"], ds.Tables["BusinessHours"].Columns["BusinessId"]);
                DataRelation relateCoupon = new DataRelation("Relatecopon", ds.Tables["BusinessDetail"].Columns["BusinessId"], ds.Tables["BusinessCoupon"].Columns["BusinessId"]);
                DataRelation relateImage = new DataRelation("Relateimg", ds.Tables["BusinessDetail"].Columns["BusinessId"], ds.Tables["BusinessImage"].Columns["BusinessId"]);
                relateHours.Nested = true;
                relateCoupon.Nested = true;
                relateImage.Nested = true;

                ds.Relations.Add(relateHours);
                ds.Relations.Add(relateCoupon);
                ds.Relations.Add(relateImage);

                jsonstr = JsonSerilizerHelper.SerializeDataSetToJson(ds);
                return jsonstr;
            }
            catch (Exception ex)
            {
                _Logger.Log("GetBusinessListByUpdateSearch Error", ex);
                throw ex;
            }
            finally
            {
                _Logger.Log("GetBusinessListByUpdateSearch Output: [" + jsonstr + "]");
            }
        }

        public string EncryptDecryptMD5(string key)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(key, "MD5");
        }
    }
}