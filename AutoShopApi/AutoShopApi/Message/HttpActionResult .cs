﻿
using System.Web.Http;
using System.Web.Http.Results;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;

namespace AutoShopApi
{
    public class HttpActionResult : IHttpActionResult
    {
        ResponseMsg _msg;

        public HttpActionResult(ResponseMsg msg)
        {
            _msg = msg;
        }
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage()
            {
                Content = new ObjectContent<ResponseMsg>(_msg, new JsonMediaTypeFormatter()),               
            };
            return Task.FromResult(response);
        }
    }

    public class ResponseMsg
    {
        public HttpStatusCode Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }    
}